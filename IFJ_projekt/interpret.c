#include "interpret.h"
#include "instruction.h"
#include "ial.h"

int TESTFLAG = 0;
struct stackS *stackStrings = NULL;
struct stackD *stackDoubles = NULL;
struct stackI *stackInts = NULL;

SVariableRecord * GetVar(long long int a, SLocalBlockList * block, const char * ClassName, SMethodRecord *method, int Visible)
{
	SVariableRecord * s = (SVariableRecord *)a;
	SVariableRecord * hold = HashTable_LookForVariable(block, ClassName, method, Visible);
	if (hold == NULL)
		return s;
	else
		return hold;
}

double GetDouble(SVariableRecord * a)
{
	if (a->type == INT)
		return (double)(*(int*)(a->value));
	else
		return (*(double*)(a->value));
}

int GetInt(SVariableRecord * a)
{
	if (a->type == INT)
		return (*(int*)(a->value));
	else
		return (int)(*(double*)(a->value));
}

void SetInt(SVariableRecord * a, int b)
{
	
}

double ComputeD(SVariableRecord * a, SVariableRecord * b, OPCODE op)
{
	double va;
	double vb;

	if (a->type == INT)
		va = (double)(*((int*)(a->value)));
	else
		va = *(double*)(a->value);

	if (b->type == INT)
		vb = (double)(*((int*)(b->value)));
	else
		vb = *(double*)(b->value);

	double vysl;
	switch (op)
	{
		case opSUM:
			vysl = va + vb;
			break;
		case opSUB:
			vysl = va - vb;
			break;
		case opMUL:
			vysl = va * vb;
			break;
		case opDIV:
			vysl = va / vb;
			break;
	}
}

void AccIntOp(long long * acc, long long val, OPCODE op)
{
	switch (op)
	{
		case opSUM:
			*acc += val;
			break;
		case opSUB:
			*acc -= val;
		case opMUL:
			*acc *= val;
			break;
		case opDIV:
			*acc /= val;
			break;
	}
}
void AccDblOp(double * acc, double val, OPCODE op)
{
	switch (op)
	{
		case opSUM:
			*acc += val;
			break;
		case opSUB:
			*acc -= val;
		case opMUL:
			*acc *= val;
			break;
		case opDIV:
			*acc /= val;
			break;
	}
}

int ExecuteInstruction(struct Instruction * Instr, int Function, SLocalBlockList * block, const char * ClassName, SMethodRecord *method)
{
	static InstrJump * Jumps = NULL;
	//static long long accumulator = 0; // here ?
	// VYSLEDNY ACC[0]
	static double accumdouble[3] = { 0, 0, 0 };
	static int curracc = 0;
	// (2*3) + (2-1) / 5 + 7
	// 23*21-5/+7+
	static char * buffer = 0;
	SVariableRecord * p1, *p2, *p3;
	char idConvert[50];
	// (1*2) / (2+3)
	// 1*2
	// 2+3
	// 1+2*3
	//  /
	// 12*23+/
	// mul 1 2 +
	// sum 2 3 /
	int len = 0;

	int FncErr = 0;
	// PRINT
	int prntAllocated = 0;
	static char * prntBuffer = 0;
	int prntBufLen = 0;
	/*int * addr = (int*)value;
	*addr = xxx;*/

	switch (Instr->instr)
	{
		case SUMAAD:
			accumdouble[0] = accumdouble[0] + accumdouble[1];
			break;
		case SUBAAD:
			accumdouble[0] = accumdouble[0] - accumdouble[1];
			break;
		case MULAAD:
			accumdouble[0] = accumdouble[0] * accumdouble[1];
			break;
		case DIVAAD:
			accumdouble[0] = accumdouble[0] / accumdouble[1];
			break;
		case SWAPAAD:
			accumdouble[2] = 0;
			accumdouble[2] = accumdouble[1];
			accumdouble[1] = accumdouble[0];
			accumdouble[0] = accumdouble[2];
			accumdouble[2] = 0;
			break;
		case SETCURACC:
			curracc = Instr->param1;
			break;
		case CLRA: // Vy�i�t�n� akumul�toru, prvn� v�c p�ed po��t�n�m
			//accumulator = 0;
			accumdouble[2] = 0;
			accumdouble[1] = 0;
			accumdouble[0] = 0;
			break;
		case SETA: // Nastaven� akumul�toru na hodnotu v param1
			accumdouble[curracc] = Instr->param1;
			break;
		case SETAD:
			accumdouble[curracc] = *(double*)(Instr->param1);
			break;
		case ADD: // Se�t�n� dvou ��seln�ch hodnot a proveden� po�adovan� operace s akumul�torem
			AccDblOp(&(accumdouble[curracc]), Instr->param1 + Instr->param2, Instr->param3);
			//printf("\nInstruction %d (Add):\n\tparam1: %lld param2: %lld\n\tacc=%lld\n", Instr->instr, Instr->param1, Instr->param2, accumulator);
			break;
		case ADDXV:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), *(int*)(p1->value) + Instr->param2, Instr->param3);

			//printf("\nInstruction %d (Addx):\n\tparam1: %p param2: %lld\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), Instr->param2, accumulator);
			break;
		case ADDVX:
			p2 = GetVar(Instr->param2, block, ClassName, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), Instr->param1 + *(int*)(p2->value), Instr->param3);
			//printf("\nInstruction %d (Addx):\n\tparam1: %p param2: %lld\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), Instr->param2, accumulator);
			break;
		case ADDXX:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;

			p2 = GetVar(Instr->param2, block, ClassName, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), *(int*)(p1->value) + *(int*)(p2->value), Instr->param3);
			//printf("\nInstruction %d (Addxx):\n\tparam1: %p param2: %p\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), (void*)(Instr->param2), accumulator);
			break;
		case SUB: // Ode�ten� dvou ��seln�ch hodnot a proveden� po�adovan� operace s akumul�torem
			AccDblOp(&(accumdouble[curracc]), Instr->param1 - Instr->param2, Instr->param3);
			//printf("\nInstruction %d (Add):\n\tparam1: %lld param2: %lld\n\tacc=%lld\n", Instr->instr, Instr->param1, Instr->param2, accumulator);
			break;
		case SUBXV:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), *(int*)(p1->value) - Instr->param2, Instr->param3);
			//printf("\nInstruction %d (Addx):\n\tparam1: %p param2: %lld\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), Instr->param2, accumulator);
			break;
		case SUBVX:
			p2 = GetVar(Instr->param2, block, ClassName, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), Instr->param1 - *(int*)(p2->value), Instr->param3);
			//printf("\nInstruction %d (Addx):\n\tparam1: %p param2: %lld\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), Instr->param2, accumulator);
			break;
		case SUBXX:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;

			p2 = GetVar(Instr->param2, block, ClassName, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), *(int*)(p1->value) - *(int*)(p2->value), Instr->param3);
			//printf("\nInstruction %d (Addxx):\n\tparam1: %p param2: %p\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), (void*)(Instr->param2), accumulator);
			break;
		case MUL:
			AccDblOp(&(accumdouble[curracc]), Instr->param1 * Instr->param2, Instr->param3);
			//printf("\nInstruction %d (Add):\n\tparam1: %lld param2: %lld\n\tacc=%lld\n", Instr->instr, Instr->param1, Instr->param2, accumulator);
			break;
		case MULXV:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), *(int*)(p1->value) * Instr->param2, Instr->param3);
			//printf("\nInstruction %d (Addx):\n\tparam1: %p param2: %lld\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), Instr->param2, accumulator);
			break;
		case MULVX:
			p2 = GetVar(Instr->param2, block, ClassName, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), Instr->param1 * *(int*)(p2->value), Instr->param3);
			//printf("\nInstruction %d (Addx):\n\tparam1: %p param2: %lld\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), Instr->param2, accumulator);
			break;
		case MULXX:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;

			p2 = GetVar(Instr->param2, block, ClassName, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), *(int*)(p1->value) * *(int*)(p2->value), Instr->param3);
			//printf("\nInstruction %d (Addxx):\n\tparam1: %p param2: %p\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), (void*)(Instr->param2), accumulator);
			break;
		case DIV:
			if (Instr->param2 == 0)
			{
				//DIVIDED BY ZERO
				return ERR_DIVIDED_BY_ZERO;
			}
			AccDblOp(&(accumdouble[curracc]), Instr->param1 / Instr->param2, Instr->param3);
			//printf("\nInstruction %d (Add):\n\tparam1: %lld param2: %lld\n\tacc=%lld\n", Instr->instr, Instr->param1, Instr->param2, accumulator);
			break;
		case DIVXV:
			if (Instr->param2 == 0)
			{
				//DIVIDED BY ZERO
				return ERR_DIVIDED_BY_ZERO;
			}
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), *(int*)(p1) / Instr->param2, Instr->param3);
			//printf("\nInstruction %d (Addx):\n\tparam1: %p param2: %lld\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), Instr->param2, accumulator);
			break;
		case DIVVX:
			p2 = GetVar(Instr->param2, block, ClassName, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			if (*(int*)(p2->value) == 0)
			{
				//DIVIDED BY ZERO
				return ERR_DIVIDED_BY_ZERO;
			}
			AccDblOp(&(accumdouble[curracc]), Instr->param1 / *(int*)(p2->value), Instr->param3);
			//printf("\nInstruction %d (Addx):\n\tparam1: %p param2: %lld\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), Instr->param2, accumulator);
			break;
		case DIVXX:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;

			p2 = GetVar(Instr->param2, block, ClassName, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			if (*(int*)(p2->value) == 0)
			{
				//DIVIDED BY ZERO
				return ERR_DIVIDED_BY_ZERO;
			}
			AccDblOp(&(accumdouble[curracc]), *(int*)(p1->value) / *(int*)(p2->value), Instr->param3);
			//printf("\nInstruction %d (Addxx):\n\tparam1: %p param2: %p\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), (void*)(Instr->param2), accumulator);
			break;
// FLOAT START
		case ADDD: // Se�t�n� dvou ��seln�ch hodnot a proveden� po�adovan� operace s akumul�torem
			AccDblOp(&(accumdouble[curracc]), *(double*)(Instr->param1) + *(double*)(Instr->param2), Instr->param3);
			//printf("\nInstruction %d (Add):\n\tparam1: %lld param2: %lld\n\tacc=%lld\n", Instr->instr, Instr->param1, Instr->param2, accumulator);
			break;
		case ADDXVD:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;

			AccDblOp(&(accumdouble[curracc]), GetDouble(p1) + *(double*)(Instr->param2), Instr->param3);

			//printf("\nInstruction %d (Addx):\n\tparam1: %p param2: %lld\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), Instr->param2, accumulator);
			break;
		case ADDVXD:
			p2 = GetVar(Instr->param2, block, ClassName, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), *(double*)(Instr->param1) + GetDouble(p2), Instr->param3);
			//printf("\nInstruction %d (Addx):\n\tparam1: %p param2: %lld\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), Instr->param2, accumulator);
			break;
		case ADDXXD:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;

			p2 = GetVar(Instr->param2, block, ClassName, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), GetDouble(p1) + GetDouble(p2), Instr->param3);
			//printf("\nInstruction %d (Addxx):\n\tparam1: %p param2: %p\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), (void*)(Instr->param2), accumulator);
			break;
		case SUBD: // Ode�ten� dvou ��seln�ch hodnot a proveden� po�adovan� operace s akumul�torem
			AccDblOp(&(accumdouble[curracc]), *(double*)(Instr->param1) - *(double*)(Instr->param2), Instr->param3);
			//printf("\nInstruction %d (Add):\n\tparam1: %lld param2: %lld\n\tacc=%lld\n", Instr->instr, Instr->param1, Instr->param2, accumulator);
			break;
		case SUBXVD:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), GetDouble(p1) - *(double*)(Instr->param2), Instr->param3);
			//printf("\nInstruction %d (Addx):\n\tparam1: %p param2: %lld\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), Instr->param2, accumulator);
			break;
		case SUBVXD:
			p2 = GetVar(Instr->param2, block, ClassName, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), *(double*)(Instr->param1) - GetDouble(p2), Instr->param3);
			//printf("\nInstruction %d (Addx):\n\tparam1: %p param2: %lld\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), Instr->param2, accumulator);
			break;
		case SUBXXD:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;

			p2 = GetVar(Instr->param2, block, ClassName, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), GetDouble(p1) - GetDouble(p2), Instr->param3);
			//printf("\nInstruction %d (Addxx):\n\tparam1: %p param2: %p\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), (void*)(Instr->param2), accumulator);
			break;
		case MULD:
			AccDblOp(&(accumdouble[curracc]), *(double*)(Instr->param1) * *(double*)(Instr->param2), Instr->param3);
			//printf("\nInstruction %d (Add):\n\tparam1: %lld param2: %lld\n\tacc=%lld\n", Instr->instr, Instr->param1, Instr->param2, accumulator);
			break;
		case MULXVD:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), GetDouble(p1) * *(double*)(Instr->param2), Instr->param3);
			//printf("\nInstruction %d (Addx):\n\tparam1: %p param2: %lld\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), Instr->param2, accumulator);
			break;
		case MULVXD:
			p2 = GetVar(Instr->param2, block, ClassName, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), *(double*)(Instr->param1) * GetDouble(p2), Instr->param3);
			//printf("\nInstruction %d (Addx):\n\tparam1: %p param2: %lld\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), Instr->param2, accumulator);
			break;
		case MULXXD:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;

			p2 = GetVar(Instr->param2, block, ClassName, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), GetDouble(p1) * GetDouble(p2), Instr->param3);
			//printf("\nInstruction %d (Addxx):\n\tparam1: %p param2: %p\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), (void*)(Instr->param2), accumulator);
			break;
		case DIVD:
			if (*(double*)(Instr->param2) == 0)
			{
				//DIVIDED BY ZERO
				return ERR_DIVIDED_BY_ZERO;
			}
			AccDblOp(&(accumdouble[curracc]), *(double*)(Instr->param1) / *(double*)(Instr->param2), Instr->param3);
			//printf("\nInstruction %d (Add):\n\tparam1: %lld param2: %lld\n\tacc=%lld\n", Instr->instr, Instr->param1, Instr->param2, accumulator);
			break;
		case DIVXVD:
			if (Instr->param2 == 0)
			{
				//DIVIDED BY ZERO
				return ERR_DIVIDED_BY_ZERO;
			}
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;
			AccDblOp(&(accumdouble[curracc]), *(int*)(p1->value) / *(double*)(Instr->param2), Instr->param3);
			//printf("\nInstruction %d (Addx):\n\tparam1: %p param2: %lld\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), Instr->param2, accumulator);
			break;
		case DIVVXD:
			p2 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			if (*(int*)(p2->value) == 0)
			{
				//DIVIDED BY ZERO
				return ERR_DIVIDED_BY_ZERO;
			}
			AccDblOp(&(accumdouble[curracc]), *(double*)(Instr->param1) / *(int*)(p2->value), Instr->param3);
			//printf("\nInstruction %d (Addx):\n\tparam1: %p param2: %lld\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), Instr->param2, accumulator);
			break;
		case DIVXXD:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;

			p2 = GetVar(Instr->param2, block, ClassName, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			if (*(int*)(p2->value) == 0)
			{
				//DIVIDED BY ZERO
				return ERR_DIVIDED_BY_ZERO;
			}
			AccDblOp(&(accumdouble[curracc]), *(int*)(p1->value) / *(int*)(p2->value), Instr->param3);
			//printf("\nInstruction %d (Addxx):\n\tparam1: %p param2: %p\n\tacc=%lld\n", Instr->instr, (void*)(Instr->param1), (void*)(Instr->param2), accumulator);
			break;

// FLOAT END
		case LOADA: // Na�ten� hodnoty z prom�nn� do akumul�toru
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;
			accumdouble[curracc] = *(int*)(p1->value);
			break;
		case LOADD:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;
			if (p1->type == INT)
				accumdouble[curracc] = *(int*)(p1->value);
			else
				accumdouble[curracc] = *(double*)(p1->value);
			break;
		case STRLOADV: // FREE param1 IN InstructionFreeQueue !!
			if (buffer != NULL)
				free(buffer);
			buffer = NULL;

			len = strlen((char*)Instr->param1);
			buffer = (char *)malloc(len + 1);
			buffer[len] = 0;
			strcpy(buffer, (char*)Instr->param1);			
			break;
		case STRLOADX:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;

			if (buffer != NULL)
				free(buffer);
			buffer = NULL;

			len = strlen((char*)p1->value);
			buffer = (char *)malloc(len + 1);
			buffer[len] = 0;
			strcpy(buffer, (char*)p1->value);
			break;
		case STRALC: // TODO: UPRAVA PRO FINDLOCALVARIABLE Alokace prostoru pro buffer
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			p1->value = (char*)malloc(Instr->param2);
			if ((p1->value) == NULL)
			return ERR_INTER;
			break;
		case STRBUFCLR: // Vy�i�t�n� a uvoln�n� bufferu
			if (buffer != NULL)
				free(buffer);
			buffer = NULL;

			break;
		case STRAPPX: // P�id�n� prom�nn� do bufferu
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;
			len = strlen((char*)(p1->value));
			if (buffer == NULL)
			{
				buffer = (char*)malloc(len + 1);
				strcpy(buffer, (char*)(p1->value));
				buffer[len] = 0;
			}
			else
			{
				len += strlen(buffer);
				buffer = (char*)realloc(buffer, len + 1);
				strcat(buffer, (char*)(p1->value));
			}
			break;
		case STRAPPV:
			len = strlen((char*)(Instr->param1));
			if (buffer == NULL)
			{
				buffer = (char*)malloc(len + 1);
				strcpy(buffer, (char*)(Instr->param1));
				buffer[len] = 0;
			}
			else
			{
				len += strlen(buffer);
				buffer = (char*)realloc(buffer, len + 1);
				strcat(buffer, (char*)(Instr->param1));
			}
			break;
		case STRAPPI:
			memset(idConvert, 0, 50);
			sprintf(idConvert, "%d", (Instr->param1));
			len = strlen(idConvert);
			if (buffer == NULL)
			{
				buffer = (char*)malloc(len + 1);
				strcpy(buffer, idConvert);
				buffer[len] = 0;
			}
			else
			{
				len += strlen(buffer);
				buffer = (char*)realloc(buffer, len + 1);
				strcat(buffer, idConvert);
			}
			break;
		case STRAPPD:
			memset(idConvert, 0, 50);
			sprintf(idConvert, "%g", *((double*)(Instr->param1)));
			len = strlen(idConvert);
			if (buffer == NULL)
			{
				buffer = (char*)malloc(len + 1);
				strcpy(buffer, idConvert);
				buffer[len] = 0;
			}
			else
			{
				len += strlen(buffer);
				buffer = (char*)realloc(buffer, len + 1);
				strcat(buffer, idConvert);
			}
			break;
		case STRAPPXI:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;
			memset(idConvert, 0, 50);
			sprintf(idConvert, "%d", *(int*)(p1->value));
			len = strlen(idConvert);
			if (buffer == NULL)
			{
				buffer = (char*)malloc(len + 1);
				strcpy(buffer, idConvert);
				buffer[len] = 0;
			}
			else
			{
				len += strlen(buffer);
				buffer = (char*)realloc(buffer, len + 1);
				strcat(buffer, idConvert);
			}
			break;
		case STRAPPXD:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;
			memset(idConvert, 0, 50);
			sprintf(idConvert, "%g", *((double*)(p1->value)));
			len = strlen(idConvert);
			if (buffer == NULL)
			{
				buffer = (char*)malloc(len + 1);
				strcpy(buffer, idConvert);
				buffer[len] = 0;
			}
			else
			{
				len += strlen(buffer);
				buffer = (char*)realloc(buffer, len + 1);
				strcat(buffer, idConvert);
			}
			break;
		case STRCAT: // TODO: UPRAVIT PROM�NN� PRO FINDLOCALVARIABLE
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;
			p2 = GetVar(Instr->param2, block, ClassName, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			if (buffer == NULL)
			{
				len = strlen((char*)p1->value) + strlen((char*)p2->value) + 1;
				buffer = (char*)malloc(len);
				if (buffer == NULL)
					return ERR_INTER;
				strcpy(buffer, (char*)p1->value);
				strcat(buffer, (char*)p2->value);
				buffer[len - 1] = 0;
			}
			else
			{
				len = strlen(buffer) + strlen((char*)p1->value) + strlen((char*)p2->value) + 1;
				buffer = (char*)realloc(buffer, len);
				if (buffer == NULL)
					return ERR_INTER;

				strcat(buffer, (char*)p1->value);
				strcat(buffer, (char*)p2->value);
			}
			break;
		case STRSTORE: // TODO:: UPRAVIT PROM�NN� PRO FINDLOCALVARIABLE, Ulo�en� bufferu do prom�nn�
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if ((char*)p1->value != NULL)
				free(p1->value);
			
			p1->value = (char*)malloc(strlen(buffer) + 1);
			
			p1->defined = true;
			strcpy((char*)p1->value, buffer);
			break;
		case PRINTINIT: // Inicializace psac�ho bufferu
			if (prntBuffer != NULL)
				free(prntBuffer);
			prntBuffer = NULL;
			break;
		case PRINTADDX:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;

			switch (p1->type)
			{
				case INT:
					memset(idConvert, 0, 50);
					sprintf(idConvert, "%d", *(int*)(p1->value));
					len = strlen(idConvert);
					if (prntBuffer == NULL)
					{
						prntBuffer = (char*)malloc(len + 1);
						strcpy(prntBuffer, idConvert);
						prntBuffer[len] = 0;
					}
					else
					{
						len += strlen(prntBuffer);
						prntBuffer = (char*)realloc(prntBuffer, len + 1);
						strcat(prntBuffer, idConvert);
					}
					break;
				case DOUBLE:
					memset(idConvert, 0, 50);
					sprintf(idConvert, "%g", *(double*)(p1->value));
					len = strlen(idConvert);
					if (prntBuffer == NULL)
					{
						prntBuffer = (char*)malloc(len + 1);
						strcpy(prntBuffer, idConvert);
						prntBuffer[len] = 0;
					}
					else
					{
						len += strlen(prntBuffer);
						prntBuffer = (char*)realloc(prntBuffer, len + 1);
						strcat(prntBuffer, idConvert);
					}
					break;
				case STRING:
					len = strlen((char*)p1->value) + 1;
					if (prntBuffer == NULL)
					{
						prntBuffer = (char*)malloc(len + 1);
						strcpy(prntBuffer, p1->value);
						prntBuffer[len] = 0;
					}
					else
					{
						len += strlen(prntBuffer);
						prntBuffer = (char*)realloc(prntBuffer, len + 1);
						strcat(prntBuffer, p1->value);
					}
					break;
			}
			break;
		case PRINTADD: // P�id�n� hodnot z textov�ch prom�nn�ch do bufferu pro psan�
			if (prntBuffer == NULL)
				len = 0;
			else
			{
				prntBufLen = len = strlen(prntBuffer);
				prntAllocated = 1;
			}

			if (Instr->param1 != NULL)
			{
				p1 = GetVar(Instr->param1, block, ClassName, method, 0);
				len += (strlen((char*)p1->value));
			}

			if (Instr->param2 != NULL)
			{
				p2 = GetVar(Instr->param2, block, ClassName, method, 0);
				len += (strlen((char*)p2->value));
			}

			if (Instr->param3 != NULL)
			{
				p3 = GetVar(Instr->param3, block, ClassName, method, 0);
				len += (strlen((char*)p3->value));
			}

			if (prntAllocated == 1)
			{
				prntBuffer = (char*)realloc(prntBuffer, len + 1);
				if (prntBuffer == NULL)
					return ERR_INTER;
				prntBuffer[prntBufLen] = 0;
			}
			else
			{
				prntBuffer = (char*)malloc(len + 1);
				if (prntBuffer == NULL)
					return ERR_INTER;
				prntBuffer[0] = 0;
			}

			if (Instr->param1 != NULL)
				strcat(prntBuffer, (char*)p1->value);

			if (Instr->param2 != NULL)
				strcat(prntBuffer, (char*)p2->value);

			if (Instr->param3 != NULL)
				strcat(prntBuffer, (char*)p3->value);
			break;
		case PRINTADDSTR: // P�id�n� �ist�ho textu do psac� prom�nn�
			if (prntBuffer == NULL)
				len = 0;
			else
			{
				prntBufLen = len = strlen(prntBuffer);
				prntAllocated = 1;
			}

			if (Instr->param1 != NULL)
				len += (strlen((char*)Instr->param1));

			if (prntAllocated == 1)
			{
				prntBuffer = (char*)realloc(prntBuffer, len + 1);
				if (prntBuffer == NULL)
					return ERR_INTER;
				prntBuffer[prntBufLen] = 0;
			}
			else
			{
				prntBuffer = (char*)malloc(len + 1);
				if (prntBuffer == NULL)
					return ERR_INTER;
				prntBuffer[0] = 0;
			}

			if (Instr->param1 != NULL)
				strcat(prntBuffer, (char*)Instr->param1);

			if (Instr->param2 != NULL)
				strcat(prntBuffer, (char*)Instr->param2);

			if (Instr->param3 != NULL)
				strcat(prntBuffer, (char*)Instr->param3);
			break;
		case PRINT: // Vyti�t�n� psac�ho bufferu
			printf(prntBuffer);
			break;
		case STOREA: // Na�ten� akumul�toru do prom�nn�
			p1 = GetVar(Instr->param1, block, ClassName, method, 1);
			*(int*)(p1->value) = (int)accumdouble[curracc];
			p1->defined = true;
			break;
		case STORED:
			p1 = GetVar(Instr->param1, block, ClassName, method, 1);
			if (Function == 0)
			{
				if (p1->type == DOUBLE)
					*(double*)(p1->value) = accumdouble[curracc];
				else
					*(int*)(p1->value) = accumdouble[curracc];
			}
			else
			{
				if (p1->type == DOUBLE)
					stackDoubles = stackPopD(stackDoubles, (double*)(p1->value));
				else
					stackInts = stackPopD(stackInts, (int*)(p1->value));
			}

			p1->defined = true;
			break;
		case STORESTATICD:
			p1 = GetVar(Instr->param1, block, ClassName, method, 1);
			if (p1->type == DOUBLE)
				*(double*)(p1->value) = accumdouble[curracc];
			else
				*(int*)(p1->value) = accumdouble[curracc];

			int saved = *(int*)(p1->value);
			p1->defined = true;
			break;
		case JUMP:

			break;
		case FNCALL: // P��chod do funkce Mus� se odeslat SMethodRecord v param1 a CLASSNAME v param2
		{
			SMethodRecord *curr = (SMethodRecord*)(Instr->param1);
			FncErr = StartExecutingMethod(curr->Function, curr);
			if (FncErr != 0)
				return FncErr;
		}
			break;
		case FNLDPM:

			// TODO: make stack, load from stack
			p1 = GetVar(Instr->param1, block, ClassName, method, 1);

			switch (p1->type)
			{
				case INT:
					stackInts = stackPopI(stackInts, (int*)(p1->value));
					int helloworld = *(int*)(p1->value);
					break;
				case STRING:
					stackStrings = stackPopS(stackStrings, p1->value);
					break;
				case DOUBLE:
					stackDoubles = stackPopD(stackDoubles, (double*)(p1->value));
					break;
			}

			// TODO: LOAD VARIABLE

			break;
		case FNSTD:
			stackDoubles = stackPushD(stackDoubles, *(double*)Instr->param1);
			break;
		case FNSTI:
			stackInts = stackPushI(stackInts, Instr->param1);
			break;
		case FNSTS:
			stackStrings = stackPushS(stackStrings, (char*)Instr->param1);
			break;
		case FNSTX:
			p1 = GetVar(Instr->param1, block, ClassName, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;

			switch (p1->type)
			{
				case INT:
					stackInts = stackPushI(stackInts, *(int*)p1->value);
					break;
				case DOUBLE:
					stackDoubles = stackPushD(stackDoubles, *(double*)p1->value);
					break;
				case STRING:
					stackStrings = stackPushS(stackStrings, (char*)p1->value);
					break;
			}
			break;
		case FNCEND:
			switch (method->retType)
			{
				case 'i': // integer
					stackInts = stackPushI(stackInts, accumdouble[curracc]);
					break;
				case 'v': // void
					break;
				case 's': // string
					stackStrings = stackPushS(stackStrings, buffer);
					break;
				case 'd': // double
					stackDoubles = stackPushD(stackDoubles, accumdouble[0]);
					break;
			}
			break;
		case DECLRVAR:
			p1 = GetVar(Instr->param1, block, ClassName, method, 1);
			break;
		default:
			printf("Unknown instruction %d\n", Instr->instr);
			break;
	}
	return 0;
}

int ExecuteCode(struct InsList **Instrs) // must be like this for release when error occures
{
	int RetVal = 0;
	
	// FIRST LOAD
	struct InsList * pointer = (*Instrs);
	SMethodRecord *curr;
	if (pointer->Instr->instr == FNCALL)
	{
		curr = (SMethodRecord*)(pointer->Instr->param1);
	}
	else
	{
		// TODO: NOT RIGHT FIRST INSTRUCTION
		return -1;
	}
	RetVal = StartExecutingMethod(*Instrs, curr);
	if (RetVal == 0)
		InstructionFreeQueue(Instrs);
	return RetVal;
}

int SolveCmpCode(int a, int b, CMPCODE c)
{
	switch (c)
	{
		case EQ:
			return (a == b);
			break;
		case LT:
			return (a < b);
			break;
		case GT:
			return (a > b);
			break;
		case LEQ:
			return (a <= b);
			break;
		case GEQ:
			return (a >= b);
			break;
		case NEQ:
			return (a != b);
			break;
	}
}

int SolveCmpCodeD(double a, double b, CMPCODE c)
{
	switch (c)
	{
		case EQ:
			return (a == b);
			break;
		case LT:
			return (a < b);
			break;
		case GT:
			return (a > b);
			break;
		case LEQ:
			return (a <= b);
			break;
		case GEQ:
			return (a >= b);
			break;
		case NEQ:
			return (a != b);
			break;
	}
}

int SolveComparsion(struct Instruction * instr, SLocalBlockList *m, const char * CLASSNAME, SMethodRecord *method)
{
	SVariableRecord *p1, *p2;

	switch (instr->instr)
	{
		case TESTII:
			return SolveCmpCode((instr->param1), (instr->param2), instr->param3);
			break;
		case TESTVI:
			p1 = GetVar(instr->param1, m, CLASSNAME, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;
			if (p1->type == INT)
				return SolveCmpCode(*(int*)(p1->value), instr->param2, instr->param3);
			else
				return SolveCmpCodeD(*(double*)(p1->value), instr->param2, instr->param3);
			break;
		case TESTIV:
			p2 = GetVar(instr->param2, m, CLASSNAME, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			if (p2->type == INT)
				return SolveCmpCode(instr->param1, *(int*)(p2->value), instr->param3);
			else
				return SolveCmpCodeD(instr->param1, *(double*)(p2->value), instr->param3);
			break;
		case TESTID:
			return SolveCmpCodeD(*(int*)(instr->param1), *(double*)(instr->param2), instr->param3);
			break;
		case TESTDI:
			return SolveCmpCodeD(*(double*)(instr->param1), *(int*)(instr->param2), instr->param3);
			break;
		case TESTDD:
			return SolveCmpCodeD(*(double*)(instr->param1), *(double*)(instr->param2), instr->param3);
			break;
		case TESTVD:
			p1 = GetVar(instr->param1, m, CLASSNAME, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;
			if (p1->type == INT)
				return SolveCmpCodeD((double)(*(int*)(p1->value)), *(double*)(instr->param2), instr->param3);
			else
				return SolveCmpCodeD(*(double*)(p1->value), *(double*)(instr->param2), instr->param3);
			break;
		case TESTDV:
			p2 = GetVar(instr->param2, m, CLASSNAME, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;
			if (p2->type == INT)
				return SolveCmpCodeD(*(double*)(instr->param1), (double)(*(int*)(p2->value)), instr->param3);
			else
				return SolveCmpCodeD(*(double*)(instr->param1), *(double*)(p2->value), instr->param3);
			break;
		case TESTVV:
			p1 = GetVar(instr->param1, m, CLASSNAME, method, 0);
			if (!p1->defined)
				return ERR_UNINITIALIZED_VAR;

			p2 = GetVar(instr->param2, m, CLASSNAME, method, 0);
			if (!p2->defined)
				return ERR_UNINITIALIZED_VAR;

			if (p1->type == p2->type && p1->type == INT)
				return SolveCmpCode(*(int*)(instr->param1), *(int*)(instr->param2), instr->param3);

			if (p1->type == INT)
				return SolveCmpCodeD((double)(*(int*)(instr->param1)), *(double*)(instr->param2), instr->param3);
			else
				return SolveCmpCodeD(*(double*)(instr->param1), (double)(*(int*)(instr->param2)), instr->param3);
			break;
		default:
			break;
	}
}

int StartExecutingMethod(struct InsList * Instrs, SMethodRecord * s)
{
	int RetVal = 0;

	int skipTests = 0;
	int ExecutingIf = 0;
	SLocalBlockList * m = HashTable_CopyMethodBlockList(s);
	if (m == NULL)
	{
		return 99;
	}
	struct InsList * pointer = Instrs;
	char * ClassName = (char*)(pointer->Instr->param2);

	pointer = pointer->Next;
	if (pointer == NULL)
	{
		return 0;
	}

	while (pointer != NULL)
	{
		if (pointer->Instr->instr == WHILESTART)
		{
			pointer = pointer->Next;
			RetVal = SolveComparsion(pointer->Instr, m, ClassName, s);
			if (RetVal > 1)
				return RetVal;

			if (RetVal == 0)
			{
				while (pointer->Instr->instr != WHILEEND)
				{
					pointer = pointer->Next;
					if (pointer->Instr->instr == WHILESTART)
					{
						skipTests++;
					}else if (pointer->Instr->instr == WHILEEND && skipTests != 0)
					{
						pointer = pointer->Next;
						skipTests--;
					}
				}
			}
		}
		else if (pointer->Instr->instr == WHILEEND)
		{
			while (pointer->Instr->instr != WHILESTART)
			{
				pointer = pointer->Prev;
				if (pointer->Instr->instr == WHILEEND)
				{
					skipTests++;
				}
				else if (pointer->Instr->instr == WHILESTART && skipTests != 0)
				{
					pointer = pointer->Prev;
					skipTests--;
				}
			}
			pointer = pointer->Prev;
		}
		else if (pointer->Instr->instr == IFSTART)
		{
			pointer = pointer->Next;
			RetVal = SolveComparsion(pointer->Instr, m, ClassName, s);
			if (RetVal > 1)
				return RetVal;

			if (RetVal == 0)
			{
				while (pointer->Instr->instr != IFELSE)
				{
					pointer = pointer->Next;
					if (pointer->Instr->instr == IFSTART)
					{
						skipTests++;
					}
					else if (pointer->Instr->instr == IFELSE && skipTests != 0)
					{
						pointer = pointer->Next;
						skipTests--;
					}
				}
			}
		}
		else if (pointer->Instr->instr == IFELSE)
		{
			while (pointer->Instr->instr != IFEND)
			{
				pointer = pointer->Next;
				if (pointer->Instr->instr == IFSTART)
				{
					skipTests++;
				}
				else if (pointer->Instr->instr == IFEND && skipTests != 0)
				{
					pointer = pointer->Next;
					skipTests--;
				}
			}
		}
		else if (pointer->Instr->instr == IFEND)
		{
			// Do nothing
		}
		else
		{
			if (pointer->Prev != NULL && pointer->Prev->Instr->instr == FNCALL)
			{
				RetVal = ExecuteInstruction(pointer->Instr, 1, m, ClassName, s);
			}
			else
			{
				RetVal = ExecuteInstruction(pointer->Instr, 0, m, ClassName, s);
			}
			if (RetVal != 0)
			{
				InstructionFreeQueue(&Instrs);
				return RetVal;
			}
		}
		if (pointer->Next == NULL)
		{
			if (s->retType != 'v')
				if (stackInts == NULL && stackDoubles == NULL && stackStrings == NULL)
					return ERR_UNINITIALIZED_VAR;
			pointer = NULL;
		}
		else
			pointer = pointer->Next;
	}

	return RetVal;
}

struct InstrJump *pushJump(struct InstrJump * jmpStack, int returnpos)
{
	struct InstrJump *oldSt = jmpStack;
	struct InstrJump *newSt = malloc(sizeof(struct InstrJump));
	if (newSt == NULL) return NULL;

	newSt->next = oldSt;
	newSt->retpos = returnpos;

	return newSt;
}

struct InstrJump *popJump(struct InstrJump * jmpStack, int * returnpos)
{
	*returnpos = jmpStack->retpos;
	struct InstrJump *oldSt = jmpStack;
	struct InstrJump *newSt = jmpStack->next;
	free(oldSt);
	return newSt;
}