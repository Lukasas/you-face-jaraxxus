﻿#include "lexical_analyzator.h"

/*
struct tToken *token_init () {
struct tToken *token;
token = malloc (sizeof(struct tToken));
if (token != NULL) {
token->token_id = 0;
token->token_value = malloc (LENGTH_VALUE * sizeof(char));
if (token->token_value == NULL)
return NULL;
strcpy (token->token_value, "\0");
}
return token;
}
*/
void free_token(struct tToken *token)
{
	free(token->token_value);
	free(token);
}

/* you shall definitely not pass token which was read but didn't belong to token */
void you_shall_not_pass(FILE *source_file)
{
	fseek(source_file, -1, SEEK_CUR);
}

/* check if token value size is enough wide
* issues: realloc size...check
*/
// Lukas: Tato funkce je správnì jen trochu zbyteèná
int check_size(char *string)
{
	size_t size = strlen(string);
	if (size >= LENGTH_VALUE)
	{
		string = realloc(string, (size + 2) * sizeof(char));
		if (string == NULL)
			return E_INTERN;
	}
	return E_OK;
}

// Funkce vytvoøena pro opravu špatného skládání øetìzcù
int char_append(char ** dest, const char c)
{
	size_t len = strlen(*dest);
	*dest = (char*)realloc(*dest, sizeof(char) * (len + 2));
	if (*dest == NULL)
		return E_INTERN;
	(*dest)[len] = c;
	(*dest)[len + 1] = 0;
	return E_OK;
}

int string_append(char ** dest, const char str)
{
	return -1;
}

/* issues: alpha in num...check
*		   free before return token->..check
*/
int lexical_analyzator(FILE *source_file, struct tToken *token)
{
	int state = S_BEGIN;

	char c;
	//token = token_init ();

	if (token == NULL)
		return E_INTERN;

	while (1)
	{

		c = getc(source_file);

		switch (state)
		{
			case S_BEGIN:
				if (isspace(c)) break;
				if (isalpha(c) || (c == '_') || (c == '$'))
				{
					CUSTOM_APPEND(token->token_value, c);
					//strcat (token->token_value, c);

					state = S_IDENTIFICATOR;
					break;
				}
				if (isdigit(c))
				{
					CUSTOM_APPEND(token->token_value, c);
					//strcat (token->token_value, c);
					state = S_NUMBER;
					break;
				}
				if (c == '+')
				{
					state = S_OK;
					token->token_id = T_ADD;
					break;
				}
				if (c == '-')
				{
					state = S_OK;
					token->token_id = T_SUB;
					break;
				}
				if (c == '*')
				{
					state = S_OK;
					token->token_id = T_MUL;
					break;
				}
				if (c == '/')
				{
					state = S_DIV;
					break;
				}
				if (c == '<')
				{
					state = S_SMALLER;
					break;
				}
				if (c == '>')
				{
					state = S_GREATER;
					break;
				}
				if (c == '=')
				{
					state = S_EQUAL;
					break;
				}
				if (c == '!')
				{
					state = S_EXC;
					break;
				}
				if (c == '(')
				{
					state = S_OK;
					token->token_id = T_L_BRACKET;
					break;
				}
				if (c == ')')
				{
					state = S_OK;
					token->token_id = T_R_BRACKET;
					break;
				}
				if (c == '{')
				{
					state = S_OK;
					token->token_id = T_L_BRACE;
					break;
				}
				if (c == '}')
				{
					state = S_OK;
					token->token_id = T_R_BRACE;
					break;
				}
				if (c == ',')
				{
					state = S_OK;
					token->token_id = T_COMMA;
					break;
				}
				if (c == ';')
				{
					state = S_OK;
					token->token_id = T_SEMICOLON;
					break;
				}
				if (c == '"')
				{
					state = S_QUOTES;
					break;
				}
				if (c == EOF)
				{
					state = S_OK;
					token->token_id = T_EOF;
					break;
				}
				if (!isspace(c))
					you_shall_not_pass(source_file);
				state = S_ERR;
				break;

			case S_IDENTIFICATOR:
				if (isalnum(c) || (c == '$') || (c == '_'))
				{
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						//free_token (token);
						return E_INTERN;
					}
					//strcat (token->token_value, c);
					break;
				}

				if (c == '.')
				{
					state = S_IDENTIFICATOR_POINT;
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						//free_token (token);
						return E_INTERN;
					}
					//strcat (token->token_value, c);
					break;
				}

				if (!isspace(c))
					you_shall_not_pass(source_file);

				for (int i = 0; i < KEY_COUNT; i++)
				{
					if (strcmp(token->token_value, keywords[i]) == 0)
					{
						token->token_id = T_KEYWORD;
						state = S_OK;
						break;
					}
				}

				for (int i = 0; i < RESERVED_COUNT; i++)
				{
					if (strcmp(token->token_value, reserved[i]) == 0)
					{
						token->token_id = T_RESERVED;
						state = S_OK;
						break;
					}
				}
				if (state == S_OK) break;
				token->token_id = T_IDENTIFICATOR;
				state = S_OK;
				break;

			case S_IDENTIFICATOR_POINT:
				if (isalpha(c) || (c == '_') || (c == '$'))
				{
					state = S_IDENTIFICATOR_POINT_IDENTIFICATOR;
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						//free_token (token);
						return E_INTERN;
					}
					//strcat (token->token_value, c);
					break;
				}

				state = S_ERR;
				break;

			case S_IDENTIFICATOR_POINT_IDENTIFICATOR:
				if (isalnum(c) || (c == '$') || (c == '_'))
				{
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						//free_token (token);
						return E_INTERN;
					}
					//strcat (token->token_value, c);
					break;
				}

				if (!isspace(c))
					you_shall_not_pass(source_file);

				token->token_id = T_IDENTIFICATOR_POINT_IDENTIFICATOR;
				state = S_OK;
				break;

			case S_NUMBER:
				if (isdigit(c))
				{
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						//free_token (token);
						return E_INTERN;
					}
					//strcat (token->token_value, c);
					break;
				}

				if (c == '.')
				{
					state = S_NUMBER_POINT;
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						//free_token (token);
						return E_INTERN;
					}
					//strcat (token->token_value, c);
					break;
				}

				if ((c == 'e') || (c == 'E'))
				{
					state = S_NUMBER_EXP;
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						//free_token (token);
						return E_INTERN;
					}
					//strcat (token->token_value, c);
					break;
				}

				if (isalpha(c))
				{
					state = S_ERR;
					break;
				}

				if (!isspace(c))
					you_shall_not_pass(source_file);

				token->token_id = T_INT;
				state = S_OK;
				break;

			case S_NUMBER_POINT:
				if (isdigit(c))
				{
					state = S_NUMBER_POINT_NUMBER;
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						//free_token (token);
						return E_INTERN;
					}
					//strcat (token->token_value, c);
					break;
				}

				state = S_ERR;
				break;

			case S_NUMBER_POINT_NUMBER:
				if (isdigit(c))
				{
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						//free_token (token);
						return E_INTERN;
					}
					//strcat (token->token_value, c);
					break;
				}

				if ((c == 'e') || (c == 'E'))
				{
					state = S_NUMBER_EXP;
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						free_token(token);
						return E_INTERN;
					}
					//strcat (token->token_value, c);
					break;
				}

				if (isalpha(c))
				{
					state = S_ERR;
					break;
				}

				if (!isspace(c))
					you_shall_not_pass(source_file);
				token->token_id = T_DOUBLE;
				state = S_OK;
				break;

			case S_NUMBER_EXP:
				if (isdigit(c))
				{
					state = S_NUMBER_EXP_NUMBER;
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						free_token(token);
						return E_INTERN;
					}
					//strcat (token->token_value, c);
					break;
				}

				if ((c == '+') || (c == '-'))
				{
					state = S_NUMBER_EXP_OP;
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						free_token(token);
						return E_INTERN;
					}
					//strcat (token->token_value, c);
					break;
				}

				state = S_ERR;
				break;

			case S_NUMBER_EXP_OP:
				if (isdigit(c))
				{
					state = S_NUMBER_EXP_NUMBER;
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						free_token(token);
						return E_INTERN;
					}
					//strcat (token->token_value, c);
					break;
				}

				state = S_ERR;
				break;

			case S_NUMBER_EXP_NUMBER:
				if (isdigit(c))
				{
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						free_token(token);
						return E_INTERN;
					}
					//strcat (token->token_value, c);
					break;
				}

				if (isalpha(c))
				{
					state = S_ERR;
					break;
				}

				if (!isspace(c))
					you_shall_not_pass(source_file);

				token->token_id = T_DOUBLE;
				state = S_OK;
				break;

			case S_DIV:
				if (c == '/')
				{
					state = S_COMMENT_LINE;
					break;
				}

				if (c == '*')
				{
					state = S_COMMENT_BLOCK;
					break;
				}

				if (!isspace(c))
					you_shall_not_pass(source_file);

				token->token_id = T_DIV;
				state = S_OK;
				break;
			case S_COMMENT_LINE:
				if (c == '\n' || c == EOF)
					state = S_BEGIN;
				break;

			case S_COMMENT_BLOCK:
				if (c == '*')
				{
					state = S_COMMENT_BLOCK_END;
				}
				else if (c == EOF) state = S_ERR;
				break;

			case S_COMMENT_BLOCK_END:
				if (c == '/')
					state = S_BEGIN;
				else
					state = S_COMMENT_BLOCK;
				break;

			case S_SMALLER:
				if (c == '=')
				{
					token->token_id = T_SMALLER_EQ;
					state = S_OK;
					break;
				}

				if (!isspace(c))
					you_shall_not_pass(source_file);

				token->token_id = T_SMALLER;
				state = S_OK;
				break;

			case S_GREATER:
				if (c == '=')
				{
					token->token_id = T_GREATER_EQ;
					state = S_OK;
					break;
				}

				if (!isspace(c))
					you_shall_not_pass(source_file);

				token->token_id = T_GREATER;
				state = S_OK;
				break;

			case S_EQUAL:
				if (c == '=')
				{
					token->token_id = T_EQ;
					state = S_OK;
					break;
				}

				if (!isspace(c))
					you_shall_not_pass(source_file);

				token->token_id = T_ASSIGN;
				state = S_OK;
				break;

			case S_EXC:
				if (c == '=')
				{
					token->token_id = T_NOT_EQ;
					state = S_OK;
					break;
				}

				state = S_ERR;
				break;

			case S_QUOTES:
				if (c == '"')
				{
					token->token_id = T_STRING;
					state = S_OK;
					break;
				}
				else if (c == '\\')
				{
					state = S_BSLASH;
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						//free_token(token);
						return E_INTERN;
					}
					break;
				}
				else if (c == EOF || c == '\n')
				{
					state = S_ERR;
					break;
				}

				if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
				{
					//free_token(token);
					return E_INTERN;
				}
				//strcat (token->token_value, c);
				break;

			case S_BSLASH:
				if (c == '"' || c == 'n' || c == 't' || c == '\\')
				{
					state = S_QUOTES;
					size_t len = strlen(token->token_value);
					if (c == '"') (token->token_value)[len - 1] = '\"';
					else if (c == 'n') (token->token_value)[len - 1] = '\n';
					else if (c == 't') (token->token_value)[len - 1] = '\t';
					else (token->token_value)[len - 1] = '\\';
					(token->token_value)[len] = 0;
					break;
				}
				else if (c == '0' || c == '1' || c == '2' || c == '3')
				{
					state = S_OCTAL_SECOND;
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						//free_token(token);
						return E_INTERN;
					}
					break;
				}

				state = S_ERR;
				break;

			case S_OCTAL_SECOND:
				if (isdigit(c) && c != '8' && c != '9')
				{
					state = S_OCTAL_THIRD;
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						//free_token(token);
						return E_INTERN;
					}
					break;
				}

				state = S_ERR;
				break;

			case S_OCTAL_THIRD:
				if (isdigit(c) && c != '8' && c != '9')
				{
					int i = strlen(token->token_value);
					if (((token->token_value)[i - 1] == '0') && ((token->token_value)[i - 2] == '0') && (c == '0')) {
						state = S_ERR;
						break;
					}	
					state = S_QUOTES;
					if (CUSTOM_APPEND(token->token_value, c) == E_INTERN)
					{
						//free_token(token);
						return E_INTERN;
					}

					char octal[4];
					size_t len = strlen(token->token_value);
					for (int i = len - 3; i < len; i++) octal[i - (len - 3)] = (token->token_value)[i];

					octal[3] = '\0';

					token->token_value = (char*)realloc(token->token_value, sizeof(char) * (len - 3));
					if (token->token_value == NULL)
						return E_INTERN;
					token->token_value[len - 4] = '\0';

					int xOct = atoi(octal);
					int xDec = (8 * 8 * (xOct / 100)) + (8 * ((xOct % 100) / 10)) + xOct % 10;

					if (CUSTOM_APPEND(token->token_value, (char)xDec) == E_INTERN)
					{
						//free_token(token);
						return E_INTERN;
					}
					state = S_QUOTES;
					break;
				}

				state = S_ERR;
				break;

			case S_ERR:
				if (!isspace(c))
					you_shall_not_pass(source_file);
				//free_token(token);
				return E_LEX;

			case S_OK:
				if (!isspace(c) && c != EOF)
					you_shall_not_pass(source_file);
				return E_OK;
		} /* end switch */
	} /* end while */
	return E_INTERN;	/* jump here and end my life */
}