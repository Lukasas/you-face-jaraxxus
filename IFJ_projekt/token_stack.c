#include "token_stack.h"
struct stack* stackPush (struct stack* stackPointer, struct tToken * ttPointer)
{   // funkce pushuje tToken na zasobnik, vraci ukazatel na vrchol zasobniku, pokud byl push neuspesny, vraci NULL
    struct stack* oldPointer = stackPointer;
    struct stack* newPointer = malloc(sizeof(struct stack));
    if (newPointer == NULL) return NULL;

    newPointer->tt.token_id = ttPointer->token_id;
    newPointer->tt.token_value = malloc(strlen(ttPointer->token_value)+1);
    strcpy (newPointer->tt.token_value,ttPointer->token_value);
    newPointer->next = oldPointer;

    return newPointer;
}

struct stack* stackPop (struct stack* stackPointer, struct tToken * ttPointer)
{   // funkce popuje vrchol zasobniku, data ulozi do ttPointer a vraci novy vrchol zasobniku
	if (stackPointer == NULL)
	{
		ttPointer->token_id = -1;
		return NULL;
	}
    struct stack* oldPointer = stackPointer;
    struct stack* newPointer = stackPointer->next;

	if (ttPointer != NULL)
	{
		ttPointer->token_id = oldPointer->tt.token_id;
		free(ttPointer->token_value);
		ttPointer->token_value = malloc(strlen(oldPointer->tt.token_value) + 1);
		strcpy(ttPointer->token_value, oldPointer->tt.token_value);
	}
    free(oldPointer->tt.token_value);
    free(oldPointer);

    return newPointer;
}

struct stack * stackClear(struct stack * stackPointer)
{
	while (stackPointer != NULL)
	{
		stackPointer = stackPop(stackPointer, NULL);
	}
	return NULL;
}
