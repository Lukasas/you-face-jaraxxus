#pragma once
#include "lexical_analyzator.h"

typedef struct stack
{
    struct tToken tt;
    struct stack * next;
} SStack;


struct stack* stackPush(struct stack* stackPointer, struct tToken * ttPointer);
struct stack* stackPop(struct stack* stackPointer, struct tToken * ttPointer);
struct stack* stackClear(struct stack* stackPointer);