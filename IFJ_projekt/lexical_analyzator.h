#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#define KEY_COUNT 17
#define RESERVED_COUNT 11
#define LENGTH_VALUE 64

#ifndef _LEXICAL_ANALYZATOR_H_
#define _LEXICAL_ANALYZATOR_H_

typedef struct tToken {
	int token_id;
	char *token_value;
} tToken;

enum err {
	E_OK = 0,
	E_LEX,
	E_SYN,
	E_INTERN = 99
};

enum states {
	S_BEGIN = 0,
	S_IDENTIFICATOR,
	S_IDENTIFICATOR_POINT,
	S_IDENTIFICATOR_POINT_IDENTIFICATOR,
	S_NUMBER,
	S_NUMBER_POINT,
	S_NUMBER_POINT_NUMBER,
	S_NUMBER_EXP,
	S_NUMBER_EXP_NUMBER,
	S_NUMBER_EXP_OP,
	S_DIV,
	S_GREATER,
	S_SMALLER,
	S_EQUAL,
	S_EQUAL_EQ,
	S_EXC,
	S_EXC_EQ,
	S_QUOTES,
	S_BSLASH,
	S_OCTAL_SECOND,
	S_OCTAL_THIRD,
	S_COMMENT_LINE,
	S_COMMENT_BLOCK,
	S_COMMENT_BLOCK_END,
	S_ERR,
	S_OK
};

enum tokens_id {
	T_KEYWORD = 0,							//klicove slovo (viz kousek dolu)
	T_RESERVED,								//rezervovane slovo (viz kousek vic dolu)
	T_IDENTIFICATOR,	
	T_IDENTIFICATOR_POINT_IDENTIFICATOR,	//plne kvalifikovany identifikator (identifikator1.identifikator2)
	T_INT,									//cele cislo (integer) - v retezci, pouzit funkci na prevod
	T_DOUBLE,								//desetinne cislo (double) - v retezci, pouzit funkci na prevod
	T_ADD,									//znak + (plus)
	T_MUL,									//znak * (krat)
	T_SUB,									//znak - (minus)
	T_DIV,									//znak / (deleno)
	T_SMALLER,								//znak < (mensi nez)
	T_SMALLER_EQ,							//znaky <= (mensi rovno)
	T_GREATER,								//znak > (vetsi nez)
	T_GREATER_EQ,							//znaky >= (vetsi rovno)
	T_EQ,									//znaky == (rovno)
	T_ASSIGN,								//znak = (prirad)
	T_NOT_EQ,								//znaky != (neni rovno)
	T_L_BRACKET,							//znak ( (leva kulata zavorka) 
	T_R_BRACKET,							//znak ) (prava kulata zavorka) 
	T_L_BRACE,								//znak { (leva slozena zavorka) 
	T_R_BRACE,								//znak } (prava slozena zavorka) 
	T_COMMA,								//znak , (carka)
	T_SEMICOLON,							//znak ; (strednik)
	T_STRING,								//retezec (string) - uz bez uvozovek
	T_EOF,									//konec souboru			
	T_CLASS,								//Pridana pravidla pro syntaxi						
	T_STATIC,
	T_TYPE,
	T_IF,
	T_ELSE,
	T_WHILE,
	T_RETURN,
	T_TERM,
	R_PROGRAM,
	R_CLASS,
	R_CODE,
	R_LINE,
	R_EXPRESSION,
	R_PARAMETERS,
	R_PARAMETER,
	R_COMAND,
	R_TERMS,
	R_PARAMCOMA,
	R_COMATERM
};

static char * keywords[KEY_COUNT] = { // const by default
	"boolean",	/* 1 */
	"break",
	"class",
	"continue",
	"do",		/* 5 */
	"double",
	"else",
	"false",
	"for",
	"if",		/* 10 */
	"int",
	"return",
	"String",
	"static",
	"true",		/* 15 */
	"void",
	"while"
};

static char* reserved[RESERVED_COUNT] = { // const by default
	"Main",
	"readInt",
	"readDouble",
	"readString",
	"print",
	"length",
	"substr",
	"compare",
	"find",
	"sort",
	"run"
};

int lexical_analyzator(FILE *source_file, struct tToken *token);

#define CUSTOM_APPEND(x, y) char_append(&x, y)
#endif // _LEXICAL_ANALYZATOR_H_