﻿#include "semantics_analyzator.h"
#include "syntax_analyzator.h"
#include "lexical_analyzator.h"
#include "instruction.h"

#include "precedence_analyzator.h"
#define DEBUG 0
bool LexErrorFlag = false;
int LexErrorNum = 0;

// pravidla
/*
Odkazy na jine pravidla a pravidla zacinaji velkym pismenem, tokeny malym, * oznacuje ze se muze opakovat(min 0), + oznacuje ze se muze opakovat (min 1)
*prida 600
+prida 1000
class = 25
name 1
static = 26 
type = 27 "int, string, double, boolean"
int = 4
float = 5
string = 23

*/

// p�enos tabulek
// p�enos prom�nn�ch
// p�enos funkc�
// ukon�en� funkc� - ka�d� funkce povinn� mus� m�t return jako zar�ku pro konec funkce

/*
IAL funkce ukl�daj� prom�nn� apd do struktur a to hashuj�
Pot�ebuji pro 3 adresn� k�d n�sleduj�c�:
Pokud se bude nap��klad s��tat prom�nn� s ��slem bude to vypadat n�sledovn�:
2     50     3
(addx param1 param2)
tak�e ��slo 50 je adresa prom�nn� v n�jak�m t�eba poli struktur
tak�e pot�ebuji, aby IAL v hashov�n� ukl�dal do value integer,
kter� bude reprezentovat adresu t� prom�nn� v poli struktur,
kter� bude obsahovat v�echny prom�nn�
toto pole struktur se bude p�ed�vat interpretu, kter� z toho bude potom �erpat prom�nn�

My�leno asi takto:

struct Variable
{
void * data;
}

struct * Variable = malloc....
Vars[x]->data = malloc(sizeof(int)); // pro int
Vars[y]->data = malloc(sizeof(float)); // pro int
Vars[z]->data = malloc(sizeof(char)); // pro char

mus� b�t n�jak o�et�eno pro �et�zec, to by se mohlo vy�e�it v interpretu / syntaxy
p��padn� se nap��klad p�i konkatenaci �et�zc� vytvo�� upln� nov� pam� v interpretu
pomoc� malloc a ta star� se prost� sma�e a p�ep�e se t�m nov�m u� spojen�m

jedin� co je takov� �e ble je to, �e t�m se vlastn� upln� zavrhne ta hashovan� tabulka
jen�e v tom 3 adresnem kodu nelze pouzit retezec
jeste me napada vyresit to tak, ze proste nazvy promennych budou ulozeny v tom poli
tak�e napriklad
2 50 3
kde 50 je adresa promenne vr�t� z pole prom�nn�ch n�zev promenn�, kter� se pot�
pouzije v IAL funkc�ch pro vyhled�v�n�
tak�e by to m�la vy�e�it syntax, tak�e asi tak to bude nejlep��

*/

int Rule_inprint(FILE *f, struct tToken *tt)
{
#ifdef DEBUG == 1
	FILE* log = fopen("log.txt", "a");
	fprintf(log, "Called Rule_inprint\n");
	fclose(log);
#endif
	struct InsList *InstrFoo;
	if (methodRecord != NULL && strcmp(methodRecord->methodName, "run")) InstrFoo = methodRecord->Function;
	else InstrFoo = Program;

	InsPrintInit(InstrFoo);
	do
	{
		if ((tt->token_id > 1 && tt->token_id <= 5) || tt->token_id == 23)
		{
			if (tt->token_id == 2)
			{ 
				SVariableRecord * m = HashTable_LookForVariable(methodRecord->first, tt->token_value, classNamespace->className, 2);
				InsPrintAddV(InstrFoo, m);
			}
			else if (tt->token_id == 3)
			{
				char * holder = malloc(strlen(tt->token_value) + 1);
				strcpy(holder, tt->token_value);
				char * CLS = strtok(holder, ".");
				char * varName = strtok(NULL, ".");
				SVariableRecord * m = HashTable_FindStaticVariable(hashTable, CLS, varName);
				free(holder);
				InsPrintAddV(InstrFoo, m);
			}
			else
			{
				InsPrintAddStr(InstrFoo, tt->token_value);
			}
			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
			{
				LexErrorFlag = true;
				return false;
			}
			if (tt->token_id == 6)
			{
				free(tt->token_value);
				tt->token_value = (char*)malloc(1);
				tt->token_value[0] = 0;
				if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
				{
					LexErrorFlag = true;
					return false;
				}
				//pokracujeme
				if (tt->token_id == 18) tt->token_id = -1; // osetruje zavorku za +
			}
			else if (tt->token_id == 18)
			{
				InsPrintExecute(InstrFoo);
				return true;
			}
			else
			{
				LexErrorNum = 4;
				LexErrorFlag = true;
				return false;
			}

		}
		else
		{
			LexErrorNum = 4;
			LexErrorFlag = true;
			return false;
		}
	} while (tt->token_id != 18 && tt->token_id != -1);
}

int Rule_while(FILE *f, struct tToken *tt)
{
#ifdef DEBUG == 1
	FILE* log = fopen("log.txt", "a");
	fprintf(log, "Called Rule_while\n");
	fclose(log);
#endif

	struct InsList *InstrFoo;
	if (methodRecord != NULL && strcmp(methodRecord->methodName, "run")) InstrFoo = methodRecord->Function;
	else InstrFoo = Program;

	if (tt->token_id == 17)
	{

		//Nasla se leva zavorka
		free(tt->token_value);
		tt->token_value = (char*)malloc(1);
		tt->token_value[0] = 0;
		if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
		{
			LexErrorFlag = true;
			return false;
		}

		InsWhileStart(InstrFoo);

		//Zavola se fce, zavorka uz preskocena
		if (Rule_expression_brace(f, tt))
		{
			//funkce probehla, mela by byt zavorka
			if (tt->token_id == 18)
			{
				SemanStack = stackPop(SemanStack, NULL);
				if ((LexErrorNum = CheckCondition(SemanStack)) != E_OK)
				{
					LexErrorFlag = true;
					return false;
				}
				SemanStack = stackClear(SemanStack);
				free(tt->token_value);
				tt->token_value = (char*)malloc(1);
				tt->token_value[0] = 0;
				if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
				{
					LexErrorFlag = true;
					return false;
				}
				if (tt->token_id == 19)
				{

					free(tt->token_value);
					tt->token_value = (char*)malloc(1);
					tt->token_value[0] = 0;
					if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}
					if (Rule_code_block(f, tt))
					{
						InsWhileEnd(InstrFoo);
						if (tt->token_id == 20)
						{
							free(tt->token_value);
							tt->token_value = (char*)malloc(1);
							tt->token_value[0] = 0;
							if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
							{
								LexErrorFlag = true;
								return false;
							}
							return true;

						}
						else return false;

					}
					else return false;

				}
				else return false;

			}
			else return false;

		}
		else return false;

	}
	else return false;
}

int Rule_if(FILE *f, struct tToken *tt)
{
#ifdef DEBUG == 1
	FILE* log = fopen("log.txt", "a");
	fprintf(log, "Called Rule_if\n");
	fclose(log);
#endif

	struct InsList *InstrFoo;
	if (methodRecord != NULL && strcmp(methodRecord->methodName, "run")) InstrFoo = methodRecord->Function;
	else InstrFoo = Program;

	if (tt->token_id == 17)
	{

		//Nasla se leva zavorka
		free(tt->token_value);
		tt->token_value = (char*)malloc(1);
		tt->token_value[0] = 0;
		if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
		{
			LexErrorFlag = true;
			return false;
		}

		InsIfStart(InstrFoo);

		//Zavola se fce, zavorka uz preskocena
		if (Rule_expression_brace(f, tt))
		{
			//funkce probehla, mela by byt zavorka
			if (tt->token_id == 18)
			{
				SemanStack = stackPop(SemanStack, NULL);
				if ((LexErrorNum = CheckCondition(SemanStack)) != E_OK)
				{
					LexErrorFlag = true;
					return false;
				}
				SemanStack = stackClear(SemanStack);
				free(tt->token_value);
				tt->token_value = (char*)malloc(1);
				tt->token_value[0] = 0;
				if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
				{
					LexErrorFlag = true;
					return false;
				}
				if (tt->token_id == 19)
				{

					free(tt->token_value);
					tt->token_value = (char*)malloc(1);
					tt->token_value[0] = 0;
					if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}
					if (Rule_code_block(f, tt))
					{
						if (tt->token_id == 20)
						{
							free(tt->token_value);
							tt->token_value = (char*)malloc(1);
							tt->token_value[0] = 0;
							if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
							{
								LexErrorFlag = true;
								return false;
							}
							if (tt->token_id == 0 && !strcmp(tt->token_value, "else"))
							{
								InsIfElse(InstrFoo);
								//Dosli jsme k else
								free(tt->token_value);
								tt->token_value = (char*)malloc(1);
								tt->token_value[0] = 0;
								if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
								{
									LexErrorFlag = true;
									return false;
								}
								if (tt->token_id == 19)
								{

									free(tt->token_value);
									tt->token_value = (char*)malloc(1);
									tt->token_value[0] = 0;
									if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
									{
										LexErrorFlag = true;
										return false;
									}
									if (Rule_code_block(f, tt))
									{
										if (tt->token_id == 20)
										{
											InsIfEnd(InstrFoo);
											free(tt->token_value);
											tt->token_value = (char*)malloc(1);
											tt->token_value[0] = 0;
											if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
											{
												LexErrorFlag = true;
												return false;
											}
											return true;

										}
										else return false;

									}
									else return false;

								}
								else return false;

							}
							else return false;

						}
						else return false;

					}
					else return false;

				}
				else return false;

			}
			else return false;

		}
		else return false;

	}
	else return false;
}

int Rule_call_parameters(FILE *f, struct tToken *tt)
{
#ifdef DEBUG == 1
	FILE* log = fopen("log.txt", "a");
	fprintf(log, "Called Rule_call_parameters\n");
	fclose(log);
#endif
	struct InsList *InstrFoo;
	if (methodRecord != NULL && strcmp(methodRecord->methodName, "run")) InstrFoo = methodRecord->Function;
	else InstrFoo = Program;
	while (tt->token_id != 18 && tt->token_id != -1)
	{

		if ((tt->token_id>1 && tt->token_id <= 5 )|| tt->token_id == 23)
		{
			//begin coma
			if (tt->token_id == 2 || tt->token_id == 3)
			{
				if (tt->token_id == 2)
				{
					SVariableRecord * m = HashTable_LookForVariable(methodRecord->first, tt->token_value, classNamespace->className, 2);
					InsStoreFncParamX(InstrFoo, m);
				}
				else if (tt->token_id == 3)
				{
					char * holder = malloc(strlen(tt->token_value) + 1);
					strcpy(holder, tt->token_value);
					char * CLS = strtok(holder, ".");
					char * varName = strtok(NULL, ".");
					SVariableRecord * m = HashTable_FindStaticVariable(hashTable, CLS, varName);
					free(holder);
					InsStoreFncParamX(InstrFoo, m);
				}
			}
			else if (tt->token_id == 4)
			{
				InsStoreFncParamI(InstrFoo, atoi(tt->token_value));
			}
			else if (tt->token_id == 5)
			{
				InsStoreFncParamD(InstrFoo, atof(tt->token_value));
			}
			else if (tt->token_id == 23)
			{
				InsStoreFncParamS(InstrFoo, tt->token_value);
			}
			SemanStack = stackPush(SemanStack, tt);
			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
			{
				LexErrorFlag = true;
				return false;
			}
			if (tt->token_id == 21)
			{

				SemanStack = stackPush(SemanStack, tt);
				//nasla se carka, cyklime
				free(tt->token_value);
				tt->token_value = (char*)malloc(1);
				tt->token_value[0] = 0;
				if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
				{
					LexErrorFlag = true;
					return false;
				}
				//pokracujeme
				if (tt->token_id == 18) tt->token_id = -1;
			}
			//konec, prava zavorka
			else if (tt->token_id == 18)
			{
				return true;
			}
			else return false;

		}
	}
	return true;
}

int Rule_parameters(FILE *f, struct tToken *tt)
{
	#ifdef DEBUG == 1
		FILE* log = fopen("log.txt", "a");
		fprintf(log, "Called Rule_parameters\n");
		fclose(log);
	#endif
		while (tt->token_id != 18 && tt->token_id != -1)
		{
			if (tt->token_id == 0 && (!strcmp(tt->token_value, "String") || !strcmp(tt->token_value, "void") || !strcmp(tt->token_value, "int") || !strcmp(tt->token_value, "double")))
			{	
				SemanStack = stackPush(SemanStack, tt);
				// vezme jeden parameter
				//begin name
				free(tt->token_value);
				tt->token_value = (char*)malloc(1);
				tt->token_value[0] = 0;
				if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
				{
					LexErrorFlag = true;
					return false;
				}
				if (tt->token_id == 1 || tt->token_id == 2)
				{
					SemanStack = stackPush(SemanStack, tt);
					//begin coma
					free(tt->token_value);
					tt->token_value = (char*)malloc(1);
					tt->token_value[0] = 0;
					if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}
					if (tt->token_id == 21)
					{
						//nasla se carka, cyklime
						free(tt->token_value);
						tt->token_value = (char*)malloc(1);
						tt->token_value[0] = 0;
						if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
						{
							LexErrorFlag = true;
							return false;
						}
						//pokracujeme
						if (tt->token_id == 18) tt->token_id = -1; // osetruje zavorku za ,
					}
					//konec, prava zavorka
					else if (tt->token_id == 18)
					{
						return true;
					}
					else return false;

				}
				else return false;

			}
			else return false;
		}
		return true;
}

int Rule_block_line(FILE *f, struct tToken *tt)
{
#ifdef DEBUG == 1
	FILE* log = fopen("log.txt", "a");
	fprintf(log, "Called Rule_block_line\n");
	fclose(log);
#endif
	struct InsList *InstrFoo;
	if (methodRecord != NULL && strcmp(methodRecord->methodName, "run")) InstrFoo = methodRecord->Function;
	else InstrFoo = Program;
	// Radek kodu, opakuje se mezi hranatymi zavorkami
	/////////////////////////////////////////////////////////
	/////						If						/////
	/////////////////////////////////////////////////////////
	if (tt->token_id == 0 && !strcmp(tt->token_value, "if"))
	{
		free(tt->token_value);
		tt->token_value = (char*)malloc(1);
		tt->token_value[0] = 0;
		if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
		{
			LexErrorFlag = true;
			return false;
		}
		return Rule_if (f, tt);
	}
	/////////////////////////////////////////////////////////
	/////						While					/////
	/////////////////////////////////////////////////////////
	else if (tt->token_id == 0 && !strcmp(tt->token_value, "while"))
	{
		free(tt->token_value);
		tt->token_value = (char*)malloc(1);
		tt->token_value[0] = 0;
		if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
		{
			LexErrorFlag = true;
			return false;
		}
		return Rule_while(f, tt);
	}
	/////////////////////////////////////////////////////////
	/////						Return					/////
	/////////////////////////////////////////////////////////
	else if (tt->token_id == 0 && (!strcmp(tt->token_value, "return")))
	{
		SemanStack = stackPush(SemanStack, tt);
		free(tt->token_value);
		tt->token_value = (char*)malloc(1);
		tt->token_value[0] = 0;
		if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
		{
			LexErrorFlag = true;
			return false;
		}
		if (tt->token_id == 22)
		{

			if ((LexErrorNum = CheckTypeForReturn(SemanStack)) != E_OK)
			{
				LexErrorFlag = true;
				return false;
			}
			InsEndFunction(InstrFoo);
			SemanStack = stackClear(SemanStack);
			// nasel se strednik, jenom return
			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
			{
				LexErrorFlag = true;
				return false;
			}
			return true;
		}
		else if (Rule_expression(f, tt))
		{
			//nasel se expression
			if (tt->token_id == 22)
			{

				SemanStack = stackPush(SemanStack, tt);
				if ((LexErrorNum = CheckTypeForReturn(SemanStack)) != E_OK)
				{
					LexErrorFlag = true;
					return false;
				}
				InsEndFunction(InstrFoo);
				SemanStack = stackClear(SemanStack);
				free(tt->token_value);
				tt->token_value = (char*)malloc(1);
				tt->token_value[0] = 0;
				if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
				{
					LexErrorFlag = true;
					return false;
				}
				return true;
			}
		}
		else return false;
	}
	/////////////////////////////////////////////////////////
	/////		Print Special Fucking Snowflake			/////
	/////////////////////////////////////////////////////////
	else if (tt->token_id == 3 && !strcmp(tt->token_value, "ifj16.print"))
	{
		free(tt->token_value);
		tt->token_value = (char*)malloc(1);
		tt->token_value[0] = 0;
		if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
		{
			LexErrorFlag = true;
			return false;
		}
		if (tt->token_id == 17)
		{

			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
			{
				LexErrorFlag = true;
				return false;
			}
			if (Rule_inprint(f, tt))
			{
				if (tt->token_id == 18)
				{
					free(tt->token_value);
					tt->token_value = (char*)malloc(1);
					tt->token_value[0] = 0;
					if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}
					if (tt->token_id == 22)
					{

						free(tt->token_value);
						tt->token_value = (char*)malloc(1);
						tt->token_value[0] = 0;
						if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
						{
							LexErrorFlag = true;
							return false;
						}
						return true;

					}
					else return false;

				}
				else return false;

			}
			else return false;

		}
		else return false;
	}
	/////////////////////////////////////////////////////////
	/////						Name					/////
	/////////////////////////////////////////////////////////
	else if (tt->token_id == 1 || tt->token_id == 2 || tt->token_id == 3)
	{

		SemanStack = stackPush(SemanStack, tt);
		free(tt->token_value);
		tt->token_value = (char*)malloc(1);
		tt->token_value[0] = 0;
		if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
		{
			LexErrorFlag = true;
			return false;
		}
		if (tt->token_id == 15)
		{
			SemanStack = stackPush(SemanStack, tt);
			//naslo se rovna se
			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
			{
				LexErrorFlag = true;
				return false;
			}
			if (Rule_expression(f, tt))
			{
				if (tt->token_id == 22)
				{
					if ((LexErrorNum = CheckTypeForExpression(SemanStack)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}

					while ((SemanStack = stackPop(SemanStack, NULL))->next != NULL);
					SVariableRecord *m = HashTable_LookForVariable(methodRecord->first, SemanStack->tt.token_value, classNamespace->className, 2);
					switch (m->type)
					{
					case INT:
						InsStoreDblVal(InstrFoo, m);
						break;
					case DOUBLE:
						InsStoreDblVal(InstrFoo, m);
						break;
					case STRING:
						InsStrStore(InstrFoo, m);
						break;
					default:
						break;
					}
					InsStrBufClr(InstrFoo);
					InsClearIntAcc(InstrFoo);
					SemanStack = stackClear(SemanStack);
					free(tt->token_value);
					tt->token_value = (char*)malloc(1);
					tt->token_value[0] = 0;
					if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}
					return true;
				}
				else return false;
			}
			else return false;

		}
		else if (tt->token_id == 17)
		{

			SemanStack = stackPush(SemanStack, tt);
			//nasla se zavorka
			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
			{
				LexErrorFlag = true;
				return false;
			}
			if (Rule_call_parameters(f, tt))
			{

				if (tt->token_id == 18)
				{

					SemanStack = stackPush(SemanStack, tt);
					if ((LexErrorNum = CheckCall(SemanStack)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}
					while ((SemanStack = stackPop(SemanStack, NULL))->next != NULL);

					char * holder = malloc(strlen(SemanStack->tt.token_value) + 1);
					strcpy(holder, SemanStack->tt.token_value);
					char * CLS = strtok(holder, ".");
					char * varName = strtok(NULL, ".");

					if (varName == NULL)
						InsCallFunction(InstrFoo, HashTable_FindMethod(hashTable, classNamespace->className, CLS), classNamespace->className);
					else
						InsCallFunction(InstrFoo, HashTable_FindMethod(hashTable, CLS, varName), CLS);

					free(holder);
					SemanStack = stackClear(SemanStack);
					//nasla se prava zavorka
					free(tt->token_value);
					tt->token_value = (char*)malloc(1);
					tt->token_value[0] = 0;
					if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}
					if (tt->token_id == 22)
					{
						free(tt->token_value);
						tt->token_value = (char*)malloc(1);
						tt->token_value[0] = 0;
						if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
						{
							LexErrorFlag = true;
							return false;
						}
						return true;

					}
					else return false;

				}
				else return false;

			}
			else return false;

		}
		else return false;

	}
	else return false;
}

int Rule_code_block(FILE *f, struct tToken *tt)
{
#ifdef DEBUG == 1
	FILE* log = fopen("log.txt", "a");
	fprintf(log, "Called Rule_code_block\n");
	fclose(log);
#endif
	while (Rule_block_line(f, tt));
	if (tt->token_id == 20)
		return true;
	else return false;
}

int Rule_line(FILE *f, struct tToken *tt)
{	
	#ifdef DEBUG == 1
		FILE* log = fopen("log.txt", "a");
		fprintf(log, "Called Rule_line\n");
		fclose(log);
	#endif

		struct InsList *InstrFoo;
		if (methodRecord != NULL && strcmp(methodRecord->methodName, "run")) InstrFoo = methodRecord->Function;
		else InstrFoo = Program;
	// Radek kodu, opakuje se mezi hranatymi zavorkami
	/////////////////////////////////////////////////////////
	/////						If						/////
	/////////////////////////////////////////////////////////
	if (tt->token_id == 0 && !strcmp(tt->token_value, "if"))
	{
		free(tt->token_value);
		tt->token_value = (char*)malloc(1);
		tt->token_value[0] = 0;
		if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
		{
			LexErrorFlag = true;
			return false;
		}
		return Rule_if(f, tt);
	}
	/////////////////////////////////////////////////////////
	/////						While					/////
	/////////////////////////////////////////////////////////
	else if (tt->token_id == 0 && !strcmp(tt->token_value, "while"))
	{
		free(tt->token_value);
		tt->token_value = (char*)malloc(1);
		tt->token_value[0] = 0;
		if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
		{
			LexErrorFlag = true;
			return false;
		}
		return Rule_while(f, tt);
	}
	/////////////////////////////////////////////////////////
	/////						Type					/////
	/////////////////////////////////////////////////////////
	else if (tt->token_id == 0 && (!strcmp(tt->token_value, "String") || !strcmp(tt->token_value, "int") || !strcmp(tt->token_value, "double")))
	{
		SemanStack = stackPush(SemanStack, tt);
		//begin name
		free(tt->token_value);
		tt->token_value = (char*)malloc(1);
		tt->token_value[0] = 0;
		if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
		{
			LexErrorFlag = true;
			return false;
		}
		if (tt->token_id == 1 || tt->token_id == 2)
		{
			SemanStack = stackPush(SemanStack, tt);
			//tady uz mame name
			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
			{
				LexErrorFlag = true;
				return false;
			}
			if (tt->token_id == 22)
			{
				if ((LexErrorNum = DeclareVar(SemanStack)) != E_OK)
				{
					LexErrorFlag = true;
					return false;
				}
				SemanStack = stackClear(SemanStack);
				//verze se strednikem
				free(tt->token_value);
				tt->token_value = (char*)malloc(1);
				tt->token_value[0] = 0;
				if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
				{
					LexErrorFlag = true;
					return false;
				}
				return true;
			}
			else if (tt->token_id == 15)
			{
				SemanStack = stackPush(SemanStack, tt);
				//verze s prirazenim
				free(tt->token_value);
				tt->token_value = (char*)malloc(1);
				tt->token_value[0] = 0;
				if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
				{
					LexErrorFlag = true;
					return false;
				}
				if (Rule_expression(f, tt))
				{
					if ((LexErrorNum = DefineLocalVar(SemanStack)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}
					while ((SemanStack = stackPop(SemanStack, NULL))->next->next != NULL);
					SVariableRecord *m = HashTable_LookForVariable(methodRecord->first, SemanStack->tt.token_value, classNamespace->className, 2);
					switch (m->type)
					{
						case INT:
							InsStoreDblVal(InstrFoo, m);
							break;
						case DOUBLE:
							InsStoreDblVal(InstrFoo, m);
							break;
						case STRING:
							InsStrStore(InstrFoo, m);
							break;
						default:
							break;
					}
					InsStrBufClr(InstrFoo);
					InsClearIntAcc(InstrFoo);
					SemanStack = stackClear(SemanStack);
					//nasel se expression
					if (tt->token_id == 22)
					{
						free(tt->token_value);
						tt->token_value = (char*)malloc(1);
						tt->token_value[0] = 0;
						if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
						{
							LexErrorFlag = true;
							return false;
						}
						return true;
					}
				}

			}
			else return false;

		}
		else return false;
	}
	/////////////////////////////////////////////////////////
	/////						Return					/////
	/////////////////////////////////////////////////////////
	else if (tt->token_id == 0 && (!strcmp(tt->token_value, "return")))
	{
		SemanStack = stackPush(SemanStack, tt);
		free(tt->token_value);
		tt->token_value = (char*)malloc(1);
		tt->token_value[0] = 0;
		if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
		{
			LexErrorFlag = true;
			return false;
		}
		if (tt->token_id == 22)
		{

			if ((LexErrorNum = CheckTypeForReturn(SemanStack)) != E_OK)
			{
				LexErrorFlag = true;
				return false;
			}
			InsEndFunction(methodRecord->Function);
			SemanStack = stackClear(SemanStack);
			// nasel se strednik, jenom return
			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
			{
				LexErrorFlag = true;
				return false;
			}
			return true;
		}
		else if (Rule_expression(f, tt))
		{
			//nasel se expression
			if (tt->token_id == 22)
			{

				SemanStack = stackPush(SemanStack, tt);
				if ((LexErrorNum = CheckTypeForReturn(SemanStack)) != E_OK)
				{
					LexErrorFlag = true;
					return false;
				}
				InsEndFunction(methodRecord->Function);
				SemanStack = stackClear(SemanStack);
				free(tt->token_value);
				tt->token_value = (char*)malloc(1);
				tt->token_value[0] = 0;
				if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
				{
					LexErrorFlag = true;
					return false;
				}
				return true;
			}
		}
		else return false;
	}
	/////////////////////////////////////////////////////////
	/////		Print Special Fucking Snowflake			/////
	/////////////////////////////////////////////////////////
	else if (tt->token_id == 3 && !strcmp(tt->token_value,"ifj16.print"))
	{
		free(tt->token_value);
		tt->token_value = (char*)malloc(1);
		tt->token_value[0] = 0;
		if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
		{
			LexErrorFlag = true;
			return false;
		}
		if (tt->token_id == 17)
		{

			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
			{
				LexErrorFlag = true;
				return false;
			}
			if (Rule_inprint(f, tt))
			{
				if (tt->token_id == 18)
				{
					free(tt->token_value);
					tt->token_value = (char*)malloc(1);
					tt->token_value[0] = 0;
					if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}
					if (tt->token_id == 22)
					{

						free(tt->token_value);
						tt->token_value = (char*)malloc(1);
						tt->token_value[0] = 0;
						if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
						{
							LexErrorFlag = true;
							return false;
						}
						return true;

					}
					else return false;

				}
				else return false;

			}
			else return false;

		}
		else return false;
	}
	/////////////////////////////////////////////////////////
	/////						Name					/////
	/////////////////////////////////////////////////////////
	else if (tt->token_id == 1 || tt->token_id == 2 || tt->token_id == 3)
	{

		SemanStack = stackPush(SemanStack, tt);
		free(tt->token_value);
		tt->token_value = (char*)malloc(1);
		tt->token_value[0] = 0;
		if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
		{
			LexErrorFlag = true;
			return false;
		}
		if (tt->token_id == 15)
		{
			SemanStack = stackPush(SemanStack, tt);
			//naslo se rovna se
			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
			{
				LexErrorFlag = true;
				return false;
			}
			if (Rule_expression(f, tt))
			{
				if (tt->token_id == 22)
				{
					if ((LexErrorNum = CheckTypeForExpression(SemanStack)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}

					while ((SemanStack = stackPop(SemanStack, NULL))->next != NULL);
					SVariableRecord *m = HashTable_LookForVariable(methodRecord->first, SemanStack->tt.token_value, classNamespace->className, 2);
					switch (m->type)
					{
						case INT:
							InsStoreDblVal(InstrFoo, m);
							break;
						case DOUBLE:
							InsStoreDblVal(InstrFoo, m);
							break;
						case STRING:
							InsStrStore(InstrFoo, m);
							break;
						default:
							break;
					}
					InsStrBufClr(InstrFoo);
					InsClearIntAcc(InstrFoo);
					SemanStack = stackClear(SemanStack);
					free(tt->token_value);
					tt->token_value = (char*)malloc(1);
					tt->token_value[0] = 0;
					if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}
					return true;
				}
				else return false;
			}
			else return false;

		}
		else if (tt->token_id == 17)
		{

			SemanStack = stackPush(SemanStack, tt);
			//nasla se zavorka
			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
			{
				LexErrorFlag = true;
				return false;
			}
			if (Rule_call_parameters(f, tt))
			{
				
				if (tt->token_id == 18)
				{

					SemanStack = stackPush(SemanStack, tt);
					if ((LexErrorNum = CheckCall(SemanStack)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}
				while ((SemanStack = stackPop(SemanStack, NULL))->next != NULL);

				char * holder = malloc(strlen(SemanStack->tt.token_value) + 1);
				strcpy(holder, SemanStack->tt.token_value);
				char * CLS = strtok(holder, ".");
				char * varName = strtok(NULL, ".");

				if (varName == NULL)
					InsCallFunction(InstrFoo, HashTable_FindMethod(hashTable, classNamespace->className, CLS), classNamespace->className);
				else
					InsCallFunction(InstrFoo, HashTable_FindMethod(hashTable, CLS, varName), CLS);

				free(holder);
					SemanStack = stackClear(SemanStack);
					//nasla se prava zavorka
					free(tt->token_value);
					tt->token_value = (char*)malloc(1);
					tt->token_value[0] = 0;
					if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}
					if (tt->token_id == 22)
					{
						free(tt->token_value);
						tt->token_value = (char*)malloc(1);
						tt->token_value[0] = 0;
						if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
						{
							LexErrorFlag = true;
							return false;
						}
						return true;

					}
					else return false;

				}
				else return false;

			}
			else return false;

		}
		else return false;

	}
	else return false;
}

int Rule_code(FILE *f, struct tToken *tt)
{
	#ifdef DEBUG == 1
		FILE* log = fopen("log.txt", "a");
		fprintf(log, "Called Rule_code\n");
		fclose(log);
	#endif
	while (Rule_line(f, tt));
	if (tt->token_id == 20)
		return true;
	else return false;
}

int Rule_class_line(FILE *f, struct tToken *tt)
{	
	#ifdef DEBUG == 1
		FILE* log = fopen("log.txt", "a");
		fprintf(log, "Called Rule_class_line\n");
		fclose(log);
	#endif

	struct InsList *InstrFoo;
	if (methodRecord != NULL && strcmp(methodRecord->methodName, "run")) InstrFoo = methodRecord->Function;
	else InstrFoo = Program;
	// Radek kodu, opakuje se mezi hranatymi zavorkami
	/////////////////////////////////////////////////////////
	/////						Static					/////
	/////////////////////////////////////////////////////////
	if (tt->token_id == 0 && (!strcmp(tt->token_value, "static")))
	{

		//Begin Type
		SemanStack = stackPush(SemanStack, tt);
		free(tt->token_value);
		tt->token_value = (char*)malloc(1);
		tt->token_value[0] = 0;
		if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
		{
			LexErrorFlag = true;
			return false;
		}
		if (tt->token_id == 0 && (!strcmp(tt->token_value, "String") || !strcmp(tt->token_value, "void") || !strcmp(tt->token_value, "int") || !strcmp(tt->token_value, "double")))
		{
			SemanStack = stackPush(SemanStack, tt);
			//Begin name
			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f,tt)) != E_OK)
			{
				LexErrorFlag = true;
				return false;
			}
			if (tt->token_id == 1 || tt->token_id == 2)
			{
				SemanStack = stackPush(SemanStack, tt);
				// Zjistili jsme, že mame static type name
				free(tt->token_value);
				tt->token_value = (char*)malloc(1);
				tt->token_value[0] = 0;
				if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
				{
					LexErrorFlag = true;
					return false;
				}
				// Varianta se strednikem
				if (tt->token_id == 22)
				{
					if ((LexErrorNum = DeclareVar(SemanStack)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}
					SemanStack = stackClear(SemanStack);
					free(tt->token_value);
					tt->token_value = (char*)malloc(1);
					tt->token_value[0] = 0;
					if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}
					return true;
				}
				// Varianta s rovna se -> prirazuje
				else if (tt->token_id == 15)
				{
					SemanStack = stackPush(SemanStack, tt);
					// expression
					free(tt->token_value);
					tt->token_value = (char*)malloc(1);
					tt->token_value[0] = 0;
					if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}
					if (Rule_class_expression(f,tt))
					{
						if ((LexErrorNum = DefineStaticVarWithExpr(SemanStack)) != E_OK)
						{
							LexErrorFlag = true;
							return false;
						}
						while ((SemanStack = stackPop(SemanStack, NULL))->next->next->next != NULL);
						SVariableRecord *m = HashTable_FindStaticVariable(hashTable, classNamespace->className, SemanStack->tt.token_value);
						InsStoreStaticDbl(InstrFoo, m);
						// strednik ukoncuje
						SemanStack = stackClear(SemanStack);
						if (tt->token_id == 22)
						{
							
							free(tt->token_value);
							tt->token_value = (char*)malloc(1);
							tt->token_value[0] = 0;
							if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
							{
								LexErrorFlag = true;
								return false;
							}
							return true;
						}
						else return false;

					}
					else return false;

				}
				// Varianta s levou zavorkou -> funkce
				else if (tt->token_id == 17)
				{
					SemanStack = stackPush(SemanStack, tt);
					//begin Parameters
					free(tt->token_value);
					tt->token_value = (char*)malloc(1);
					tt->token_value[0] = 0;
					if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
					{
						LexErrorFlag = true;
						return false;
					}
					if (Rule_parameters(f, tt))
					{

						//begin prava zavorka
						if (tt->token_id == 18)
						{
							SemanStack = stackPush(SemanStack, tt);
							//
							//if ((LexErrorNum))
							if ((LexErrorNum = DefineMethod(SemanStack)) != E_OK)
							{
								LexErrorFlag = true;
								return false;
							}

							while ((SemanStack = stackPop(SemanStack, NULL))->next->next->next != NULL);

							char * holder = malloc(strlen(SemanStack->tt.token_value) + 1);
							strcpy(holder, SemanStack->tt.token_value);
							char * CLS = strtok(holder, ".");
							char * varName = strtok(NULL, ".");
							SMethodRecord * mmm;
							if (strcmp(CLS, "run") != 0)
							{
								if (varName == NULL)
								{
									mmm = HashTable_FindMethod(hashTable, classNamespace->className, CLS);
									mmm->Function = InsCallFirstFunction(mmm->Function, mmm, classNamespace->className);
								}
								else
								{
									mmm = HashTable_FindMethod(hashTable, CLS, varName);
									mmm->Function = InsCallFirstFunction(mmm->Function, mmm, CLS);
								}

								free(holder);

							}
							else
							{
								Program = InsCallFirstFunction(InstrFoo, HashTable_FindMethod(hashTable, "Main", "run"), "Main");
							}							

							SemanStack = stackClear(SemanStack);
							//begin leva slozena
							free(tt->token_value);
							tt->token_value = (char*)malloc(1);
							tt->token_value[0] = 0;
							if ((LexErrorNum = lexical_analyzator(f,tt)) != E_OK)
							{
								LexErrorFlag = true;
								return false;
							}
							if (tt->token_id == 19)
							{

								//begin code
								free(tt->token_value);
								tt->token_value = (char*)malloc(1);
								tt->token_value[0] = 0;
								if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
								{
									LexErrorFlag = true;
									return false;
								}
								if (Rule_code(f,tt))
								{

									//begin prava slozena
									if (tt->token_id == 20)
									{

										free(tt->token_value);
										tt->token_value = (char*)malloc(1);
										tt->token_value[0] = 0;
										if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
										{
											LexErrorFlag = true;
											return false;
										}
										ResetCurrentMethod();
										return true;

									}
									else return false;

								}
								else return false;

							}
							else return false;

						}
						else return false;

					}
					else return false;

				}
				else return false;

			}
			else return false;

		}
		else return false;
	}
	else return false;
}

int Rule_class_code(FILE *f, struct tToken *tt)
{
	#ifdef DEBUG == 1
		FILE* log = fopen("log.txt", "a");
		fprintf(log, "Called Rule_class_code\n");
		fclose(log);
	#endif
	while (Rule_class_line(f, tt));
	if (tt->token_id == 20)
		return true;
	else return false;
}

int Rule_class(FILE *f,struct tToken *tt)
{
	#ifdef DEBUG == 1
		FILE* log = fopen("log.txt", "a");
		fprintf(log, "Called Rule_class\n");
		fclose(log);
	#endif
	//Starting class
	if (tt->token_id == 0 && !strcmp(tt->token_value,"class"))
	{	
		SemanStack = stackPush(SemanStack, tt);
		//Starting Main
		free(tt->token_value);
		tt->token_value = (char*)malloc(1);
		tt->token_value[0] = 0;
		if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
		{
			LexErrorFlag = true;
			return false;
		}
		if ((tt->token_id == 1 && !strcmp(tt->token_value, "Main")) || (tt->token_id == 2))
		{
			SemanStack = stackPush(SemanStack, tt);
			if ((LexErrorNum = DefineClass(SemanStack)) != E_OK)
			{
				LexErrorFlag = true;
				return false;
			}
			SemanStack = stackClear(SemanStack);
			// Starting {
			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
			{
				LexErrorFlag = true;
				return false;
			}
			if (tt->token_id == 19)
			{	// Starting Code
				free(tt->token_value);
				tt->token_value = (char*)malloc(1);
				tt->token_value[0] = 0;
				if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
				{
					LexErrorFlag = true;
					return false;
				}
				if (Rule_class_code(f, tt))
				{	// starting }
					if (tt->token_id == 20)
					{	
						free(tt->token_value);
						tt->token_value = (char*)malloc(1);
						tt->token_value[0] = 0;
						if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
						{
							LexErrorFlag = true;
							return false;
						}
						ResetCurrentClass();
						return true;
					}
					else return false;
				}
				else return false;
			}
			else return false;
		}
		else return false;
	}
	else return false;
}

int Rule_program(FILE *f, struct tToken *tt)
{								// dokud se opakuje class a nebo se dojde na eof
	#ifdef DEBUG == 1
		FILE* log = fopen("log.txt", "a");
		fprintf(log, "Called Rule_program\n");
		fclose(log);
	#endif
	while (Rule_class(f, tt));	
	if (tt->token_id == 24)
	{
		return true;
	}
	else return false;
}

int SyntaxAnalyzator(FILE *f)
{
	SemanStack = NULL;
	#ifdef DEBUG == 1
		FILE* log = fopen("log.txt", "w");
		fprintf(log, "Log of syntax activity\n\n");
		fclose(log);
	#endif

	struct tToken tt;
	struct stack *StackPointer = NULL;
	struct stack *DebugPointer = NULL;
	tt.token_value = (char*)malloc(1);
	tt.token_value[0] = 0;
	int Retval;
	if ((Retval = lexical_analyzator(f, &tt)) != E_OK)
	{
		printf("Ended with error.\n");
		return Retval;
	}
	/*while ((Retval = lexical_analyzator(f, &tt)) == E_OK && Retval != E_INTERN && Retval != E_LEX && tt.token_id != T_EOF)
	{
		if (DEBUG)
		{
			FILE* log = fopen("log.txt", "a");
			DebugPointer = StackPointer;
			fprintf(log, "Token	{");
			while (DebugPointer != NULL)
			{
				fprintf(log, " %s(%d) ", DebugPointer->tt.token_value, DebugPointer->tt.token_id);
				DebugPointer = DebugPointer->next;
			}
			fprintf(log, "}\n");
			fclose(log);
		}
		free(tt.token_value);
		tt.token_value = (char*)malloc(1);
		tt.token_value[0] = 0;
	}*/
	
	if (Rule_program(f, &tt) && !LexErrorFlag)
	{
		SemanStack = stackClear(SemanStack);
		return 0;
	}
	else if (LexErrorFlag)
	{
		SemanStack = stackClear(SemanStack);
		return LexErrorNum;
	}
	else
	{
		SemanStack = stackClear(SemanStack);
		return E_SYN;
	}

}