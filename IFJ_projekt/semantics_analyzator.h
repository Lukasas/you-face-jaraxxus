#pragma once

#include <stdbool.h>
#include <stdio.h>
#include "instruction.h"
#include "token_stack.h"
#include "ial.h"
#define REALOC_NUM 5

enum s_errors
{
	SEM_OK = 0,
	SEM_UNDEFINED_REDEF = 3,
	SEM_INCOMPATIBLE,
	SEM_ELSE = 6
};

/* DECLARATIONS */
char *CreateSignature(SStack *stack, int *retcode, int *offset, bool condition);
bool isFunc(SStack * stack);
int DefineFunParams(SStack* stack);
char *CreateFunParamSignature(SStack *stack, int *retcode, int *offset);
int CheckCall(SStack * stack);
int CheckTypeForReturn(SStack * stack);
int CheckTypeForFunCall(SStack * stack);
int CheckTypeForExpression(SStack * stack);
void SemanticInit();
void ResetCurrentMethod();
int DefineClass(SStack * stack);
int DefineMethod(SStack * stack);
int DeclareVar(SStack * stack);
int DefineStaticVarWithExpr(SStack * stack);
int DefineLocalVar(SStack * stack);
int DefineLocalVarWithExpr(SStack * stack);
int DefineStaticVarWithFunction(SStack * stack);
int DefineLocalVarWithFunction(SStack * stack);
char VarType(EDataTypes e);
char VarTypeByStr(char* str);
void reverseWords(char * s);
EDataTypes VarEnum(char* arr);
char * CreateFunCallSignature(SStack * stack, int * retcode, int * offset);
int Prior(char c);
char * PostFixConverter(const char * input);
bool isOperator(char c);
char ExpResult(char l, char r, char operand);
bool Conversions(char a, char b);
char GetType(char* signature);

SHashTable		*hashTable;
SClassNamespace	*classNamespace;
SMethodRecord	*methodRecord;
//static int				retCode;

//char *GetType(char* auto_type);
//
//int CheckTypeForExpression(SStack *stack, SMethodRecord *methodRecord, SClassNamespace *classNamespace);
//int CheckTypeForReturn(SStack *stack, SMethodRecord *methodRecord, SClassNamespace *classNamespace);

//int CheckTypeForExpression(SStack * stack);



/*
PROMENNE

int a;
int a = 5;
int a = foo();
a = 5;
a = foo();

FUNKCE

foo();
foo( int i, double d)    -- signatura funkce pri volani


if / while (type)
return x
*/


