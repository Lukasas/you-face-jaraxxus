#pragma once
#include "precedence_analyzator.h"
#include "syntax_analyzator.h"

extern bool LexErrorFlag;
extern int LexErrorNum;
extern SMethodRecord *methodRecord;
extern struct InsList * Program;
extern SHashTable *hashTable;
extern SClassNamespace	*classNamespace;

/*	
	funkce urci typ struktury z id tokenu 
	pokud se nejdna o typ prijimany precedencni analyzou 
	vrati -1
*/

int getExprType(int token_id)
{
	switch (token_id)
	{
	case T_IDENTIFICATOR:
	case T_IDENTIFICATOR_POINT_IDENTIFICATOR:
		return E_IDENTIFICATOR;
	case T_INT:
	case T_DOUBLE:
	case T_STRING:
		return E_DATATYPE;
	case T_ADD:
		return E_ADD;
	case T_MUL:
		return E_MUL;
	case T_SUB:
		return E_SUB;
	case T_DIV:
		return E_DIV;
	case T_SMALLER:
		return E_SMALLER;
	case T_SMALLER_EQ:
		return E_SMALLER_EQUAL;
	case T_GREATER:
		return E_GREATER;
	case T_GREATER_EQ:
		return E_GREATER_EQUAL;
	case T_EQ:
		return E_EQUAL;
	case T_NOT_EQ:
		return E_NOT_EQUAL;
	case T_L_BRACKET:
		return E_L_BRACKET;
	case T_R_BRACKET:
		return E_R_BRACKET;
	case T_SEMICOLON:
		return E_SEMICOLON;
	default:
		return -1;
	}
}

/*
	struktura pro ulozeni informaci o polozce
*/

struct tExpr {
	int type;
	int dataType;
	char *value;
	long long int address;
	struct stackExpr* eStack;
};

/*
	zasobnik tExpr struktur
*/

struct stackExpr {
	struct tExpr expr;
	struct stackExpr *next;
};

/*
	funkce pro ziskani struktury z vrcholu zasobniku stackPointer
	a jeji ulozeni do struktury pointer
	funkce vraci ukazatel na nasledujici prvek zasobniku
*/

struct stackExpr* stackExprPush(struct stackExpr  *stackPointer, struct tExpr *pointer)
{
	struct stackExpr* oldPointer = stackPointer;
	struct stackExpr* newPointer = malloc(sizeof(struct stackExpr));
	if (newPointer == NULL) return NULL;

	newPointer->expr.type = pointer->type;
	if (newPointer->expr.type == E_DATATYPE)
	{
		newPointer->expr.dataType = pointer->dataType;
		newPointer->expr.value = malloc(strlen(pointer->value) + 1);
		strcpy(newPointer->expr.value, pointer->value);
	}
	else if (newPointer->expr.type == E_IDENTIFICATOR)
	{
		newPointer->expr.dataType = pointer->dataType;
		newPointer->expr.address = pointer->address;
	}
	else if (newPointer->expr.type == E_EXPRESSION)
	{
		newPointer->expr.dataType = pointer->dataType;
		if (newPointer->expr.dataType < 3)
		{
			newPointer->expr.value = malloc(strlen(pointer->value) + 1);
			strcpy(newPointer->expr.value, pointer->value);
		}
		else newPointer->expr.address = pointer->address;
		newPointer->expr.eStack = pointer->eStack;
	}
	newPointer->next = oldPointer;

	return newPointer;
}

/*
	funkce pro ulozeni struktury pointer na vrchol zasobniku stackPointer
	funkce vraci ukazatel na novy vrchol zasobniku
*/

struct stackExpr* stackExprPop(struct stackExpr *stackPointer, struct tExpr *pointer)
{
	if (stackPointer == NULL) return NULL;
	struct stackExpr *oldPointer = stackPointer;
	struct stackExpr *newPointer = stackPointer->next;

	pointer->type = oldPointer->expr.type;
	if (pointer->type == E_DATATYPE)
	{
		pointer->dataType = oldPointer->expr.dataType;
		pointer->value = malloc(strlen(oldPointer->expr.value) + 1);
		strcpy(pointer->value, oldPointer->expr.value);
	}
	else if (pointer->type == E_IDENTIFICATOR)
	{
		pointer->dataType = oldPointer->expr.dataType;
		pointer->address = oldPointer->expr.address;
	}
	else if (pointer->type == E_EXPRESSION)
	{
		pointer->dataType = oldPointer->expr.dataType;
		if (pointer->dataType < 3)
		{
			pointer->value = malloc(strlen(oldPointer->expr.value) + 1);
			strcpy(pointer->value, oldPointer->expr.value);
		}
		else pointer->address = oldPointer->expr.address;
		pointer->eStack = oldPointer->expr.eStack;
	}
	free(oldPointer);

	return newPointer;
}

/*
	funkce pro ziskani nejblizsiho terminalu od vrcholu zasobniku
*/

struct tExpr getTopTerminal(struct stackExpr *exprStack)
{
	struct stackExpr *tmp;
	tmp = exprStack;
	while (tmp->expr.type > 14)
	{
		tmp = tmp->next;
	}
	return tmp->expr;
}

/*
	funkce pro vytvoreni struktury pro ulozeni polozky tExpr
	a jeji naplneni hodnotami odvozenymi z tokenu
*/

struct tExpr fillExpr(struct tToken token)
{
	struct tExpr tmp;
	tmp.type = getExprType(token.token_id);
	if (tmp.type == E_DATATYPE)
	{
		if (token.token_id == T_INT) tmp.dataType = INT;
		else if (token.token_id == T_DOUBLE) tmp.dataType = DOUBLE;
		else tmp.dataType = STRING;

		tmp.value = malloc(strlen(token.token_value) + 1);
		strcpy(tmp.value, token.token_value);
	}
	else if (tmp.type == E_IDENTIFICATOR)
	{
		tmp.dataType = 3;
		if (token.token_id == T_IDENTIFICATOR)
		{
			if (methodRecord != NULL)
			{
				if ((tmp.address = HashTable_FindLocalVariable(methodRecord->first, token.token_value)) == NULL)
				{
					tmp.address = HashTable_FindStaticVariable(hashTable, classNamespace->className, token.token_value);
				}
			}
			else tmp.address = HashTable_FindStaticVariable(hashTable, classNamespace->className, token.token_value);
		}
		else
		{
			char *tmp1 = strtok(token.token_value, ".");
			char *tmp2 = strtok(NULL, ".");
			tmp.address = HashTable_FindStaticVariable(hashTable, tmp1, tmp2);
		}
	}
	return tmp;
}

/*
	funkce zjisti, zda polozky ulozene na zasobniku tmpStack
	odpovidaji nekteremu z pravidel pro praci s vyrazy
	je-li takove pravidlo nalezeno, vraci funkce jeho index
	v opacnem pripade vraci -1
*/

int findRule(struct stackExpr *tmpStack)
{
	struct stackExpr *tmp;
	tmp = tmpStack;
	switch (tmp->expr.type)
	{
	case E_IDENTIFICATOR:
		if (tmp->next != NULL) return -1;
		else return 0;

	case E_DATATYPE:
		if (tmpStack->next != NULL) return -1;
		else return 1;

	case E_EXPRESSION:
		if (tmp->next == NULL) return -1;
		tmp = tmp->next;
		if (tmp->next == NULL) return -1;
		if (tmp->next->expr.type != E_EXPRESSION) return -1;
		else switch (tmp->expr.type)
		{
		case E_ADD:
			return 2;
		case E_SUB:
			return 3;
		case E_MUL:
			return 4;
		case E_DIV:
			return 5;
		case E_SMALLER:
			return 6;
		case E_GREATER:
			return 7;
		case E_SMALLER_EQUAL:
			return 8;
		case E_GREATER_EQUAL:
			return 9;
		case E_EQUAL:
			return 10;
		case E_NOT_EQUAL:
			return 11;
		default:
			return -1;
		}

	case E_L_BRACKET:
		if (tmp->next == NULL) return -1;
		tmp = tmp->next;
		if (tmp->expr.type != E_EXPRESSION) return -1;
		if (tmp->next == NULL) return -1;
		tmp = tmp->next;
		if (tmp->expr.type != E_R_BRACKET) return -1;
		else return 12;

	}
	return -1;
}

/*
	pomocna funkce pro vytvoreni instrukce matematicke operace
*/

void accNumber(struct tExpr operand, struct tExpr operator, struct InsList *InstrFoo)
{
	switch (operator.type)
	{
	case E_ADD:
		InsSumAccDbl(InstrFoo, atof(operand.value));
		break;
	case E_SUB:
		InsSubAccDbl(InstrFoo, atof(operand.value));
		break;
	case E_MUL:
		InsMulAccDbl(InstrFoo, atof(operand.value));
		break;
	case E_DIV:
		InsDivAccDbl(InstrFoo, atof(operand.value));
		break;
		//TODO relacni operatory ??
	}

	return;
}

/*
	pomocna funkce pro vytvoreni instrukce matematicke operace
*/

void accIdent(struct tExpr operand, struct tExpr operator, struct InsList *InstrFoo)
{
	switch (operator.type)
	{
	case E_ADD:
		InsSumAccVarDbl(InstrFoo, operand.address);
		break;
	case E_SUB:
		InsSubAccVarDbl(InstrFoo, operand.address);
		break;
	case E_MUL:
		InsMulAccVarDbl(InstrFoo, operand.address);
		break;
	case E_DIV:
		InsDivAccVarDbl(InstrFoo, operand.address);
		break;
		//TODO relacni operatory ??
	}

	return;
}

/*
	funkce pro vytvoreni instrukce/instrukci
	vstupem je zasobnik obsahujici syntakticky spravny 
	vyraz v postfuxove notaci
	ten je v cyklu postupne prochazen a vytvari se instrukce
*/


void createInstructions(struct stackExpr *exprStack)
{
	struct InsList *InstrFoo;
	if (methodRecord != NULL && strcmp(methodRecord->methodName, "run")) InstrFoo = methodRecord->Function;
	else InstrFoo = Program;

	if (exprStack->next == NULL)
	{
		if (exprStack->expr.dataType == INT || exprStack->expr.dataType == DOUBLE) InsSetDblAcc(InstrFoo, atof(exprStack->expr.value));
		else if (exprStack->expr.dataType == STRING) InsStrLoadV(InstrFoo, exprStack->expr.value);
		else
		{
			struct sVariableRecord *s = exprStack->expr.address;
			if (s->type == INT || s->type == DOUBLE) InsLoadDblVal(InstrFoo, exprStack->expr.address);
			else InsStrLoadX(InstrFoo, exprStack->expr.address);
		}
	}
	/*struct stackExpr *helpPointer = exprStack;
	while (helpPointer != NULL)
	{
		if (helpPointer->expr.type == E_EXPRESSION && helpPointer->expr.dataType < 3) printf("value: %s\n", helpPointer->expr.value);
		else if (helpPointer->expr.type == E_EXPRESSION) printf("ident addr: %lld\n", helpPointer->expr.address);
		else printf("op: %d\n", helpPointer->expr.type);
		helpPointer = helpPointer->next;
	}
	printf("\n\n");*/
	int accCnt = 0;

	//dokud jsou v zasobniku polozky, poslupne z nej odebirame a vytvarime instrukce
	while (exprStack->next != NULL)
	{
		struct stackExpr *pointer = exprStack;
		while (((pointer->next)->next)->expr.type == E_EXPRESSION || ((pointer->next)->next)->expr.type == E_ACCUMULATOR_1 || ((pointer->next)->next)->expr.type == E_ACCUMULATOR_2) pointer = pointer->next;
		struct tExpr rightOperand = pointer->expr;
		struct tExpr leftOperand = (pointer->next)->expr;
		struct tExpr operator = ((pointer->next)->next)->expr;

		struct stackExpr *accExpr;
		accExpr = malloc(sizeof(struct stackExpr));

		if ((leftOperand.dataType == INT || leftOperand.dataType == DOUBLE) && (rightOperand.dataType == INT || rightOperand.dataType == DOUBLE))
		{
			//oba oberandy jsou literaly s datovym typen bud INT nebo DOUBLE
			if ((leftOperand.type == E_ACCUMULATOR_1 && rightOperand.type == E_ACCUMULATOR_2) || 
				(leftOperand.type == E_ACCUMULATOR_2 && rightOperand.type == E_ACCUMULATOR_1))
			{
				switch (operator.type)
				{
				case E_ADD:
					InsSumAccAccDbl(InstrFoo);
					break;
				case E_SUB:
					InsSubAccAccDbl(InstrFoo);
					break;
				case E_MUL:
					InsMulAccAccDbl(InstrFoo);
					break;
				case E_DIV:
					InsDivAccAccDbl(InstrFoo);
					break;
				}
				InsSetCurrAcc(InstrFoo, 1);
				InsMulAccDbl(InstrFoo, 0);
			}
			else if (leftOperand.type == E_ACCUMULATOR_1)
			{
				InsSetCurrAcc(InstrFoo, 0);
				accNumber(rightOperand, operator, InstrFoo);
			}
			else if (leftOperand.type == E_ACCUMULATOR_2)
			{
				InsSetCurrAcc(InstrFoo, 1);
				accNumber(rightOperand, operator, InstrFoo);
			}
			else if (rightOperand.type == E_ACCUMULATOR_1)
			{
				InsSetCurrAcc(InstrFoo, 0);
				accNumber(leftOperand, operator, InstrFoo);
			}
			else if (rightOperand.type == E_ACCUMULATOR_2)
			{
				InsSetCurrAcc(InstrFoo, 1);
				accNumber(leftOperand, operator, InstrFoo);
			}
			else
			{
				switch (operator.type)
				{
				case E_ADD:
					if (accCnt == 0) InsSetCurrAcc(InstrFoo, 0);
					else InsSetCurrAcc(InstrFoo, 1);
					InsSumDblDbl(InstrFoo, atof(leftOperand.value), atof(rightOperand.value), opSUM);
					break;
				case E_SUB:
					if (accCnt == 0) InsSetCurrAcc(InstrFoo, 0);
					else InsSetCurrAcc(InstrFoo, 1);
					InsSubDblDbl(InstrFoo, atof(leftOperand.value), atof(rightOperand.value), opSUM);
					break;
				case E_MUL:
					if (accCnt == 0) InsSetCurrAcc(InstrFoo, 0);
					else InsSetCurrAcc(InstrFoo, 1);
					InsMulDblDbl(InstrFoo, atof(leftOperand.value), atof(rightOperand.value), opSUM);
					break;
				case E_DIV:
					if (accCnt == 0) InsSetCurrAcc(InstrFoo, 0);
					else InsSetCurrAcc(InstrFoo, 1);
					InsDivDblDbl(InstrFoo, atof(leftOperand.value), atof(rightOperand.value), opSUM);
					break;
				case E_SMALLER:
					if (leftOperand.dataType == INT)
					{
						if (rightOperand.dataType == INT) InsTestIntInt(InstrFoo, atoi(leftOperand.value), atoi(rightOperand.value), LT);
						else InsTestIntDbl(InstrFoo, atoi(leftOperand.value), atof(rightOperand.value), LT);
					}
					else
					{
						if (rightOperand.dataType == INT)InsTestDblInt(InstrFoo, atof(leftOperand.value), atoi(rightOperand.value), LT);
						else InsTestDblDbl(InstrFoo, atof(leftOperand.value), atof(rightOperand.value), LT);
					}					
					break;
				case E_GREATER:
					if (leftOperand.dataType == INT)
					{
						if (rightOperand.dataType == INT) InsTestIntInt(InstrFoo, atoi(leftOperand.value), atoi(rightOperand.value), GT);
						else InsTestIntDbl(InstrFoo, atoi(leftOperand.value), atof(rightOperand.value), GT);
					}
					else
					{
						if (rightOperand.dataType == INT)InsTestDblInt(InstrFoo, atof(leftOperand.value), atoi(rightOperand.value), GT);
						else InsTestDblDbl(InstrFoo, atof(leftOperand.value), atof(rightOperand.value), GT);
					}
					break;
				case E_SMALLER_EQUAL:
					if (leftOperand.dataType == INT)
					{
						if (rightOperand.dataType == INT) InsTestIntInt(InstrFoo, atoi(leftOperand.value), atoi(rightOperand.value), LEQ);
						else InsTestIntDbl(InstrFoo, atoi(leftOperand.value), atof(rightOperand.value), LEQ);
					}
					else
					{
						if (rightOperand.dataType == INT)InsTestDblInt(InstrFoo, atof(leftOperand.value), atoi(rightOperand.value), LEQ);
						else InsTestDblDbl(InstrFoo, atof(leftOperand.value), atof(rightOperand.value), LEQ);
					}
					break;
				case E_GREATER_EQUAL:
					if (leftOperand.dataType == INT)
					{
						if (rightOperand.dataType == INT) InsTestIntInt(InstrFoo, atoi(leftOperand.value), atoi(rightOperand.value), GEQ);
						else InsTestIntDbl(InstrFoo, atoi(leftOperand.value), atof(rightOperand.value), GEQ);
					}
					else
					{
						if (rightOperand.dataType == INT)InsTestDblInt(InstrFoo, atof(leftOperand.value), atoi(rightOperand.value), GEQ);
						else InsTestDblDbl(InstrFoo, atof(leftOperand.value), atof(rightOperand.value), GEQ);
					}
					break;
				case E_EQUAL:
					if (leftOperand.dataType == INT)
					{
						if (rightOperand.dataType == INT) InsTestIntInt(InstrFoo, atoi(leftOperand.value), atoi(rightOperand.value), EQ);
						else InsTestIntDbl(InstrFoo, atoi(leftOperand.value), atof(rightOperand.value), EQ);
					}
					else
					{
						if (rightOperand.dataType == INT)InsTestDblInt(InstrFoo, atof(leftOperand.value), atoi(rightOperand.value), EQ);
						else InsTestDblDbl(InstrFoo, atof(leftOperand.value), atof(rightOperand.value), EQ);
					}
					break;
				case E_NOT_EQUAL:
					if (leftOperand.dataType == INT)
					{
						if (rightOperand.dataType == INT) InsTestIntInt(InstrFoo, atoi(leftOperand.value), atoi(rightOperand.value), NEQ);
						else InsTestIntDbl(InstrFoo, atoi(leftOperand.value), atof(rightOperand.value), NEQ);
					}
					else
					{
						if (rightOperand.dataType == INT)InsTestDblInt(InstrFoo, atof(leftOperand.value), atoi(rightOperand.value), NEQ);
						else InsTestDblDbl(InstrFoo, atof(leftOperand.value), atof(rightOperand.value), NEQ);
					}
					break;
				}
			}
			accExpr->expr.dataType = DOUBLE;
		}
		else if ((leftOperand.dataType == INT || leftOperand.dataType == DOUBLE) && rightOperand.dataType == STRING)
		{
			//levy operand je literal s datovym tym=pem INT nebo DOUBLE, pravy operand je literal s datovym typem STRING
			if (leftOperand.dataType == INT) InsStrAppI(InstrFoo, atoi(leftOperand.value));
			else InsStrAppD(InstrFoo, atof(leftOperand.value));
			InsStrAppV(InstrFoo, rightOperand.value);
		}
		else if (leftOperand.dataType == STRING && (rightOperand.dataType == INT || rightOperand.dataType == DOUBLE))
		{
			//levy operand je literal s datovym typem STRING, pravy operand je literal s datovym tym=pem INT nebo DOUBLE
			InsStrAppV(InstrFoo, leftOperand.value);
			if (rightOperand.dataType == INT) InsStrAppI(InstrFoo, atoi(rightOperand.value));
			else InsStrAppD(InstrFoo, atof(rightOperand.value));
		}
		else if (leftOperand.dataType == STRING && rightOperand.dataType == STRING)
		{
			//oba operandy jsou literaly s datovym typem STRING
			InsStrAppV(InstrFoo, leftOperand.value);
			InsStrAppV(InstrFoo, rightOperand.value);
		}
		else if (leftOperand.dataType == INT || leftOperand.dataType == DOUBLE)
		{
			//levy operator je literal s datovym typem INT nebo DOUBLE, pravy operand je identifikator
			if (leftOperand.type == E_ACCUMULATOR_1)
			{
				InsSetCurrAcc(InstrFoo, 0);
				accIdent(rightOperand, operator, InstrFoo);
			}
			else if (leftOperand.type == E_ACCUMULATOR_2)
			{
				InsSetCurrAcc(InstrFoo, 1);
				accIdent(rightOperand, operator, InstrFoo);
			}
			else
			{
				switch (operator.type)
				{
				case E_ADD:
					if (accCnt == 0) InsSetCurrAcc(InstrFoo, 0);
					else InsSetCurrAcc(InstrFoo, 1);
					InsSumDblVar(InstrFoo, atof(leftOperand.value), rightOperand.address, opSUM);
					break;
				case E_SUB:
					if (accCnt == 0) InsSetCurrAcc(InstrFoo, 0);
					else InsSetCurrAcc(InstrFoo, 1);
					InsSubDblVar(InstrFoo, atof(leftOperand.value), rightOperand.address, opSUM);
					break;
				case E_MUL:
					if (accCnt == 0) InsSetCurrAcc(InstrFoo, 0);
					else InsSetCurrAcc(InstrFoo, 1);
					InsMulDblVar(InstrFoo, atof(leftOperand.value), rightOperand.address, opSUM);
					break;
				case E_DIV:
					if (accCnt == 0) InsSetCurrAcc(InstrFoo, 0);
					else InsSetCurrAcc(InstrFoo, 1);
					InsDivDblVar(InstrFoo, atof(leftOperand.value), rightOperand.address, opSUM);
					break;
				case E_SMALLER:
					if (leftOperand.dataType == INT)
						InsTestIntVar(InstrFoo, atoi(leftOperand.value), rightOperand.address, LT);
					else
						InsTestDblVar(InstrFoo, atoi(leftOperand.value), rightOperand.address, LT);
					break;
				case E_GREATER:
					if (leftOperand.dataType == INT)
						InsTestIntVar(InstrFoo, atoi(leftOperand.value), rightOperand.address, GT);
					else
						InsTestDblVar(InstrFoo, atoi(leftOperand.value), rightOperand.address, GT);
					break;
				case E_SMALLER_EQUAL:
					if (leftOperand.dataType == INT)
						InsTestIntVar(InstrFoo, atoi(leftOperand.value), rightOperand.address, LEQ);
					else
						InsTestDblVar(InstrFoo, atoi(leftOperand.value), rightOperand.address, LEQ);
					break;
				case E_GREATER_EQUAL:
					if (leftOperand.dataType == INT)
						InsTestIntVar(InstrFoo, atoi(leftOperand.value), rightOperand.address, GEQ);
					else
						InsTestDblVar(InstrFoo, atoi(leftOperand.value), rightOperand.address, GEQ);
					break;
				case E_EQUAL:
					if (leftOperand.dataType == INT)
						InsTestIntVar(InstrFoo, atoi(leftOperand.value), rightOperand.address, EQ);
					else
						InsTestDblVar(InstrFoo, atoi(leftOperand.value), rightOperand.address, EQ);
					break;
				case E_NOT_EQUAL:
					if (leftOperand.dataType == INT)
						InsTestIntVar(InstrFoo, atoi(leftOperand.value), rightOperand.address, NEQ);
					else
						InsTestDblVar(InstrFoo, atoi(leftOperand.value), rightOperand.address, NEQ);
					break;
				}
			}
			accExpr->expr.dataType = DOUBLE;

		}
		else if (leftOperand.dataType == STRING)
		{
			//levy operand je literal s datovym typem STRING, pravy operand je identifikator
			InsStrAppV(InstrFoo, leftOperand.value);
			InsStrAppX(InstrFoo, rightOperand.address);
		}
		else if (rightOperand.dataType == INT || rightOperand.dataType == DOUBLE)
		{
			//levy operand je identifikator, pravy operand je literal s datovym typem INT nebo DOUBLE
			if (rightOperand.type == E_ACCUMULATOR_1)
			{
				InsSetCurrAcc(InstrFoo, 0);
				accIdent(leftOperand, operator, InstrFoo);
			}
			else if (rightOperand.type == E_ACCUMULATOR_2)
			{
				InsSetCurrAcc(InstrFoo, 1);
				accIdent(leftOperand, operator, InstrFoo);
			}
			else
			{
				switch (operator.type)
				{
				case E_ADD:
					if (accCnt == 0) InsSetCurrAcc(InstrFoo, 0);
					else InsSetCurrAcc(InstrFoo, 1);
					InsSumVarDbl(InstrFoo, leftOperand.address, atof(rightOperand.value), opSUM);
					break;
				case E_SUB:
					if (accCnt == 0) InsSetCurrAcc(InstrFoo, 0);
					else InsSetCurrAcc(InstrFoo, 1);
					InsSubVarDbl(InstrFoo, leftOperand.address, atof(rightOperand.value), opSUM);
					break;
				case E_MUL:
					if (accCnt == 0) InsSetCurrAcc(InstrFoo, 0);
					else InsSetCurrAcc(InstrFoo, 1);
					InsMulVarDbl(InstrFoo, leftOperand.address, atof(rightOperand.value), opSUM);
					break;
				case E_DIV:
					if (accCnt == 0) InsSetCurrAcc(InstrFoo, 0);
					else InsSetCurrAcc(InstrFoo, 1);
					InsDivVarDbl(InstrFoo, leftOperand.address, atof(rightOperand.value), opSUM);
					break;
				case E_SMALLER:
					if (rightOperand.dataType == INT)
						InsTestVarInt(InstrFoo, leftOperand.address, atoi(rightOperand.value), LT);
					else
						InsTestVarDbl(InstrFoo, leftOperand.address, atof(rightOperand.value), LT);
					break;
				case E_GREATER:
					if (rightOperand.dataType == INT)
						InsTestVarInt(InstrFoo, leftOperand.address, atoi(rightOperand.value), GT);
					else
						InsTestVarDbl(InstrFoo, leftOperand.address, atof(rightOperand.value), GT);
					break;
				case E_SMALLER_EQUAL:
					if (rightOperand.dataType == INT)
						InsTestVarInt(InstrFoo, leftOperand.address, atoi(rightOperand.value), LEQ);
					else
						InsTestVarDbl(InstrFoo, leftOperand.address, atof(rightOperand.value), LEQ);
					break;
				case E_GREATER_EQUAL:
					if (rightOperand.dataType == INT)
						InsTestVarInt(InstrFoo, leftOperand.address, atoi(rightOperand.value), GEQ);
					else
						InsTestVarDbl(InstrFoo, leftOperand.address, atof(rightOperand.value), GEQ);
					break;
				case E_EQUAL:
					if (rightOperand.dataType == INT)
						InsTestVarInt(InstrFoo, leftOperand.address, atoi(rightOperand.value), EQ);
					else
						InsTestVarDbl(InstrFoo, leftOperand.address, atof(rightOperand.value), EQ);
					break;
				case E_NOT_EQUAL:
					if (rightOperand.dataType == INT)
						InsTestVarInt(InstrFoo, leftOperand.address, atoi(rightOperand.value), NEQ);
					else
						InsTestVarDbl(InstrFoo, leftOperand.address, atof(rightOperand.value), NEQ);
					break;
				}
			}
			accExpr->expr.dataType = DOUBLE;
		}
		else  if (rightOperand.dataType == STRING)
		{
			//levy operand je identifikator, pravy operand je literal s datovym typem STRING
			InsStrAppX(InstrFoo, leftOperand.address);
			InsStrAppV(InstrFoo, rightOperand.value);
			
		}
		else
		{
			//oba operandy jsou identifikatory
			struct sVariableRecord *s = leftOperand.address;
			int leftDataType = s->type;
			s = rightOperand.address;
			int rightDataType = s->type;
			if ((leftDataType == INT || leftDataType == DOUBLE) && (rightDataType == INT || rightDataType == DOUBLE))
			{
				switch (operator.type)
				{
				case E_ADD:
					InsSumVarVar(InstrFoo, leftOperand.address, rightOperand.address, opSUM);
					break;
				case E_SUB:
					InsSubVarVar(InstrFoo, leftOperand.address, rightOperand.address, opSUM);
					break;
				case E_MUL:
					InsMulVarVar(InstrFoo, leftOperand.address, rightOperand.address, opSUM);
					break;
				case E_DIV:
					InsDivVarVar(InstrFoo, leftOperand.address, rightOperand.address, opSUM);
					break;
				case E_SMALLER:
					InsTestVarVar(InstrFoo, leftOperand.address, rightOperand.address, LT);
					break;
				case E_GREATER:
					InsTestVarVar(InstrFoo, leftOperand.address, rightOperand.address, GT);
					break;
				case E_SMALLER_EQUAL:
					InsTestVarVar(InstrFoo, leftOperand.address, rightOperand.address, LEQ);
					break;
				case E_GREATER_EQUAL:
					InsTestVarVar(InstrFoo, leftOperand.address, rightOperand.address, GEQ);
					break;
				case E_EQUAL:
					InsTestVarVar(InstrFoo, leftOperand.address, rightOperand.address, EQ);
					break;
				case E_NOT_EQUAL:
					InsTestVarVar(InstrFoo, leftOperand.address, rightOperand.address, NEQ);
					break;
				}
			}

		}
		
		if (leftOperand.type == E_ACCUMULATOR_1 || leftOperand.type == E_ACCUMULATOR_2)(accExpr->expr).type = leftOperand.type;
		else if (rightOperand.type == E_ACCUMULATOR_1 || rightOperand.type == E_ACCUMULATOR_2) (accExpr->expr).type = rightOperand.type;
		else if (accCnt == 0) { (accExpr->expr).type = E_ACCUMULATOR_1; accCnt++; }
		else (accExpr->expr).type = E_ACCUMULATOR_2;

		//odstranime operandy a operator ze zasobniku
		if (exprStack == pointer)
		{
			exprStack = accExpr;
			accExpr->next = ((pointer->next)->next)->next;
		}
		else
		{
			struct stackExpr *helpPointer = exprStack;
			while (helpPointer->next != pointer) helpPointer = helpPointer->next;
			helpPointer->next = accExpr;
			accExpr->next = ((pointer->next)->next)->next;
		}
		/*helpPointer = exprStack;
		while (helpPointer != NULL)
		{
			if (helpPointer->expr.type == E_EXPRESSION && helpPointer->expr.dataType < 3) printf("value: %s\n", helpPointer->expr.value);
			else if (helpPointer->expr.type == E_EXPRESSION) printf("ident addr: %lld\n", helpPointer->expr.address);
			else if (helpPointer->expr.type == E_ACCUMULATOR_1 || helpPointer->expr.type == E_ACCUMULATOR_2) printf("acc\n");
			else printf("op: %d\n", helpPointer->expr.type);
			helpPointer = helpPointer->next;
		}
		printf("\n\n");*/
	}

}

/*
	funkce pro nacteni vyrazu a zkontrolovani jeho syntakticke spravnosti pomoci
	precedencni tabulky 
	syntakticky spravny vyraz je ulozen v postfixove notaci a predan funkci
	createInstructions, ktera vytvori instrukce jednotlivych operaci
*/


int Rule_expression(FILE *f, struct tToken *tt)
{
#ifdef DEBUG == 1
	FILE* log = fopen("log.txt", "a");
	fprintf(log, "Called Rule_expression\n");
	fclose(log);
#endif

	struct stackExpr *exprStack = NULL;
	struct tExpr tmp;
	tmp.type = E_SEMICOLON;
	exprStack = stackExprPush(exprStack, &tmp);

	struct tExpr stackTop;
	struct tExpr input;

	SemanStack = stackPush(SemanStack, tt);

	input = fillExpr(*tt);
	if (input.type == -1) return 0;
	stackTop = getTopTerminal(exprStack);

	do {
		int a = stackTop.type;
		if (a == E_DATATYPE) a = E_IDENTIFICATOR;
		int b = input.type;
		if (b == E_DATATYPE) b = E_IDENTIFICATOR;
		char c = precedence_table[a][b];
		if (c == '=')
		{
			exprStack = stackExprPush(exprStack, &input);
			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK) { LexErrorFlag = true;  return 0; }
			SemanStack = stackPush(SemanStack, tt);
			input = fillExpr(*tt);
			if (input.type == -1) return 0;
			stackTop = getTopTerminal(exprStack);
		}
		else if (c == '<')
		{
			struct stackExpr *tmpStack = NULL;
			while (exprStack->expr.type > 14)
			{
				exprStack = stackExprPop(exprStack, &tmp);
				tmpStack = stackExprPush(tmpStack, &tmp);
			}
			struct tExpr tmpTmp;
			tmpTmp.type = E_LESS;
			exprStack = stackExprPush(exprStack, &tmpTmp);
			while (tmpStack != NULL)
			{
				tmpStack = stackExprPop(tmpStack, &tmp);
				exprStack = stackExprPush(exprStack, &tmp);
			}

			exprStack = stackExprPush(exprStack, &input);
			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK) { LexErrorFlag = true;  return 0; }
			SemanStack = stackPush(SemanStack, tt);
			input = fillExpr(*tt);
			if (input.type == -1) return 0;
			stackTop = getTopTerminal(exprStack);
		}
		else if (c == '>')
		{
			struct stackExpr *tmpStack = NULL;
			int cnt = 0;
			while (exprStack->expr.type != E_LESS && exprStack != NULL)
			{
				exprStack = stackExprPop(exprStack, &tmp);
				tmpStack = stackExprPush(tmpStack, &tmp);
				cnt++;
			}
			if (cnt > 3) return 0;
			int rule = findRule(tmpStack);
			if (rule == -1) return 0;
			exprStack = stackExprPop(exprStack, &tmp);
			tmp.type = E_EXPRESSION;
			tmp.eStack = NULL;
			if (rule < 2)
			{
				tmp.dataType = tmpStack->expr.dataType;
				if (tmp.type == E_DATATYPE)
				{
					tmp.value = malloc(strlen(tmpStack->expr.value) + 1);
					strcpy(tmp.value, tmpStack->expr.value);
				}
				else
				{
					tmp.address = tmpStack->expr.address;
				}
			}
			else if (rule < 12)
			{
				

				if (((tmpStack->next)->next)->expr.eStack != NULL)
				{
					struct stackExpr *tmpTmpStack = NULL;
					while (((tmpStack->next)->next)->expr.eStack != NULL)
					{
						struct tExpr tmpTmp;
						((tmpStack->next)->next)->expr.eStack = stackExprPop(((tmpStack->next)->next)->expr.eStack, &tmpTmp);
						tmpTmpStack = stackExprPush(tmpTmpStack, &tmpTmp);
					}
					while (tmpTmpStack != NULL)
					{
						struct tExpr tmpTmp;
						tmpTmpStack = stackExprPop(tmpTmpStack, &tmpTmp);
						tmp.eStack = stackExprPush(tmp.eStack, &tmpTmp);
					}
				}
				else tmp.eStack = stackExprPush(tmp.eStack, (tmpStack->next)->next);

				if (tmpStack->expr.eStack != NULL)
				{
					struct stackExpr *tmpTmpStack = NULL;
					while (tmpStack->expr.eStack != NULL)
					{
						struct tExpr tmpTmp;
						tmpStack->expr.eStack = stackExprPop(tmpStack->expr.eStack, &tmpTmp);
						tmpTmpStack = stackExprPush(tmpTmpStack, &tmpTmp);
					}
					while (tmpTmpStack != NULL)
					{
						struct tExpr tmpTmp;
						tmpTmpStack = stackExprPop(tmpTmpStack, &tmpTmp);
						tmp.eStack = stackExprPush(tmp.eStack, &tmpTmp);
					}
				}
				else tmp.eStack = stackExprPush(tmp.eStack, tmpStack);
				tmp.eStack = stackExprPush(tmp.eStack, tmpStack->next);
			}
			else
			{
				if ((tmpStack->next)->expr.eStack != NULL)
				{
					struct stackExpr *tmpTmpStack = NULL;
					while ((tmpStack->next)->expr.eStack != NULL)
					{
						struct tExpr tmpTmp;
						(tmpStack->next)->expr.eStack = stackExprPop((tmpStack->next)->expr.eStack, &tmpTmp);
						tmpTmpStack = stackExprPush(tmpTmpStack, &tmpTmp);
					}
					while (tmpTmpStack != NULL)
					{
						struct tExpr tmpTmp;
						tmpTmpStack = stackExprPop(tmpTmpStack, &tmpTmp);
						tmp.eStack = stackExprPush(tmp.eStack, &tmpTmp);
					}
				}
				else tmp.eStack = stackExprPush(tmp.eStack, (tmpStack->next));
			}
			exprStack = stackExprPush(exprStack, &tmp);
			if (tmp.type == E_DATATYPE) free(tmp.value);
			stackTop = getTopTerminal(exprStack);

		}
		else
		{
			if (stackTop.type == E_IDENTIFICATOR && input.type == E_L_BRACKET)
			{
				//nasla se zavorka
				struct tExpr tmp;
				exprStack = stackExprPop(exprStack, &tmp);
				if ((getTopTerminal(exprStack)).type != E_SEMICOLON) return 0;
				else exprStack = stackExprPush(exprStack, &tmp);
				free(tt->token_value);
				tt->token_value = (char*)malloc(1);
				tt->token_value[0] = 0;
				if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
				{
					LexErrorFlag = true;
					return false;
				}
				if (Rule_call_parameters(f, tt))
				{
					SemanStack = stackPush(SemanStack, tt);
					if (tt->token_id == 18)
					{
						if ((LexErrorNum = CheckCall(SemanStack)) != E_OK)
						{
							LexErrorFlag = true;
							return false;
						}
						struct stack * p = SemanStack;
						while ((p)->next->tt.token_id != 15) p = p->next;

						char * holder = malloc(strlen(p->tt.token_value) + 1);
						strcpy(holder, p->tt.token_value);
						char * CLS = strtok(holder, ".");
						char * varName = strtok(NULL, ".");

						if (varName == NULL)
							InsCallFunction(Program, HashTable_FindMethod(hashTable, classNamespace->className, CLS), classNamespace->className);
						else
							InsCallFunction(Program, HashTable_FindMethod(hashTable, CLS, varName), CLS);

						free(holder);

						//nasla se prava zavorka
						free(tt->token_value);
						tt->token_value = (char*)malloc(1);
						tt->token_value[0] = 0;
						if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK)
						{
							LexErrorFlag = true;
							return false;
						}
						//SemanStack = stackPush(SemanStack, tt);

						return true;

					}
					else return false;

				}
				else return false;
			}
			else return 0;
		}
	} while ((stackTop.type != E_SEMICOLON) || (input.type != E_SEMICOLON));

	exprStack = stackExprPop(exprStack, &tmp);
	struct stackExpr *reversedStack = NULL;
	if (tmp.eStack == NULL)
	{
		reversedStack = stackExprPush(reversedStack, &tmp);
	}
	else while (tmp.eStack != NULL)
	{
		struct tExpr tmpExpr;
		tmp.eStack = stackExprPop(tmp.eStack, &tmpExpr);
		reversedStack = stackExprPush(reversedStack, &tmpExpr);
	}
	
	createInstructions(reversedStack);

	return 1;
}

/*
	funkce pro nacteni vyrazu v podmince (if nebo while) a zkontrolovani jeho syntakticke spravnosti 
	pomoci precedencni tabulky 
	syntakticky spravny vyraz je ulozen v postfixove notaci a predan funkci
	createInstructions, ktera vytvori instrukce jednotlivych operaci
*/

int Rule_expression_brace(FILE *f, struct tToken *tt)
{
#ifdef DEBUG == 1
	FILE* log = fopen("log.txt", "a");
	fprintf(log, "Called Rule_expression_brace\n");
	fclose(log);
#endif

	struct stackExpr *exprStack = NULL;
	struct tExpr tmp;
	tmp.type = E_SEMICOLON;
	exprStack = stackExprPush(exprStack, &tmp);

	struct tExpr stackTop;
	struct tExpr input;

	SemanStack = stackPush(SemanStack, tt);

	input = fillExpr(*tt);
	if (input.type == -1) return 0;
	stackTop = getTopTerminal(exprStack);

	do {

		int a = stackTop.type;
		if (a == E_DATATYPE) a = E_IDENTIFICATOR;
		int b = input.type;
		if (b == E_DATATYPE) b = E_IDENTIFICATOR;
		char c = precedence_table[a][b];
		if (c == '=')
		{
			exprStack = stackExprPush(exprStack, &input);
			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK) { LexErrorFlag = true;  return 0; }
			SemanStack = stackPush(SemanStack, tt);
			input = fillExpr(*tt);
			if (input.type == -1) return 0;
			stackTop = getTopTerminal(exprStack);
		}
		else if (c == '<')
		{
			struct stackExpr *tmpStack = NULL;
			while (exprStack->expr.type > 14)
			{
				exprStack = stackExprPop(exprStack, &tmp);
				tmpStack = stackExprPush(tmpStack, &tmp);
			}
			struct tExpr tmpTmp;
			tmpTmp.type = E_LESS;
			exprStack = stackExprPush(exprStack, &tmpTmp);
			while (tmpStack != NULL)
			{
				tmpStack = stackExprPop(tmpStack, &tmp);
				exprStack = stackExprPush(exprStack, &tmp);
			}

			exprStack = stackExprPush(exprStack, &input);
			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK) { LexErrorFlag = true;  return 0; }
			SemanStack = stackPush(SemanStack, tt);
			input = fillExpr(*tt);
			if (input.type == -1) return 0;
			stackTop = getTopTerminal(exprStack);
		}
		else if (c == '>')
		{
			struct stackExpr *tmpStack = NULL;
			int cnt = 0;
			while (exprStack->expr.type != E_LESS && exprStack != NULL)
			{
				exprStack = stackExprPop(exprStack, &tmp);
				tmpStack = stackExprPush(tmpStack, &tmp);
				cnt++;
			}
			if (cnt > 3) return 0;
			int rule = findRule(tmpStack);
			if (rule == -1) return 0;
			exprStack = stackExprPop(exprStack, &tmp);
			tmp.type = E_EXPRESSION;
			tmp.eStack = NULL;
			if (rule < 2)
			{
				tmp.dataType = tmpStack->expr.dataType;
				if (tmp.type == E_DATATYPE)
				{
					tmp.value = malloc(strlen(tmpStack->expr.value) + 1);
					strcpy(tmp.value, tmpStack->expr.value);
				}
				else
				{
					tmp.address = tmpStack->expr.address;
				}
			}
			else if (rule < 12)
			{
				if (((tmpStack->next)->next)->expr.eStack != NULL)
				{
					struct stackExpr *tmpTmpStack = NULL;
					while (((tmpStack->next)->next)->expr.eStack != NULL)
					{
						struct tExpr tmpTmp;
						((tmpStack->next)->next)->expr.eStack = stackExprPop(((tmpStack->next)->next)->expr.eStack, &tmpTmp);
						tmpTmpStack = stackExprPush(tmpTmpStack, &tmpTmp);
					}
					while (tmpTmpStack != NULL)
					{
						struct tExpr tmpTmp;
						tmpTmpStack = stackExprPop(tmpTmpStack, &tmpTmp);
						tmp.eStack = stackExprPush(tmp.eStack, &tmpTmp);
					}
				}
				else tmp.eStack = stackExprPush(tmp.eStack, (tmpStack->next)->next);

				if (tmpStack->expr.eStack != NULL)
				{
					struct stackExpr *tmpTmpStack = NULL;
					while (tmpStack->expr.eStack != NULL)
					{
						struct tExpr tmpTmp;
						tmpStack->expr.eStack = stackExprPop(tmpStack->expr.eStack, &tmpTmp);
						tmpTmpStack = stackExprPush(tmpTmpStack, &tmpTmp);
					}
					while (tmpTmpStack != NULL)
					{
						struct tExpr tmpTmp;
						tmpTmpStack = stackExprPop(tmpTmpStack, &tmpTmp);
						tmp.eStack = stackExprPush(tmp.eStack, &tmpTmp);
					}
				}
				else tmp.eStack = stackExprPush(tmp.eStack, tmpStack);


				tmp.eStack = stackExprPush(tmp.eStack, tmpStack->next);
			}
			else
			{
				if ((tmpStack->next)->expr.eStack != NULL)
				{
					struct stackExpr *tmpTmpStack = NULL;
					while ((tmpStack->next)->expr.eStack != NULL)
					{
						struct tExpr tmpTmp;
						(tmpStack->next)->expr.eStack = stackExprPop((tmpStack->next)->expr.eStack, &tmpTmp);
						tmpTmpStack = stackExprPush(tmpTmpStack, &tmpTmp);
					}
					while (tmpTmpStack != NULL)
					{
						struct tExpr tmpTmp;
						tmpTmpStack = stackExprPop(tmpTmpStack, &tmpTmp);
						tmp.eStack = stackExprPush(tmp.eStack, &tmpTmp);
					}
				}
				else tmp.eStack = stackExprPush(tmp.eStack, (tmpStack->next));
			}
			exprStack = stackExprPush(exprStack, &tmp);
			if (tmp.type == E_DATATYPE) free(tmp.value);
			stackTop = getTopTerminal(exprStack);

		}
		else
		{
			return 0;
		}
	} while ((stackTop.type != E_SEMICOLON) || (input.type != E_R_BRACKET));

	exprStack = stackExprPop(exprStack, &tmp);
	struct stackExpr *reversedStack = NULL;
	if (tmp.eStack == NULL)
	{
		reversedStack = stackExprPush(reversedStack, &tmp);
	}
	else while (tmp.eStack != NULL)
	{
		struct tExpr tmpExpr;
		tmp.eStack = stackExprPop(tmp.eStack, &tmpExpr);
		reversedStack = stackExprPush(reversedStack, &tmpExpr);
	}

	createInstructions(reversedStack);

	return 1;

}

/*
	funkce pro nacteni vyrazu v prirazovaneho do staticke promenne pri jeji definici 
	a zkontrolovani jeho syntakticke spravnosti pomoci precedencni tabulky 
	syntakticky spravny vyraz je ulozen v postfixove notaci a predan funkci
	createInstructions, ktera vytvori instrukce jednotlivych operaci
*/

int Rule_class_expression(FILE *f, struct tToken *tt)
{
#ifdef DEBUG == 1
	FILE* log = fopen("log.txt", "a");
	fprintf(log, "Called Rule_class_expression\n");
	fclose(log);
#endif

	struct stackExpr *exprStack = NULL;
	struct tExpr tmp;
	tmp.type = E_SEMICOLON;
	exprStack = stackExprPush(exprStack, &tmp);

	struct tExpr stackTop;
	struct tExpr input;

	SemanStack = stackPush(SemanStack, tt);

	input = fillExpr(*tt);
	if (input.type == -1) return 0;
	stackTop = getTopTerminal(exprStack);

	do {
		int a = stackTop.type;
		if (a == E_DATATYPE) a = E_IDENTIFICATOR;
		int b = input.type;
		if (b == E_DATATYPE) b = E_IDENTIFICATOR;
		char c = precedence_table[a][b];
		if (c == '=')
		{
			exprStack = stackExprPush(exprStack, &input);
			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK) { LexErrorFlag = true;  return 0; }
			SemanStack = stackPush(SemanStack, tt);
			input = fillExpr(*tt);
			if (input.type == -1) return 0;
			stackTop = getTopTerminal(exprStack);
		}
		else if (c == '<')
		{
			struct stackExpr *tmpStack = NULL;
			while (exprStack->expr.type > 14)
			{
				exprStack = stackExprPop(exprStack, &tmp);
				tmpStack = stackExprPush(tmpStack, &tmp);
			}
			struct tExpr tmpTmp;
			tmpTmp.type = E_LESS;
			exprStack = stackExprPush(exprStack, &tmpTmp);
			while (tmpStack != NULL)
			{
				tmpStack = stackExprPop(tmpStack, &tmp);
				exprStack = stackExprPush(exprStack, &tmp);
			}

			exprStack = stackExprPush(exprStack, &input);
			free(tt->token_value);
			tt->token_value = (char*)malloc(1);
			tt->token_value[0] = 0;
			if ((LexErrorNum = lexical_analyzator(f, tt)) != E_OK) { LexErrorFlag = true;  return 0; }
			SemanStack = stackPush(SemanStack, tt);
			input = fillExpr(*tt);
			if (input.type == -1) return 0;
			stackTop = getTopTerminal(exprStack);
		}
		else if (c == '>')
		{
			struct stackExpr *tmpStack = NULL;
			int cnt = 0;
			while (exprStack->expr.type != E_LESS && exprStack != NULL)
			{
				exprStack = stackExprPop(exprStack, &tmp);
				tmpStack = stackExprPush(tmpStack, &tmp);
				cnt++;
			}
			if (cnt > 3) return 0;
			int rule = findRule(tmpStack);
			if (rule == -1) return 0;
			exprStack = stackExprPop(exprStack, &tmp);
			tmp.type = E_EXPRESSION;
			tmp.eStack = NULL;
			if (rule < 2)
			{
				tmp.dataType = tmpStack->expr.dataType;
				if (tmp.type == E_DATATYPE)
				{
					tmp.value = malloc(strlen(tmpStack->expr.value) + 1);
					strcpy(tmp.value, tmpStack->expr.value);
				}
				else
				{
					tmp.address = tmpStack->expr.address;
				}
			}
			else if (rule < 12)
			{
				if (((tmpStack->next)->next)->expr.eStack != NULL)
				{
					struct stackExpr *tmpTmpStack = NULL;
					while (((tmpStack->next)->next)->expr.eStack != NULL)
					{
						struct tExpr tmpTmp;
						((tmpStack->next)->next)->expr.eStack = stackExprPop(((tmpStack->next)->next)->expr.eStack, &tmpTmp);
						tmpTmpStack = stackExprPush(tmpTmpStack, &tmpTmp);
					}
					while (tmpTmpStack != NULL)
					{
						struct tExpr tmpTmp;
						tmpTmpStack = stackExprPop(tmpTmpStack, &tmpTmp);
						tmp.eStack = stackExprPush(tmp.eStack, &tmpTmp);
					}
				}
				else tmp.eStack = stackExprPush(tmp.eStack, (tmpStack->next)->next);

				if (tmpStack->expr.eStack != NULL)
				{
					struct stackExpr *tmpTmpStack = NULL;
					while (tmpStack->expr.eStack != NULL)
					{
						struct tExpr tmpTmp;
						tmpStack->expr.eStack = stackExprPop(tmpStack->expr.eStack, &tmpTmp);
						tmpTmpStack = stackExprPush(tmpTmpStack, &tmpTmp);
					}
					while (tmpTmpStack != NULL)
					{
						struct tExpr tmpTmp;
						tmpTmpStack = stackExprPop(tmpTmpStack, &tmpTmp);
						tmp.eStack = stackExprPush(tmp.eStack, &tmpTmp);
					}
				}
				else tmp.eStack = stackExprPush(tmp.eStack, tmpStack);


				tmp.eStack = stackExprPush(tmp.eStack, tmpStack->next);
			}
			else
			{
				if ((tmpStack->next)->expr.eStack != NULL)
				{
					struct stackExpr *tmpTmpStack = NULL;
					while ((tmpStack->next)->expr.eStack != NULL)
					{
						struct tExpr tmpTmp;
						(tmpStack->next)->expr.eStack = stackExprPop((tmpStack->next)->expr.eStack, &tmpTmp);
						tmpTmpStack = stackExprPush(tmpTmpStack, &tmpTmp);
					}
					while (tmpTmpStack != NULL)
					{
						struct tExpr tmpTmp;
						tmpTmpStack = stackExprPop(tmpTmpStack, &tmpTmp);
						tmp.eStack = stackExprPush(tmp.eStack, &tmpTmp);
					}
				}
				else tmp.eStack = stackExprPush(tmp.eStack, (tmpStack->next));
			}
			exprStack = stackExprPush(exprStack, &tmp);
			if (tmp.type == E_DATATYPE) free(tmp.value);
			stackTop = getTopTerminal(exprStack);

		}
		else
		{
			return 0;
		}
	} while ((stackTop.type != E_SEMICOLON) || (input.type != E_SEMICOLON));

	exprStack = stackExprPop(exprStack, &tmp);
	struct stackExpr *reversedStack = NULL;
	if (tmp.eStack == NULL)
	{
		reversedStack = stackExprPush(reversedStack, &tmp);
	}
	else while (tmp.eStack != NULL)
	{
		struct tExpr tmpExpr;
		tmp.eStack = stackExprPop(tmp.eStack, &tmpExpr);
		reversedStack = stackExprPush(reversedStack, &tmpExpr);
	}

	createInstructions(reversedStack);

	return 1;
}