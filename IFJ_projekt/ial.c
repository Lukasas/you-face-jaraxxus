#include "ial.h"
#include "instruction.h"
#include "semantics_analyzator.h"
/**
* USAGE
* ALL FUNCTIONS HERE ARE PREFIXED WITH "HashTable_"
* ALL FUNCTIONS RETURNS NULL IF THEY FAIL!!!!
* DO NOT LOSE THE POINTER RETURNED BY HashTable_Init()!
* FREE ONLY WITH HashTable_Free()!!!!
*
*
* 1. HashTable_Init() returns a new empty hash table eg. SHashTable*
* 2. HashTable_AddClass( SHashTable*, ... ) adds a new class to the hash table given in arguments and returns its pointer in it.
*		eg. SClassNamespace*
* 3. HashTable_AddVariableToClass( SClassNamespace*, ... ) adds the global (static) variable
*		to the ClassNamespace given in arguments
* 4. HashTable_AddMethodToClass( SClassNamespace*, ... )  adds a class method to the ClassNamespace given in arguments
* 5. HashTable_FindMethod() and HashTable_FindStaticVariable() for searching in HashTable
**/


/**
DO NOT USE, different hash function from wiki. Maybe faster. Do not know.

unsigned int _jenkins_one_at_a_time_hash(char *key, size_t len)
{
	unsigned int hash, i;
	for (hash = i = 0; i < strlen(key); ++i)
	{
		hash += key[i];
		hash += (hash << 10);
		hash ^= (hash >> 6);
	}
	hash += (hash << 3);
	hash ^= (hash >> 11);
	hash += (hash << 15);
	return hash%len;
}**/

// Basic hashing function from IJC
unsigned HashTable_HashFunction(const char *str, unsigned htab_size)
{
	unsigned int h = 0;
	const unsigned char *p;
	for (p = (const unsigned char*)str; *p != '\0'; p++)
		h = 65599 * h + *p;
	return h % htab_size;
}

/* 
* @brief Default initialization of Hash tables,  malloced pointers inside! 
* @ret SHashTable* malloced symbols table
*/
SHashTable* HashTable_Init()
{
	SHashTable *symbolsTable = (SHashTable*)malloc(sizeof(SHashTable));
	if (symbolsTable == NULL)
	{
		fprintf(stderr, "Malloc error.\n");
		return NULL;
	}

	symbolsTable->tableSize = HASH_TABLE_SIZE;				// default size
	//symbolsTable->tableData = (ClassNamespace*)malloc(sizeof(ClassNamespace) * 10);

	for (unsigned l = 0; l < HASH_TABLE_SIZE; l++)
		symbolsTable->tableData[l] = NULL;


	SClassNamespace *ifjClass = HashTable_AddClass(symbolsTable, "ifj16");
	HashTable_AddMethodToClass(ifjClass, "length", "is");
	HashTable_AddMethodToClass(ifjClass, "substr", "ssii");
	HashTable_AddMethodToClass(ifjClass, "compare", "iss");
	HashTable_AddMethodToClass(ifjClass, "find", "iss");
	HashTable_AddMethodToClass(ifjClass, "sort", "ss");
	HashTable_AddMethodToClass(ifjClass, "readInt", "i");
	HashTable_AddMethodToClass(ifjClass, "readDouble", "d");
	HashTable_AddMethodToClass(ifjClass, "readString", "s");
	HashTable_AddMethodToClass(ifjClass, "print", "vs");

	return symbolsTable;
}

SClassNamespace *HashTable_FindClass(SHashTable *hashTable, const char* className)
{
	if (hashTable == NULL)
	{
		return NULL;
	}
	SClassNamespace *tmp;
	for (int i = 0; i < HASH_TABLE_SIZE; i++)
	{
		tmp = hashTable->tableData[i];
		while (tmp != NULL)
		{
			if (strcmp(tmp->className, className) == 0)
				return tmp;
			tmp = tmp->next;
		}
	}
	return NULL;

}

/*
* @brief Function to find a Method record in hashTable
* @param SHashTable* hashTable - pointer to the main SHashTable* returned by HashTable_Init()
* @param const char* className - name of a class where the method is declared
* @param const char* methodName - name of a method to find
* @ret SMethodRecord* - Pointer to the record of method inside SHashTable or NULL
*/
SMethodRecord* HashTable_FindMethod(SHashTable* hashTable, const char* className, const char* methodName)
{
	if (hashTable == NULL)
	{
		fprintf(stderr, "NULL HashTable pointer.\n");
		return NULL;
	}
	unsigned classIndex		= HashTable_HashFunction(className, hashTable->tableSize);
	if (HashTable_FindClass(hashTable, className) == NULL)
		return NULL;
	unsigned methodIndex	= HashTable_HashFunction(methodName, hashTable->tableData[classIndex]->classMethodsTableSize);
	SClassNamespace* cls	= hashTable->tableData[classIndex];
	//SMethodRecord* temp		= hashTable->tableData[classIndex]->classMethods[methodIndex];
	if (cls == NULL) return NULL;
	while(cls != NULL) 
	{
		SMethodRecord *temp = cls->classMethods[methodIndex];
		while (strcmp(cls->className, className) == 0)
		{
			
			if (temp == NULL)
				return NULL;
			if (strcmp(temp->methodName, methodName) == 0)
				return temp;
			else if (temp->next == NULL)
				return NULL;
			else
				temp = temp->next;
		}
		cls = cls->next;
	}
}

/*
* @brief Function to find a Method record in classNamespace
* @param SClassnamespace* classNamespace - pointer to the ClassNamespace where the we want to find method 
* @param const char* methodName - name of a looked method
* @ret SMethodRecord* - Pointer to the record of method inside SClassNamespace or NULL
*/
SMethodRecord* HashTable_FindMethod_ByPointer(SClassNamespace* classNamespace, const char* methodName)
{
	if (classNamespace == NULL)
	{
		fprintf(stderr, "HashTable_FindMethod recieved a null pointer.\n");
		return NULL;
	}

	unsigned index = HashTable_HashFunction(methodName, classNamespace->classMethodsTableSize);
	SMethodRecord* methodRecord = classNamespace->classMethods[index];

	while (true)
	{
		if (strcmp(methodName, methodRecord->methodName) == 0)
			return methodRecord;
		else if (methodRecord->next == NULL)
			return NULL;
		else
			methodRecord = methodRecord->next;
	}
}

/*
* @brief Function to find a GLOBAL variable in whole HashTable
* @param SHashTable* hashTable - pointer to the main SHashTable* returned by HashTable_Init()
* @param const char* className - name of a class where the variable should be
* @param const char* variable - name of a variable to find
* @ret SVariableRecord* - Pointer to the record of variable inside SHashTable or NULL
*/
SVariableRecord* HashTable_FindStaticVariable(SHashTable* hashTable, const char* className, const char* variable)
{
	if (hashTable == NULL)
	{
		fprintf(stderr, "NULL HashTable pointer.\n");
		return NULL;
	}
	unsigned classIndex = HashTable_HashFunction(className, hashTable->tableSize);
	if (HashTable_FindClass(hashTable, className) == NULL)
		return NULL;
	unsigned variableIndex = HashTable_HashFunction(variable, hashTable->tableData[classIndex]->classVariablesTableSize);
	SClassNamespace* temp = hashTable->tableData[classIndex];// ->classVariables[variableIndex];
	SVariableRecord* v = NULL;

	if (temp == NULL)
		return NULL;
	while (temp != NULL)
	{
		if (strcmp(temp->className, className) == 0)
		{
			v = temp->classVariables[variableIndex];
			if (v == NULL)
				return NULL;
			while (true)
			{
				if (strcmp(v->varName, variable) == 0)
					return v;
				else if (v->next == NULL)
					return NULL;
				else
					v = v->next;
			}
		}
		temp = temp->next;
	}
}

/*
* @brief Function to find a variable record in ClassNamespace HashTable
* @param SClassNamespace* classNamespace - classNamespace pointer to the ClassNamespace where we want to find it
* @param const char* varName - name of looked variable
* @ret	 SVariableRecord* - Pointer to the record of variable inside SClassNamespace or NULL
*/
SVariableRecord* HashTable_FindStaticVariable_ByPointer(SClassNamespace* classNamespace, const char* varName)
{
	if (classNamespace == NULL)
	{
		fprintf(stderr, "HashTable_FindStaticVariableFromClass recieved a null pointer.\n");
		return NULL;
	}
	
	unsigned index = HashTable_HashFunction(varName, classNamespace->classVariablesTableSize);
	SVariableRecord* varRecord = classNamespace->classVariables[index];
	if (varRecord == NULL)
		return NULL;
	while (varRecord != NULL)
	{
			if (strcmp(varName, varRecord->varName) == 0)
				return varRecord;
			else
				varRecord = varRecord->next;
	}
	return NULL;
}

/*
* @brief Function to add a SClassNamespace record in a SHashTable
* @param SHashTable* hashTable - SHashTable, returned by HashTable_Init()
* @param const char* className - 
*/
SClassNamespace* HashTable_AddClass(SHashTable* hashTable, const char* className)
{
	if (hashTable == NULL)
	{
		fprintf(stderr, "NULL HashTable pointer.\n");
		return NULL;
	}

	SClassNamespace* record = (SClassNamespace*) malloc(sizeof(SClassNamespace));
	if (record == NULL)
	{
		fprintf(stderr, "Malloc error.\n");
		return NULL;
	}

	char * holder = malloc(strlen(className) + 1);
	strcpy(holder, className);
	record->className				= holder;
	record->classMethodsTableSize	= HASH_TABLE_SIZE;
	record->classVariablesTableSize = HASH_TABLE_SIZE;
	record->next					= NULL;

	for (unsigned i = 0; i < HASH_TABLE_SIZE; i++)
	{
		record->classMethods[i] = NULL;
		record->classVariables[i] = NULL;
	}

	unsigned index	= HashTable_HashFunction(className, hashTable->tableSize);

	SClassNamespace* temporary = hashTable->tableData[index];
	SClassNamespace* temporary2;
	if (temporary != NULL)
	{
		while (temporary != NULL)								// look for the last record
		{
			if (strcmp(className, temporary->className) == 0)		// already present in hashtable
			{
				fprintf(stderr, "Namespace already exists.\n");
				free(record);
				return NULL;
			}
			temporary2 = temporary;
			temporary = temporary->next;
		}
		// not present
		temporary2->next = record;
		return temporary2->next;
	}
	hashTable->tableData[index] = record;
	return hashTable->tableData[index];

}

SVariableRecord* HashTable_AddVariableToClass(SClassNamespace* classNamespace, char* variableName, 
												EDataTypes dataType, char* string, bool defined)
{
	if (classNamespace == NULL)
	{
		fprintf(stderr, "HashTable_AddVariableToClass() recieved a NULL pointer. Unacceptable!\n");
		return NULL;
	}
	unsigned index = HashTable_HashFunction(variableName, classNamespace->classVariablesTableSize);

	SVariableRecord* record = (SVariableRecord*)malloc(sizeof(SVariableRecord));
	if (record == NULL)
	{
		fprintf(stderr, "Malloc error.\n");
		return NULL;
	}
	char * holder = malloc(strlen(variableName) + 1);
	strcpy(holder, variableName);
	record->varName		= holder;
	record->next		= NULL;
	record->type		= dataType;
	record->defined		= defined;
	record->declared	= true;
	switch (dataType)
	{
	case INT:
		record->value = (int*)malloc(sizeof(int));
		break;
	case DOUBLE:
		record->value = (int*)malloc(sizeof(double));
		break;
	case STRING:
		record->value = (char*)malloc(strlen(string) * sizeof(char) +1);
		strcpy(record->value, string);
		break;
	}
	//record->address		= address;

	SVariableRecord* temporary = classNamespace->classVariables[index];
	SVariableRecord* temporary2;

	if (temporary != NULL)											// not the first record
	{
		while (temporary != NULL)								// look for the last record
		{
			if (strcmp(variableName, temporary->varName) == 0)		// is already present in hashtable
			{
				fprintf(stderr, "Variable in class already exists.\n");
				free(record);
				return NULL;
			}
			temporary2 = temporary;
			temporary = temporary->next;
		}
		// not present
		temporary2->next = record;
		return temporary2->next;
	}

	// nothing in the table yet
	classNamespace->classVariables[index] = record;
	return classNamespace->classVariables[index];
}

void HashTable_Free(SHashTable* hashTable) {

	if (hashTable == NULL)
	{
		fprintf(stderr, "HashTable_Free recieved a NULL pointer. Unacceptable!\n");
		return;
	}
	
	SMethodRecord	*methodRecordTmp;
	SMethodRecord	*nextMethodToFree;
	SVariableRecord	*variableRecordTmp;
	SVariableRecord	*nextVariableToFree;
	SLocalBlockList	*localBlockListTmp;
	SLocalBlockList	*nextBlockToFree;

	for (unsigned i = 0; i < hashTable->tableSize; i++)
	{
		if (hashTable->tableData[i] != NULL)
		{
			for (unsigned l = 0; l < hashTable->tableData[i]->classMethodsTableSize; l++)			// metody
			{
				if (hashTable->tableData[i]->classMethods[l] == NULL)
					continue;
				methodRecordTmp = hashTable->tableData[i]->classMethods[l];

				while (methodRecordTmp != NULL)
				{
					if (methodRecordTmp->first != NULL)
					{
						localBlockListTmp = methodRecordTmp->first;
						while (localBlockListTmp != NULL)
						{
							for (unsigned j = 0; j < localBlockListTmp->tableSize; j++)
							{
								if (localBlockListTmp->localVariables[j] != NULL)
								{
									variableRecordTmp = localBlockListTmp->localVariables[j];
									while (variableRecordTmp != NULL)
									{
										nextVariableToFree = variableRecordTmp->next;
										free(variableRecordTmp->varName);
										free(variableRecordTmp->value);
										free(variableRecordTmp);
										variableRecordTmp = nextVariableToFree;
									}
								}
							}
							variableRecordTmp = NULL;
							nextVariableToFree = NULL;
							nextBlockToFree = localBlockListTmp->next;
							free(localBlockListTmp);
							localBlockListTmp = nextBlockToFree;
						}
					}
					nextMethodToFree = methodRecordTmp->next;
					InstructionFreeQueue(&(methodRecordTmp->Function));
					free(methodRecordTmp);
					methodRecordTmp = nextMethodToFree;

				}
				hashTable->tableData[i]->classMethods[l] = NULL;
			}			
			//free(hashTable->tableData[i]->classMethods);

			for (unsigned k = 0; k < hashTable->tableData[i]->classVariablesTableSize; k++)				// staticke promenne
			{
				if (hashTable->tableData[i]->classVariables[k] == NULL)
					continue;
				variableRecordTmp = hashTable->tableData[i]->classVariables[k];

				while (variableRecordTmp != NULL)
				{
					nextVariableToFree = variableRecordTmp->next;
					free(variableRecordTmp->varName);
					free(variableRecordTmp->value);
					free(variableRecordTmp);
					variableRecordTmp = nextVariableToFree;
				}
				hashTable->tableData[i]->classVariables[k] = NULL;
			}	   
			//free(hashTable->tableData[i]->classVariables);

			free(hashTable->tableData[i]);
			hashTable->tableData[i] = NULL;
		}
	}
	// free(hashTable->tableData);
	free(hashTable);
	hashTable = NULL;
}

void HashTable_FreeCopy(SLocalBlockList* blocksList)
{
	if (blocksList == NULL)
		return;

	SLocalBlockList *localBlocksTmp;
	SLocalBlockList *localBlocksTmp2;
	SVariableRecord	*varRecordTmp;
	SVariableRecord *varRecordTmp2;

	localBlocksTmp = blocksList;

	while (localBlocksTmp != NULL)
	{
		for (unsigned i = 0; i < HASH_TABLE_SIZE; i++)
		{
			if (localBlocksTmp->localVariables[i] == NULL)
				continue;
			varRecordTmp = localBlocksTmp->localVariables[i];
			while (varRecordTmp != NULL)
			{
				varRecordTmp2 = varRecordTmp->next;
				free(varRecordTmp);
				varRecordTmp = varRecordTmp2;
			}
		}
		localBlocksTmp2 = localBlocksTmp->next;
		free(localBlocksTmp);
		localBlocksTmp = localBlocksTmp2;
	}
}

SMethodRecord* HashTable_AddMethodToClass(SClassNamespace* classNamespace, const char* methodName, 
											const char* typeSignature)
{
	if (classNamespace == NULL)
	{
		fprintf(stderr, "HashTable_AddMethodToClass() recieved a NULL pointer. Unacceptable!\n");
		return NULL;
	}

	unsigned index = HashTable_HashFunction(methodName, classNamespace->classMethodsTableSize);

	SMethodRecord* record = (SMethodRecord*)malloc(sizeof(SMethodRecord));
	if (record == NULL)
	{
		fprintf(stderr, "Malloc error.\n");
		return NULL;
	}

	char * holder = malloc(strlen(methodName) + 1);
	strcpy(holder, methodName);
	record->methodName		= holder;
	record->Function		= InstructionInitQueueFunction();
	record->next			= NULL;					// next record in hashtable list
	record->first			= NULL;
	record->retType			= (typeSignature == "" ? "v" : typeSignature[0]);	// assume void?
	record->typeSignature	= (typeSignature == "" ? "v" : typeSignature);
	record->numOfParameters = (unsigned short)((strlen(typeSignature) == 0) ? 0 : (strlen(typeSignature) - 1));

	SMethodRecord* temporary = classNamespace->classMethods[index];
	SMethodRecord* temporary2;
	if (temporary != NULL)											// not the first record
	{
		while (temporary != NULL)								// look for the last record
		{
			if (strcmp(methodName, temporary->methodName) == 0)		// is already present in hashtable
			{
				fprintf(stderr, "Method in class already exists.\n");
				free(record);
				return NULL;
			}
			temporary2 = temporary;
			temporary = temporary->next;
		}
		// not present
		temporary2->next = record;
		return temporary2->next;
	}

	// nothing in the table yet
	classNamespace->classMethods[index] = record;
	return classNamespace->classMethods[index];
}


SLocalBlockList* HashTable_AddBlockToMethod(SMethodRecord* methodRecord, int submerge)
{
	SLocalBlockList *newRecord = (SLocalBlockList*)malloc(sizeof(SLocalBlockList));
	if (newRecord == NULL)
	{
		fprintf(stderr, "Malloc error\n.");
		return NULL;
	}

	newRecord->submerge = submerge;
	newRecord->tableSize = HASH_TABLE_SIZE;

	for (unsigned i = 0; i < newRecord->tableSize; i++)
		newRecord->localVariables[i] = NULL;

	newRecord->next = NULL;

	if (methodRecord->first == NULL)		// first block
	{
		methodRecord->first = newRecord;
		newRecord->prev = NULL;
	}
	else									// other
	{
		SLocalBlockList *temp = methodRecord->first;
		while (temp->next != NULL)
			temp = temp->next;

		temp->next = newRecord;
		temp->next->prev = temp;
	}
	return newRecord;
}

SVariableRecord* HashTable_AddVariableToBlock(SLocalBlockList* localBlockList, char* variableName, 
													EDataTypes dataType, char* string, bool defined)
{
	if (localBlockList == NULL)
	{
		fprintf(stderr, "LocalBlockList_AddVariableToBlock recieved a NULL pointer.\n");
		return NULL;
	}

	SVariableRecord *record = (SVariableRecord*)malloc(sizeof(SVariableRecord));
	if (record == NULL)
	{
		fprintf(stderr, "Malloc error.\n");
		return NULL;
	}
	record->next = NULL;
	record->type = dataType;
	record->defined = defined;
	record->declared = false;
	switch (dataType)
	{
	case INT:
		record->value = (int*)malloc(sizeof(int));
		break;
	case DOUBLE:
		record->value = (int*)malloc(sizeof(double));
		break;
	case STRING:
		record->value = (char*)malloc((strlen(string) + 1) * sizeof(char));
		strcpy(record->value, string);
		break;
	}
	//record->address = address;

	char * holder = malloc(strlen(variableName) + 1);
	strcpy(holder, variableName);
	record->varName = holder;

	unsigned index = HashTable_HashFunction(variableName, localBlockList->tableSize);

	if (localBlockList->localVariables[index] == NULL)	 // first variable
	{
		localBlockList->localVariables[index] = record;
	}
	else
	{
		SVariableRecord *temp = localBlockList->localVariables[index];
		while (temp->next != NULL)
		{
			// possible testing for redefining the variable
			temp = temp->next;
		}
		temp->next = record;
	}
	return record;
}


SVariableRecord* HashTable_FindLocalVariable(SLocalBlockList* localBlockList, const char* varName)
{
	if (localBlockList == NULL)
	{
		fprintf(stderr, "HashTable_FindLocalVariable recieved a null pointer.\n");
		return NULL;
	}

	unsigned index = HashTable_HashFunction(varName, localBlockList->tableSize);
	SVariableRecord* varRecord;
	SLocalBlockList* blockRecord;
	unsigned submerge = localBlockList->submerge;
	bool first = true;

	for (blockRecord = localBlockList; blockRecord != NULL; blockRecord = blockRecord->prev) 
	{
		if (blockRecord->submerge >= submerge && !first)
			continue;
		first = false;
		varRecord = blockRecord->localVariables[index];
		if (varRecord == NULL)
			continue;
		while (true)
		{
			if (strcmp(varName, varRecord->varName) == 0)
				return varRecord;
			else if (varRecord->next == NULL)
				break;
			else
				varRecord = varRecord->next;
		}
	}
	return NULL;
}

SVariableRecord*	HashTable_LookForVariable(SLocalBlockList* localBlockList, const char * varName, const char * className, int Visible)
{
	SVariableRecord *a = NULL;
	if ((a = HashTable_FindLocalVariable(localBlockList, varName)) != NULL)
	{
		if (a->declared || Visible == 2)
			return a;
		else
		{
			if (Visible == 1)
			{
				a->declared = true;
				return a;
			}
		}
	}

	return HashTable_FindStaticVariable(hashTable, className, varName);		
}

SLocalBlockList* HashTable_CopyMethodBlockList(SMethodRecord* methodRecord)
{
	if (methodRecord->first == NULL)
	{
		return NULL;
	}
	
	SMethodRecord method;

	method.first = NULL;
	method.methodName = methodRecord->methodName;
	method.next = NULL;
	method.numOfParameters = methodRecord->numOfParameters;
	method.retType = methodRecord->retType;
	method.typeSignature = methodRecord->typeSignature;

	//SLocalBlockList* temp = HashTable_AddBlockToMethod(method, methodRecord->first->submerge);
	//if (temp == NULL)
		//return NULL;		// malloc error, pass forward
	void *retVal;
	SLocalBlockList* tmp = methodRecord->first;
	
	while (tmp != NULL)
	{
		retVal = __CopyBlock(tmp , &method, tmp->submerge);
		if (retVal == NULL)
			return NULL;			// possible memory leaks
		tmp = tmp->next;
	}


	return method.first;
}

SLocalBlockList* __CopyBlock(SLocalBlockList* localBlock, SMethodRecord* methodRecord, int submerge)
{
	SLocalBlockList  *block = HashTable_AddBlockToMethod(methodRecord, submerge);
	if (block == NULL)
		return NULL;				// pass 

	SVariableRecord* tmp;
	SVariableRecord* tmp2;

	for (unsigned i = 0; i < HASH_TABLE_SIZE; i++)
	{
		block->localVariables[i] = NULL;
		if (localBlock->localVariables[i] == NULL)
			continue;
		block->localVariables[i] = (SVariableRecord*)malloc(sizeof(SVariableRecord));

		tmp = localBlock->localVariables[i];
		tmp2 = block->localVariables[i];

		while (tmp != NULL)
		{
			//tmp2->address = tmp->address;
			switch (tmp->type)
			{
			case INT:
				tmp2->value = (int*)malloc(sizeof(int));
				memcpy(tmp2->value, tmp->value, sizeof(int));
				break;
			case DOUBLE: 
				tmp2->value = (double*)malloc(sizeof(double));
				memcpy(tmp2->value, tmp->value, sizeof(double));
				break;
			case STRING:
				tmp2->value = (char*)malloc(sizeof(char)* (strlen(tmp->value) + 1));
				memcpy(tmp2->value, tmp->value, sizeof(char)* (strlen(tmp->value) + 1));
				break;
			}
			//tmp2->value = tmp->value;
			tmp2->defined = tmp->defined;
			tmp2->type = tmp->type;
			tmp2->varName = tmp->varName;
			tmp2->next = (tmp->next == NULL ? NULL : (SVariableRecord*)malloc(sizeof(SVariableRecord)));

			tmp2 = tmp2->next;
			tmp = tmp->next;
		}
	}
	return block;
}

SLocalBlockList* HashTable_CopyMethodBlockList_ByName(SHashTable* hashTable, const char* className, const char* methodName)
{
	return HashTable_CopyMethodBlockList(HashTable_FindMethod(hashTable, className, methodName));
}


/* INTERNI FUNKCE */
int length(char* arr) 
{
	return (int)strlen(arr);
}

char* substr(char *arr, int i, int n) 
{
	if( i < 0 || n < 0)
		return NULL;

	int stringLength = length(arr);
	if ((i + n) > stringLength)
		return NULL;

	char *tmp = (char*)malloc(n + 1 * sizeof(char));
	if (tmp == NULL) 
		return NULL;


	unsigned compare = i + n;
	if ((unsigned)length(arr) < compare) 
	{
		int l = (length(arr) - i);
		strncpy(tmp, arr + i, l);
	}
	else			// length == i + n
	{
		strncpy(tmp, arr + i, n);
	}
	tmp[n] = '\0';
	return tmp;
}

int compare(const char* s1, const char* s2)
{
	return strcmp(s1, s2);
}

/*  Knuth-Morris-Pratt */
int find(char* source, char* search) 
{
	int sourceLen = length(source);
	int searchLen = length(search);

	/* vyhledavany nemuze byt veci nez zdroj */
	if (searchLen > sourceLen) 
		return -1;
	
	int i;
	int j = 0;
	int *fail = (int*)malloc(searchLen * sizeof(int));
	if (fail == NULL)
		return -1;

	fail[0] = 0;  /* prvni hodnota vektoru nastavena na 0 */
	for (i = 1; i < searchLen; i++) 
	{
		while ((j > 0) && (search[j] != search[i]))
			j = fail[j - 1];

		if (search[j] == search[i]) 
			j++;
		fail[i] = j;
	}
	j = 0;  /* reset indexu */
			/* vykonani samotneho porovnani */
	for (i = 0; i < sourceLen; i++) 
	{
		while ((j > 0) && (search[j] != source[i])) 
			j = fail[j - 1];
	
		if (search[j] == source[i]) 
			j++;
	
		if (j == searchLen) 
		{
			free(fail);
			return (i - j + 1);
		}
	}
	free(fail);
	return -1;
}

void FrontBackSplit(SElem* source, SElem** front, SElem** back)
{
	SElem* fast;
	SElem* slow;
	if (source == NULL || source->next == NULL)
	{
		*front = source;
		*back = NULL;
	}
	else
	{
		slow = source;
		fast = source->next;
		while (fast != NULL)
		{
			fast = fast->next;
			if (fast != NULL)
			{
				slow = slow->next;
				fast = fast->next;
			}
		}
		*front = source;
		*back = slow->next;
		slow->next = NULL;
	}
}

SElem* SortedMerge(SElem* left, SElem* right)
{
	SElem* res = NULL;
	if (left == NULL)
		return right;

	if (right == NULL)
		return left;

	if (left->data <= right->data)
	{
		res = left;
		res->next = SortedMerge(left->next, right);
	}
	else
	{
		res = right;
		res->next = SortedMerge(left, right->next);
	}
	return res;
}

/* usporada pointery ne data */
void MergeSort(SElem** head) 
{
	SElem* tmphead = *head;
	SElem* tmp1;
	SElem* tmp2;
	if ((tmphead != NULL) && (tmphead->next != NULL))
	{
		FrontBackSplit(tmphead, &tmp1, &tmp2);
		MergeSort(&tmp1);
		MergeSort(&tmp2);
		*head = SortedMerge(tmp1, tmp2);
	}
}


/* nahrani serazenych prvku do navratove promenne */
char* returnList(SElem *element, size_t size) 
{
	char *s = (char*)malloc((size + 1) * sizeof(char));
	memset(s, '\0', size + 1);
	int i = 0;
	while (element != NULL) 
	{
		s[i] = element->data;
		element = element->next;
		i++;
	}
	return s;
}
/* vytvori novy prvek, naplni ho daty ze vstupu, propoji se zbytkem seznamu */
void push(SElem** head, char data) 
{
	SElem* new = (SElem*)malloc(sizeof(SElem));
	new->data = data;
	new->next = (*head);
	*head = new;
}

char* sort(char *s) 
{
	SElem* tmp = NULL;
	/* nacteni vstupnich dat */
	size_t i;
	for (i = strlen(s) - 1; i >= 0; i--) 
	{
		push(&tmp, s[i]);
	}
	MergeSort(&tmp);
	return returnList(tmp, strlen(s));
}

int readInt()
{
	int c;
	char arr[255];
	char *end = NULL;
	int i = 0;
	while (true)
	{
		c = getchar();
		if (c == EOF || c == '\n')
			break;
		arr[i] = (char)c;
		i++;
	}
	long l = strtol(arr, &end, 10);
	if (end != NULL)
		return -1;
	return (int)l;
}

double readDouble()
{
	int c;
	char arr[255];
	char *end = NULL;
	int i = 0;
	while (true)
	{
		c = getchar();
		if (c == EOF || c == '\n')
			break;
		arr[i] = (char)c;
		i++;
	}
	double d = strtod(arr, &end);
	if (end != NULL)
		return -1;
	return d;
}

// speed vs memory
const char* readString()
{
	int c;
	unsigned multiplier = 1;
	char* arr = (char*) malloc(sizeof(char) * 255);
	memset(arr, 0, sizeof(char)*255);

	int i = 0;
	while (true)
	{
		c = getchar();
		if (c == EOF || c == '\n')
			break;

		if ( i+1 == 255 * multiplier)
		{
			multiplier++;
			arr = (char*) realloc(arr, sizeof(char) * 255 * multiplier);
			if (arr == NULL)
				return NULL;
			memset(arr + i, 0, (255 * multiplier)-i);			// maybe -1
		}
		arr[i] = (char)c;
		i++;

	}
	return arr;
}

void print(const char* s)
{
	printf("%s", s);
}