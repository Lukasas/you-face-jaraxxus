#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define DEBUG 1
#define HASH_TABLE_SIZE 20

/**
* USAGE
* ALL FUNCTIONS HERE ARE PREFIXED WITH "HashTable_"
* ALL FUNCTIONS RETURNS NULL IF THEY FAIL!!!!
* DO NOT LOSE THE POINTER RETURNED BY HashTable_Init()!
* FREE ONLY WITH HashTable_Free()!!!!
*
*
* 1. HashTable_Init() returns a new empty hash table eg. SHashTable*
* 2. HashTable_AddClass( SHashTable*, ... ) adds a new class to the hash table given in arguments and returns its pointer in it.
*		eg. SClassNamespace*
* 3. HashTable_AddVariableToClass( SClassNamespace*, ... ) adds the global (static) variable
*		to the ClassNamespace given in arguments
* 4. HashTable_AddMethodToClass( SClassNamespace*, ... )  adds a class method to the ClassNamespace given in arguments
* 5. HashTable_FindMethod() and HashTable_FindStaticVariable() for searching in HashTable
**/

typedef enum eDataTypes
{
	INT,
	DOUBLE,
	STRING
	//BOOL
	//VOID

} EDataTypes;



typedef struct sVariableRecord	
{
	char					*varName;		
	EDataTypes				type;
	//int					address;
	void					*value;
	bool					defined; // x = 5;
	bool					declared; // int x;
	struct sVariableRecord	*next;
} SVariableRecord;

typedef struct sMethodRecord
{
	const char				*methodName;
	char					retType;
	const char				*typeSignature;
	unsigned short			numOfParameters;
	struct InsList			*Function;
	struct sMethodRecord	*next;
	struct sLocalBlockList	*first;
} SMethodRecord;

typedef struct sClassNamespace
{
	const char				*className;					
	struct sClassNamespace	*next;						
	unsigned 				classMethodsTableSize;							// size of classMethods Hash Table
	SMethodRecord			*classMethods[HASH_TABLE_SIZE];					// classMethods Hash Table
	unsigned				classVariablesTableSize;						// size of variables Hash Table
	SVariableRecord			*classVariables[HASH_TABLE_SIZE];				// classVariables Hash Table 
} SClassNamespace;

typedef struct sHashTable
{
	unsigned				tableSize;									// Size of symbols table.
	SClassNamespace			*tableData[HASH_TABLE_SIZE];				// Symbols table implemented as Hash Table. 
} SHashTable;

typedef struct sLocalBlockList
{
	unsigned				submerge;
	struct sLocalBlockList	*next;
	struct sLocalBlockList	*prev;
	unsigned				tableSize;
	SVariableRecord			*localVariables[HASH_TABLE_SIZE];
} SLocalBlockList;

typedef struct sElem
{
	char data;
	struct sElem* next;
}SElem;

unsigned			HashTable_HashFunction(const char *str, unsigned htab_size);

SHashTable*			HashTable_Init();

SClassNamespace*	HashTable_FindClass(SHashTable *hashTable, const char* className);
SMethodRecord*		HashTable_FindMethod(SHashTable* hashTable, const char* className, const char* methodName);
SVariableRecord*	HashTable_FindStaticVariable(SHashTable* hashTable, const char* className, const char* variable);
SVariableRecord*	HashTable_FindStaticVariable_ByPointer(SClassNamespace* classNamespace, const char* varName);

SClassNamespace*	HashTable_AddClass(SHashTable* hashTable, const char* className);
SVariableRecord*	HashTable_AddVariableToClass(SClassNamespace* classNamespace, char* variableName, EDataTypes dataType, char* string, bool defined);
SMethodRecord*		HashTable_AddMethodToClass(SClassNamespace* classNamespace, const char* methodName, const char* typeSignature);

SLocalBlockList*	HashTable_AddBlockToMethod(SMethodRecord* methodRecord, int submerge);
SVariableRecord*	HashTable_AddVariableToBlock(SLocalBlockList* localBlockList, char* variableName, EDataTypes dataType, char* string, bool defined);

SVariableRecord*	HashTable_FindLocalVariable(SLocalBlockList* localBlockList, const char* varName);

SVariableRecord*	HashTable_LookForVariable(SLocalBlockList* localBlockList, const char * varName, const char * className, int Visible);

SLocalBlockList*	HashTable_CopyMethodBlockList(SMethodRecord* methodRecord);
SLocalBlockList*	HashTable_CopyMethodBlockList_ByName(SHashTable* hashTable, const char* className, const char* methodName);
SLocalBlockList*	__CopyBlock(SLocalBlockList* localBlock, SMethodRecord* methodRecord, int submerge);

void				HashTable_FreeCopy(SLocalBlockList* blocksList);
void				HashTable_Free(SHashTable* hashTable);

/* INNER FUNCTIONS */

char*				sort(char *s);
int					find(char* source, char* search);
int					compare(const char* s1, const char* s2);
int					compare(const char* s1, const char* s2);
int					length(char* arr);
int					readInt();
double				readDouble();
const char*			readString();
void				print(const char* s);