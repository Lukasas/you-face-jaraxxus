#include "syn_tree.h"

STree * STCreateNode()
{
	STree * temp = (STree*)malloc(sizeof(STree));
	temp->parent = NULL;
	temp->LNode = NULL;
	temp->RNode = NULL;
	temp->data = NULL;
	return temp;
}

STree * STAddLNode(STree * parent, ST_DATA_TYPE data)
{
	parent->LNode = STCreateNode();
	return parent->LNode;
}

STree * STAddRNode(STree * parent, ST_DATA_TYPE data)
{
	parent->RNode = STCreateNode();
	return parent->RNode;
}

void STRemoveNode(STree * Node)
{
	if (FREE_DATA)
	{
		if (Node->data != NULL)
			free(Node->data);
		Node->data = NULL;
	}


	if (Node->LNode != NULL)
		STRemoveNode(Node->LNode);
	free(Node->LNode);
	Node->LNode = NULL;

	if (Node->RNode != NULL)
		STRemoveNode(Node->RNode);
	free(Node->RNode);
	Node->RNode = NULL;
}


#if FREE_DATA == 1
ST_DATA_TYPE STCreateValueChar(char * data, int size)
{
	if (FREE_DATA)
	{
		ST_DATA_TYPE NodeData = (ST_DATA_TYPE)malloc(size);
		memcpy(NodeData, data, size);
		return NodeData;
	}
}
#endif
