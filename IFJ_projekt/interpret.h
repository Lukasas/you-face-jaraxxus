#pragma once
#include <stdio.h>
#include <stdlib.h>
#include "instruction.h"
#include "ial.h"

typedef struct Memories // Not finished yet
{
	struct sVariableRecord * sVars;
	struct sMethodRecord * sMeth;
	struct sClassNamespace * sClass;
} Memories;

enum InterpretErrors
{
	ERR_LOAD_FROM_INPUT = 7,
	ERR_UNINITIALIZED_VAR = 8,
	ERR_DIVIDED_BY_ZERO = 9,
	ERR_OTHER_ERRORS = 10,
	ERR_INTER = 99,
};

int ExecuteInstruction(struct Instruction * Instr, int Function, SLocalBlockList * block, const char * ClassName, SMethodRecord *method);
int ExecuteCode(struct InsList **Instrs);