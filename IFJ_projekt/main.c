#include <stdio.h>
#include <stdlib.h>
#include "syntax_analyzator.h"
#include "lexical_analyzator.h"
#include "semantics_analyzator.h"
#include "instruction.h"
#include "token_stack.h"
#include "interpret.h"
#include "ial.h"

// TODO: Everything except main
// Change

int main(int args, char *argv[])
{	
	Program = InstructionInitQueue();
	SemanticInit();
	
	FILE *f;
	if (args == 1)
	{
		f = stdin;
	}
	else if (args == 2)
	{
		f = fopen(argv[1], "r");
	}
	else
	{
		fprintf(stderr, "Wrong aruments.");
		return 99;
	}

	int RetVal = 0;
	RetVal = SyntaxAnalyzator(f);
	if (RetVal != 0)
	{
		return RetVal;
	}

	fclose(f);
	RetVal = ExecuteCode(&Program);
	if (RetVal != 0)
	{
		return RetVal;
	}
	InstructionFreeQueue(&Program);

	return RetVal;

}