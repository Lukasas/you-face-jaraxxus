﻿/*

FILE: semantics.h
Authors: Jakub Chlebík

*/
#ifndef SEMANTICS_H_
#define SEMANTICS_H_

#include "ial.h"

#define _alloc(x, y, z) __alloc(&y, z)
#define my_free(x) _my_free(&x)


enum {
	BEGIN = 0,
	DEC,
	EQ_FC,
	BEGIN_FC,
	FC_PARAM_SEPAR,
	FC_PARAM,
	FC_END,
	FC_CALL,
	VAL_EVAL,
	VAL_EVAL_2,
	FOR_BEGIN,
	FOR_DEF_BEGIN,
	FOR_DEF_ID,
	FOR_DEF_EQ,
	FOR_EXP_NEG,
	FOR_CONDITION,
	FOR_ITER,
	FOR_BLOCK,
	FOR_ITER_EQ,
	IF,
	ELSE,
	CIN,
	CIN_NEXT,
	CIN_END,
	COUT,
	COUT_NEXT,
	COUT_END,
};

void _my_free(char**);
int garbage(int);
bool recast(char*,char*);
char* GetType(char*);
bool __alloc(char**, char*);
int iSemantAnal(FILE*);




#endif
