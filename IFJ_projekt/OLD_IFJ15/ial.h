﻿/*

FILE: ial.h
Authors: Adam Huspenina, Jakub Chlebík

*/
#ifndef IAL_H_
#define IAL_H_

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


#define MALLOC_ERR 99
#define OUTSIDE_ERR 2
#define UNDECLARED_VAR 3
#define CONFLICT_NAME 3
#define WRONG_FC_CALL 3
#define UNSPECIFIED_ERR 6
#define NOT_ADDED 99
#define SUCC 0
#define CANT_MOVE 2
#define SYNTAX_ERR 2
#define ALREADY_EXIST 3
#define AUTO_FUNC 3
#define AUTO_DEC 156
#define FC_PARAM_ERR 4
#define REDEC 3
#define REDEF 3
#define FOR_FUNC 6
#define VAR_ERR 5
#define FOR_ERR_DEF 3
#define NOT_MAIN 6


typedef struct VarNode {								  // strom pro bloky
	char type;											  // prvni znak typu. pro blok "b"
	char* name;											  // pro blok pretypovano na int a obsahuje hodnotu zanorení
	struct VarNode * LPtr;                                // levý podstrom
	struct VarNode * RPtr;                                // pravý podstrom
} *VarNodePtr;


typedef struct FcNode {										// strom pro declarace funkcí
	char* FType;			                               	// navratovy typ a typy parametru
	char* FName;                                          	// jmeno funkce
	char* FParamNames;
	bool def;												// def
	struct FcNode * LPtr;                                  	// levý podstrom
	struct FcNode * RPtr;                                  	// pravý podstrom
} *FcNodePtr;

struct elem {
	char data;
	struct elem* next;
};



// funkce pro praci se stromem pro "bloky"
void BTInitV(VarNodePtr *);											// inicializace stromu
void BTDispose(VarNodePtr *, FcNodePtr *);							// vymazani stromu
int BTAddNodeV(VarNodePtr *, char *, char, bool);					// vlozi do stromu node
bool BTBlockUpV(VarNodePtr *, VarNodePtr *, int);							// posune ukazatel o blok vis
struct VarNode* BTSearchVarName(VarNodePtr *, VarNodePtr, char *);	// vyhledava ve stromu


// funkce pro praci se stromem funkci
void BTInitF(FcNodePtr *);											// inicializace
int BTAddFcNode(FcNodePtr *, char *, char *, bool);					// pridani nodu funkce
struct FcNode* BTSearchFcName (FcNodePtr *, char * );				// vyhledani specificke funkce v podstromu podle jejiho jmena
bool BTFCAddParams(FcNodePtr, char*);
struct FcNode* initFcTree(FcNodePtr );

// interni funkce jazyka IFJ15
int lenght(char* );
char* concat(char *, char *);
char* substr(char *, int , int );
int find (char*, char*);
char* sort(char* );

/* pomocne funkce pro MERGE-LIST SORT */
void MergeSort(struct elem**);
struct elem* SortedMerge(struct elem* , struct elem* );
void FrontBackSplit(struct elem* , struct elem** , struct elem** );
char* returnList(struct elem*, size_t);
void push(struct elem**, char);

#endif
