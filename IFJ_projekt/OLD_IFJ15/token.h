/*

FILE: token.h
Authors: Luk� Ch�bek

*/
#ifndef TOKEN_H_
#define TOKEN_H_

// Structs
#include <string.h>
#include <stdlib.h>

#define CIN_BUFFER_SIZE 500 // here comes the magic
#define SAVE_FOR_DOUBLE 30 // this is not magic, it cannot be bigger
struct Token
{
	char id[10];
	char desc[64];
};

typedef struct Vars
{
	char type[10];
	char desc[64];
	void * value;
}*VarsPtr;

typedef struct DymTok
{
	char id[10];
	char *desc;
}*DymTokPtr;
/*
struct _Dec
{
	struct Token id;
	struct Token typ;
};
*/
struct _Def
{
	struct Token id;
	struct Token typ;
	struct Token expr;
};

typedef struct FcTok
{
	struct Token type;
	struct Token id;
	struct DymTok dec;
	struct DymTok combcmd;
}*FcTokPtr;

typedef struct CmdIf
{
	struct DymTok expr;
	struct DymTok combcmd;
	struct DymTok combcmdnd;
}*CmdIfPtr;

typedef struct CmdFor
{
	struct DymTok combcmd;
	struct DymTok cmdfill;
	struct DymTok combcmdnd;
}*CmdForPtr;

struct _Ptr
{
	char desc[64];
	void * mem;
};


void EraseToken(struct Token * tTok);
void SetToken(struct Token * tTok, char * id, char * desc);
void SetTokenByToken(struct Token * tTok, struct Token tToknd);

DymTokPtr CreateDymTok(char * id, char * desc);
void GetDymTok(DymTokPtr tok, DymTokPtr get);
void DestroyDymTok(DymTokPtr tok);
void SetDymTok(DymTokPtr tok, char * id, char * desc);

FcTokPtr CreateFcTok(struct Token type, struct Token id, DymTokPtr dec, DymTokPtr combcmd);// POUZE TAKTO VYTV��ET DYNAMICK� TOKENY
void CopyFcTok(FcTokPtr tok, FcTokPtr from);
void DestroyFcTok(FcTokPtr tok);

CmdIfPtr CreateCmdIf(DymTokPtr expr, DymTokPtr combcmd, DymTokPtr combcmdnd); // POUZE TAKTO VYTV��ET DYNAMICK� TOKENY
void CopyCmdIf(CmdIfPtr tok, CmdIfPtr from);
void DestroyCmdIf(CmdIfPtr tok);

CmdForPtr CreateCmdFor(DymTokPtr cmdfill, DymTokPtr combcmd, DymTokPtr combcmdnd); // POUZE TAKTO VYTV��ET DYNAMICK� TOKENY
void CopyCmdFor(CmdForPtr tok, CmdForPtr from);
void DestroyCmdFor(CmdForPtr tok);

VarsPtr CreateVarTok(char * type, char * desc, void * value); // POUZE TAKTO VYTV��ET DYNAMICK� TOKENY
void SetVarTok(VarsPtr a, char * type, char * desc, void * value);
void GetVarTok(VarsPtr a, VarsPtr b);
void DestroyVarTok(VarsPtr ptr);
void GetVarVal(VarsPtr a, void * value);
#endif