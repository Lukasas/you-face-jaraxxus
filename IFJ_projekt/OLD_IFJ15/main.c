/*
****PROJECT INFO****

FILE: Main.c
Authors: Lukáš Chábek, Jiří Čermák, Anna Čorbová, Adam Huspenina, Jakub Chlebík

*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>

#include "stack.h"
#include "error_handler.h"
#include "garbage_remover.h"
#include "token.h"
#include "lexical_analyzator.h"
#include "semantics.h"
#include "rules.h"
#include "ial.h"

#define DEBUG 0

char prec_tab[15][15] = {
	//   +       *       =       <>      ,       (       )       i       else    id      >>      ;       {       }       $ 
	{   '<',    '>',	'>',    '>',    '>',	'<',	'>',	'<',	'<',	'<',	'>',	'>',	'>',	'>',	'>'}, // +
	{	'<',	'<',	'>',	'>',	'>',	'<',	'>',	'<',	'<',	'<',	'>',	'>',	'>',	'>',	'>'}, // *
    {   '<',	'<',	'=',	'<',	'>',	'<',	'>',	'<',	'<',	'<',	'>',	'>',	'>',	'>',	'>'}, // =
    {   '<',	'<',	'>',	'<',	'>',	'<',	'>',	'<',	'<',	'<',	'>',	'>',	'>',	'>',	'>'}, // <>
    {	'<',	'<',	'<',	'<',	'<',	'<',	'>',	'<',	'<',	'<',	'>',	'>',	'>',	'>',	'>'}, // ,
	{	'<',	'<',	'>',	'<',	'<',	'<',	'=',	'<',	'<',	'<',	'<',	'<',	'>',	'>',	'>'}, // (
	{	'>',	'>',	'>',	'>',	'>',	'<',	'<',	'<',	'=',	'=',	'>',	'>',	'=',	'>',	'>'}, // )
	{	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>'}, // i 
	{	'>',	'>',	'=',	'>',	'>',	'=',	'>',	'<',	'<',	'<',	'>',	'>',	'=',	'>',	'>'}, // else
	{	'>',	'>',	'=',	'>',	'>',	'=',	'>',	'<',	'<',	'<',	'=',	'>',	'<',	'>',	'>'}, // id
	{	'<',	'<',	'<',	'<',	'<',	'<',	'>',	'<',	'=',	'=',	'>',	'>',	'>',	'>',	'>'}, // >>
	{	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'=',	'>',	'>',	'>'}, // ;
	{	'<',    '<',	'<',    '<',    '<',    '<',    '<',    '<',    '<',    '<',    '<',    '<',    '<',    '=',    '>'}, // {
	{	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'=',	'>',	'>',	'>',	'>',	'>',	'>'}, // }
	{	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'>',	'>'}  // $
};
enum TOKEN_TYPE
{
	TOK_PLUS_MINUS,
	TOK_MULTIPLY,
	TOK_EQUAL,
	TOK_MORELESS,
	TOK_COMMA,
	TOK_LEFT_BRACKET,
	TOK_RIGHT_BRACKET,
	TOK_I,
	TOK_ELSE,
	TOK_ID,
	TOK_DOUBLE_MORE_THAN,
	TOK_SEMICOLON,
	TOK_LEFT_MULTI_BRACKET,
	TOK_RIGHT_MULTI_BRACKET,
	TOK_NOTHING,
	TOK_ERROR
};

enum ERROR_HANDLER
{
	ERR_OK,
	ERR_UNINIT_VAR
};

int iProceed(FILE * fileSourceCode);

void LexTypeCheck(struct Token * tTok);

int iSyntactAnalyze(FILE * fileSourceCode, sStackPtr rulesStack);

int iInterpret(int rule, sStackPtr Tokens, int * err_handler);
char * SolveString(char * math, double * vysledek, int * error_handler);

void ResolveCommand(int * err_handler, FcTokPtr tFcTok);
sStackPtr EXPRSTACK;
sStackPtr TYPESTACK;
sStackPtr DECSTACK;
sStackPtr DECLISTSTACK;
sStackPtr DEFSTACK;
sStackPtr PTRSTACK;
sStackPtr CMDSTACK;
sStackPtr COMBCMDSTACK;
sStackPtr FCSTACK;
sStackPtr CMDFILLSTACK;
sStackPtr CMDIFSTACK;
sStackPtr CMDFORSTACK;
sStackPtr RETSTACK;
sStackPtr LOADSTACK;
sStackPtr WRITESTACK;
sStackPtr FUNCVARSTACK;

sStackPtr FUNCVARBUFFER;

bool sedi_syntax = false;

int main(int argc, char * argv[])
{
	FILE * fileSourceCode;

	if (argc == 1)
	{
		// no args
	}
	else if (argc == 2)
	{
		fileSourceCode = fopen(argv[1], "r");
	}
	else
	{
		// more args
	}

	int iReturnValue = 0;

	EXPRSTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(EXPRSTACK);
	TYPESTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(TYPESTACK);
	DECSTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(DECSTACK);
	DECLISTSTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(DECLISTSTACK);
	DEFSTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(DEFSTACK);
	PTRSTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(PTRSTACK);
	CMDSTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(CMDSTACK);
	COMBCMDSTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(COMBCMDSTACK);
	FCSTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(FCSTACK);
	CMDFILLSTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(CMDFILLSTACK);
	CMDIFSTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(CMDFILLSTACK);
	CMDFORSTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(CMDFORSTACK);
	RETSTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(RETSTACK);
	LOADSTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(LOADSTACK);
	WRITESTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(WRITESTACK);
	FUNCVARBUFFER = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(FUNCVARBUFFER);
	FUNCVARSTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(FUNCVARSTACK);

	iReturnValue = iProceed(fileSourceCode);

	// err handle ?
	// RemoveGarbage(); TODO: FIX THIS

	if (fileSourceCode)
		fclose(fileSourceCode);

	return iReturnValue;
}

int iProceed(FILE * fileSourceCode)
{

	struct Token *toktok = (struct Token*)malloc(sizeof(struct Token));
	if (toktok == NULL)
	{
		ErrorHandler(99, "Not enough memory #1.");
		return 99;
	}

	int iRetVal = 0;
	/*
	kaћdб funkce:
	iRetVal = Function();
	if (iRetVal != 0)
	return iRetVal;
	*/


	sStackPtr RuleNumbers = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(RuleNumbers);

	iRetVal = iSyntactAnalyze(fileSourceCode, RuleNumbers);
	if (iRetVal != 0)
		return iRetVal;

	ST_DisposeStack(RuleNumbers);
	
	fseek(fileSourceCode, 0, SEEK_SET);

	sedi_syntax = true;
	
	iRetVal = iSemantAnal(fileSourceCode);
	if (iRetVal != 0)
		return iRetVal;
	
	fseek(fileSourceCode, 0, SEEK_SET);
	
	RuleNumbers = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(RuleNumbers);
	
	iRetVal = iSyntactAnalyze(fileSourceCode, RuleNumbers);
	if (iRetVal != 0)
		return iRetVal;

	return iRetVal;
}

void LexTypeCheck(struct Token * tTok) // Determining if id (auto, if...)
{
	if (strcmp(tTok->desc, "auto") == 0)
	{
		strcpy(tTok->id, "auto");
	}
	else if (strcmp(tTok->desc, "cin") == 0)
	{
		strcpy(tTok->id, "cin");
	}
	else if (strcmp(tTok->desc, "cout") == 0)
	{
		strcpy(tTok->id, "cout");
	}
	else if (strcmp(tTok->desc, "double") == 0)
	{
		strcpy(tTok->id, "double");
	}
	else if (strcmp(tTok->desc, "else") == 0)
	{
		strcpy(tTok->id, "else");
	}
	else if (strcmp(tTok->desc, "for") == 0)
	{
		strcpy(tTok->id, "for");
	}
	else if (strcmp(tTok->desc, "if") == 0)
	{
		strcpy(tTok->id, "if");
	}
	else if (strcmp(tTok->desc, "int") == 0)
	{
		strcpy(tTok->id, "int");
	}
	else if (strcmp(tTok->desc, "return") == 0)
	{
		strcpy(tTok->id, "return");
	}
	else if (strcmp(tTok->desc, "string") == 0)
	{
		strcpy(tTok->id, "string");
	}
}

enum TOKEN_TYPE DetermineTokenID(const struct Token *tTok)
{
	if (tTok == NULL || strcmp(tTok->id, "") == 0)
		return TOK_NOTHING;

	const char * tok_id = tTok->id;

	if (strcmp(tok_id, "+") == 0)
		return TOK_PLUS_MINUS;
	else if (strcmp(tok_id, "*") == 0)
		return TOK_MULTIPLY;
	else if (strcmp(tok_id, "=") == 0)
		return TOK_EQUAL;
	else if (strcmp(tok_id, "<") == 0 || strcmp(tok_id, ">") == 0 || strcmp(tok_id, "<=") == 0 || strcmp(tok_id, ">=") == 0 || strcmp(tok_id, "==") == 0 || strcmp(tok_id, "!=") == 0)
		return TOK_MORELESS;
	else if (strcmp(tok_id, ",") == 0)
		return TOK_COMMA;
	else if (strcmp(tok_id, "(") == 0)
		return TOK_LEFT_BRACKET;
	else if (strcmp(tok_id, ")") == 0)
		return TOK_RIGHT_BRACKET;
	else if (strcmp(tok_id, "id") == 0 || strcmp(tok_id, "return") == 0 || strcmp(tok_id, "if") == 0 || strcmp(tok_id, "for") == 0 || strcmp(tok_id, "cout") == 0 || strcmp(tok_id, "cin") == 0)
		return TOK_ID;
	else if (strcmp(tok_id, "else") == 0)
		return TOK_ELSE;
	else if (strcmp(tok_id, ">>") == 0 || strcmp(tok_id, "<<") == 0)
		return TOK_DOUBLE_MORE_THAN;
	else if (strcmp(tok_id, ";") == 0)
		return TOK_SEMICOLON;
	else if (strcmp(tok_id, "{") == 0)
		return TOK_LEFT_MULTI_BRACKET;
	else if (strcmp(tok_id, "}") == 0)
		return TOK_RIGHT_MULTI_BRACKET;
	else if (strcmp(tok_id, "") == 0)
		return TOK_NOTHING;

	return TOK_I;
}


bool bCheckRule(sStackPtr S, int check_rule, char * pos)
{
	if (pos == NULL)
		return false;

	if (ST_TopType(S) == _CHAR)
		return true;

	struct Token tTok;
	ST_Pop(S, &tTok);
	char * ptr = strtok(pos, " ");

	if (ptr == NULL)
		return false;
	
	if (strcmp(ptr, tTok.id) == 0)
	{
		if (ST_TopType(S) == _CHAR)
			return true;
		else
			return bCheckRule(S, check_rule, pos + strlen(ptr) + 1); // 1 mezera
	}
	else
	{
		ST_Push(S, &tTok, _TOKEN);
		return false;
	}

}
// vrací id pravidla při úspěchu -2 při chybě
int TestRule(sStackPtr S, bool test_semicol, char * old_rule, bool * force_trans)
{
	char buffer[255];
	char buffer2[255];
	int err_handler = 0;
	bool return_semicol = false;
	strcpy(buffer, "");
	struct Token tTok, tToknd, tTokTemp;
	sStackPtr tokenUsed = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(tokenUsed);

	char * ptr;
	char * temp;
	if (old_rule == NULL)
	{
		while (ST_TopType(S) != _CHAR && ST_TopType(S) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(S, &tTok);
			ST_Push(tokenUsed, &tTok, _TOKEN); // pushování tokenů pro Interpret
			strcat(buffer, tTok.id);
			if (ST_TopType(S) != ST_ER_EMPTY_STACK && ST_TopType(S) != _CHAR)
				strcat(buffer, " ");
		}
		char delimer[2];

		
		// otáčení pravidel  id expr => expr id
		sStackPtr buff = (sStackPtr)malloc(sizeof(struct sStack)); // dočasný zásobník pro otáčení
		ST_InitStack(buff);

		temp = (char *)malloc(strlen(buffer) + 1);
		strcpy(temp, buffer);
		ptr = strtok(temp, " ");
		while (ptr != NULL)
		{
			ST_Push(buff, ptr, _CHAR);
			ptr = strtok(NULL, " ");
		}

		// vytvoření bufferu pro pravidla
		strcpy(buffer, "");
		// popvání zásobníku a vytváření pravidla v buferu
		while (ST_TopType(buff) != ST_ER_EMPTY_STACK)
		{
			strcpy(buffer2, "");
			ST_Pop(buff, buffer2);
			strcat(buffer, buffer2);
			if (ST_TopType(buff) != ST_ER_EMPTY_STACK)
				strcat(buffer, " ");
		}
		
		ST_DisposeStack(buff);

		free(temp);
		ptr = NULL;
		temp = NULL;

		// Pokud není zásobník prázdný, je na něm zarážka "<", musí se popnout
		if (ST_TopType(S) != ST_ER_EMPTY_STACK)
			ST_Pop(S, delimer); // POP <

		// pokud je v pravidle středník - cmd def ;, odebere se a uloží se, že se s ním může pracovat
		if (buffer[strlen(buffer) - 1] == ';')
		{
			buffer[strlen(buffer) - 2] = '\0';
			SetToken(&tTok, ";", "");
			return_semicol = true;
			ST_Push(S, &tTok, _TOKEN);
		}
	}
	else
	{
		strcat(old_rule, " ;");
		strcpy(buffer, old_rule);
	} 

	int offset;
	// cyklí zkrz všechna pravidla, dokud nenarazí na "e"
	for (int i = 0; strcmp(rules[i], "e") != 0; i++)
	{
		// offset pro pohyb v ukazateli na char
		offset = 0;
		// dočasná proměnná pro kouskování pravidla
		temp = (char *)malloc(strlen(rules[i]) + 1);
		// načtení všeho kromě prvního kusu pravidla "expr 'expr + expr'"
		strcpy(temp, rules[i]);
		ptr = strtok(temp, " ");
		offset = 1 + strlen(ptr);
		strcpy(temp, rules[i]);
		// porovnání, jestl bylo pravidlo nalezeno

		if (strcmp(buffer, "dec , declist") == 0)
			strcpy(buffer, "declist , dec");

		if (strcmp(temp + offset, buffer) == 0)
		{
			if (test_semicol == true)
				ST_Pop(S, &tTok);
			strcpy(temp, rules[i]);
			ptr = strtok(temp, " ");
			if (return_semicol == true)
			{
				SetToken(&tToknd, "", "");
				ST_Pop(S, &tToknd);
				SetToken(&tToknd, ";", "");
			}
			SetToken(&tTok, ptr, "rule");
			free(temp);

			if (strcmp(tTok.id, "cmdif") == 0 || strcmp(tTok.id, "cmdfor") == 0)
			{
				sStackPtr buff = (sStackPtr)malloc(sizeof(struct sStack));
				ST_InitStack(buff);

				while (ST_TopType(S) == _TOKEN)
				{
					ST_Top(S, &tTokTemp);
					if (strcmp(tTokTemp.desc, "rule") != 0)
						break;
					ST_Pop(S, &tTokTemp);
					ST_Push(buff, &tTokTemp, _TOKEN);
					EraseToken(&tTokTemp);
				}
				EraseToken(&tTokTemp);

				ST_Push(S, "<", _CHAR);

				while (ST_TopType(buff) == _TOKEN)
				{
					ST_Pop(buff, &tTokTemp);
					ST_Push(S, &tTokTemp, _TOKEN);
					EraseToken(&tTokTemp);
				}
				EraseToken(&tTokTemp);

				if (strcmp(tTok.id, "cmdfor") == 0 || strcmp(tTok.id, "cmdif") == 0)
					*force_trans = true;
				if (strcmp(tTok.id, "fcdef") != 0 && strcmp(tTok.id, "fcdec") != 0)
				{
					iInterpret(i, tokenUsed, &err_handler);
					if (err_handler != 0)
						return -3;

					ST_Push(S, &tTok, _TOKEN); // end push
					return i;
				}
				ST_DisposeStack(buff);
			}
			else
			{
				if (strcmp(tTok.id, "fcdef") != 0 && strcmp(tTok.id, "fcdec") != 0)
				{
					iInterpret(i, tokenUsed, &err_handler);
					if (err_handler != 0)
						return -3;
					ST_Push(S, &tTok, _TOKEN); // end push
					return i;
				}
			}
			if (return_semicol == true)
			{
				if (strcmp(tTok.id, "fcdef") != 0 && strcmp(tTok.id, "fcdec") != 0)
				{
					iInterpret(i, tokenUsed, &err_handler);
					if (err_handler != 0)
						return -3;
					ST_Push(S, &tToknd, _TOKEN); // end push
					return i;
				}
			}
			iInterpret(i, tokenUsed, &err_handler);
			if (err_handler != 0)
				return -3;
			return i;
		}
		else
		{
			free(temp);
			continue;
		}
	}

#if DEBUG == 1
	printf("Rule Check: %s\n", buffer);
#endif
	if (test_semicol == false && return_semicol == true)
	{
		return TestRule(S, true, buffer, force_trans);
	}
	else
	{
		return -2; // chyba
	}
}

int iSyntactAnalyze(FILE * fileSourceCode, sStackPtr rulesStack)
{
	sStackPtr MyStack = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(MyStack);

	char * buffer = (char *)malloc(64);

	sStackPtr RuleStack = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(RuleStack);

	bool force_trans = false;
	unsigned int line;
	struct Token tTok, tToknd, tTokTemp;

	enum TOKEN_TYPE a, b;

	int iRetVal = 0;
	EraseToken(&tTok);
	if ((iRetVal = iLexicalAnal(fileSourceCode, &tTok, &line)) == 1)
	{
		ErrorHandler(1, "Chyba na urovni lexikalni analyzy. Radek cislo: ", line);
		return iRetVal; // TODO: Error
	}
	do
	{
		if (ST_Top(MyStack, &tToknd) == ST_ER_EMPTY_STACK)
			a = DetermineTokenID(NULL);
		else
		{
			int _look_behind = 1;
			bool found = true;
			
			while (strcmp(tToknd.desc, "rule") == 0)
			{
				EraseToken(&tToknd);
				
				if (ST_TopBackType(MyStack, _look_behind) == _CHAR)
				{
					found = false;
					break;
				}

				if (ST_LookBehind(MyStack, &tToknd, _look_behind++) == ST_ER_EMPTY_STACK)
				{
					found = false;
					a = DetermineTokenID(NULL);
					EraseToken(&tToknd);
					break;
				}
			}
			if (found == true)
				a = DetermineTokenID(&tToknd);
			else
				a = DetermineTokenID(NULL);
		}
		b = DetermineTokenID(&tTok);

		force_trans = false;
		switch ((int)prec_tab[a][b])
		{
		case '<':
			if (ST_TopType(MyStack) == _TOKEN)
			{
				EraseToken(&tToknd);
				ST_Top(MyStack, &tToknd);

				if ((strcmp(tToknd.desc, "rule") == 0 && (strcmp(tToknd.id, "cmd") != 0 && strcmp(tToknd.id, "combcmd") != 0) && strcmp(tTok.id, ";") != 0) ||
					(strcmp(tToknd.desc, "rule") == 0 && strcmp(tTok.id, ";") == 0))
				{
					while (ST_TopType(MyStack) == _TOKEN)
					{
						ST_Top(MyStack, &tTokTemp);
						if (strcmp(tTokTemp.desc, "rule") != 0)
							break;
						else if ((strcmp(tTokTemp.id, "cmd") == 0 ||
							strcmp(tTokTemp.id, "combcmd") == 0) &&
							strcmp(tTok.id, ";") != 0)
							break;
						ST_Pop(MyStack, &tTokTemp);
						ST_Push(RuleStack, &tTokTemp, _TOKEN);
						EraseToken(&tTokTemp);
					}
					EraseToken(&tTokTemp);

			        ST_Push(MyStack, "<", _CHAR);
					
					while (ST_TopType(RuleStack) == _TOKEN)
					{
						ST_Pop(RuleStack, &tTokTemp);
						ST_Push(MyStack, &tTokTemp, _TOKEN);
						EraseToken(&tTokTemp);
					}
					EraseToken(&tTokTemp);
					ST_Push(MyStack, &tTok, _TOKEN);
				}
				else
				{
					ST_Push(MyStack, "<", _CHAR);
					ST_Push(MyStack, &tTok, _TOKEN);
				}
			}
			else
			{
				ST_Push(MyStack, "<", _CHAR);
				ST_Push(MyStack, &tTok, _TOKEN);
			}
			EraseToken(&tTok);
			if ((iRetVal = iLexicalAnal(fileSourceCode, &tTok, &line)) == 1)
			{
				ErrorHandler(1, "Chyba na urovni lexikalni analyzy. Radek cislo: ", line);
				return iRetVal; // TODO: Error
			}

			break;
		case '>':
			{
				int ruleID = TestRule(MyStack, false, NULL, &force_trans);
				
#if DEBUG == 1
				printf("%s\n", rules[ruleID]);
#endif
				if (ruleID == -2)
				{
					ErrorHandler(2, "Chyba na urovni syntakticke analyzy.");
					return 2;
				}
				else if (ruleID == -3)
				{
					ErrorHandler(3, "Chyba interpretu.");
					return 3;
				}
				// save ruleID
				ST_Push(rulesStack, &ruleID, _INT);


				if (force_trans == true)
				{
					ruleID = TestRule(MyStack, false, NULL, &force_trans);
					// save ruleID

#if DEBUG == 1
					printf("%s\n", rules[ruleID]);
#endif
					if (ruleID == -2)
					{
						ErrorHandler(2, "Chyba na urovni syntakticke analyzy.");
						return 2;
					}
					else if(ruleID == -3)
					{
						ErrorHandler(3, "Chyba interpretu.");
						return 3;
					}
					ST_Push(rulesStack, &ruleID, _INT);
				}

			}
			break;
		case '=':
			ST_Push(MyStack, &tTok, _TOKEN);

			EraseToken(&tTok);
			if ((iRetVal = iLexicalAnal(fileSourceCode, &tTok, &line)) == 1)
			{
				ErrorHandler(1, "Chyba na urovni lexikalni analyzy. Radek cislo: ", line);
				return iRetVal; 
			}
			break;
		}
	} while (!ST_IsEmpty(MyStack) || b != TOK_NOTHING );

	if (sedi_syntax == false)
		return 0;

	sStackPtr FCTEMPSTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(FCTEMPSTACK);
	FcTokPtr tFcTok, tFcTok2;
		tFcTok = CreateFcTok(tTok, tTok, NULL, NULL);
		tFcTok2 = CreateFcTok(tTok, tTok, NULL, NULL);

		while (ST_TopType(FCSTACK) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(FCSTACK, tFcTok);
			if (strcmp(tFcTok->id.desc, "main") == 0)
				break;
			ST_Push(FCTEMPSTACK, tFcTok, _FCTOK);
		}

		while (ST_TopType(FCTEMPSTACK) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(FCTEMPSTACK, tFcTok);
			ST_Push(FCSTACK, tFcTok2, _FCTOK);
		}

		int err_handler = 0;
		ResolveCommand(&err_handler, tFcTok);

		DestroyFcTok(tFcTok);
		DestroyFcTok(tFcTok2);
		ST_DisposeStack(FCTEMPSTACK);
	ST_SwapStackForInt(rulesStack);
	if (ST_TopType(MyStack) != ST_ER_EMPTY_STACK || b != TOK_NOTHING)
	{
		ErrorHandler(2, "Chyba na urovni syntakticke analyzy.");
		return 2;
	}
	ST_DisposeStack(RuleStack);
	ST_DisposeStack(MyStack);
	free(buffer);
	return 0;
}


char * Charit(char *a, char *b, char *znamenko, double *vysledek)
{
	char * buffer = (char*)malloc(2);
	if (strcmp(znamenko, "+") == 0)
	{
		free(buffer);
		buffer = (char*)malloc(strlen(a) + strlen(b) + 1);
		strcpy(buffer, a);
		strcat(buffer, b);
		return buffer;
	}
	else if (strcmp(znamenko, "<") == 0)
	{
		if (strcmp(a, b) < 0)
			*vysledek = 1;
		else
			*vysledek = 0;

		return NULL;
	}
	else if (strcmp(znamenko, ">") == 0)
	{
		if (strcmp(a, b) > 0)
			*vysledek = 1;
		else
			*vysledek = 0;

		return NULL;
	}
	else if (strcmp(znamenko, "==") == 0)
	{
		if (strcmp(a, b) == 0)
			*vysledek = 1;
		else
			*vysledek = 0;

		return NULL;
	}
	else if (strcmp(znamenko, "<=") == 0)
	{
		if (strcmp(a, b) <= 0)
			*vysledek = 1;
		else
			*vysledek = 0;

		return NULL;
	}
	else if (strcmp(znamenko, ">=") == 0)
	{
		if (strcmp(a, b) >= 0)
			*vysledek = 1;
		else
			*vysledek = 0;

		return NULL;
	}
	else if (strcmp(znamenko, "!=") == 0)
	{
		if (strcmp(a, b) != 0)
			*vysledek = 1;
		else
			*vysledek = 0;

		return NULL;
	}
	return 0;
}

void Countit(double *a, double *b, char *c)
{
	if (strcmp(c, "+") == 0)
	{
		*a = *a + *b;
	}
	else if (strcmp(c, "-") == 0)
	{
		*a = *a - *b;
	}
	else if (strcmp(c, "*") == 0)
	{
		*a = *a * *b;
	}
	else if (strcmp(c, "/") == 0)
	{
		*a = *a / *b;
	}
	else if (strcmp(c, "<") == 0)
	{
		*a = (*a < *b);
	}
	else if (strcmp(c, ">") == 0)
	{
		*a = (*a > *b);
	}
	else if (strcmp(c, "==") == 0)
	{
		*a = (*a == *b);
	}
	else if (strcmp(c, "<=") == 0)
	{
		*a = (*a <= *b);
	}
	else if (strcmp(c, ">=") == 0)
	{
		*a = (*a >= *b);
	}
	else if (strcmp(c, "!=") == 0)
	{
		*a = (*a != *b);
	}
}

void MakeCmdfill(char * ble, int *err_handler)
{
	char *chr_vysledek;
	double vysledek;
	VarsPtr VarTok;

	char *buf, *cast2, *cast4;
	buf = (char *)malloc(strlen(ble) + 1);
	strcpy(buf, ble);

	cast2 = strtok(buf, " ");  // cmdfill
	cast2 = strtok(NULL, " "); // id
	cast4 = strtok(NULL, " "); // =
	cast4 = strtok(NULL, " "); // expr

	chr_vysledek = SolveString(cast4, &vysledek, err_handler);

	if (*err_handler != 0)
		return;

	VarTok = CreateVarTok("", "", NULL);
	sStackPtr FUNCVARSTACK_TEMP = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(FUNCVARSTACK_TEMP);

	while (ST_TopType(FUNCVARSTACK) != _CHAR && ST_TopType(FUNCVARSTACK) != ST_ER_EMPTY_STACK)
	{
		ST_Pop(FUNCVARSTACK, VarTok);
		if (strcmp(VarTok->desc, cast2) == 0)
		{
			break;
		}
		ST_Push(FUNCVARSTACK_TEMP, VarTok, _VARS);
	}

	if (chr_vysledek == NULL)
	{
		SetVarTok(VarTok, VarTok->type, VarTok->desc, &vysledek);
	}
	else
	{
		SetVarTok(VarTok, VarTok->type, VarTok->desc, chr_vysledek);
	}

	ST_Push(FUNCVARSTACK, VarTok, _VARS);

	while (ST_TopType(FUNCVARSTACK_TEMP) != ST_ER_EMPTY_STACK)
	{
		ST_Pop(FUNCVARSTACK_TEMP, VarTok);
		ST_Push(FUNCVARSTACK, VarTok, _VARS);
	}
	DestroyVarTok(VarTok);
	free(buf);
}

void MakeDef(char * ble, int *err_handler, sStackPtr FUNCVARSTACK)
{
	char *chr_vysledek;
	char *buf;
	double vysledek;
	VarsPtr VarTok;

	char *cast2, *cast3, *cast5;
	buf = (char *)malloc(strlen(ble) + 1);
	strcpy(buf, ble);

	cast2 = strtok(buf, " ");  // def
	cast2 = strtok(NULL, " "); // type
	cast3 = strtok(NULL, " "); // id
	cast5 = strtok(NULL, " "); // =
	cast5 = strtok(NULL, " "); // expr

	chr_vysledek = SolveString(cast5, &vysledek, err_handler);

	if (*err_handler != 0)
		return;

	if (chr_vysledek == NULL)
	{
		// is this right ? cast2 or double
		VarTok = CreateVarTok(cast2, cast3, &vysledek);

		ST_Push(FUNCVARSTACK, VarTok, _VARS);
		DestroyVarTok(VarTok);
	}
	else
	{
		// is this right ? cast2 or string
		VarTok = CreateVarTok(cast2, cast3, chr_vysledek);

		ST_Push(FUNCVARSTACK, VarTok, _VARS);
		DestroyVarTok(VarTok);
	}
	if (chr_vysledek != NULL)
		free(chr_vysledek);


	VarTok = CreateVarTok(cast2, cast3, cast5);
	free(buf);

}

void ResolveCommand(int * err_handler, FcTokPtr tFcTok)
{
	DymTokPtr tDymTok = CreateDymTok("", "");
	VarsPtr VarTok;
	char *chr_vysledek;
	double vysledek;
	char *buf = NULL, *buf2 = NULL;
	char * ble = (char *)malloc(strlen(tFcTok->combcmd.desc) + 1);
	strcpy(ble, tFcTok->combcmd.desc);

	char delimer[20];
	strcpy(delimer, "<");
	ST_Push(FUNCVARSTACK, delimer, _CHAR);

	VarTok = CreateVarTok("", "", NULL);

	while (ST_TopType(FUNCVARBUFFER) != ST_ER_EMPTY_STACK)
	{
		ST_Pop(FUNCVARBUFFER, VarTok);
		ST_Push(FUNCVARSTACK, VarTok, _VARS);
	}
	DestroyVarTok(VarTok);

	char * tokenFC = strchr(ble, ';');
	while (tokenFC != NULL)
	{
		*tokenFC++ = '\0';

		char *cast1, *cast2, *cast3, *cast4, *cast5;
		cast1 = NULL;
		cast2 = NULL;
		cast3 = NULL;
		cast4 = NULL;
		cast5 = NULL;

		char * tempbuf = (char *)malloc(strlen(ble) + 1);
		strcpy(tempbuf, ble);
		char *tempbufptr = strtok(tempbuf, " ");

		if (strcmp(tempbufptr, "def") == 0)
		{
			if (buf != NULL)
				free(buf);
			buf = (char *)malloc(strlen(ble) + 1);
			strcpy(buf, ble);

			cast1 = strtok(buf, " ");  // def
			cast2 = strtok(NULL, " "); // type
			cast3 = strtok(NULL, " "); // id
			cast4 = strtok(NULL, " "); // =
			cast5 = strtok(NULL, " "); // expr

			chr_vysledek = SolveString(cast5, &vysledek, err_handler);

			if (*err_handler != 0)
				return;

			if (chr_vysledek == NULL)
			{
				// is this right ? cast2 or double
				VarTok = CreateVarTok(cast2, cast3, &vysledek);

				ST_Push(FUNCVARSTACK, VarTok, _VARS);
				DestroyVarTok(VarTok);
			}
			else
			{
				// is this right ? cast2 or string
				VarTok = CreateVarTok(cast2, cast3, chr_vysledek);

				ST_Push(FUNCVARSTACK, VarTok, _VARS);
				DestroyVarTok(VarTok);
			}
			if (chr_vysledek != NULL)
				free(chr_vysledek);


			VarTok = CreateVarTok(cast2, cast3, cast5);
			free(buf);

		}
		else if (strcmp(tempbufptr, "dec") == 0)
		{
			if (buf != NULL)
				free(buf);
			buf = (char *)malloc(strlen(ble) + 1);
			strcpy(buf, ble);

			cast1 = strtok(buf, " ");  // dec
			cast2 = strtok(NULL, " "); // type
			cast3 = strtok(NULL, " "); // id

			VarTok = CreateVarTok(cast2, cast3, NULL);

			ST_Push(FUNCVARSTACK, VarTok, _VARS);
			DestroyVarTok(VarTok);
			free(buf);
		}
		else if (strcmp(tempbufptr, "return") == 0)
		{
			if (buf != NULL)
				free(buf);
			buf = (char *)malloc(strlen(ble) + 1);
			strcpy(buf, ble);

			cast2 = strtok(buf, " "); // return
			cast3 = strtok(NULL, ";"); // expr

			chr_vysledek = SolveString(cast3, &vysledek, err_handler);

			if (*err_handler != 0)
				return;

			if (chr_vysledek == NULL)
			{
				if (buf2 != NULL)
					free(buf2);
				buf2 = (char*)malloc(SAVE_FOR_DOUBLE);
				// is this right ? cast2 or double
				sprintf(buf2, "%f", vysledek);
				SetDymTok(tDymTok, "double", buf2);
				ST_Push(EXPRSTACK, tDymTok, _DYMTOK);
				free(buf2);
			}
			else
			{
				SetDymTok(tDymTok, "string", chr_vysledek);
				ST_Push(EXPRSTACK, tDymTok, _DYMTOK);
			}

			VarTok = CreateVarTok("", "", NULL);

			while (ST_TopType(FUNCVARSTACK) != _CHAR && ST_TopType(FUNCVARSTACK) != ST_ER_EMPTY_STACK)
			{
				ST_Pop(FUNCVARSTACK, VarTok);
			}

			if (ST_TopType(FUNCVARSTACK) == _CHAR)
			{
				ST_Pop(FUNCVARSTACK, delimer);
			}

			DestroyVarTok(VarTok);

			if (chr_vysledek != NULL)
				free(chr_vysledek);

			free(buf);

		}
		else if (strcmp(tempbufptr, "cmdfill") == 0)
		{
			MakeCmdfill(ble, err_handler);
			if (*err_handler != 0)
				return;
		}
		else if (strcmp(tempbufptr, "cmdload") == 0)
		{
			if (buf != NULL)
				free(buf);
			buf = (char *)malloc(strlen(ble) + 1);
			strcpy(buf, ble);

			cast1 = strtok(buf, " ");  // cmdload
			cast2 = strtok(NULL, " "); // cin
			cast3 = strtok(NULL, " "); // >>
			cast4 = strtok(NULL, " "); // id
			while (cast3 != NULL)
			{
				VarTok = CreateVarTok("", "", NULL);
				sStackPtr FUNCVARSTACK_TEMP = (sStackPtr)malloc(sizeof(struct sStack));
				ST_InitStack(FUNCVARSTACK_TEMP);

				while (ST_TopType(FUNCVARSTACK) != _CHAR && ST_TopType(FUNCVARSTACK) != ST_ER_EMPTY_STACK)
				{
					ST_Pop(FUNCVARSTACK, VarTok);
					if (strcmp(VarTok->desc, cast4) == 0)
					{
						break;
					}
					ST_Push(FUNCVARSTACK_TEMP, VarTok, _VARS);
				}

				double cin_load = 0;
				char cin_char[CIN_BUFFER_SIZE]; // ssshhhhhh
				char buffer_for_double[SAVE_FOR_DOUBLE];
				if (scanf("%lf", &cin_load) == 1)
				{
					sprintf(buffer_for_double, "%f", cin_load);
					SetVarTok(VarTok, VarTok->type, VarTok->desc, &buffer_for_double);
				}
				else
				{
					if (scanf("%s", cin_char) == 1)
					{
						SetVarTok(VarTok, VarTok->type, VarTok->desc, cin_char);
					}
				}

				ST_Push(FUNCVARSTACK, VarTok, _VARS);

				while (ST_TopType(FUNCVARSTACK_TEMP) != ST_ER_EMPTY_STACK)
				{
					ST_Pop(FUNCVARSTACK_TEMP, VarTok);
					ST_Push(FUNCVARSTACK, VarTok, _VARS);
				}

				cast3 = strtok(NULL, " "); // >>
				cast4 = strtok(NULL, " "); // id
			}
			DestroyVarTok(VarTok);
			free(buf);
		}
		else if (strcmp(tempbufptr, "cmdwrite") == 0)
		{
			if (buf != NULL)
				free(buf);
			buf = (char *)malloc(strlen(ble) + 1);
			strcpy(buf, ble);

			cast1 = strtok(buf, " ");  // cmdwrite
			cast2 = strtok(NULL, " "); // cout
			cast3 = strtok(NULL, " "); // <<
			cast4 = strtok(NULL, " "); // id | expr
			while (cast4 != NULL)
			{
				chr_vysledek = SolveString(cast4, &vysledek, err_handler);

				if (*err_handler != 0)
					return;

				if (chr_vysledek == NULL)
				{
					printf("%lf", vysledek);
				}
				else
				{
					printf("%s", chr_vysledek);
				}

				free(chr_vysledek);
				cast3 = strtok(NULL, " "); // <<
				cast4 = strtok(NULL, " "); // id | expr
			}
			DestroyVarTok(VarTok);
			free(buf);
		}
		else if (strcmp(tempbufptr, "cmdif") == 0)
		{
			if (buf != NULL)
				free(buf);
			buf = (char *)malloc(strlen(ble) + 1);
			strcpy(buf, ble);

			cast1 = strchr(buf, '(');
			*cast1++ = '\0';
			buf = cast1;
			cast1 = strchr(buf, ')');
			*cast1++ = '\0';

			buf = cast1; // { combcmd....

			chr_vysledek = SolveString(cast1, &vysledek, err_handler);

			if (*err_handler != 0)
				return;

			if (chr_vysledek == NULL)
			{
				if (vysledek != 0)
				{
					cast1 = strchr(buf, '{');
					*cast1++ = '\0';
					buf = cast1;
					cast1 = strchr(buf, '}');
					*cast1++ = '\0';
					buf = cast1; // combcmd

					SetDymTok(tDymTok, "combcmd", cast1);

					struct Token tTok;
					
					FcTokPtr var_combcmd = CreateFcTok(tTok, tTok, NULL, tDymTok);

					ResolveCommand(err_handler, var_combcmd);
					if (*err_handler != 0)
						return;

				}
				else
				{
					cast1 = strchr(buf, '{');
					*cast1++ = '\0';
					buf = cast1;
					cast1 = strchr(buf, '}');
					*cast1++ = '\0';
					buf = cast1; // combcmd
					cast1 = strchr(buf, '{');
					*cast1++ = '\0';
					buf = cast1; // else
					cast1 = strchr(buf, '}');
					*cast1++ = '\0';
					buf = cast1; // combcmd

					SetDymTok(tDymTok, "combcmd", cast1);

					struct Token tTok;

					FcTokPtr var_combcmd = CreateFcTok(tTok, tTok, NULL, tDymTok);

					ResolveCommand(err_handler, var_combcmd);
					if (*err_handler != 0)
						return;
					DestroyFcTok(var_combcmd);
				}
			}
			else
			{
				*err_handler = 3;
				return;
			}

			DestroyVarTok(VarTok);
			free(buf);

		}
		else if (strcmp(ble, "cmdfor") == 0)
		{
			if (buf != NULL)
				free(buf);
			buf = (char *)malloc(strlen(ble) + 1);
			strcpy(buf, ble);

			cast1 = strchr(buf, '(');
			*cast1++ = '\0';
			buf = cast1; // for
			cast1 = strchr(buf, ';');
			*cast1++ = '\0';

			buf = cast1; // def v cast1
			MakeDef(cast1, err_handler, FUNCVARSTACK);
			if (*err_handler != 0)
				return;

			cast2 = strchr(buf, ';');
			*cast2++ = '\0';
			buf = cast2; // expr

			cast3 = strchr(buf, ')');
			*cast3++ = '\0';
			buf = cast3; // cmdfill

			cast1 = strchr(buf, '{');
			*cast1++ = '\0';
			buf = cast1; // šrot

			cast4 = strchr(buf, '}'); // opravit zarážku
			*cast4++ = '\0';
			buf = cast4; // combcmd

			chr_vysledek = SolveString(cast2, &vysledek, err_handler);

			if (*err_handler != 0)
				return;
			while (vysledek != 0)
			{
				// vykonavat kód

				SetDymTok(tDymTok, "combcmd", cast4);
				struct Token tTok;

				FcTokPtr var_combcmd = CreateFcTok(tTok, tTok, NULL, tDymTok);

				ResolveCommand(err_handler, var_combcmd);
				if (*err_handler != 0)
					return;
				DestroyFcTok(var_combcmd);

				MakeCmdfill(cast3, err_handler);
				if (*err_handler != 0)
					return;

				free(chr_vysledek);
				chr_vysledek = SolveString(cast2, &vysledek, err_handler);

				if (*err_handler != 0)
					return;
			}
			if (chr_vysledek != NULL)
				free(chr_vysledek);

			VarTok = CreateVarTok(cast2, cast3, NULL);

			ST_Push(FUNCVARSTACK, VarTok, _VARS);
			DestroyVarTok(VarTok);
			free(buf);
		}
		buf = NULL;
		free(tempbuf);
		ble = tokenFC;
		tokenFC = strchr(ble, ';');
	}

	DestroyDymTok(tDymTok);
}

char * SolveString(char * math, double * vysledek, int * error_handler)
{
	sStackPtr StackOper = (sStackPtr)malloc(sizeof(sStackPtr));
	ST_InitStack(StackOper);
	sStackPtr StackValue = (sStackPtr)malloc(sizeof(sStackPtr));
	ST_InitStack(StackValue);
	sStackPtr FUNCTEMPBUFF = (sStackPtr)malloc(sizeof(sStackPtr));
	ST_InitStack(FUNCTEMPBUFF);
	VarsPtr promBuf = CreateVarTok("", "", NULL);
	VarsPtr FOR_TEMPBUFF = CreateVarTok("", "", NULL);
	char delimer[2] = "@";
	char znaminko[3] = "\0";
	bool found;
	bool just_one = true;
	double x1 = 0, x2 = 0;
	char *charx1 = NULL, *charx2 = NULL;
	charx2 = (char*)malloc(600);
	strcpy(charx2, "");
	int int_buf;
	char * buf = (char *)malloc((strlen(math) * 2) + 3);
	char * buf2 = (char *)malloc((strlen(math)*2) + 3);
	strcpy(buf2, "");
	strcpy(buf, math);
	char * ptr = strtok(buf, " ");
	while (ptr != NULL)
	{
		strcat(buf2, " ");
		strcat(buf2, ptr);
		strcat(buf2, " ");
		ptr = strtok(NULL, " ");
	}
	strcpy(buf, buf2);
	int zavorky = 0;
	free(buf2);
	ptr = strtok(buf, " ");
	while (ptr != NULL)
	{
		found = false;

		if (strcmp(ptr, "+") == 0 || strcmp(ptr, "-") == 0 || strcmp(ptr, "*") == 0 || strcmp(ptr, "/") == 0 || strcmp(ptr, "<=") == 0 || strcmp(ptr, ">=") == 0 || strcmp(ptr, "==") == 0 || strcmp(ptr, "!=") == 0)
		{
			ST_Push(StackOper, ptr, _CHAR);
			ptr = strtok(NULL, " ");
			continue;
		}

		if (strcmp(ptr, "(") == 0)
		{
			zavorky++;
			just_one = false;
			if (charx1 != NULL)
				free(charx1);
			charx1 = (char *)malloc(1);
			charx1[0] = '\0';
			ST_Push(StackValue, charx1, _CHAR);
			strcpy(charx1, "");
			ST_Push(StackValue, &x1, _DOUBLE);
			x1 = 0.0;
			ST_Push(StackOper, delimer, _CHAR);

			ptr = strtok(NULL, " ");
			continue;
		}

		if (strcmp(ptr, ")") == 0)
		{
			if (zavorky > 0)
			{
				zavorky--;
				ST_Pop(StackValue, &x2);
				ST_Pop(StackValue, charx2);
				if (ST_TopType(StackOper) == _CHAR)
				{
					ST_Pop(StackOper, znaminko);
				}
					if (strcmp(znaminko, delimer) == 0)
					{
						if (charx1 != NULL)
						{
							free(charx1);
							charx1 = NULL;
						}
					}
					else
					{
						if (strlen(charx2) > 0)
							Charit(charx1, charx2, znaminko, &x1);
						else
							Countit(&x1, &x2, znaminko);
					}
				

				if (ST_TopType(StackOper) == ST_ER_EMPTY_STACK)
				{
					free(buf);
					*vysledek = x1;
					return charx1;
				}
			}
			else
			{
				*vysledek = x1;
				return charx1;
			}

			ptr = strtok(NULL, " ");
			continue;
		}

		if (isdigit(ptr[0]))
		{
			if (just_one)
			{
				x1 = strtod(ptr, NULL);
			}
			else
			{
				
				ST_Pop(StackOper, znaminko);
				if (strcmp(znaminko, "@") == 0)
				{
					x1 = strtod(ptr, NULL);
					ST_Push(StackOper, znaminko, _CHAR);
				}
				else
				{
					x2 = strtod(ptr, NULL);
					Countit(&x1, &x2, znaminko);
				}
			}
		}
		else
		{
			while (strcmp(promBuf->desc, ptr) != 0)
			{
				if (ST_TopType(FUNCVARSTACK) == _CHAR || ST_TopType(FUNCVARSTACK) == ST_ER_EMPTY_STACK)
				{
					found = false;
					while (ST_TopType(FUNCTEMPBUFF) != ST_ER_EMPTY_STACK)
					{
						ST_Pop(FUNCTEMPBUFF, promBuf);
						ST_Push(FUNCVARSTACK, promBuf, _VARS);
					}

					break;
				}
				ST_Pop(FUNCVARSTACK, promBuf);
				if (strcmp(promBuf->desc, ptr) == 0)
				{
					found = true;
					break;
				}
				ST_Push(FUNCTEMPBUFF, promBuf, _VARS);
			}

			while (ST_TopType(FUNCTEMPBUFF) != ST_ER_EMPTY_STACK)
			{
				ST_Pop(FUNCTEMPBUFF, FOR_TEMPBUFF);
				ST_Push(FUNCVARSTACK, FOR_TEMPBUFF, _VARS);
			}

			if (found == true)
			{
				if (promBuf->value == NULL)
				{
					*error_handler = 1;
					//ERROR Použití neinicializované proměnné
				}
				else if (strcmp(promBuf->type, "string") == 0)
				{
					if (just_one == true)
					{
						if (charx1 != NULL)
						{
							free(charx1);
							charx1 = NULL;
						}
						charx1 = (char *)malloc(strlen(promBuf->value) + 1);
						strcpy(charx1, promBuf->value);
					}
					else
					{
						ST_Pop(StackOper, znaminko);

						char * string_result = Charit(charx1, promBuf->value, znaminko, &x1);
						if (string_result == NULL) // vysledek v x1, porovnávání
						{

						}
						else
						{
							if (charx1 != NULL)
							{
								free(charx1);
								charx1 = NULL;
							}
							charx1 = (char *)malloc(strlen(string_result) + 1);
							strcpy(charx1, string_result);
							free(string_result);
						}
					}
					ST_Push(FUNCVARSTACK, promBuf, _VARS);
				}
				else
				{
					if (strcmp(promBuf->type, "int") == 0)
					{
						GetVarVal(promBuf, &int_buf);
						x2 = int_buf;
					}
					else if (strcmp(promBuf->type, "double") == 0)
					{
						GetVarVal(promBuf, &x2);
						if (ST_TopType(StackOper) != ST_ER_EMPTY_STACK)
						{
							ST_Pop(StackOper, znaminko);
							if (strcmp(znaminko, "@") == 0)
							{
								x1 = x2;
								ST_Push(StackOper, znaminko, _CHAR);
							}
							else
							{
								Countit(&x1, &x2, znaminko);
							}
						}
						else
						{
							x1 = x2;
						}
					}
					if (just_one)
					{
						x1 = x2;
					}
				}
			}
			else
			{

				if (ST_TopType(StackOper) == _CHAR)
				{
					ST_Pop(StackOper, znaminko);

					if (strcmp(znaminko, delimer) == 0)
					{
						ST_Push(StackOper, delimer, _CHAR);
						charx1 = (char *)malloc(strlen(ptr) + 1);
						strcpy(charx1, ptr);
						return charx1;
					}
					else
					{
						char * string_result = Charit(charx1, ptr, znaminko, &x1);
						if (string_result == NULL) // vysledek v x1, porovnávání
						{
							*vysledek = x1;
							return NULL;
						}
						else
						{
							if (charx1 != NULL)
							{
								free(charx1);
								charx1 = NULL;
							}
							charx1 = (char *)malloc(strlen(string_result) + 1);
							strcpy(charx1, string_result);
							free(string_result);
						}
					}
				}
				else
				{
					charx1 = (char *)malloc(strlen(ptr) + 1);
					strcpy(charx1, ptr);
					return charx1;
				}
			}
		}

		ptr = strtok(NULL, " ");
	}
	strcpy(buf, math);
	free(buf);

	if (just_one == true)
	{
		*vysledek = x1;
		return charx1;
	}
	return charx1;
}

int iInterpret(int rule, sStackPtr Tokens, int * err_handler)
{

	if (sedi_syntax == false)
	{
		return 0;
	}

	struct Token tTok; // token saved
	struct Token tToknd; // token rules
	struct Token tTokth; // token rules
	struct Token tToktemp;
	DymTokPtr tDymTok, tDymToknd, tDymTokth; // token saved
	tDymTok = CreateDymTok("", "");
	tDymToknd = CreateDymTok("", "");
	tDymTokth = CreateDymTok("", "");

	sStackPtr FCTEMPSTACK = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(FCTEMPSTACK);

	FcTokPtr tFcTok = NULL;
	FcTokPtr tFcTok2;

	VarsPtr VarTok;

	/*struct _Dec dec;
	struct _Def def;
	*/

	char * buf = NULL, *buf2 = NULL;
	double result;
		switch (rule)
	{
	case 0: // dec
		ST_Pop(TYPESTACK, &tTok);
		ST_Pop(Tokens, &tToknd);
		ST_Pop(Tokens, &tToknd);
			
		buf = malloc(strlen(tTok.desc) + strlen(tToknd.desc) + 7);
		strcpy(buf, "");		
		sprintf(buf, " dec %s %s", tTok.desc, tToknd.desc);		
		
		SetDymTok(tDymTok, "dec", buf);

		free(buf);

		ST_Push(DECSTACK, tDymTok, _DYMTOK);
		break;
		// push typů do zásobníku, pushujou se tokeny
	case 1: // def


		ST_Pop(EXPRSTACK, tDymTok);

		ST_Pop(TYPESTACK, &tToknd);
		
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tToktemp);
			if (strcmp(tToktemp.id, "id") == 0)
				memcpy(&tTokth, &tToktemp, sizeof(struct Token));
		}

		buf = (char *)malloc(strlen(tDymTok->desc) + strlen(tToknd.desc) + strlen(tTokth.desc) + 10);

		sprintf(buf, " def %s %s = %s", tToknd.desc, tTokth.desc, tDymTok->desc);

		SetDymTok(tDymTok, "def", buf);
		free(buf);

		ST_Push(DEFSTACK, tDymTok, _DYMTOK);
		break;
	case 2:
	case 3:
	case 4:
	case 5:
		ST_Pop(Tokens, &tTok);
		ST_Push(TYPESTACK, &tTok, _TOKEN);
		break;
	case 6:
	case 7:
	case 8:
	case 9:

		ST_Pop(Tokens, &tTok);

		// save id somehow
		if (strcmp(tTok.id, "id") == 0)
		{
			// this needs to be done better
			SetDymTok(tDymTok, tTok.id, tTok.desc);
			ST_Push(EXPRSTACK, tDymTok, _DYMTOK);
		}
		else
		{
			SetDymTok(tDymTok, tTok.id, tTok.desc);
			ST_Push(EXPRSTACK, tDymTok, _DYMTOK);
		}
		break;
	case 10: // +
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}
		ST_Pop(EXPRSTACK, tDymTok);
		ST_Pop(EXPRSTACK, tDymToknd);
		
		buf = (char *)malloc(strlen(tDymTok->desc) + strlen(tDymToknd->desc) + 8);

		
		if (strcmp(tDymTok->id, "id") == 0 || strcmp(tDymToknd->id, "id") == 0)
		{
			sprintf(buf, "( %s + %s )", tDymTok->desc, tDymToknd->desc);
			SetDymTok(tDymTokth, "id", buf);
			ST_Push(EXPRSTACK, tDymTokth, _DYMTOK);
		}
		else
		{
			result = strtof(tDymTok->desc, NULL) + strtof(tDymToknd->desc, NULL);

			if (strcmp(tDymTok->id, "dl") == 0 || strcmp(tDymToknd->id, "dl") == 0)
			{
				sprintf(buf, "%lf", result);
				SetDymTok(tDymTok, "dl", buf);
			}
			else
			{
				sprintf(buf, "%d", (int)result);
				SetDymTok(tDymTok, "cl", buf);
			}
			ST_Push(EXPRSTACK, tDymTok, _DYMTOK);
		}
		free(buf);
		break;
	case 11: // -
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}
		ST_Pop(EXPRSTACK, tDymTok);
		ST_Pop(EXPRSTACK, tDymToknd);

		buf = (char *)malloc(strlen(tDymTok->desc) + strlen(tDymToknd->desc) + 8);


		if (strcmp(tDymTok->id, "id") == 0 || strcmp(tDymToknd->id, "id") == 0)
		{
			sprintf(buf, "( %s - %s )", tDymTok->desc, tDymToknd->desc);
			SetDymTok(tDymTokth, "id", buf);
			ST_Push(EXPRSTACK, tDymTokth, _DYMTOK);
		}
		else
		{
			result = strtof(tDymTok->desc, NULL) - strtof(tDymToknd->desc, NULL);

			if (strcmp(tDymTok->id, "dl") == 0 || strcmp(tDymToknd->id, "dl") == 0)
			{
				sprintf(buf, "%lf", result);
				SetDymTok(tDymTok, "dl", buf);
			}
			else
			{
				sprintf(buf, "%d", (int)result);
				SetDymTok(tDymTok, "cl", buf);
			}
			ST_Push(EXPRSTACK, tDymTok, _DYMTOK);
		}
		free(buf);
		break;
	case 12: // *
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}
		ST_Pop(EXPRSTACK, tDymTok);
		ST_Pop(EXPRSTACK, tDymToknd);

		buf = (char *)malloc(strlen(tDymTok->desc) + strlen(tDymToknd->desc) + 8);


		if (strcmp(tDymTok->id, "id") == 0 || strcmp(tDymToknd->id, "id") == 0)
		{
			sprintf(buf, "( %s * %s )", tDymTok->desc, tDymToknd->desc);
			SetDymTok(tDymTokth, "id", buf);
			ST_Push(EXPRSTACK, tDymTokth, _DYMTOK);
		}
		else
		{
			result = strtof(tDymTok->desc, NULL) * strtof(tDymToknd->desc, NULL);

			if (strcmp(tDymTok->id, "dl") == 0 || strcmp(tDymToknd->id, "dl") == 0)
			{
				sprintf(buf, "%lf", result);
				SetDymTok(tDymTok, "dl", buf);
			}
			else
			{
				sprintf(buf, "%d", (int)result);
				SetDymTok(tDymTok, "cl", buf);
			}
			ST_Push(EXPRSTACK, tDymTok, _DYMTOK);
		}
		free(buf);
		break;
	case 13: // / 
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}
		ST_Pop(EXPRSTACK, tDymTok);
		ST_Pop(EXPRSTACK, tDymToknd);

		buf = (char *)malloc(strlen(tDymTok->desc) / strlen(tDymToknd->desc) + 8);


		if (strcmp(tDymTok->id, "id") == 0 || strcmp(tDymToknd->id, "id") == 0)
		{
			sprintf(buf, "( %s / %s )", tDymTok->desc, tDymToknd->desc);
			SetDymTok(tDymTokth, "id", buf);
			ST_Push(EXPRSTACK, tDymTokth, _DYMTOK);
		}
		else
		{
			result = strtof(tDymTok->desc, NULL) + strtof(tDymToknd->desc, NULL);

			if (strcmp(tDymTok->id, "dl") == 0 || strcmp(tDymToknd->id, "dl") == 0)
			{
				sprintf(buf, "%lf", result);
				SetDymTok(tDymTok, "dl", buf);
			}
			else
			{
				sprintf(buf, "%d", (int)result);
				SetDymTok(tDymTok, "cl", buf);
			}
			ST_Push(EXPRSTACK, tDymTok, _DYMTOK);
		}
		free(buf);
		break;
	case 14: // <
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}
		ST_Pop(EXPRSTACK, tDymToknd);
		ST_Pop(EXPRSTACK, tDymTok);

		buf = (char *)malloc(strlen(tDymTok->desc) + strlen(tDymToknd->desc) + 8);

		if (strcmp(tDymTok->id, "id") == 0 || strcmp(tDymToknd->id, "id") == 0)
		{
			sprintf(buf, "( %s < %s )", tDymTok->desc, tDymToknd->desc);
			SetDymTok(tDymTokth, "id", buf);
		}
		else if (strcmp(tDymTok->id, "rl") == 0 || strcmp(tDymToknd->id, "rl") == 0)
		{
			result = strcmp(tDymTok->desc, tDymToknd->desc) < 0 ? 1 : 0;
			sprintf(buf, "%d", (int)result);
			SetDymTok(tDymTokth, "cl", buf);
		}
		else
		{
			result = strtof(tDymTok->desc, NULL) < strtof(tDymToknd->desc, NULL);
			sprintf(buf, "%d", (int)result);
			SetDymTok(tDymTokth, "cl", buf);
		}
		ST_Push(EXPRSTACK, tDymTokth, _DYMTOK);
		free(buf);
		break;
	case 15: // >
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}
		ST_Pop(EXPRSTACK, tDymToknd);
		ST_Pop(EXPRSTACK, tDymTok);

		buf = (char *)malloc(strlen(tDymTok->desc) + strlen(tDymToknd->desc) + 8);

		if (strcmp(tDymTok->id, "id") == 0 || strcmp(tDymToknd->id, "id") == 0)
		{
			sprintf(buf, "( %s > %s )", tDymTok->desc, tDymToknd->desc);
			SetDymTok(tDymTokth, "id", buf);
		}
		else if (strcmp(tDymTok->id, "rl") == 0 || strcmp(tDymToknd->id, "rl") == 0)
		{
			result = strcmp(tDymTok->desc, tDymToknd->desc) > 0 ? 1 : 0;
			sprintf(buf, "%d", (int)result);
			SetDymTok(tDymTokth, "cl", buf);
		}
		else
		{
			result = strtof(tDymTok->desc, NULL) > strtof(tDymToknd->desc, NULL);
			sprintf(buf, "%d", (int)result);
			SetDymTok(tDymTokth, "cl", buf);
		}
		ST_Push(EXPRSTACK, tDymTokth, _DYMTOK);
		free(buf);
		break;
	case 16: // ==
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}
		ST_Pop(EXPRSTACK, tDymToknd);
		ST_Pop(EXPRSTACK, tDymTok);

		buf = (char *)malloc(strlen(tDymTok->desc) + strlen(tDymToknd->desc) + 9);

		if (strcmp(tDymTok->id, "id") == 0 || strcmp(tDymToknd->id, "id") == 0)
		{
			sprintf(buf, "( %s == %s )", tDymTok->desc, tDymToknd->desc);
			SetDymTok(tDymTokth, "id", buf);
		}
		else if (strcmp(tDymTok->id, "rl") == 0 || strcmp(tDymToknd->id, "rl") == 0)
		{
			result = strcmp(tDymTok->desc, tDymToknd->desc) == 0 ? 1 : 0;
			sprintf(buf, "%d", (int)result);
			SetDymTok(tDymTokth, "cl", buf);
		}
		else
		{
			result = strtof(tDymTok->desc, NULL) == strtof(tDymToknd->desc, NULL);
			sprintf(buf, "%d", (int)result);
			SetDymTok(tDymTokth, "cl", buf);
		}
		ST_Push(EXPRSTACK, tDymTokth, _DYMTOK);
		free(buf);
		break;
	case 17: // <=
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}
		ST_Pop(EXPRSTACK, tDymToknd);
		ST_Pop(EXPRSTACK, tDymTok);

		buf = (char *)malloc(strlen(tDymTok->desc) + strlen(tDymToknd->desc) + 8);

		if (strcmp(tDymTok->id, "id") == 0 || strcmp(tDymToknd->id, "id") == 0)
		{
			sprintf(buf, "( %s <= %s )", tDymTok->desc, tDymToknd->desc);
			SetDymTok(tDymTokth, "id", buf);
		}
		else if (strcmp(tDymTok->id, "rl") == 0 || strcmp(tDymToknd->id, "rl") == 0)
		{
			result = strcmp(tDymTok->desc, tDymToknd->desc) <= 0 ? 1 : 0;
			sprintf(buf, "%d", (int)result);
			SetDymTok(tDymTokth, "cl", buf);
		}
		else
		{
			result = strtof(tDymTok->desc, NULL) <= strtof(tDymToknd->desc, NULL);
			sprintf(buf, "%d", (int)result);
			SetDymTok(tDymTokth, "cl", buf);
		}
		ST_Push(EXPRSTACK, tDymTokth, _DYMTOK);
		free(buf);
		break;
	case 18: // >=
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}
		ST_Pop(EXPRSTACK, tDymToknd);
		ST_Pop(EXPRSTACK, tDymTok);

		buf = (char *)malloc(strlen(tDymTok->desc) + strlen(tDymToknd->desc) + 8);

		if (strcmp(tDymTok->id, "id") == 0 || strcmp(tDymToknd->id, "id") == 0)
		{
			sprintf(buf, "( %s >= %s )", tDymTok->desc, tDymToknd->desc);
			SetDymTok(tDymTokth, "id", buf);
		}
		else if (strcmp(tDymTok->id, "rl") == 0 || strcmp(tDymToknd->id, "rl") == 0)
		{
			result = strcmp(tDymTok->desc, tDymToknd->desc) >= 0 ? 1 : 0;
			sprintf(buf, "%d", (int)result);
			SetDymTok(tDymTokth, "cl", buf);
		}
		else
		{
			result = strtof(tDymTok->desc, NULL) >= strtof(tDymToknd->desc, NULL);
			sprintf(buf, "%d", (int)result);
			SetDymTok(tDymTokth, "cl", buf);
		}
		ST_Push(EXPRSTACK, tDymTokth, _DYMTOK);
		free(buf);
		break;
	case 19: // !=
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}
		ST_Pop(EXPRSTACK, tDymToknd);
		ST_Pop(EXPRSTACK, tDymTok);

		buf = (char *)malloc(strlen(tDymTok->desc) + strlen(tDymToknd->desc) + 8);

		if (strcmp(tDymTok->id, "id") == 0 || strcmp(tDymToknd->id, "id") == 0)
		{
			sprintf(buf, "( %s != %s )", tDymTok->desc, tDymToknd->desc);
			SetDymTok(tDymTokth, "id", buf);
		}
		else if (strcmp(tDymTok->id, "rl") == 0 || strcmp(tDymToknd->id, "rl") == 0)
		{
			result = strcmp(tDymTok->desc, tDymToknd->desc) != 0 ? 1 : 0;
			sprintf(buf, "%d", (int)result);
			SetDymTok(tDymTokth, "cl", buf);
		}
		else
		{
			result = strtof(tDymTok->desc, NULL) != strtof(tDymToknd->desc, NULL);
			sprintf(buf, "%d", (int)result);
			SetDymTok(tDymTokth, "cl", buf);
		}
		ST_Push(EXPRSTACK, tDymTokth, _DYMTOK);
		free(buf);
		break;
	case 25:
	case 26:
	case 20:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}
		ST_Pop(EXPRSTACK, tDymToknd);
		ST_Pop(EXPRSTACK, tDymTok);

		buf = (char*)malloc(strlen(tDymTok->desc) + strlen(tDymToknd->desc) + 8);

		sprintf(buf, " %s , %s ", tDymTok->desc, tDymToknd->desc);
	
		SetDymTok(tDymTokth, "exprlist", buf);
		free(buf);
		ST_Push(EXPRSTACK, tDymTokth, _DYMTOK);
		break;
	case 21: // volání fce
	case 22: // volání fce
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
			if (strcmp(tTok.id, "id") == 0)
				memcpy(&tTokth, &tTok, sizeof(struct Token));
		}
		//tTokth = id
		ST_Pop(EXPRSTACK, tDymTok);
		int x;
		char * buf_for_concat = NULL;
		if (strcmp(tTokth.desc, "length") == 0)
		{
			x = lenght(tDymTok->desc);
			char buf_for_len[255];
			sprintf(buf_for_len, "%d", x);
			SetDymTok(tDymTok, "expr", buf_for_len);
			ST_Push(EXPRSTACK, tDymTok, _DYMTOK);
		}
		else if (strcmp(tTokth.desc, "concat") == 0)
		{
			if (buf != NULL)
				free(buf);
			buf = (char *)malloc(strlen(tDymTok->desc) + 1);
			strcpy(buf, tDymTok->desc);
			char * param1, *param2;
			
			param1 = strtok(buf, " ,");
			param2 = strtok(NULL, " ,");
			buf_for_concat = concat(param1, param2);
			SetDymTok(tDymTok, "expr", buf_for_concat);
			ST_Push(EXPRSTACK, tDymTok, _DYMTOK);
			free(buf_for_concat);
			strcpy(buf, tDymTok->desc);
			free(buf);
			//strtok(tDy)
		}
		else if (strcmp(tTokth.desc, "substr") == 0)
		{
			if (buf != NULL)
				free(buf);
			buf = (char *)malloc(strlen(tDymTok->desc) + 1);
			strcpy(buf, tDymTok->desc);
			char * param1, *param2, *param3;

			param1 = strtok(buf, " ,");
			param2 = strtok(NULL, " ,");
			param3 = strtok(NULL, " ,");

			int pos = atoi(param2);
			int n = atoi(param3);

			buf_for_concat = substr(param1, pos, n);
			SetDymTok(tDymTok, "expr", buf_for_concat);
			ST_Push(EXPRSTACK, tDymTok, _DYMTOK);
			free(buf_for_concat);
			strcpy(buf, tDymTok->desc);
			free(buf);
		}
		else if (strcmp(tTokth.desc, "find") == 0)
		{
			if (buf != NULL)
				free(buf);
			buf = (char *)malloc(strlen(tDymTok->desc) + 1);
			strcpy(buf, tDymTok->desc);
			char * param1, *param2;

			param1 = strtok(buf, " ,");
			param2 = strtok(NULL, " ,");
			x = find(param1, param2);
			char buf_for_len[255];
			sprintf(buf_for_len, "%d", x);
			SetDymTok(tDymTok, "expr", buf_for_len);
			ST_Push(EXPRSTACK, tDymTok, _DYMTOK);
			free(buf_for_concat);
			strcpy(buf, tDymTok->desc);
			free(buf);
		}
		else if (strcmp(tTokth.desc, "sort") == 0)
		{
			buf_for_concat = sort(tDymTok->desc);
			SetDymTok(tDymTok, "expr", buf_for_concat);
			ST_Push(EXPRSTACK, tDymTok, _DYMTOK);
			free(buf_for_concat);
		}
		else
		{
			tFcTok = CreateFcTok(tTok, tTok, NULL, NULL);
			tFcTok2 = CreateFcTok(tTok, tTok, NULL, NULL);

			while (ST_TopType(FCSTACK) != ST_ER_EMPTY_STACK)
			{
				ST_Pop(FCSTACK, tFcTok);
				if (strcmp(tFcTok->id.desc, tTokth.desc) == 0)
					break;
				ST_Push(FCTEMPSTACK, tFcTok, _FCTOK);
			}

			while (ST_TopType(FCTEMPSTACK) != ST_ER_EMPTY_STACK)
			{
				ST_Pop(FCTEMPSTACK, tFcTok);
				ST_Push(FCSTACK, tFcTok2, _FCTOK);
			}

			DestroyFcTok(tFcTok2);
			buf = tFcTok->dec.desc;
			//tFcTok->dec.desc

			buf2 = tDymTok->desc;
			double vysledek;
			char * chr_vysledek;
			char * tokenFC = strchr(buf, ',');
			char * tokenDT = strchr(buf2, ',');
			if (tokenFC == NULL)
			{
				chr_vysledek = SolveString(buf2, &vysledek, err_handler);

				if (*err_handler != 0)
					return 0;
				char * done = strtok(buf, " ");

				done = strtok(NULL, " ");
				done = strtok(NULL, " ");
				if (chr_vysledek == NULL)
				{
					VarTok = CreateVarTok("double", done, &vysledek);

					ST_Push(FUNCVARBUFFER, VarTok, _VARS);
					DestroyVarTok(VarTok);
				}
				else
				{
					VarTok = CreateVarTok("string", done, chr_vysledek);

					ST_Push(FUNCVARBUFFER, VarTok, _VARS);
					DestroyVarTok(VarTok);
				}
				if (chr_vysledek != NULL)
					free(chr_vysledek);
			}
			else
			{
				while (tokenFC != NULL)
				{
					*tokenFC++ = '\0';
					*tokenDT++ = '\0';
					strtok(buf, " "); // dec
					char * ptr3 = strtok(NULL, " "); // typ
					ptr3 = strtok(NULL, " "); // id

					chr_vysledek = SolveString(buf2, &vysledek, err_handler);

					if (*err_handler != 0)
						return 0;
					if (chr_vysledek == NULL)
					{
						VarTok = CreateVarTok("double", ptr3, &vysledek);

						ST_Push(FUNCVARBUFFER, VarTok, _VARS);
						DestroyVarTok(VarTok);
					}
					else
					{
						VarTok = CreateVarTok("string", ptr3, chr_vysledek);

						ST_Push(FUNCVARBUFFER, VarTok, _VARS);
						DestroyVarTok(VarTok);
					}
					if (chr_vysledek != NULL)
						free(chr_vysledek);

					buf = tokenFC;
					buf2 = tokenDT;
					tokenFC = strchr(buf, ',');
					tokenDT = strchr(buf2, ',');
				}
			}
			if (strlen(buf) > 0)
			{
				strtok(buf, " "); // dec
				char * ptr3 = strtok(NULL, " "); // typ
				ptr3 = strtok(NULL, " "); // id

				chr_vysledek = SolveString(buf2, &vysledek, err_handler);

				if (*err_handler != 0)
					return 0;
				if (chr_vysledek == NULL)
				{
					VarTok = CreateVarTok("double", ptr3, &vysledek);

					ST_Push(FUNCVARBUFFER, VarTok, _VARS);
					DestroyVarTok(VarTok);
				}
				else
				{
					VarTok = CreateVarTok("string", ptr3, chr_vysledek);

					ST_Push(FUNCVARBUFFER, VarTok, _VARS);
					DestroyVarTok(VarTok);
				}
				if (chr_vysledek != NULL)
					free(chr_vysledek);

			}
			ResolveCommand(err_handler, tFcTok);

			if (*err_handler != 0)
				return 0;
		}
		if (tFcTok != NULL)
			DestroyFcTok(tFcTok);
		break;
		case 23:
			while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
			{
				ST_Pop(Tokens, &tTok);
				if (strcmp(tTok.id, "id") == 0)
					memcpy(&tTokth, &tTok, sizeof(struct Token));
			}
			//tTokth = id
			
		
				tFcTok = CreateFcTok(tTok, tTok, NULL, NULL);
				tFcTok2 = CreateFcTok(tTok, tTok, NULL, NULL);

				while (ST_TopType(FCSTACK) != ST_ER_EMPTY_STACK)
				{
					ST_Pop(FCSTACK, tFcTok);
					if (strcmp(tFcTok->id.desc, tTokth.desc) == 0)
						break;
					ST_Push(FCTEMPSTACK, tFcTok, _FCTOK);
				}

				while (ST_TopType(FCTEMPSTACK) != ST_ER_EMPTY_STACK)
				{
					ST_Pop(FCTEMPSTACK, tFcTok);
					ST_Push(FCSTACK, tFcTok2, _FCTOK);
				}

				DestroyFcTok(tFcTok2);
				buf = tFcTok->dec.desc;
				//tFcTok->dec.desc

				buf2 = tDymTok->desc;
				double vysledek;
				char * chr_vysledek;
				char * tokenFC = strchr(buf, ',');
				char * tokenDT = strchr(buf2, ',');
				if (tokenFC == NULL)
				{
					chr_vysledek = SolveString(buf2, &vysledek, err_handler);

					if (*err_handler != 0)
						return 0;
					char * done = strtok(buf, " ");
					done = strtok(NULL, " ");
					done = strtok(NULL, " ");
					if (chr_vysledek == NULL)
					{
						VarTok = CreateVarTok("double", done, &vysledek);

						ST_Push(FUNCVARBUFFER, VarTok, _VARS);
						DestroyVarTok(VarTok);
					}
					else
					{
						VarTok = CreateVarTok("string", done, chr_vysledek);

						ST_Push(FUNCVARBUFFER, VarTok, _VARS);
						DestroyVarTok(VarTok);
					}
					if (chr_vysledek != NULL)
						free(chr_vysledek);
				}
				else
				{
					while (tokenFC != NULL)
					{
						*tokenFC++ = '\0';
						*tokenDT++ = '\0';
						strtok(buf, " "); // dec
						char * ptr3 = strtok(NULL, " "); // typ
						ptr3 = strtok(NULL, " "); // id

						chr_vysledek = SolveString(buf2, &vysledek, err_handler);

						if (*err_handler != 0)
							return 0;
						if (chr_vysledek == NULL)
						{
							VarTok = CreateVarTok("double", ptr3, &vysledek);

							ST_Push(FUNCVARBUFFER, VarTok, _VARS);
							DestroyVarTok(VarTok);
						}
						else
						{
							VarTok = CreateVarTok("string", ptr3, chr_vysledek);

							ST_Push(FUNCVARBUFFER, VarTok, _VARS);
							DestroyVarTok(VarTok);
						}
						if (chr_vysledek != NULL)
							free(chr_vysledek);

						buf = tokenFC;
						buf2 = tokenDT;
						tokenFC = strchr(buf, ',');
						tokenDT = strchr(buf2, ',');
					}
				}
				if (strlen(buf) > 0)
				{
					strtok(buf, " "); // dec
					char * ptr3 = strtok(NULL, " "); // typ
					ptr3 = strtok(NULL, " "); // id

					chr_vysledek = SolveString(buf2, &vysledek, err_handler);

					if (*err_handler != 0)
						return 0;
					if (chr_vysledek == NULL)
					{
						VarTok = CreateVarTok("double", ptr3, &vysledek);

						ST_Push(FUNCVARBUFFER, VarTok, _VARS);
						DestroyVarTok(VarTok);
					}
					else
					{
						VarTok = CreateVarTok("string", ptr3, chr_vysledek);

						ST_Push(FUNCVARBUFFER, VarTok, _VARS);
						DestroyVarTok(VarTok);
					}
					if (chr_vysledek != NULL)
						free(chr_vysledek);

				}
				ResolveCommand(err_handler, tFcTok);

				if (*err_handler != 0)
					return 0;
			
			if (tFcTok != NULL)
				DestroyFcTok(tFcTok);
			break;
	case 24: break; // nothing to be done
	case 27:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}
		// ST_Pop(EXPRSTACK, &tToknd);
		ST_Pop(EXPRSTACK, tDymTok);
		buf = (char *)malloc(strlen(tDymTok->desc) + 4);
		sprintf(buf, " %s ;", tDymTok->desc);
		SetDymTok(tDymTok, "cmd", buf);
		ST_Push(CMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		// Maybe bad
		break;
	case 28:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}
		// ST_Pop(EXPRSTACK, &tToknd);
		ST_Pop(EXPRSTACK, tDymToknd);
		ST_Pop(CMDSTACK, tDymTok);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTok->desc) + 5);

		sprintf(buf, " %s %s ;", tDymTok->desc, tDymToknd->desc);
		
		SetDymTok(tDymTokth, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTokth, _DYMTOK);
		free(buf);
		// Maybe bad

		break;
	case 29:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}
		// ST_Pop(EXPRSTACK, &tToknd);
		
		ST_Pop(COMBCMDSTACK, tDymTok);
		ST_Pop(CMDSTACK, tDymToknd);

		buf = (char *)malloc(strlen(tDymTok->desc) + strlen(tDymToknd->desc) + 5);

		sprintf(buf, " %s %s ;", tDymToknd->desc, tDymTok->desc);

		SetDymTok(tDymTokth, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTokth, _DYMTOK);
		free(buf);
		// Maybe bad

		break;

	case 30:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
			if (strcmp(tTok.id, "id") == 0)
			{
				SetTokenByToken(&tToknd, tTok);
			}
		}

		ST_Pop(TYPESTACK, &tTok);
		
		ST_Pop(DECSTACK, tDymTok);
		ST_Pop(COMBCMDSTACK, tDymToknd);
		

		tFcTok = CreateFcTok(tTok, tToknd, tDymTok, tDymToknd);
		ST_Push(FCSTACK, tFcTok, _FCTOK);
		DestroyFcTok(tFcTok);
		//ST_Pop()

		break;
	case 31:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
			if (strcmp(tTok.id, "id") == 0)
			{
				SetTokenByToken(&tToknd, tTok);
			}
		}

		ST_Pop(TYPESTACK, &tTok);

		ST_Pop(DECSTACK, tDymTok);
		ST_Pop(CMDSTACK, tDymToknd);


		tFcTok = CreateFcTok(tTok, tToknd, tDymTok, tDymToknd);
		ST_Push(FCSTACK, tFcTok, _FCTOK);
		DestroyFcTok(tFcTok);
		//ST_Pop()
		break;
	case 32:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
			if (strcmp(tTok.id, "id") == 0)
			{
				SetTokenByToken(&tToknd, tTok);
			}
		}

		ST_Pop(TYPESTACK, &tTok);

		
		ST_Pop(CMDSTACK, tDymToknd);

		SetDymTok(tDymTok, "", "");
			
		tFcTok = CreateFcTok(tTok, tToknd, tDymTok, tDymToknd);
		ST_Push(FCSTACK, tFcTok, _FCTOK);
		DestroyFcTok(tFcTok);
		//ST_Pop()

		break;
	case 33:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
			if (strcmp(tTok.id, "id") == 0)
			{
				SetTokenByToken(&tToknd, tTok);
			}
		}

		ST_Pop(TYPESTACK, &tTok);


		ST_Pop(COMBCMDSTACK, tDymToknd);

		SetDymTok(tDymTok, "", "");

		tFcTok = CreateFcTok(tTok, tToknd, tDymTok, tDymToknd);
		ST_Push(FCSTACK, tFcTok, _FCTOK);
		DestroyFcTok(tFcTok);
		//ST_Pop()
		break;
	case 34:
	case 35:
		break; // nothing to do
	case 36:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
			if (strcmp(tTok.id, "id") == 0)
			{
				SetTokenByToken(&tToknd, tTok);
			}
		}

		ST_Pop(TYPESTACK, &tTok);

		ST_Pop(DECLISTSTACK, tDymTok);
		ST_Pop(COMBCMDSTACK, tDymToknd);


		tFcTok = CreateFcTok(tTok, tToknd, tDymTok, tDymToknd);
		ST_Push(FCSTACK, tFcTok, _FCTOK);
		DestroyFcTok(tFcTok);
		//ST_Pop()
		
		break;
	case 37:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
			if (strcmp(tTok.id, "id") == 0)
			{
				SetTokenByToken(&tToknd, tTok);
			}
		}

		ST_Pop(TYPESTACK, &tTok);

		ST_Pop(DECLISTSTACK, tDymTok);
		ST_Pop(CMDSTACK, tDymToknd);

		tFcTok = CreateFcTok(tTok, tToknd, tDymTok, tDymToknd);
		ST_Push(FCSTACK, tFcTok, _FCTOK);
		DestroyFcTok(tFcTok);
		//ST_Pop()

		
		break;
	case 38:
	case 39:
		break; // nothing to do
	case 40:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(DECSTACK, tDymToknd);
		ST_Pop(DECSTACK, tDymTokth);
		
		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 5);
		sprintf(buf, " %s , %s", tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "declist", buf);
		ST_Push(DECLISTSTACK, tDymTok, _DYMTOK);
		free(buf);		
		break;
	case 41:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(DECLISTSTACK, tDymTokth);
		ST_Pop(DECSTACK, tDymToknd);

		buf = (char *)malloc(strlen(tDymTokth->desc) + strlen(tDymToknd->desc) + 5);
		sprintf(buf, " %s , %s", tDymToknd->desc, tDymTokth->desc);
		SetDymTok(tDymTok, "declist", buf);
		ST_Push(DECLISTSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 42:
	case 44:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(CMDSTACK, tDymToknd);
		ST_Push(COMBCMDSTACK, tDymToknd, _DYMTOK);

		break;
	case 43:
	case 45:
		break; // nothing to do
	case 46:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(DEFSTACK, tDymToknd);

		buf = (char *)malloc(strlen(tDymToknd->desc) + 3);
		sprintf(buf, "%s ;", tDymToknd->desc);
		SetDymTok(tDymTok, "cmd", buf);
		ST_Push(CMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 47:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(DEFSTACK, tDymToknd);
		ST_Pop(CMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 4);
		sprintf(buf, "%s %s ;", tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 48:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(DEFSTACK, tDymToknd);
		ST_Pop(COMBCMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 4);
		sprintf(buf, "%s %s ;", tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTok, _DYMTOK);
		free(buf);

		break;
	case 49:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(DECSTACK, tDymToknd);

		buf = (char *)malloc(strlen(tDymToknd->desc) + 3);
		sprintf(buf, "%s ;", tDymToknd->desc);
		SetDymTok(tDymTok, "cmd", buf);
		ST_Push(CMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 50:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(DECSTACK, tDymToknd);
		ST_Pop(CMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 4);
		sprintf(buf, "%s %s ;", tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 51:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(DECSTACK, tDymToknd);
		ST_Pop(COMBCMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 4);
		sprintf(buf, "%s %s ;", tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTok, _DYMTOK);
		free(buf);

		break;
	case 52:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(CMDFILLSTACK, tDymToknd);

		buf = (char *)malloc(strlen(tDymToknd->desc) + 3);
		sprintf(buf, "%s ;", tDymToknd->desc);
		SetDymTok(tDymTok, "cmd", buf);
		ST_Push(CMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 53:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(CMDFILLSTACK, tDymToknd);
		ST_Pop(CMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 4);
		sprintf(buf, "%s %s ;", tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 54:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(CMDFILLSTACK, tDymToknd);
		ST_Pop(COMBCMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 4);
		sprintf(buf, "%s %s ;", tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTok, _DYMTOK);
		free(buf);

		break;
	case 55:
		ST_Pop(EXPRSTACK, tDymTok);

		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tToktemp);
			if (strcmp(tToktemp.id, "id") == 0)
				memcpy(&tTokth, &tToktemp, sizeof(struct Token));
		}

		buf = (char *)malloc(strlen(tDymTok->desc) + strlen(tTokth.desc) + 13);

		sprintf(buf, " cmdfill %s = %s", tTokth.desc, tDymTok->desc);

		SetDymTok(tDymTok, "cmdfill", buf);
		free(buf);

		ST_Push(CMDFILLSTACK, tDymTok, _DYMTOK);
		break;
	case 56:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(CMDIFSTACK, tDymToknd);
		ST_Push(COMBCMDSTACK, tDymToknd, _DYMTOK);

		break;
	case 57:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(CMDIFSTACK, tDymToknd);
		ST_Pop(CMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 2);
		sprintf(buf, "%s %s", tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 58:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(CMDIFSTACK, tDymToknd);
		ST_Pop(COMBCMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 2);
		sprintf(buf, "%s %s", tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 59:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(CMDFORSTACK, tDymToknd);
		ST_Push(COMBCMDSTACK, tDymToknd, _DYMTOK);
		break;
	case 60:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}
				
		// OPREVA TOTO NENI DYMTOK ALE CMDFOR STRUKTURA !! OPRAVIT I OSTATNÍ
		//ST_Pop(CMDFORSTACK, aaaa);
		ST_Pop(CMDFORSTACK, tDymToknd);
		ST_Pop(CMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 2);
		sprintf(buf, "%s %s", tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 61:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(CMDFORSTACK, tDymToknd);
		ST_Pop(COMBCMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 2);
		sprintf(buf, "%s %s", tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 62:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(EXPRSTACK, tDymTok);
		ST_Pop(COMBCMDSTACK, tDymToknd);
		ST_Pop(COMBCMDSTACK, tDymTokth);
		
		buf = (char *)malloc(strlen(tDymTok->desc) + strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 26);
		sprintf(buf, " if ( %s ) { %s } else { %s } ;", tDymTok->desc, tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "cmdif", buf);

		ST_Push(CMDIFSTACK, tDymTok, _DYMTOK);
		free(buf);
		//ST_Pop()
		break;
	case 63:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(EXPRSTACK, tDymTok);
		ST_Pop(COMBCMDSTACK, tDymToknd);
		ST_Pop(CMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymTok->desc) + strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 26);
		sprintf(buf, " if ( %s ) { %s } else { %s } ;", tDymTok->desc, tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "cmdif", buf);

		ST_Push(CMDIFSTACK, tDymTok, _DYMTOK);
		free(buf);
		//ST_Pop()
		break;
	case 64:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(EXPRSTACK, tDymTok);
		ST_Pop(CMDSTACK, tDymToknd);
		ST_Pop(COMBCMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymTok->desc) + strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 26);
		sprintf(buf, " if ( %s ) { %s } else { %s } ;", tDymTok->desc, tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "cmdif", buf);

		ST_Push(CMDIFSTACK, tDymTok, _DYMTOK);
		free(buf);
		//ST_Pop()
		break;
	case 65:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(EXPRSTACK, tDymTok);
		ST_Pop(CMDSTACK, tDymToknd);
		ST_Pop(CMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymTok->desc) + strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 26);
		sprintf(buf, " if ( %s ) { %s } else { %s } ;", tDymTok->desc, tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "cmdif", buf);

		ST_Push(CMDIFSTACK, tDymTok, _DYMTOK);
		free(buf);
		//ST_Pop()
		break;
	case 66:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(CMDFILLSTACK, tDymTok);
		ST_Pop(COMBCMDSTACK, tDymToknd);
		ST_Pop(COMBCMDSTACK, tDymTokth);
		
		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTok->desc) + strlen(tDymTokth->desc) + 18);
		sprintf(buf, " for ( %s %s ) { %s } ;", tDymTokth->desc, tDymTok->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "cmdfor", buf);

		ST_Push(CMDFORSTACK, tDymTok, _DYMTOK);
		free(buf);
		//ST_Pop()
		break;
	case 67:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(CMDFILLSTACK, tDymTok);
		ST_Pop(CMDSTACK, tDymToknd);
		ST_Pop(COMBCMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTok->desc) + strlen(tDymTokth->desc) + 18);
		sprintf(buf, " for ( %s %s ) { %s } ;", tDymTokth->desc, tDymTok->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "combcmd", buf);

		ST_Push(CMDFORSTACK, tDymTok, _DYMTOK);
		free(buf);
		//ST_Pop()
		break;
	case 68:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(RETSTACK, tDymToknd);

		buf = (char *)malloc(strlen(tDymToknd->desc) + 3);
		sprintf(buf, "%s ;", tDymToknd->desc);
		SetDymTok(tDymTok, "cmd", buf);
		ST_Push(CMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 69:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(RETSTACK, tDymToknd);
		ST_Pop(CMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 4);
		sprintf(buf, "%s %s ;", tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 70:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(RETSTACK, tDymToknd);
		ST_Pop(COMBCMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 4);
		sprintf(buf, "%s %s ;", tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 84:
	case 71:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(EXPRSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymTokth->desc) + 8);
		sprintf(buf, "return %s", tDymTokth->desc);
		SetDymTok(tDymTok, "cmdret", buf);
		ST_Push(RETSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 72:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(LOADSTACK, tDymToknd);

		buf = (char *)malloc(strlen(tDymToknd->desc) + 3);
		sprintf(buf, "%s ;", tDymToknd->desc);
		SetDymTok(tDymTok, "cmd", buf);
		ST_Push(CMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 73:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(LOADSTACK, tDymToknd);
		ST_Pop(CMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 4);
		sprintf(buf, "%s %s ;", tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 74:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(LOADSTACK, tDymToknd);
		ST_Pop(COMBCMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 4);
		sprintf(buf, "%s %s ;", tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 75:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
			if (strcmp(tTok.id, "id") == 0)
				memcpy(&tTokth, &tTok, sizeof(struct Token));
		}

		buf = (char *)malloc(strlen(tTok.desc) + 17);
		
		sprintf(buf, " cmdload cin >> %s", tTok.desc);
		SetDymTok(tDymTok, "cmdload", buf);
		ST_Push(LOADSTACK, tDymTok, _DYMTOK);
		free(buf);

		break;
	case 76:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
			if (strcmp(tTok.id, "id") == 0)
				memcpy(&tTokth, &tTok, sizeof(struct Token));
		}
		ST_Pop(LOADSTACK, tDymToknd);

		buf = (char *)malloc(strlen(tTok.desc) + strlen(tDymToknd->desc) + 6);

		sprintf(buf, " %s >> %s", tDymToknd->desc, tTok.desc);
		SetDymTok(tDymTok, "cmdload", buf);
		ST_Push(LOADSTACK, tDymTok, _DYMTOK);
		free(buf);


		break;

	case 77:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(WRITESTACK, tDymToknd);

		buf = (char *)malloc(strlen(tDymToknd->desc) + 3);
		sprintf(buf, "%s ;", tDymToknd->desc);
		SetDymTok(tDymTok, "cmd", buf);
		ST_Push(CMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 78:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(WRITESTACK, tDymToknd);
		ST_Pop(CMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 4);
		sprintf(buf, "%s %s ;", tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 79:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(WRITESTACK, tDymToknd);
		ST_Pop(COMBCMDSTACK, tDymTokth);

		buf = (char *)malloc(strlen(tDymToknd->desc) + strlen(tDymTokth->desc) + 4);
		sprintf(buf, "%s %s ;", tDymTokth->desc, tDymToknd->desc);
		SetDymTok(tDymTok, "combcmd", buf);
		ST_Push(COMBCMDSTACK, tDymTok, _DYMTOK);
		free(buf);
		break;
	case 80:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
			if (strcmp(tTok.id, "id") == 0)
				memcpy(&tTokth, &tTok, sizeof(struct Token));
		}

		buf = (char *)malloc(strlen(tTok.desc) + 19);

		sprintf(buf, " cmdwrite cout << %s", tTok.desc);
		SetDymTok(tDymTok, "cmdwrite", buf);
		ST_Push(WRITESTACK, tDymTok, _DYMTOK);
		free(buf);

		break;
	case 81:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
			if (strcmp(tTok.id, "id") == 0)
				memcpy(&tTokth, &tTok, sizeof(struct Token));
		}
		ST_Pop(WRITESTACK, tDymToknd);

		buf = (char *)malloc(strlen(tTok.desc) + strlen(tDymToknd->desc) + 6);

		sprintf(buf, " %s << %s", tDymToknd->desc, tTok.desc);
		SetDymTok(tDymTok, "cmdwrite", buf);
		ST_Push(WRITESTACK, tDymTok, _DYMTOK);
		free(buf);


		break;

	case 82:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}

		ST_Pop(EXPRSTACK, tDymTok);

		buf = (char *)malloc(strlen(tDymTok->desc) + 19);

		sprintf(buf, " cmdwrite cout << %s", tDymTok->desc);
		SetDymTok(tDymTok, "cmdwrite", buf);
		ST_Push(WRITESTACK, tDymTok, _DYMTOK);
		free(buf);

		break;
	case 83:
		while (ST_TopType(Tokens) != ST_ER_EMPTY_STACK)
		{
			ST_Pop(Tokens, &tTok);
		}
		ST_Pop(WRITESTACK, tDymToknd);
		ST_Pop(EXPRSTACK, tDymTok);

		buf = (char *)malloc(strlen(tTok.desc) + strlen(tDymToknd->desc) + 6);

		sprintf(buf, " %s << %s", tDymToknd->desc, tDymTok->desc);
		SetDymTok(tDymTok, "cmdwrite", buf);
		ST_Push(WRITESTACK, tDymTok, _DYMTOK);
		free(buf);
		break;

	default:
		break;
	}
	ST_DisposeStack(FCTEMPSTACK);
	DestroyDymTok(tDymTok);
	DestroyDymTok(tDymToknd);
	DestroyDymTok(tDymTokth);
	return 0;
}
