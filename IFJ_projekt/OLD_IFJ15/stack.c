/*

FILE: stack.c
Authors: Luk� Ch�bek

*/
#include "stack.h"

static sStacksForMemoryFreePtr _stacks_for_memory_free_exit = NULL;

int ST_InitStack(sStackPtr S)
{
	int ret;
	if (_stacks_for_memory_free_exit == NULL)
	{
		_stacks_for_memory_free_exit = (sStacksForMemoryFreePtr)malloc(sizeof(struct sStacksForMemoryFree));
		_stacks_for_memory_free_exit->Top = NULL;
	}

	/*
	S = (sStackPtr)malloc(sizeof(struct sStack));
	if (S == NULL)
		return ST_ER_NOT_ENOUGH_MEMORY;
		*/
	S->Top = NULL;
	ret = ST_STACKMEMFREEPUSH(S);
	
	return ret;
}

int ST_STACKMEMFREEPUSH(sStackPtr S)
{
	sStacksForMemoryFreeItemPtr temp = (sStacksForMemoryFreeItemPtr)malloc(sizeof(struct sStacksForMemoryFreeItem));
	if (temp == NULL)
		return ST_ER_NOT_ENOUGH_MEMORY;

	temp->next = _stacks_for_memory_free_exit->Top;

	temp->_safe = S;

	_stacks_for_memory_free_exit->Top = temp;

	return ST_OK;
}

int ST_STACKMEMFREEPOP(sStackPtr S)
{	
	sStacksForMemoryFreeItemPtr temp = _stacks_for_memory_free_exit->Top;
	sStacksForMemoryFreeItemPtr temp_nd;

	if (temp == NULL)
		return ST_ER_EMPTY_STACK;

	if (temp->_safe == S)
	{
		temp_nd = temp;
		temp = temp->next;
		free(temp_nd);
		_stacks_for_memory_free_exit->Top = temp;
		temp_nd = NULL;
		return ST_OK;
	}

	while (temp != NULL)
	{
		if (temp->_safe == S)
			break;
		temp_nd = temp;
		temp = temp->next;
	}
	
	if (temp == NULL)
		return ST_ER_EMPTY_STACK;
	
	temp_nd->next = temp_nd->next->next;
	free(temp);
	temp = NULL;
	return ST_OK;
}

int ST_GetTopItemSize(sStackPtr S)
{
	if (S->Top == NULL)
		return -1;
	// determine type
	switch (S->Top->type)
	{
	case _CHAR:
		return (strlen((char *)S->Top->data) + 1);
		break;
	case _INT:
		return sizeof(int);
		break;
	case _TOKEN:
		return sizeof(struct Token);
		break;
	case _DOUBLE:
		return sizeof(double);
		break;
	/*case _DEF:
		return sizeof(struct _Def);
		break;
	case _DEC:
		return sizeof(struct _Dec);
		break;
		*/
	case _DYMTOK:
		return sizeof(struct DymTok);
		break;
	case _FCTOK:
		return sizeof(struct FcTok);
		break;
	case _CMDIF:
		return sizeof(struct CmdIf);
		break;
	case _CMDFOR:
		return sizeof(struct CmdFor);
		break;
	case _VARS:
		return sizeof(struct Vars);
		break;
	}
	return -1;
}

int ST_TopType(sStackPtr S)
{
	if (S->Top == NULL)
		return ST_ER_EMPTY_STACK;
	return S->Top->type;
}

int ST_TopBackType(sStackPtr S, int count)
{
	sStackElemPtr temp = S->Top;
	for (int i = 0; i < count; i++)
	{
		if (temp == NULL)
		{
			return ST_ER_EMPTY_STACK;
		}
		temp = temp->ptr;
	}

	if (temp == NULL)
	{
		return ST_ER_EMPTY_STACK;
	}

	return temp->type;
}

// count 0 = ST_Top
int ST_LookBehind(sStackPtr S, void * val, int count)
{

	sStackElemPtr temp = S->Top;
	if (temp == NULL)
		return ST_ER_EMPTY_STACK;

	for (int i = 0; i < count; i++)
	{
		if (temp == NULL)
		{
			return ST_ER_EMPTY_STACK;
		}
		temp = temp->ptr;
	}

	if (temp == NULL)
	{
		return ST_ER_EMPTY_STACK;
	}

	switch (temp->type)
	{
	case _INT:
		(*(int*)val) = (*(int*)temp->data);
		break;
	case _DOUBLE:
		(*(double*)val) = (*(double*)temp->data);
		break;
	case _CHAR:
		strcpy((char *)val, (char *)temp->data);
		break;
	case _TOKEN:
		memcpy(val, temp->data, sizeof(struct Token));
		/*
		strcpy(((struct Token*)val)->desc, ((struct Token*)temp->data)->desc);
		strcpy(((struct Token*)val)->id, ((struct Token*)temp->data)->id);
		*/
		break;/*
	case _DEC:
		memcpy(val, temp->data, sizeof(struct _Dec));
		break;
	case _DEF:
		memcpy(val, temp->data, sizeof(struct _Def));
		break;*/
	case _DYMTOK:
		GetDymTok(val, temp->data);
		break;
	case _FCTOK:
		CopyFcTok(val, temp->data);
		break;
	case _CMDFOR:
		return ST_ER_WRONG_STACK;
		break;
	case _CMDIF:
		return ST_ER_WRONG_STACK;
		break;
	case _VARS:
		GetVarTok(val, temp->data);
		break;
	}
	return ST_OK;
}

int ST_Top(sStackPtr S, void * val)
{
	if (val == NULL)
	{
		return ST_ER_NO_BUFFER;
	}
	
	if (S->Top == NULL)
	{
		return ST_ER_EMPTY_STACK;
	}
	// TODO: Secure size
	switch (S->Top->type)
	{
	case _INT:
		(*(int*)val) = (*(int*)S->Top->data);
		break;
	case _DOUBLE:
		(*(double*)val) = (*(double*)S->Top->data);
		break;
	case _CHAR:
		strcpy((char *)val, (char *)S->Top->data);
		break;
	case _TOKEN:
		memcpy(val, S->Top->data, sizeof(struct Token));
		break;
/*	case _DEC:
		memcpy(val, S->Top->data, sizeof(struct _Dec));
		break;
	case _DEF:
		memcpy(val, S->Top->data, sizeof(struct _Def));
		break;*/
	case _DYMTOK:
		GetDymTok(val, S->Top->data);
		break;
	case _FCTOK:
		CopyFcTok(val, S->Top->data);
		break;
	case _CMDIF:
		CopyCmdIf(val, S->Top->data);
		break;
	case _CMDFOR:
		CopyCmdFor(val, S->Top->data);
		break;
	case _VARS:
		GetVarTok(val, S->Top->data);
		break;
	}
	return ST_OK;
}

int ST_Push(sStackPtr S, void * val, int type)
{
	sStackElemPtr temp = (sStackElemPtr)malloc(sizeof(struct sStackElem));
	if (temp == NULL)
	{
		//Error();
		return ST_ER_NOT_ENOUGH_MEMORY;
	}

	if (val == NULL)
	{
		return ST_ER_NO_BUFFER;
	}

	switch (type)
	{
	case _CHAR:
		temp->data = (char *)malloc(strlen((char *)val) + 1);
		strcpy((char *)temp->data, (char *)val);
		break;
	case _INT:
		temp->data = (int *)malloc(sizeof(int));
		*((int *)temp->data) = *(int*)val;
		break;
	case _DOUBLE:
		temp->data = (double *)malloc(sizeof(double));
		*((double *)temp->data) = *(double*)val;
		break;
	case _TOKEN:
		temp->data = (struct Token *)malloc(sizeof(struct Token));
		memcpy(temp->data, val, sizeof(struct Token));
		break;
/*	case _DEC:
		temp->data = (struct _DEC *)malloc(sizeof(struct _Dec));
		memcpy(temp->data, val, sizeof(struct _Dec));
		break;
	case _DEF:
		temp->data = (struct _DEF *)malloc(sizeof(struct _Def));
		memcpy(temp->data, val, sizeof(struct _Def));
		break;
	*/case _DYMTOK:
		temp->data = CreateDymTok("", "");
		GetDymTok(temp->data, val);
		break;
	case _FCTOK:
		temp->data = CreateFcTok(((FcTokPtr)val)->type, ((FcTokPtr)val)->id, &((FcTokPtr)val)->dec, &((FcTokPtr)val)->combcmd);
		break;
	case _CMDIF:
		temp->data = CreateCmdIf(&((CmdIfPtr)val)->expr, &((CmdIfPtr)val)->combcmd, &((CmdIfPtr)val)->combcmdnd);
		break;
	case _CMDFOR:
		temp->data = CreateCmdFor(&((CmdForPtr)val)->cmdfill, &((CmdForPtr)val)->combcmd, &((CmdForPtr)val)->combcmdnd);
		break;
	case _VARS:
		temp->data = CreateVarTok(((VarsPtr)val)->type, ((VarsPtr)val)->desc, (((VarsPtr)val)->value));
		break;
	}
	temp->type = type;
	temp->ptr = S->Top;
	S->Top = temp;
	return ST_OK;
}

int ST_Pop(sStackPtr S, void * val) 
{

	if (S == NULL)
	{
		return ST_ER_NO_STACK;
	}
	if (val == NULL)
	{

		return ST_ER_NO_BUFFER;
	}

	ST_Top(S, val);

		free(S->Top->data);

	sStackElemPtr temp = S->Top;
	S->Top = S->Top->ptr;
	free(temp);

	return ST_OK;
}

int ST_PopBack(sStackPtr S, void * val, int counter)
{
	if (S == NULL)
	{
		return ST_ER_NO_STACK;
	}
	if (val == NULL)
	{

		return ST_ER_NO_BUFFER;
	}

	ST_LookBehind(S, val, counter);

	sStackElemPtr temp = S->Top;
	sStackElemPtr temp_nd = S->Top;
	if (temp == NULL)
		return ST_ER_EMPTY_STACK;

	for (int i = 0; i < counter; i++)
	{
		if (temp == NULL)
		{
			return ST_ER_EMPTY_STACK;
		}
		temp_nd = temp;
		temp = temp->ptr;
	}

	if (temp == NULL)
	{
		return ST_ER_EMPTY_STACK;
	}


	free(temp->data);

	temp_nd->ptr = temp->ptr;
	free(temp);

	return ST_OK;
}
int ST_SwapStackForInt(sStackPtr S)
{
	if (ST_TopType(S) == ST_ER_EMPTY_STACK || ST_TopType(S) == _CHAR || ST_TopType(S) == _TOKEN)
		return ST_ER_WRONG_STACK;

	sStackPtr temp = (sStackPtr)malloc(sizeof(struct sStack));
	ST_InitStack(temp);
	int a;
	while (ST_TopType(S) != ST_ER_EMPTY_STACK && ST_TopType(S) == _INT)
	{
		ST_Pop(S, &a);
		ST_Push(temp, &a, _INT);
	}

	if (ST_TopType(S) != ST_ER_EMPTY_STACK)
		return ST_ER_WRONG_STACK;

	while (ST_TopType(temp) != ST_ER_EMPTY_STACK && ST_TopType(temp) == _INT)
	{
		ST_Pop(temp, &a);
		ST_Push(S, &a, _INT);
	}

	ST_DisposeStack(temp);
	return ST_OK;
}

int ST_ClearItems(sStackPtr S)
{
	if (S == NULL)
		return ST_ER_NO_STACK;

	if (S->Top == NULL)
		return ST_OK;

	sStackElemPtr temp;
	while (S->Top != NULL)
	{
		temp = S->Top->ptr;
		if (S->Top->type == _DYMTOK)
		{
			DestroyDymTok(S->Top->data);
		}
		else if (S->Top->type == _FCTOK)
		{
			DestroyFcTok(S->Top->data);
		}
		else if (S->Top->type == _CMDIF)
		{
			DestroyCmdIf(S->Top->data);
		}
		else if (S->Top->type == _CMDFOR)
		{
			DestroyCmdFor(S->Top->data);
		}
		else if (S->Top->type == _VARS)
		{
			DestroyVarTok(S->Top->data);
		}
		else
			free(S->Top->data);
		free(S->Top);
		S->Top = temp;
	}

	return ST_OK;
}

int ST_DisposeStack(sStackPtr S)
{
	if (S == NULL)
		return ST_OK;

	ST_ClearItems(S);
	ST_STACKMEMFREEPOP(S);
	free(S);
	S = NULL;
	return ST_OK;
}

int ST_DisposeAll()
{
	sStacksForMemoryFreeItemPtr temp = _stacks_for_memory_free_exit->Top;
	int ret;
	while (temp != NULL)
	{
		ret = ST_DisposeStack(temp->_safe);
		if (ret != ST_OK) // Something went wrong, incomming memory leak
			return ret;
		free(temp);
		temp = temp->next;
	}
	free(_stacks_for_memory_free_exit);
	return ret;
}

bool ST_IsEmpty(sStackPtr S)
{
	return (S->Top == NULL);
}
