/*

FILE: rules.h
Authors: Ji�� �erm�k, Luk� Ch�bek

*/
#ifndef RULES_H_
#define RULES_H_
char *rules[] = {
	"dec type id", //  0
	"def type id = expr", // 1 
	"type auto", // 2 
	"type double", // 3 
	"type int", // 4 
	"type string", // 5 
	"expr cl", // 6 
	"expr dl", // 7 
	"expr rl", // 8 
	"expr id", // 9 
	"expr expr + expr", // 10 
	"expr expr - expr", // 11 
	"expr expr * expr", // 12 
	"expr expr / expr", // 13 
	"expr expr < expr", // 14 
	"expr expr > expr", // 15 
	"expr expr == expr", // 16 
	"expr expr <= expr", // 17 
	"expr expr >= expr", // 18 
	"expr expr != expr", // 19
	"expr expr , expr", // 20 
	"expr id ( expr )", // volani fce // 21 
	"expr id ( exprlist )", // volani fce // 22 
	"expr id ( )", // volani fce prazdne // 23 
	"expr ( expr )", // 24 
	"exprlist expr , expr", // 25 
	"exprlist exprlist , expr", // 26 
	"cmd expr ;", // 27 
	"combcmd cmd expr ;", // 28 
	"combcmd combcmd expr ;", // 29 
	"fcdef type id ( dec ) { combcmd }", // 30 
	"fcdef type id ( dec ) { cmd }", // 31 
	"fcdef type id ( ) { cmd }", // 32 
	"fcdef type id ( ) { combcmd }", // 33 
	"fcdec_n type id ( dec )", // 34 
	"fcdec_n type id ( )", // 35 
	"fcdef type id ( declist ) { combcmd }", // 36 
	"fcdef type id ( declist ) { cmd }", // 37 
	"fcdec_n type id ( declist )", // 38 
	"fcdec fcdec_n ;",	 // 39 
	"declist dec , dec", // 40 
	"declist declist , dec",	 // 41 
	"combcmd { cmd }", // 42 
	"combcmd { combcmd }", // 43 
	"combcmd cmd", // 44 
	"combcmd combcmd",	 // 45 
	"cmd def ;", // 46 
	"combcmd cmd def ;", // 47 
	"combcmd combcmd def ;",	 // 48 
	"cmd dec ;", // 49 
	"combcmd cmd dec ;", // 50 
	"combcmd combcmd dec ;", // 51 
	"cmd cmdfill ;", // 52 
	"combcmd cmd cmdfill ;", // 53 
	"combcmd combcmd cmdfill ;", // 54 
	"cmdfill id = expr", // 55 
	"combcmd cmdif", // 56 
	"combcmd cmd cmdif", // 57 
	"combcmd combcmd cmdif", // 58 
	"combcmd cmdfor", // 59 
	"combcmd cmd cmdfor", // 60 
	"combcmd combcmd cmdfor", // 61 
	"cmdif if ( expr ) { combcmd } else { combcmd }", // 62 
	"cmdif if ( expr ) { cmd } else { combcmd }", // 63 
	"cmdif if ( expr ) { combcmd } else { cmd }", // 64 
	"cmdif if ( expr ) { cmd } else { cmd }", // 65 
	"cmdfor for ( combcmd cmdfill ) { combcmd }", // 66 
	"cmdfor for ( combcmd cmdfill ) { cmd }", // 67 
	"cmd cmdret ;", // 68 
	"combcmd cmd cmdret ;", // 69 
	"combcmd combcmd cmdret ;", // 70 
	"cmdret return expr", // 71 
	"cmd cmdload ;", // 72 
	"combcmd cmd cmdload ;", // 73 
	"combcmd combcmd cmdload ;", // 74 
	"cmdload cin >> id", // 75 
	"cmdload cmdload >> id", // 76 
	"cmd cmdwrite ;", // 77 
	"combcmd cmd cmdwrite ;", // 78 
	"combcmd combcmd cmdwrite ;", // 79 
	"cmdwrite cout << id", // 80 
	"cmdwrite cmdwrite << id", // 81 
	"cmdwrite cout << expr", // 82 
	"cmdwrite cmdwrite << expr", // 83
	"cmdret return ( expr )", // 84
	"e" // 85
};
#endif