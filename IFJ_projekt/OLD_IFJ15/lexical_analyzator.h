/*

FILE: lexical_analyzator.h
Authors: Luk� Ch�bek, Ji�� �erm�k

*/
#ifndef LEXICAL_ANALYZATOR_H_
#define LEXICAL_ANALYZATOR_H_



enum
{
	NULLSTATE,
	/*SEMICOL,  		// ;
	BRACKETL, 		// (
	BRACKETR, 		// )
	CURLYBRACKETL,	// {
	CURLYBRACKETR,	// }
	EQ,				// =*/
	SENDTO,			// <<
	SENDFROM,		// >>
					/*PLUS,			// +
					MINUS,			// -
					MULTIPLY,		// ? ( * )
					DIVIDE,			// ? ( / )
					MODULO,			// %
					AMPERSANT,		// &
					COMMENT,		// //*/
	NUMBER,			// 0-9
	CHAR, 			// a-Z + _
					//DOT,			// .
	SURESTRING,		// NUMBER + CHAR
	FLOAT_1,		// xxx.xxx
	FLOAT_2,		// xxx.xxx
	FLOAT_EXP_1,
	FLOAT_EXP_2,
	UNCONDITIONAL_SURESTRING,
	UNICODE_1,
	UNICODE_2,
	POSSIBLE_COMMENT,
	LINE_COMMENT,
	MULTILINE_COMMENT,
	POSSIBLE_MULTILINE_COMMENT_END,
	POSSIBLE_COMPARE,
	POSSIBLE_NON_EQUAL,
	POSSIBLE_LESS_EQUAL,
	POSSIBLE_MORE_EQUAL
};

int iLexicalAnal(FILE * fileSourceCode, struct Token * tTok, unsigned int * line);
void LexTypeCheck(struct Token * tTok);
#endif
