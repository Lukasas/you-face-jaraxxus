/*

FILE: error_handler.c
Authors: Luk� Ch�bek

*/

#include "error_handler.h"

void MessageHandler(char * cMessage, ...)
{
	va_list args;
	va_start(args, cMessage);
	vfprintf(stdout, cMessage, args);
	va_end(args);
}

void ErrorHandler(int iError, char * cMessage, ...)
{
	va_list args;
	va_start(args, cMessage);
	fprintf(stderr, "(%d) ", iError);
	vfprintf(stderr, cMessage, args);
	fprintf(stderr, "\n");
	va_end(args);
}
