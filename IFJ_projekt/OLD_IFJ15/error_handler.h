/*

FILE: error_handler.h
Authors: Luk� Ch�bek

*/
#ifndef ERROR_HANDLER_H_
#define ERROR_HANDLER_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

void ErrorHandler(int iError, char * cMessage, ...);
void MessageHandler(char * cMessage, ...);

#endif