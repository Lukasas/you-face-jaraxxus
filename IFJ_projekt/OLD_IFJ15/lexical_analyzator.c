/*

FILE: lexical_analyzator.c
Authors: Luk� Ch�bek, Anna �orbov�, Ji�� �erm�k,  Jakub Chleb�k, Adam Huspenina

*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "token.h"
#include "lexical_analyzator.h"



int iLexicalAnal(FILE * fileSourceCode, struct Token * tTok, unsigned int * line)
{
	int state = NULLSTATE;
	bool dont_move = false, found = false;
	int currChar = -1;

	// unicode
	int hexa;

	SetToken(tTok, "", "");
	for (int i = 0; !found;)
	{
		currChar = fgetc(fileSourceCode);

		if (currChar == -1)
			return -1;

		switch (state)
		{
		case NULLSTATE:
			if (currChar == '"')
			{
				state = SURESTRING;
			}
			else if (currChar == '*')
			{
				//dont_move = true; // causing problem ? why is this here ?
				found = true;
				tTok->id[0] = '*';
				tTok->id[1] = '\0';
			}
			else if (currChar == '+')
			{
				//dont_move = true; // causing problem ? why is this here ?
				found = true;
				tTok->id[0] = '+';
				tTok->id[1] = '\0';
			}
			else if (currChar == '-')
			{
				//dont_move = true;
				found = true;
				tTok->id[0] = '-';
				tTok->id[1] = '\0';
			}
			else if (currChar == '<')
			{
				tTok->id[0] = '<';
				state = POSSIBLE_LESS_EQUAL;
			}
			else if (currChar == '>')
			{
				tTok->id[0] = '>';
				state = POSSIBLE_MORE_EQUAL;
			}
			else if (currChar == '=')
			{
				tTok->id[0] = '=';
				state = POSSIBLE_COMPARE;
			}
			else if (currChar == '!')
			{
				tTok->id[0] = '!';
				state = POSSIBLE_NON_EQUAL;
			}
			else if (currChar == '\n')
			{
				++(*line);
			}
			else if (currChar == '/')
			{
				state = POSSIBLE_COMMENT;
			}
			else if (isalpha(currChar) || currChar == '_')
			{
				state = CHAR; // CHAR
				tTok->desc[i++] = currChar;
			}
			else if (isdigit(currChar))
			{
				state = NUMBER;
				tTok->desc[i++] = currChar;
			}
			else if (currChar == ';')
			{
				strcpy(tTok->id, ";\0");
				found = true;
			}
			else if (currChar == ',')
			{
				strcpy(tTok->id, ",\0");
				found = true;
			}
			else if (currChar == '(')
			{
				//dont_move = true; // causing problem ?
				strcpy(tTok->id, "(\0");
				found = true;
			}
			else if (currChar == ')')
			{
				//dont_move = true; // causing problem ?
				strcpy(tTok->id, ")\0");
				found = true;
			}
			else if (currChar == '{')
			{
				//dont_move = true; // causing problem ?
				strcpy(tTok->id, "{\0");
				found = true;
			}
			else if (currChar == '}')
			{
				//dont_move = true; // causing problem ?
				strcpy(tTok->id, "}\0");
				found = true;
			}
			else if (isspace(currChar))
			{
				// nothing to do here...	
			}
			else
			{
				return 1;
			}
			break;
		case POSSIBLE_MORE_EQUAL: // >
			if (currChar == '=')
			{
				//dont_move = true;
				tTok->id[1] = '=';
				tTok->id[2] = '\0';
			}
			else if (currChar == '>')
			{
				//dont_move = true;
				tTok->id[1] = '>';
				tTok->id[2] = '\0';
			}
			else
			{
				dont_move = true;
				tTok->id[1] = '\0';
			}
			found = true;
			break;
		case POSSIBLE_LESS_EQUAL: // <
			if (currChar == '=')
			{
				tTok->id[1] = '=';
				tTok->id[2] = '\0';
			}
			else if (currChar == '<')
			{
				tTok->id[1] = '<';
				tTok->id[2] = '\0';
			}
			else
			{
				dont_move = true;
				tTok->id[1] = '\0';
			}
			found = true;
			break;
		case POSSIBLE_COMPARE:
		case POSSIBLE_NON_EQUAL:
			if (currChar == '=')
			{
				//dont_move = true;
				tTok->id[1] = '=';
				tTok->id[2] = '\0';
			}
			else
			{
				dont_move = true;
				tTok->id[1] = '\0';
			}
			found = true;
			break;
		case POSSIBLE_COMMENT:
			if (currChar == '*')
			{
				state = MULTILINE_COMMENT;
			}
			else if (currChar == '/')
			{
				state = LINE_COMMENT;
			}
			else
			{
				tTok->id[0] = '/';
				tTok->id[1] = '\0';
				found = true;
			}
			break;
		case LINE_COMMENT:
			if (currChar == '\n')
			{
				++(*line);
				state = NULLSTATE;
			}
			break;
		case MULTILINE_COMMENT:
			if (currChar == '\n')
			{
				++(*line);
			}
			else if (currChar == '*')
			{
				state = POSSIBLE_MULTILINE_COMMENT_END;
			}
			break;
		case POSSIBLE_MULTILINE_COMMENT_END:
			if (currChar == '\n')
			{
				++(*line);
				state = MULTILINE_COMMENT;
			}
			else if (currChar == '/')
			{
				state = NULLSTATE;
			}
			else
			{
				state = MULTILINE_COMMENT;
			}
			break;
		case CHAR:
			if (isalpha(currChar) || isdigit(currChar) || currChar == '_')
			{
				tTok->desc[i++] = currChar;
			}
			else if (!isspace(currChar) && !isalpha(currChar) ) // ADDED )
			{
				dont_move = true;
				found = true;
				tTok->desc[i] = '\0';
				strcpy(tTok->id, "id");
				LexTypeCheck(tTok);
			}
			else if (isspace(currChar)) // ADDED )
			{
				found = true;
				tTok->desc[i] = '\0';
				strcpy(tTok->id, "id");
				LexTypeCheck(tTok);
			}
			else
			{
				return 1;
			}
			if (!isspace(currChar) && !isalpha(currChar)) {
				dont_move = true;
			}
			break;
		case NUMBER:
			if (isdigit(currChar))
			{
				tTok->desc[i++] = currChar;
			}
			else if (isspace(currChar) ||
				currChar == ';' ||
				currChar == ',' ||
				currChar == '=' ||
				currChar == '(' ||
				currChar == ')' ||
				currChar == '+' ||
				currChar == '-' ||
				currChar == '%' ||
				currChar == '*' ||
				currChar == '/')
			{
				dont_move = true;
				found = true;
				tTok->desc[i] = '\0';
				strcpy(tTok->id, "cl");
			}
			else if (currChar == 'E' || currChar == 'e')
			{
				state = FLOAT_EXP_1;
				tTok->desc[i++] = 'e';
			}
			else if (currChar == '.')
			{
				state = FLOAT_1;
				tTok->desc[i++] = '.';
			}
			else
			{
				return 1;
			}
			break;
		case FLOAT_1:
			if (isdigit(currChar))
			{
				tTok->desc[i++] = currChar;
				state = FLOAT_2;
			}
			else
			{
				return 1;
			}
			break;

		case FLOAT_2:
			if (isdigit(currChar))
			{
				tTok->desc[i++] = currChar;
			}
			else if (isspace(currChar) ||
				currChar == ';' ||
				currChar == ',' ||
				currChar == '=' ||
				currChar == '(' ||
				currChar == ')' ||
				currChar == '+' ||
				currChar == '-' ||
				currChar == '%' ||
				currChar == '*' ||
				currChar == '/')
			{
				found = true;
				tTok->desc[i] = '\0';
				strcpy(tTok->id, "dl");
			}
			else if (currChar == 'E' || currChar == 'e')
			{
				state = FLOAT_EXP_1;
				tTok->desc[i++] = 'e';
			}
			else
			{
				return 1;
			}
			break;
		case FLOAT_EXP_1:
			if (isdigit(currChar))
			{
				tTok->desc[i++] = currChar;
				state = FLOAT_EXP_2;
			}
			else
			{
				return 1;
			}
			break;
		case FLOAT_EXP_2:
			if (isdigit(currChar))
			{
				tTok->desc[i++] = currChar;
				state = FLOAT_2;
			}
			else if (isspace(currChar) ||
				currChar == ';' ||
				currChar == ',' ||
				currChar == '=' ||
				currChar == '(' ||
				currChar == ')' ||
				currChar == '+' ||
				currChar == '-' ||
				currChar == '%' ||
				currChar == '*' ||
				currChar == '/')
			{
				found = true;
				tTok->desc[i] = '\0';
				strcpy(tTok->id, "dl");
			}
			else
			{
				return 1;
			}
			break;
		case SURESTRING:
			if (currChar == '"')
			{
				found = true;
				tTok->desc[i] = '\0';
				strcpy(tTok->id, "rl");
			}
			else if (currChar == '\\')
			{
				state = UNCONDITIONAL_SURESTRING;
			}
			else
			{
				tTok->desc[i++] = currChar;
			}
			break;
		case UNCONDITIONAL_SURESTRING:	//TODO: dorobit
			if (currChar == 'n')
			{
				tTok->desc[i++] = '\n';
			}
			else if (currChar == 't')
			{
				tTok->desc[i++] = '\t';
			}
			else if (currChar == 'x')
			{
				state = UNICODE_1;
			}
			else
			{
				tTok->desc[i++] = currChar;
			}
			state = SURESTRING;
			break;
		case UNICODE_1: // \xFF
			if (isxdigit(currChar))
			{
				if (isdigit(currChar))
				{
					hexa = 16 * (currChar - '0');
				}
				else if (currChar >= 'a' && currChar <= 'f')
				{
					hexa = 16 * ((currChar - 'a') + 10);
				}
				else if (currChar >= 'A' && currChar <= 'F')
				{
					hexa = 16 * ((currChar - 'A') + 10);
				}
				else
				{
					return 1;
				}
				state = UNICODE_2;
			}
			break;
		case UNICODE_2:
			if (isxdigit(currChar))
			{
				if (isdigit(currChar))
				{
					hexa += (currChar - '0');
				}
				else if (currChar >= 'a' && currChar <= 'f')
				{
					hexa += ((currChar - 'a') + 10);
				}
				else if (currChar >= 'A' && currChar <= 'F')
				{
					hexa += ((currChar - 'A') + 10);
				}
				else
				{
					return 1;
				}
				state = SURESTRING;
				tTok->desc[i++] = hexa;

			}
			break;
		default: break;
		}
		// for end
		if (dont_move)
		{
			fseek(fileSourceCode, -1, SEEK_CUR);
			dont_move = false;
		}
	}
	return 0;
}// trust me i am an engineer