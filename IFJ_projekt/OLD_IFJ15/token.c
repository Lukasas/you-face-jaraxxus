/*

FILE: token.c
Authors: Luk� Ch�bek

*/
#include "token.h"


void EraseToken(struct Token * tTok)
{
	strcpy(tTok->id, "");
	strcpy(tTok->desc, "");
}

void SetToken(struct Token * tTok, char * id, char * desc)
{
	strcpy(tTok->id, id);
	strcpy(tTok->desc, desc);
}

void SetTokenByToken(struct Token * tTok, struct Token tToknd)
{	
	memcpy(tTok, &tToknd, sizeof(struct Token));
}

DymTokPtr CreateDymTok(char * id, char * desc) // POUZE TAKTO VYTV��ET DYNAMICK� TOKENY
{
	DymTokPtr tok = (DymTokPtr)malloc(sizeof(struct DymTok));
	tok->desc = (char *)malloc(strlen(desc) + 1);
	strcpy(tok->id, id);
	strcpy(tok->desc, desc);
	return tok;
}

VarsPtr CreateVarTok(char * type, char * desc, void * value) // POUZE TAKTO VYTV��ET DYNAMICK� TOKENY
{
	VarsPtr tok = (VarsPtr)malloc(sizeof(struct Vars));

	if (strcmp(type, "string") == 0)
	{
		if (value != NULL)
		{
			tok->value = (char *)malloc(strlen(value) + 1);
			strcpy(tok->value, value);
		}
		else
		{
			tok->value = NULL;
		}
	}
	else if (strcmp(type, "int") == 0)
	{
		if (value != NULL)
		{
			tok->value = (int *)malloc(sizeof(int));
			*((int*)(tok->value)) = *((int*)value);
		}
		else
		{
			tok->value = NULL;
		}
	}
	else if (strcmp(type, "double") == 0)
	{
		if (value != NULL)
		{
			tok->value = (double *)malloc(sizeof(double));
			*((double*)(tok->value)) = *((double*)value);
		}
		else
		{
			tok->value = NULL;
		}
	}
	else if (value == NULL)
	{
		tok->value = NULL;
	}
	
	strcpy(tok->type, type);
	strcpy(tok->desc, desc);
	return tok;
}

void SetVarTok(VarsPtr a, char * type, char * desc, void * value)
{
	free(a->value);
	if (strcmp(type, "string") == 0)
	{
		a->value = (char *)malloc(strlen(value) + 1);
		strcpy(a->value, value);
	}
	else if (strcmp(type, "int") == 0)
	{
		a->value = (int *)malloc(sizeof(int));
		*((int*)(a->value)) = *((int*)value);
	}
	else if (strcmp(type, "double") == 0)
	{
		a->value = (double *)malloc(sizeof(double));
		*((double*)(a->value)) = *((double*)value);
	}

	strcpy(a->type, type);
	strcpy(a->desc, desc);
}

void GetVarVal(VarsPtr a, void * value)
{
	if (strcmp(a->type, "string") == 0)
	{
		strcpy(value, a->value);
	}
	else if (strcmp(a->type, "int") == 0)
	{
		*((int*)value) = *((int*)a->value);
	}
	else if (strcmp(a->type, "double") == 0)
	{
		*((double*)value) = *((double*)a->value);
	}
}

void GetVarTok(VarsPtr a, VarsPtr b)
{
	free(a->value);

	if (strcmp(b->type, "string") == 0)
	{
		if (b->value != NULL)
		{
			a->value = (char *)malloc(strlen(b->value) + 1);
			strcpy(a->value, b->value);
		}
		else
		{
			a->value = NULL;
		}
	}
	else if (strcmp(b->type, "int") == 0)
	{

		if (b->value != NULL)
		{
			a->value = (int *)malloc(sizeof(int));
			*((int*)(a->value)) = *((int*)b->value);
		}
		else
		{
			a->value = NULL;
		}
	}
	else if (strcmp(b->type, "double") == 0)
	{

		if (b->value != NULL)
		{
			a->value = (double *)malloc(sizeof(double));
			*((double*)(a->value)) = *((double*)b->value);
		}
		else
		{
			a->value = NULL;
		}
	}

	strcpy(a->type, b->type);
	strcpy(a->desc, b->desc);
}

void DestroyVarTok(VarsPtr ptr)
{
	free(ptr->value);
	free(ptr);
}

FcTokPtr CreateFcTok(struct Token type, struct Token id, DymTokPtr dec, DymTokPtr combcmd) // POUZE TAKTO VYTV��ET DYNAMICK� TOKENY
{
	FcTokPtr tok = (FcTokPtr)malloc(sizeof(struct FcTok));
	strcpy(tok->dec.id, "");
	tok->dec.desc = NULL;
	strcpy(tok->combcmd.id, "");
	tok->combcmd.desc = NULL;
	SetTokenByToken(&(tok->id), id);
	SetTokenByToken(&(tok->type), type);
	if (dec != NULL)
		SetDymTok(&(tok->dec), dec->id, dec->desc);
	if (combcmd != NULL)
		SetDymTok(&(tok->combcmd), combcmd->id, combcmd->desc);
	return tok;
}

CmdIfPtr CreateCmdIf(DymTokPtr expr, DymTokPtr combcmd, DymTokPtr combcmdnd) // POUZE TAKTO VYTV��ET DYNAMICK� TOKENY
{
	CmdIfPtr tok = (CmdIfPtr)malloc(sizeof(struct CmdIf));
	strcpy(tok->expr.id, "");
	tok->expr.desc = NULL;
	strcpy(tok->combcmd.id, "");
	tok->combcmd.desc = NULL;
	strcpy(tok->combcmdnd.id, "");
	tok->combcmdnd.desc = NULL;
	if (expr != NULL)
		SetDymTok(&(tok->expr), expr->id, expr->desc);
	if (combcmd != NULL)
		SetDymTok(&(tok->combcmd), combcmd->id, combcmd->desc);
	if (combcmdnd != NULL)
		SetDymTok(&(tok->combcmdnd), combcmdnd->id, combcmdnd->desc);
	return tok;
}

CmdForPtr CreateCmdFor(DymTokPtr cmdfill, DymTokPtr combcmd, DymTokPtr combcmdnd) // POUZE TAKTO VYTV��ET DYNAMICK� TOKENY
{
	CmdForPtr tok = (CmdForPtr)malloc(sizeof(struct CmdFor));
	strcpy(tok->cmdfill.id, "");
	tok->cmdfill.desc = NULL;
	strcpy(tok->combcmd.id, "");
	tok->combcmd.desc = NULL;
	strcpy(tok->combcmdnd.id, "");
	tok->combcmdnd.desc = NULL;
	if (cmdfill != NULL)
		SetDymTok(&(tok->cmdfill), (*cmdfill).id, (*cmdfill).desc);
	if (combcmd != NULL)
		SetDymTok(&(tok->combcmd), combcmd->id, combcmd->desc);
	if (combcmdnd != NULL)
		SetDymTok(&(tok->combcmdnd), combcmdnd->id, combcmdnd->desc);
	return tok;
}

void SetDymTok(DymTokPtr tok, char * id, char * desc) // POUZE TAKTO VYTV��ET DYNAMICK� TOKENY
{
	if (tok->desc != NULL)
		free(tok->desc);
	tok->desc = (char *)malloc(strlen(desc) + 1);
	strcpy(tok->id, id);
	strcpy(tok->desc, desc);
}

void CopyFcTok(FcTokPtr tok, FcTokPtr from)
{
	memcpy(&(tok->id), &(from->id), sizeof(struct Token));
	memcpy(&(tok->type), &(from->type), sizeof(struct Token));
	GetDymTok(&(tok->dec), &(from->dec));
	GetDymTok(&(tok->combcmd), &(from->combcmd));
}

void CopyCmdIf(CmdIfPtr tok, CmdIfPtr from)
{
	GetDymTok(&(tok->expr), &(from->expr));
	GetDymTok(&(tok->combcmd), &(from->combcmd));
	GetDymTok(&(tok->combcmdnd), &(from->combcmdnd));
}
void CopyCmdFor(CmdForPtr tok, CmdForPtr from)
{
	GetDymTok(&(tok->cmdfill), &(from->cmdfill));
	GetDymTok(&(tok->combcmd), &(from->combcmd));
	GetDymTok(&(tok->combcmdnd), &(from->combcmdnd));
}

void GetDymTok(DymTokPtr tok, DymTokPtr get) 
{
	strcpy(tok->id, get->id);
	if (tok->desc != NULL)
		free(tok->desc);
	tok->desc = (char *)malloc(strlen(get->desc) + 1);
	strcpy(tok->desc, get->desc);	
}

void DestroyDymTok(DymTokPtr tok)
{
	free(tok->desc);
	tok->desc = NULL;
}

void DestroyFcTok(FcTokPtr tok)
{
	DestroyDymTok(&(tok->dec));
	DestroyDymTok(&(tok->combcmd));
}

void DestroyCmdIf(CmdIfPtr tok)
{
	DestroyDymTok(&(tok->expr));
	DestroyDymTok(&(tok->combcmd));
	DestroyDymTok(&(tok->combcmdnd));
}

void DestroyCmdFor(CmdForPtr tok)
{
	DestroyDymTok(&(tok->cmdfill));
	DestroyDymTok(&(tok->combcmd));
	DestroyDymTok(&(tok->combcmdnd));
}
/*
void Set_Dec(struct _Dec * tTok, struct Token *id, struct Token *typ)
{
	memcpy(&tTok->id, id, sizeof(struct Token));
	memcpy(&tTok->typ, id, sizeof(struct Token));
}

void Set_Def(struct _Def * tTok, struct Token *id, struct Token *typ, struct Token * expr)
{
	memcpy(&tTok->id, id, sizeof(struct Token));
	memcpy(&tTok->typ, id, sizeof(struct Token));
	memcpy(&tTok->expr, id, sizeof(struct Token));
}*/