/*

FILE: stack.h
Authors: Luk� Ch�bek

*/
/*
	Stack
	
	Usage:

	Initialization: (!Important)
		sStackPtr MyStack = (sStackPtr)malloc(sizeof(struct sStack));
		ST_InitStack(MyStack);

	Push:
		typy: _INT, _CHAR, _TOKEN
		ST_Push(MyStack, "Item", typ);

	Pop:
		char * buffer = (char *)malloc(ST_GetTopItemSize(MyStack));
		ST_Pop(MyStack, buffer);
		..
		..
		free(buffer);
	Top:
		char * buffer = (char *)malloc(ST_GetTopItemSize(MyStack));
		ST_Top(MyStack, buffer);

	TopType:
		if (ST_TopType(MyStack) == _CHAR)
		... Char
		else if(.... _TOKEN) ... _INT
	Clear Stack:
		ST_ClearItems(MyStack);

	Dispose:
		ST_DisposeStack(MyStack);
		

*/

#ifndef STACK_H_
#define STACK_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include "token.h"
#include <stdbool.h>
// Stack

enum SState
{
	ST_OK,
	ST_ER_NO_STACK,
	ST_ER_NO_BUFFER,
	ST_ER_NOT_ENOUGH_MEMORY,
	ST_ER_ALREADY_INITED,
	ST_ER_EMPTY_STACK,
	ST_ER_WRONG_STACK,
	_INT,
	_CHAR,
	_DOUBLE,
	_TOKEN,
	_DYMTOK,
	_FCTOK,
	_CMDIF,
	_CMDFOR,
	_DEC,
	_DEF,
	_VARS
};

typedef struct sStackElem {
	struct sStackElem *ptr;
	int type;
	void *data;
} *sStackElemPtr;

typedef struct sStack {
	sStackElemPtr Top;
} *sStackPtr;


// Global Dispose

typedef struct sStacksForMemoryFreeItem
{
	struct sStacksForMemoryFreeItem *next;
	sStackPtr _safe;

}*sStacksForMemoryFreeItemPtr;

typedef struct sStacksForMemoryFree
{
	sStacksForMemoryFreeItemPtr Top;
}*sStacksForMemoryFreePtr;



int ST_STACKMEMFREEPUSH(sStackPtr S); // DO NOT USE !!
int ST_STACKMEMFREEPOP(sStackPtr S); // DO NOT USE !!
//----------------------------------
//typedef sStackElem sStack;


int ST_InitStack(sStackPtr S);
int ST_GetTopItemSize(sStackPtr S);
int ST_Top(sStackPtr S, void * val);
int ST_TopType(sStackPtr S);
int ST_Push(sStackPtr S, void * val, int type);
int ST_Pop(sStackPtr S, void * val);
int ST_SwapStackForInt(sStackPtr S);
int ST_ClearItems(sStackPtr S);
int ST_DisposeStack(sStackPtr S);
int ST_DisposeAll();
int ST_LookBehind(sStackPtr S, void * val, int count);
int ST_TopBackType(sStackPtr S, int count);
int ST_PopBack(sStackPtr S, void * val, int counter);
bool ST_IsEmpty(sStackPtr S);
#endif
