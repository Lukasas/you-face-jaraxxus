﻿/*

FILE: ial.c
Authors: Jakub Chlebík, Adam Huspenina

*/
#include "ial.h"


void BTInitF(FcNodePtr *Root)
{
	*Root = NULL;
}

struct FcNode* initFcTree(FcNodePtr FRoot) {

	FcNodePtr tmp = NULL;
	//FcNodePtr tmp_2 = NULL;

	if ( BTAddFcNode(&FRoot, "is", "length", true) != SUCC )	return NULL;
	if ( BTAddFcNode(&FRoot, "ssii", "substr", true) != SUCC )	return NULL;
	if ( BTAddFcNode(&FRoot, "sss", "concat", true) != SUCC )	return NULL;
	if ( BTAddFcNode(&FRoot, "iss", "find", true) != SUCC )		return NULL;
    if ( BTAddFcNode(&FRoot, "ss", "sort", true) != SUCC )		return NULL;
			
	

	if ((tmp = BTSearchFcName(&FRoot, "length")) == NULL)			return NULL;
		if ((tmp->FParamNames = (char*)malloc( 3  * sizeof(char))) == NULL) return NULL;
		strcpy(tmp->FParamNames, "s$");
	if ((tmp = BTSearchFcName(&FRoot, "substr")) == NULL)			return NULL;
		if ((tmp->FParamNames = (char*)malloc(7 * sizeof(char))) == NULL) return NULL;
		strcpy(tmp->FParamNames, "s$i$n$");
	if ((tmp = BTSearchFcName(&FRoot, "concat")) == NULL)			return NULL;
		if ((tmp->FParamNames = (char*)malloc(7 * sizeof(char))) == NULL) return NULL;
		strcpy(tmp->FParamNames, "s1$s2$");
	if ((tmp = BTSearchFcName(&FRoot, "find")) == NULL)			return NULL;
		if ((tmp->FParamNames = (char*)malloc(10 * sizeof(char))) == NULL) return NULL;
		strcpy(tmp->FParamNames, "s$search$");
	if ((tmp = BTSearchFcName(&FRoot, "sort")) == NULL)			return NULL;
		if ((tmp->FParamNames = (char*)malloc(3 * sizeof(char))) == NULL) return NULL;
		strcpy(tmp->FParamNames, "s$");

	return FRoot;
}

int BTAddFcNode(FcNodePtr *Root, char *t, char *n, bool d) {

	FcNodePtr tmp = NULL, tmp_2 = (*Root);
	size_t F_Name = strlen(n) + 1;
	size_t F_Type = strlen(t) + 1;

	if ((tmp = (FcNodePtr)malloc(sizeof(struct FcNode))) == NULL) return MALLOC_ERR;
	if ((tmp->FName = (char *)malloc(F_Name * sizeof(char))) == NULL) {
		free(tmp);
		return MALLOC_ERR;
	}
	if ((tmp->FType = (char *)malloc(F_Type * sizeof(char))) == NULL) {
		free(tmp->FName);
		free(tmp);
		return MALLOC_ERR;
	}

	if ((*Root) == NULL) {
		tmp->RPtr = NULL;
		tmp->LPtr = NULL;
		tmp->def = d;
		strcpy(tmp->FName, n);
		tmp->FName[F_Name] = '\0';
		strcpy(tmp->FType, t);
		tmp->FType[F_Type] = '\0';
		(*Root) = tmp;
		return SUCC;
	}
	else if ((*Root) != NULL) {
		while (true) {

			if ((strcmp(tmp_2->FName, n) == 0)) return CONFLICT_NAME;

			if ((strcmp(tmp_2->FName, n) > 0) && tmp_2->LPtr == NULL) {

				tmp->RPtr = NULL;
				tmp->LPtr = NULL;
				tmp->def = d;
				strcpy(tmp->FName, n);
				tmp->FName[F_Name] = '\0';
				strcpy(tmp->FType, t);
				tmp->FType[F_Type] = '\0';
				tmp_2->LPtr = tmp;
				return SUCC;
			}

			else if ((strcmp(tmp_2->FName, n) < 0) && tmp_2->RPtr == NULL) {

				tmp->RPtr = NULL;
				tmp->LPtr = NULL;
				tmp->def = d;
				strcpy(tmp->FName, n);
				tmp->FName[F_Name] = '\0';
				strcpy(tmp->FType, t);
				tmp->FType[F_Type] = '\0';
				tmp_2->RPtr = tmp;
				return SUCC;
			}

			if ((strcmp(tmp_2->FName, n) > 0)) 	tmp_2 = tmp_2->LPtr;

			else if ((strcmp(tmp_2->FName, n) < 0)) 	tmp_2 = tmp_2->RPtr;

			if (tmp_2 == NULL)                 break;
		}
	}
	return NOT_ADDED;
}

bool BTFCAddParams(FcNodePtr FNode, char* arr) {
	if (arr != NULL && FNode != NULL) {
		FNode->FParamNames = (char*)malloc((strlen(arr) + 1)*sizeof(char));
		if (FNode->FParamNames == NULL) {
			return false;
		}
		strcpy(FNode->FParamNames, arr);
		FNode->FParamNames[strlen(arr)] = '\0';
		return true;
	}
	return false;
}

struct FcNode* BTSearchFcName(FcNodePtr *Root, char *n) {

	FcNodePtr dest = (*Root);

	if ((*Root) != NULL) {
		while (true) {

			if ((strcmp(dest->FName, n) == 0)) 	return (dest);

			if ((strcmp(dest->FName, n) > 0))	dest = dest->LPtr;
			else if ((strcmp(dest->FName, n) < 0))	dest = dest->RPtr;

			if (dest == NULL) 					break;
		}
	}
	return NULL;
}

void BTDispose(VarNodePtr* VRoot, FcNodePtr *FRoot)
{
	VarNodePtr pom = NULL;
	if (*FRoot != NULL) {
		BTDispose(&pom, &(*FRoot)->RPtr);		// posunuj se dolů v pravém podstromu
		BTDispose(&pom, &(*FRoot)->LPtr);		// posunuj se dolů v levém podstromu
		free(*FRoot);
		*FRoot = NULL;						// uvolni kořen
	}

	FcNodePtr pom_2 = NULL;
	if (*VRoot != NULL) {
		BTDispose(&(*VRoot)->RPtr, &pom_2);		// posunuj se dolů v pravém podstromu
		BTDispose(&(*VRoot)->LPtr, &pom_2);		// posunuj se dolů v levém podstromu
		free(*VRoot);
		*VRoot = NULL;							// uvolni kořen
	}						// uvedení do stejného stavu jako po inicializaci
}

/**
Funkce pro praci s TS funkci a promenych
**/
void BTInitV(VarNodePtr* Root) {

	*Root = NULL;
}


int BTAddNodeV(VarNodePtr *Root, char * n, char t, bool isblock) {

	VarNodePtr tmp = NULL, tmp_2;
	size_t end_zero = strlen(n) + 1;


	if ((tmp = (VarNodePtr)malloc(sizeof(struct VarNode))) == NULL) 		return MALLOC_ERR;
	if ((tmp->name = (char *)malloc(end_zero * sizeof(char))) == NULL) {
		free(tmp);
		return MALLOC_ERR;
	}

	if (isblock) {
		while ((*Root) != NULL && (*Root)->RPtr != NULL) (*Root) = (*Root)->RPtr;

		tmp->RPtr = NULL;
		tmp->LPtr = NULL;
		tmp->type = t;
		strcpy(tmp->name, n);
		tmp->name[end_zero] = '\0';
		if ((*Root) != NULL)
			(*Root)->RPtr = tmp;
		(*Root) = tmp;
		return SUCC;
	}

	else if (!isblock) {			// nepridavame block ale promenou
		tmp_2 = (*Root)->LPtr;
		if (tmp_2 != NULL) {
			while (true) {

				if ((strcmp(tmp_2->name, n)) == 0) {
					return CONFLICT_NAME;
				}

				if ((strcmp(tmp_2->name, n) > 0) && tmp_2->LPtr == NULL) {

					tmp->RPtr = NULL;
					tmp->LPtr = NULL;
					tmp->type = t;
					strcpy(tmp->name, n);
					tmp->name[end_zero] = '\0';
					tmp_2->LPtr = tmp;
					return SUCC;
				}

				else if ((strcmp(tmp_2->name, n) < 0) && tmp_2->RPtr == NULL) {

					tmp->RPtr = NULL;
					tmp->LPtr = NULL;
					tmp->type = t;
					strcpy(tmp->name, n);
					tmp->name[end_zero] = '\0';
					tmp_2->RPtr = tmp;
					return SUCC;
				}

				// tmp_2->name > n
				if ((strcmp(tmp_2->name, n) > 0))			tmp_2 = tmp_2->LPtr;

				//tmp_2->name < n
				else if ((strcmp(tmp_2->name, n)) < 0)		tmp_2 = tmp_2->RPtr;

				if (tmp_2 == NULL) return (NOT_ADDED);

			}
		}
		else {   // tmp_2 == NULL

			tmp->RPtr = NULL;
			tmp->LPtr = NULL;
			tmp->type = t;
			strcpy(tmp->name, n);
			tmp->name[end_zero] = '\0';
			(*Root)->LPtr = tmp;
			return SUCC;
		}
	}

	else return NOT_ADDED;
}

bool BTBlockUpV(VarNodePtr *RootBlock, VarNodePtr *CurrBlock, int submerge) {
	if ((*RootBlock) != NULL && *CurrBlock != NULL) {
		if ((*RootBlock) == *CurrBlock) return false;

		if (submerge == 0) return false;

		VarNodePtr tmp = (*RootBlock);
		while (tmp->RPtr != *CurrBlock) {

			tmp = tmp->RPtr;

		}
		*CurrBlock = tmp;
		if (submerge < atoi((*CurrBlock)->name))
			return BTBlockUpV(RootBlock, CurrBlock, submerge);
		else
			return true;
	}
	else return false;
}

struct VarNode* BTSearchVarName(VarNodePtr *Root, VarNodePtr CurrBlock, char *n) {

	if ((*Root) == NULL || CurrBlock == NULL || n == NULL) return (NULL);
	VarNodePtr dest = CurrBlock->LPtr;
	VarNodePtr tmp = CurrBlock;
	int submerge = atoi(CurrBlock->name);
	int i = submerge;


	while (true) {

		if (dest != NULL)
		{
			if ((strcmp(dest->name, n)) == 0) {
				return dest;
			}

			// tmp_2->name > n
			if ((strcmp(dest->name, n) > 0))			dest = dest->LPtr;

			//tmp_2->name < n
			else if ((strcmp(dest->name, n)) < 0)		dest = dest->RPtr;
		}

		if (dest == NULL)
		{
			if (i == 0) break;  // vys uz jit nemuzu
			else {
				if (!BTBlockUpV((&(*Root)), &tmp, (atoi(CurrBlock->name)))) {
					return NULL;
				}
				dest = tmp->LPtr;

			}
		}
	}
	return NULL;
}



int lenght(char* arr) {

	return strlen(arr);
}



char* concat(char *arr, char *ptr) {
	char *tmp = NULL;

	if (arr != NULL && ptr != NULL) {
		if ((tmp = (char*)malloc((strlen(arr) + strlen(ptr) + 1) * sizeof(char))) == NULL) return NULL;
		strcpy(tmp, arr);
		strcat(tmp, ptr);
		return tmp;
	}
	else {
		if (arr != NULL) {
			if ((tmp = (char*)malloc((strlen(arr) + 1) * sizeof(char))) == NULL) return NULL;
			strcpy(tmp, arr);
			return tmp;
		}
		else {
			if ((tmp = (char*)malloc((strlen(ptr) + 1) * sizeof(char))) == NULL) return NULL;
			strcpy(tmp, ptr);
			return tmp;
		}
	}
	return NULL;
}

/*  Knuth-Morris-Pratt */
int find(char* source, char* search) {
	int sourcelen = strlen(source);
	int searchlen = strlen(search);
	/* vyhledavany nemuze byt veci nez zdroj */
	if (searchlen > sourcelen) {
		return (-1);
	}
	int i, j = 0;
	int *fail = (int*)malloc(searchlen*sizeof(int));
	fail[0] = 0;  /* prvni hodnota vektoru nastavena na 0 */
	for (i = 1; i < searchlen; i++) {
		while ((j > 0) && (search[j] != search[i])) {
			j = fail[j - 1];
		}
		if (search[j] == search[i]) {
			j++;
		}
		fail[i] = j;
	}
	j = 0;  /* reset indexu */
			/* vykonani samotneho porovnani */
	for (i = 0; i < sourcelen; i++) {
		while ((j > 0) && (search[j] != source[i])) {
			j = fail[j - 1];
		}
		if (search[j] == source[i]) {
			j++;
		}
		if (j == searchlen) {
			free(fail);
			return(i - j + 1);
		}
	}
	free(fail);
	return(-1);
}



/* usporada pointery ne data */
void MergeSort(struct elem** head) {
	struct elem* tmphead = *head;
	struct elem* tmp1;
	struct elem* tmp2;
	if ((tmphead != NULL) && (tmphead->next != NULL)) {
		FrontBackSplit(tmphead, &tmp1, &tmp2);
		MergeSort(&tmp1);
		MergeSort(&tmp2);
		*head = SortedMerge(tmp1, tmp2);
	}
}

struct elem* SortedMerge(struct elem* left, struct elem* right) {
	struct elem* res = NULL;
	if (left == NULL) {
		return(right);
	}
	if (right == NULL) {
		return(left);
	}
	if (left->data <= right->data) {
		res = left;
		res->next = SortedMerge(left->next, right);
	}
	else {
		res = right;
		res->next = SortedMerge(left, right->next);
	}
	return(res);
}

void FrontBackSplit(struct elem* source, struct elem** front, struct elem** back) {
	struct elem* fast;
	struct elem* slow;
	if (source == NULL || source->next == NULL) {
		*front = source;
		*back = NULL;
	}
	else {
		slow = source;
		fast = source->next;
		while (fast != NULL) {
			fast = fast->next;
			if (fast != NULL) {
				slow = slow->next;
				fast = fast->next;
			}
		}
		*front = source;
		*back = slow->next;
		slow->next = NULL;
	}
}
/* nahrani serazenych prvku do navratove promenne */
char* returnList(struct elem *element, size_t size) {
	char *s = (char*)malloc((size + 1) * sizeof(char));
	memset(s, '\0', size + 1);
	int i = 0;
	while (element != NULL) {
		s[i] = element->data;
		element = element->next;
		i++;
	}
	return s;
}
/* vytvori novy prvek, naplni ho daty ze vstupu, propoji se zbytkem seznamu */
void push(struct elem** head, char data) {
	struct elem* new = (struct elem*)malloc(sizeof(struct elem));
	new->data = data;
	new->next = (*head);
	*head = new;
}

char* sort(char *s) {
	struct elem* tmp = NULL;
	/* nacteni vstupnich dat */
	int i;
	for (i = strlen(s) - 1; i >= 0; i--) {
		push(&tmp, s[i]);
	}
	MergeSort(&tmp);
	return returnList(tmp, strlen(s));
}


