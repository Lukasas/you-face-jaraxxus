﻿/*

FILE: semantics.c
Authors: Lukáš Chábek

*/
//#include "ial.h"
#include "semantics.h"
#include "token.h"
#include "lexical_analyzator.h"

char *type = NULL, *auto_type = NULL, *param_names = NULL, *tmp = NULL, *f_call = NULL, *for_iter_type = NULL;

VarNodePtr Root = NULL;
FcNodePtr FRoot = NULL;

int iSemantAnal(FILE * fileSourceCode)
{
	VarNodePtr CurrBlock = NULL;
	VarNodePtr VarRoot = NULL;
	FcNodePtr FcNodePtr = NULL;
    FRoot = initFcTree(FRoot);
	if ( FRoot == NULL) {
		BTDispose(&Root, &FRoot);
		return NOT_ADDED;
	}

	size_t i = 0;
	struct Token tTok;
	int state = BEGIN, l = 1, for_iter_br = 0, br = 0, k = 0, blok = 0;
	unsigned int line;
	bool if_state = false, for_cond_flag = false, for_def_flag = false, for_iter_flag = false, first_call = true, call = false, exist = false;
	bool first_block = true;
	char desc[65];	/* ulozi id */
	memset(desc, '\0', 65);

	char f_type[2];
	memset(f_type, '\0', 2);
	char arr[2];

	//if ((param_names = (char*)malloc(sizeof(char) * sizeof(tTok.desc))) == NULL) return MALLOC_ERR;
	//memset(param_names, '\0', sizeof(tTok.desc));

	while (iLexicalAnal(fileSourceCode, &tTok, &line) == 0)
	{
		switch (state)
		{
			/*pocatecni stav nebo stav po dokonceni jenoho termu */
		case BEGIN:
		{

			my_free(type);
			my_free(auto_type);
			//my_free(param_names);
			my_free(tmp);
			my_free(f_call);
			my_free(for_iter_type);
			if_state = false, for_cond_flag = false, for_def_flag = false, for_iter_flag = false, first_call = true, call = false, exist = false;
			l = 1, br = 0;

			if (strcmp(tTok.id, "int") == 0)
			{
				i = 1;
				if (!_alloc(i, type, "i")) return garbage(MALLOC_ERR);
				state = DEC;
			}
			else if (strcmp(tTok.id, "auto") == 0)
			{
				i = 1;
				if (!_alloc(i, type, "a")) return garbage(MALLOC_ERR);
				state = DEC;
			}
			else if (strcmp(tTok.id, "double") == 0)
			{
				i = 1;
				if (!_alloc(i, type, "d")) return garbage(MALLOC_ERR);
				state = DEC;
			}
			else if (strcmp(tTok.id, "string") == 0)
			{
				i = 1;
				if (!_alloc(i, type, "s")) return garbage(MALLOC_ERR);
				state = DEC;
			}
			else if (strcmp(tTok.id, "id") == 0)
			{
				if (blok == 0) return garbage(OUTSIDE_ERR);
				VarRoot = BTSearchVarName((&Root), CurrBlock, tTok.desc);
				FcNodePtr = BTSearchFcName((&FRoot), tTok.desc);

				if (FcNodePtr != NULL) {
					if (VarRoot == NULL) 	return garbage(WRONG_FC_CALL);
					else 					return garbage(CONFLICT_NAME);
				}
				if(VarRoot == NULL){
					return garbage(UNDECLARED_VAR);
				}
				if (VarRoot != NULL) {
					arr[0] = VarRoot->type;
					arr[1] = '\0';
					i = 1;
					if(!_alloc(i, type, arr)) return garbage(MALLOC_ERR);
					state = EQ_FC;
					exist = true;
				}
				else {
					return garbage(UNSPECIFIED_ERR);
				}
			}
			else if (strcmp(tTok.id, "if") == 0)
			{
				if (blok == 0) return garbage(OUTSIDE_ERR);
				state = IF;
			}
			else if (strcmp(tTok.id, "else") == 0)
			{
				if (blok == 0) return garbage(OUTSIDE_ERR);
				state = ELSE;
			}
			else if (strcmp(tTok.id, "for") == 0)
			{
				if (blok == 0) return garbage(OUTSIDE_ERR);
				state = FOR_BEGIN;
			}
			else if (strcmp(tTok.id, "return") == 0)
			{
				if (blok == 0) return garbage(OUTSIDE_ERR);
				if (f_type[0] == '\0') return garbage(UNSPECIFIED_ERR);
				i = 1;
				if (!_alloc(i, type, f_type)) return garbage(MALLOC_ERR);
				state = VAL_EVAL;
				exist = true;
			}
			else if (strcmp(tTok.id, "cin") == 0)
			{
				if (blok == 0) return garbage(OUTSIDE_ERR);
				state = CIN;
			}
			else if (strcmp(tTok.id, "cout") == 0)
			{
				if (blok == 0) return garbage(OUTSIDE_ERR);
				state = COUT;
			}
			else if (strcmp(tTok.id, "{") == 0)
			{

				char buff[5];
				sprintf(buff, "%d", blok);
				if (BTAddNodeV(&CurrBlock, buff, 'b', true) != SUCC) return garbage(NOT_ADDED);
				if (first_block)
				{
					Root = CurrBlock;
					first_block = false;
				}
				blok++;
				state = BEGIN;
			}
			else if (strcmp(tTok.id, "}") == 0)
			{
				if (blok == 0) return garbage(OUTSIDE_ERR);
				blok--;
				if (blok > 0)
				{
					if (!BTBlockUpV( &Root, &CurrBlock, atoi(CurrBlock->name))) return garbage(CANT_MOVE);
				}
				state = BEGIN;
			}
			else {
				return garbage(SYNTAX_ERR);
			}
			break;
		}

		/* stav za tokenem datoveho typu */
		case DEC:
		{
			if (strcmp(tTok.id, "id") == 0)
			{
				VarRoot = BTSearchVarName((&Root), CurrBlock, tTok.desc);	// prohledat tabulku
				if (VarRoot != NULL) return garbage(ALREADY_EXIST);				// pokud se naslo, -> error
				memset(desc, '\0', sizeof(desc));							// vynulovani description
				strcpy(desc, tTok.desc);									// ulozeni
				state = EQ_FC;
			}
			else		// za datovym typem muze byt jen id
			{
				return garbage(SYNTAX_ERR);
			}
			break;
		}

		/* stav pro rozhodnuti, jestli se jedna o funkci nebo promenou*/
		case EQ_FC:
		{
			if (strcmp(tTok.id, "=") == 0) // promenna
			{
				state = VAL_EVAL;
			}
			else if (strcmp(tTok.id, "(") == 0) // deklarace nebo definice funkci
			{
				if (strcmp(type, "a") == 0) return garbage(AUTO_FUNC); // auto funkce neni
				state = BEGIN_FC;
			}
			else if (strcmp(tTok.id, ";") == 0) // deklarace promenne
			{
				if ((strcmp(type, "a") != 0) )
				{
					if (exist) return garbage(UNSPECIFIED_ERR); // no effect statement eg. i;

					if (BTAddNodeV(&CurrBlock, desc, (*type), false) != SUCC) return garbage(NOT_ADDED); // pridani promenne do stromu
					//memset(desc, '\0', 65);
					state = BEGIN;
				}
				else
				{
					return garbage(VAR_ERR);			// auto a;
				}
			}
			break;
		}

		/* stav pro typy parametru funkce */
		case BEGIN_FC:
		{
			if (!call)  // nejedna se o volani funkce
			{
				if (strcmp(tTok.id, "int") == 0)
				{
					i++;
					if (!_alloc(i, type, "i")) return garbage(MALLOC_ERR);
					state = FC_PARAM;
				}
				else if (strcmp(tTok.id, "auto") == 0)
				{
					i++;
					if (!_alloc(i, type, "a")) return garbage(MALLOC_ERR);
					state = FC_PARAM;
				}
				else if (strcmp(tTok.id, "double") == 0)
				{
					i++;
					if (!_alloc(i, type, "d")) return garbage(MALLOC_ERR);
					state = FC_PARAM;
				}
				else if (strcmp(tTok.id, "string") == 0)
				{
					i++;
					if (!_alloc(i, type, "s")) return garbage(MALLOC_ERR);
					state = FC_PARAM;
				}
				else if (first_call && strcmp(tTok.id, ")") == 0) // bool first call zajistuje prazdne parametry
				{
					state = FC_END;
				}
				else
				{
					return garbage(SYNTAX_ERR);
				}
			}
			else // call = true
			{
				if (strcmp(tTok.id, "dl") == 0)
				{
					if (!_alloc(k, f_call, "d")) return garbage(MALLOC_ERR);
					k++;
					state = FC_PARAM_SEPAR;
				}
				else if (strcmp(tTok.id, "rl") == 0)
				{
					if (!_alloc(k, f_call, "s")) return garbage(MALLOC_ERR);
					k++;
					state = FC_PARAM_SEPAR;
				}
				else if (strcmp(tTok.id, "cl") == 0)
				{
					if (!_alloc(k, f_call, "i")) return garbage(MALLOC_ERR);
					k++;
					state = FC_PARAM_SEPAR;
				}
				else if (strcmp(tTok.id, "id") == 0)
				{
					VarRoot = BTSearchVarName((&Root), CurrBlock, tTok.desc); // hledani v tabulce promennych
					if (VarRoot == NULL) return garbage(UNDECLARED_VAR); // nenalezena
					arr[0] = VarRoot->type;
					arr[1] = '\0';
					if (!_alloc(k, f_call, arr)) return garbage(MALLOC_ERR);
					k++;
					state = FC_PARAM_SEPAR;
				}
				else if (first_call && strcmp(tTok.id, ")") == 0)
				{
					state = FC_END;
				}
				else {
					return garbage(FC_PARAM_ERR);
				}
			}
			break;
		}

		/* stav pro konrolu carky mezi parametry a konce listu parametru */
		case FC_PARAM_SEPAR:
		{
			if (strcmp(tTok.id, ",") == 0)
			{
				first_call = false;			// nasle se carka, uz nemuze nastat ")" v BEGIN_FC
				state = BEGIN_FC;
			}
			else if (strcmp(tTok.id, ")") == 0)
			{
				state = FC_END;				// ukoncen list parametru
			}
			else
			{
				return garbage(SYNTAX_ERR);
			}
			break;
		}

		/* stav pro ulozeni jmen parametru funkce  */
		case FC_PARAM:
		{
			if (strcmp(tTok.id, "id") == 0)
			{
				char hlp[66];
				memset(hlp, '\0', sizeof(hlp));
				strcpy(hlp, tTok.desc);
				strcat(hlp, "$");
				//if (param_names != NULL) size = strlen(param_names);
				if (!_alloc(0, param_names, hlp)) return garbage(MALLOC_ERR);
				state = FC_PARAM_SEPAR;
			}
			else return garbage(FC_PARAM_ERR);
			break;
		}

		/* stav, rozdhodujici, zda se jednalo o deklaraci nebo definici */
		case FC_END:
		{
			k = 1;
			first_call = true; // vyresetovani boolu pro dalsi nalez funkce

			if (call)  			// funkce je v kodu volana
			{
				if ((strcmp(tTok.id, ";") == 0))
				{
					if (!recast(tmp, f_call)) return garbage(FC_PARAM_ERR);  // nesedi typova signatura funkce s volanim a nelze ji ani pretypovat
					my_free(tmp);
					if (!exist)
					{
						if (BTAddNodeV(&CurrBlock, desc, (*type), false) != SUCC)
						{
							return garbage(NOT_ADDED);
						}
					}
					state = BEGIN;
				}
			}
			else 		//nejednalo se o volani funkce
			{
				if ((strcmp(tTok.id, ";") == 0)) // jedna se o deklaraci funkce
				{

					if ((FcNodePtr = BTSearchFcName((&FRoot), desc)) != NULL) // funkce jiz zapsana je
					{
						if (strcmp(FcNodePtr->FType, type) != 0)
						{
							return garbage(REDEC); // typova signatura nesedi
						}

						if (strlen(FcNodePtr->FType) != 1)  // pokazde, kdyz bude mit funkce parametry
						{
							if (param_names != NULL){
								if (strcmp(param_names, FcNodePtr->FParamNames) != 0) {    // porovnam jmena parametru
									return garbage(FC_PARAM_ERR);
								}
							}
							else {
								return garbage(REDEC);
							}
						}
						else if (param_names != NULL){
							return garbage(REDEC);
						}
					}

					else if (BTAddFcNode((&FRoot), type, desc, false) == SUCC) {  // funkce se jeste v tabulce nenachazi, pridame ji
						if (param_names != NULL)
						{
							FcNodePtr = BTSearchFcName((&FRoot), desc);  // zjistime si jeji pointer
							if (FcNodePtr == NULL) return garbage(NOT_ADDED);
							if (!BTFCAddParams(FcNodePtr, param_names)) {			// a pridame ji jmena parametru
								return garbage(FC_PARAM_ERR);
							}
						}
					}
					else {
						return garbage(NOT_ADDED);  // nepodarilo se ji pridat
					}

					param_names = NULL;
					state = BEGIN;
				}
				else if (strcmp(tTok.id, "{") == 0) // jedna se definici funkce
				{
					FcNodePtr = BTSearchFcName((&FRoot), desc);		// vyhledame funkci


					if (FcNodePtr != NULL) // funkce jiz je v tabulce zapsana
					{
						if (FcNodePtr->def) {			// jiz byla definovana
							return garbage(REDEF);
						}
						else FcNodePtr->def = true;
						if (strcmp(FcNodePtr->FType, type) != 0) {		// sedi typova signatura
							return garbage(REDEF);

						}
						else if (param_names != NULL) {
							if (strlen(FcNodePtr->FType) == 1) return garbage(REDEF);
							else {
								if (strcmp(param_names, FcNodePtr->FParamNames) != 0) {		// sedi jmena parametru
									return garbage(REDEF);
								}
							}
						}

					}
					else 	// funkce se v tabulce nenachazi, je treba zapsat
					{
						if (BTAddFcNode((&FRoot), type, desc, true) != SUCC) return garbage(NOT_ADDED);

						if (param_names != NULL) // funkce ma parametry
						{
							FcNodePtr = BTSearchFcName((&FRoot), desc);  // vyhledame si ptr na fci
							if (FcNodePtr == NULL) {
								return (NOT_ADDED);
							}
							if (!BTFCAddParams(FcNodePtr, param_names)) { // prida ji parametry
								return garbage(FC_PARAM_ERR);
							}
						}

					}

					char buff[5];
					sprintf(buff, "%d", blok);
					if (BTAddNodeV(&CurrBlock, buff, 'b', true) != SUCC) // pridame novy blok
					{
						return garbage(NOT_ADDED);
					}
					if (first_block)
					{
						Root = CurrBlock;
						first_block = false;
					}
					blok++;


					if (param_names != NULL) // funkce ma parametry
					{
						char *token;
						token = strtok(param_names, "$");
						int counter = 1;
						while (token != NULL)
						{
							BTAddNodeV(&CurrBlock, token, type[counter++], false);
							token = strtok(NULL, "$");
						}

					}

					param_names = NULL;
					f_type[0] = type[0];
					state = BEGIN;
				}
			}
			break;
		}

		/* stav pro volani funkce */
		case FC_CALL:
		{
			if (strcmp(tTok.id, "(") == 0)
			{
				state = BEGIN_FC;
				call = true;
			}
			else
			{
				return garbage(SYNTAX_ERR);
			}
			break;
		}

		/* stav pro vyhodnoceni vysledneho typu vyrazu */
		case VAL_EVAL:
		{

			if (strcmp(tTok.id, "cl") == 0)
			{
				if (!_alloc(l, auto_type, "i")) return garbage(MALLOC_ERR);
				l++;
				state = VAL_EVAL_2;
			}
			else if (strcmp(tTok.id, "id") == 0)
			{
				VarRoot = BTSearchVarName(&Root, CurrBlock, tTok.desc);
				FcNodePtr = BTSearchFcName(&FRoot, tTok.desc);
				if (for_iter_flag && strcmp(tTok.desc, desc) == 0) {
					if (!_alloc(l, auto_type, desc)) return garbage(MALLOC_ERR);
					l++;
					state = VAL_EVAL_2;
					break;
				}
				if (VarRoot != NULL && FcNodePtr != NULL) return garbage(CONFLICT_NAME);
				// hledej id v tabulkï¿½ch, pokud najdeï¿½ v obou, error


				if (VarRoot != NULL)
				{
					state = VAL_EVAL_2;
					char arr[2];
					arr[0] = VarRoot->type;
					arr[1] = '\0';
					if (!_alloc(l, auto_type, arr)) return garbage(MALLOC_ERR);
					l++;

				}
				else if (FcNodePtr != NULL)
				{
					if (for_def_flag || for_iter_flag || for_cond_flag) return garbage(FOR_FUNC);
					if (!_alloc(strlen(FcNodePtr->FType), tmp, FcNodePtr->FType)) return garbage(MALLOC_ERR);
					if (((*tmp) == (*type)) || ((*type) == 'i' && (*tmp) == 'd') || ((*type) == 'd' && (*tmp) == 'i'))
					{
						k = 1;
						if (!_alloc(k, f_call, type)) return garbage(MALLOC_ERR);
						k++;
						state = FC_CALL;

					}
					else return garbage (WRONG_FC_CALL);
				}
				else return garbage(UNDECLARED_VAR);
			}
			else if (strcmp(tTok.id, "dl") == 0)
			{
				if (!_alloc(l, auto_type, "d")) return garbage(MALLOC_ERR);
				l++;
				state = VAL_EVAL_2;
			}
			else if (strcmp(tTok.id, "rl") == 0)
			{
				if (!_alloc(l, auto_type, "s")) return garbage(MALLOC_ERR);
				l++;
				state = VAL_EVAL_2;
			}
			else if (strcmp(tTok.id, "(") == 0)
			{
				if (!_alloc(l, auto_type, "(")) return garbage(MALLOC_ERR);
				l++;
				if(for_iter_flag) br++;
				state = VAL_EVAL;
			}

			else if (strcmp(tTok.id, ")") == 0)
			{
				if (for_iter_flag) // prace se zavorkami
				{
					if (br == 0 && for_iter_br > 0)
					{
						for_iter_br--;
					}
					else if (br > 0)
					{
						if (!_alloc(l, auto_type, ")")) return garbage(MALLOC_ERR);
						l++;
						br--;
					}
					else
					{
						br--;
					}
					if (br == -1) {
						char *arr = GetType(auto_type);
						auto_type = NULL;
						if (arr == NULL) return garbage(VAR_ERR);
						else if (((*for_iter_type) == (*arr)) || ((*for_iter_type) == 'i' && (*arr) == 'd') || ((*for_iter_type) == 'd' && (*arr) == 'i')){
							my_free(arr);
							my_free(for_iter_type);
							state = FOR_BLOCK;
							for_iter_flag = false;
						}
						else return garbage(VAR_ERR);
					}
					else
					{
						state = VAL_EVAL_2;
					}
				}
				else
				{
					if (!_alloc(l, auto_type, ")")) return garbage(MALLOC_ERR);
					l++;
					//if (for_iter_flag) br--;
					state = VAL_EVAL_2;
				}
			}
			else
			{
				return garbage(UNSPECIFIED_ERR);
			}
			break;
		}

		/* druhy stav pro typy vyrazu, zajistuje spravne poradi operandu */
		case VAL_EVAL_2:
		{
			if (!if_state && strcmp(tTok.id, ";") == 0) // vyhodnoceni vyrazu zakoncenych ";"
			{
				if (!for_cond_flag) // nevyhodnocujeme podminku for cyklu
				{
					char *arr = GetType(auto_type);
					auto_type = NULL;
					l = 1;
					if (arr == NULL) return garbage(VAR_ERR);
					if ((*type) == 'a')
					{
						type[0] = (*arr);

					}
					else if ( !(((*type) == (*arr)) || ((*type) == 'i' && (*arr) == 'd') || ((*type) == 'd' && (*arr) == 'i')) )
					{

						return garbage(VAR_ERR);
					}

					if (!exist && !for_def_flag)
					{
						if (BTAddNodeV(&CurrBlock, desc, (*type), false) != SUCC) return garbage(NOT_ADDED);
						state = BEGIN;
					}
					else if (for_def_flag) // vyhodnocovali jsme cast definice ve for-u
					{				// desc i typ zustavaji zachovany na svych mistech
						state = FOR_CONDITION;
						for_def_flag = false;
						my_free(arr);
					}
					else
					{
						state = BEGIN;
					}
				}
				else  // vyhodnocujeme podminku ve for cyklu
				{
					char *arr = GetType(auto_type);
					auto_type = NULL;
					if (arr == NULL) return garbage(VAR_ERR);
					if (((*arr) == 'i') || ((*arr) == 'd'))
					{
						for_cond_flag = false;
						my_free(arr);
						state = FOR_ITER;
					}
					else return garbage(VAR_ERR);
				}
			//	else return garbage(SYNTAX_ERR);
			}
			else if (if_state && ((strcmp(tTok.id, "{")) == 0))
			{
				char *arr = GetType(auto_type);
				auto_type = NULL;
				l = 1;
				if (arr == NULL) return garbage(VAR_ERR);
				if (((*arr) == 'i') || ((*arr) == 'd'))
				{
					if_state = false;

					char buff[5];
					sprintf(buff, "%d", blok);
					if (BTAddNodeV(&CurrBlock, buff, 'b', true) != SUCC) return garbage(NOT_ADDED);
					if (first_block)
					{
						Root = CurrBlock;
						first_block = false;
					}
					blok++;
					state = BEGIN;
				}
				else	return garbage(VAR_ERR);
			}
			else if (strcmp(tTok.id, ")") == 0)
			{
				if (for_iter_flag)
				{
					if (br == 0 && for_iter_br > 0)
					{
						for_iter_br--;
					}
					else if (br > 0)
					{
						if (!_alloc(l, auto_type, ")")) return garbage(MALLOC_ERR);
						l++;
						br--;
					}
					else
					{
						br--;
					}

					if (br == -1)
					{
						char *arr = GetType(auto_type);
						auto_type = NULL;
						if (arr == NULL) return garbage(VAR_ERR);
						else if (((*for_iter_type) == (*arr)) || ((*for_iter_type) == 'i' && (*arr) == 'd') || ((*for_iter_type) == 'd' && (*arr) == 'i')) {
							my_free(arr);
							my_free(for_iter_type);
							state = FOR_BLOCK;
							for_iter_flag = false;
						}
						else return garbage(VAR_ERR);
					}
					else
					{
						state = VAL_EVAL_2;
					}
				}
				else
				{
					if (!_alloc(l, auto_type, ")")) return garbage(MALLOC_ERR);
					l++;
					state = VAL_EVAL_2;
				}
			}
			else if ((strcmp(tTok.id, "+") == 0) ||
				(strcmp(tTok.id, "-") == 0) ||
				(strcmp(tTok.id, "*") == 0) ||
				(strcmp(tTok.id, "/") == 0) ||
				(strcmp(tTok.id, "==") == 0) ||
				(strcmp(tTok.id, "=>") == 0) ||
				(strcmp(tTok.id, "=<") == 0) ||
				(strcmp(tTok.id, "!=") == 0) ||
				(strcmp(tTok.id, "<") == 0) ||
				(strcmp(tTok.id, ">") == 0)) {

				if (strlen(tTok.id) == 2) l++;
				if (!_alloc(l, auto_type, tTok.id)) return garbage(MALLOC_ERR);
				l++;
				state = VAL_EVAL;
			}
			else    return garbage(SYNTAX_ERR);
			break;
		}

		/* nalezen token for, ocekavana zavorka */
		case FOR_BEGIN:
		{
			if (strcmp(tTok.id, "(") == 0)
			{
					state = FOR_DEF_BEGIN;
			}
			else return garbage(SYNTAX_ERR);
			break;
		}

		/* zacatek definicni casti foru */
		case FOR_DEF_BEGIN:
		{
			if (strcmp(tTok.id, ";") == 0)
			{
				state = FOR_CONDITION;
			}
			else if (strcmp(tTok.id, "id") == 0)
			{
				return garbage(FOR_ERR_DEF);
			}
			else if (strcmp(tTok.id, "int") == 0)
			{
				i=1;
				if (!_alloc(i, type, "i")) return garbage(MALLOC_ERR);
				state = FOR_DEF_ID;
			}
			else if (strcmp(tTok.id, "double") == 0)
			{
				i=1;
				if (!_alloc(i, type, "d")) return garbage(MALLOC_ERR);
				state = FOR_DEF_ID;
			}
			else if (strcmp(tTok.id, "string") == 0)
			{
				i=1;
				if (!_alloc(i, type, "s")) return garbage(MALLOC_ERR);
				state = FOR_DEF_ID;
			}
			else if (strcmp(tTok.id, "auto") == 0)
			{
				i=1;
				if (!_alloc(i, type, "a")) return garbage(MALLOC_ERR);
				state = FOR_DEF_ID;
			}
			break;
		}

		/* definicni cast foru hledani ID*/
		case FOR_DEF_ID:
		{
			if (strcmp(tTok.id, "id") == 0)
			{
				memset(desc, '\0', 65);
				strcpy(desc, tTok.desc);
				state = FOR_DEF_EQ;
			}
			else return garbage(SYNTAX_ERR);
			break;
		}

		/* definicni cast foru, hledani = nebo ; */
		case FOR_DEF_EQ:
		{
			if (strcmp(tTok.id, "=") == 0)
			{
				for_def_flag = true;
				state = VAL_EVAL;
			}
			else if (strcmp(tTok.id, ";") == 0)
			{
				if (strcmp(type, "a") == 0) return garbage(VAR_ERR);
				state = FOR_CONDITION;
			}
			else return garbage(SYNTAX_ERR);
			break;
		}

		/* stav pro podminku cyklu for */
		case FOR_CONDITION:
		{
			if (strcmp(tTok.id, ";") == 0)
			{
				state = FOR_ITER;
			}
			else if (strcmp(tTok.id, "id") == 0)
			{
				if (strcmp(tTok.desc, desc) == 0) // je to ID z FOR definice
				{
					for_cond_flag = true;
					if (!_alloc(l, auto_type, type)) return garbage(MALLOC_ERR);
					l++;
					state = VAL_EVAL_2;
				}
				else
				{
					VarRoot = BTSearchVarName((&Root), CurrBlock, tTok.desc);
					if (VarRoot == NULL) return garbage(UNDECLARED_VAR);
					char arr[2];
					arr[0] = VarRoot->type;
					arr[1] = '\0';
					if (!_alloc(l, auto_type, arr)) return garbage(MALLOC_ERR);
					l++;
					for_cond_flag = true;
					state = VAL_EVAL_2;
				}

			}
			else if (strcmp(tTok.id, "cl") == 0)
			{
				for_cond_flag = true;
				if (!_alloc(l, auto_type, "i")) return garbage(MALLOC_ERR);
				l++;
				state = VAL_EVAL_2;
			}
			else if (strcmp(tTok.id, "dl") == 0)
			{
				for_cond_flag = true;
				if (!_alloc(l, auto_type, "d")) return garbage(MALLOC_ERR);
				l++;
				state = VAL_EVAL_2;
			}
			else if (strcmp(tTok.id, "rl") == 0)
			{
				for_cond_flag = true;
				if (!_alloc(l, auto_type, "s")) return garbage(MALLOC_ERR);
				l++;
				state = VAL_EVAL_2;
			}
			else if (strcmp(tTok.id, "(") == 0)
			{
				for_cond_flag = true;
				if (!_alloc(l, auto_type, "(")) return garbage(MALLOC_ERR);
				l++;
				state = VAL_EVAL;
			}
			break;
		}

		/* stav pro iteracni cast cyklu for */
		case FOR_ITER:
		{
			if (strcmp(tTok.id, ")") == 0)
			{
				if (for_iter_br) // nalezli jsme pred tim "("
				{				// jedna se tedy o retezec "()" -> error
					return garbage(6);
				}
				else
				{
					state = FOR_BLOCK;
				}

			}
			else if (strcmp(tTok.id, "id") == 0)
			{
				if (strcmp(tTok.desc, desc) == 0)
				{ // je to ID z FOR definice

					if (!_alloc(1, for_iter_type, type)) return garbage(MALLOC_ERR);
					state = FOR_ITER_EQ;
				}
				else
				{
					VarRoot = BTSearchVarName((&Root), CurrBlock, tTok.desc);
					if (VarRoot == NULL) return garbage(UNDECLARED_VAR);
					char arr[2];
					arr[0] = VarRoot->type;
					arr[1] = '\0';
					if (!_alloc(1, for_iter_type, arr)) return garbage(MALLOC_ERR);
					state = FOR_ITER_EQ;
				}
			}
			else if (strcmp(tTok.id, "(") == 0)
			{
				for_iter_br++;  // promena uchovavajici pocet zavorek pred znaminkem prirazeni iteratoru
				state = FOR_ITER;
			}
			else return garbage(SYNTAX_ERR);
			break;
		}

		/* hledani = nebo ; */
		case FOR_ITER_EQ:
		{
			if (strcmp(tTok.id, "=") == 0)
			{
				for_iter_flag = true;
				state = VAL_EVAL;
			}
			else if ((strcmp(tTok.id, ")") == 0)) 	// za idckem iteratoru se nachazi jeste ")"
			{											// validni
				for_iter_br--;							// odecti z pocitadla for_iter_br;
				if (for_iter_br == -1)					// iterator nema efekt. jedna se o ERROR?
				{
					my_free(for_iter_type);
					state = FOR_BLOCK;
					for_iter_flag = false;
				}
				state = FOR_ITER_EQ;					// a cykly ve stejnem stavu
			}
			else
			{
				return garbage(SYNTAX_ERR);     			// nalezeno neco, co tam nema co delat (v jazce IFJ15)
			}
			break;
		}

		/* inicializace bloku for s jeho vlastni promennou */
		case FOR_BLOCK:
		{
			if (strcmp(tTok.id, "{") == 0)
			{

				char buff[5];
				sprintf(buff, "%d", blok);
				if (BTAddNodeV(&CurrBlock, buff, 'b', true) != SUCC)		return garbage(NOT_ADDED);
				if (BTAddNodeV(&CurrBlock, desc, (*type), false) != SUCC)	return garbage(NOT_ADDED);
				//if ((FcNodePtr = BTSearchFcName((&FRoot), desc)) != NULL ) 	return garbage(CONFLICT_NAME);
				blok++;
				state = BEGIN;
			}
			else return garbage(SYNTAX_ERR);
		break;
		}

		/* stav po nalezeni tokenu if */
		case IF:
		{
			if (strcmp(tTok.id, "(") == 0)
			{
				if_state = true;
				if (!_alloc(l, auto_type, "(")) return garbage(MALLOC_ERR);
				l++;
				state = VAL_EVAL;
			}
			else
			{
				return garbage(SYNTAX_ERR);
			}
			break;
		}

		/* stav po nalezeni tokenu else */
		case ELSE:
		{
			if (strcmp(tTok.id, "if") == 0)
			{
				state = IF;
			}
			else if (strcmp(tTok.id, "{") == 0)
			{

				char buff[5];
				sprintf(buff, "%d", blok);
				if (BTAddNodeV(&CurrBlock, buff, 'b', true) != SUCC) return garbage(NOT_ADDED);
				blok++;
				state = BEGIN;
			}
			else
			{
				return garbage(SYNTAX_ERR);
			}
			break;
		}

		/* stav po nalezeni tokenu cin */
		case CIN:
		{
			if (strcmp(tTok.id, ">>") == 0)
			{
				state = CIN_NEXT;
			}
			break;
		}

		/* stav, ktery ocekava id po cin >> */
		case CIN_NEXT:
		{
			if (strcmp(tTok.id, "id") == 0){
				if ((VarRoot = BTSearchVarName((&Root), CurrBlock, tTok.desc)) == NULL) return garbage(UNDECLARED_VAR);
				state = CIN_END;
			}
			else {

				return garbage(SYNTAX_ERR);
			}
		break;
		}

		/* stav, ktery ocekava id pro cin >> id  */
		case CIN_END:
		{
			if (strcmp(tTok.id, ">>") == 0)
			{
				state = CIN_NEXT;
			}
			else if (strcmp(tTok.id, ";") == 0 )
			{
				state = BEGIN;
			}
			else return garbage(VAR_ERR);
			break;
		}

		case COUT:
		{
			if (strcmp(tTok.id, "<<") == 0)
			{
				state = COUT_NEXT;
			}
			else return garbage(SYNTAX_ERR);
			break;
		}

		case COUT_NEXT:
		{
			if (strcmp(tTok.id, "id") == 0){
				if ((VarRoot = BTSearchVarName((&Root), CurrBlock, tTok.desc)) == NULL) return garbage(UNDECLARED_VAR);
				arr[0] = VarRoot->type;
				arr[1] = '\0';
				if(!_alloc(l, auto_type, arr)) return garbage(MALLOC_ERR);
				l++;
				state = COUT_END;
			}
			else if (strcmp(tTok.id, "cl") == 0){
				if(!_alloc(l, auto_type, "i")) return garbage(MALLOC_ERR);
				l++;
				state = COUT_END;
			}
			else if (strcmp(tTok.id, "dl") == 0){
				if(!_alloc(l, auto_type, "d")) return garbage(MALLOC_ERR);
				l++;
				state = COUT_END;
			}
			else if (strcmp(tTok.id, "rl") == 0){
				if(!_alloc(l, auto_type, "s")) return garbage(MALLOC_ERR);
				l++;
				state = COUT_END;
			}
			else if (strcmp(tTok.id, "(") == 0){
				if(!_alloc(l, auto_type, "(")) return garbage(MALLOC_ERR);
				l++;
				state = COUT_END;
			}
			else {
				return garbage(SYNTAX_ERR);
			}
		break;
		}

		case COUT_END:
		{
			if (strcmp(tTok.id, "<<") == 0)
			{
				char *arr = GetType(auto_type);
				auto_type = NULL;
				if (arr == NULL) return garbage(VAR_ERR);
				state = COUT_NEXT;
			}
			else if (strcmp(tTok.id, ";") == 0)
			{
				char *arr = GetType(auto_type);
				auto_type = NULL;
				if (arr == NULL) return garbage(VAR_ERR);
				state = BEGIN;
			}
			else if ((strcmp(tTok.id, "+") == 0) ||
				(strcmp(tTok.id, "-") == 0) ||
				(strcmp(tTok.id, "*") == 0) ||
				(strcmp(tTok.id, "/") == 0) ||
				(strcmp(tTok.id, "==") == 0) ||
				(strcmp(tTok.id, "=>") == 0) ||
				(strcmp(tTok.id, "=<") == 0) ||
				(strcmp(tTok.id, "!=") == 0) ||
				(strcmp(tTok.id, "<") == 0) ||
				(strcmp(tTok.id, ">") == 0)) {

				if (strlen(tTok.id) == 2) l++;
				if (!_alloc(l, auto_type, tTok.id)) return garbage(MALLOC_ERR);
				l++;
				state = COUT_NEXT;
				}
			else if ((strcmp(tTok.id, ")") == 0)){
				if (!_alloc(l, auto_type, ")")) return garbage(MALLOC_ERR);
				l++;
				state = COUT_END;
			}
			else {
				return garbage(SYNTAX_ERR);
			}
		break;
		}
		}
	}

	if (BTSearchFcName((&FRoot), "main") == NULL) return garbage(NOT_MAIN);
	free(type);
	free(auto_type);
	free(param_names);
	free(tmp);
	free(f_call);
	free(for_iter_type);
	BTDispose(&Root, &FRoot);
	return SUCC;
}


void _my_free(char** ptr) {
	free(*ptr);
	*ptr = NULL;
}

int garbage(int errcode) {

	BTDispose(&Root, &FRoot);
	my_free(type);
	my_free(auto_type);
	//my_free(param_names);
	my_free(tmp);
	my_free(f_call);
	my_free(for_iter_type);
	//memset(desc, '\0', 65)
	return errcode;
}

bool recast(char* tmp, char* f_call) {

	if (strlen(tmp) != strlen(f_call))
	{
		return false;
	}
	int i = 0;
	while (tmp[i] != '\0') {
		if (!((tmp[i] == f_call[i]) || (tmp[i] == 'i' && f_call[i] == 'd') || (tmp[i] == 'd' && f_call[i] == 'i'))) {
			my_free(tmp);
			my_free(f_call);
			return false;
		}
		i++;
	}
	return true;
}

char *GetType(char * auto_type) {

	char *arr = NULL, *repl = NULL;
	if (strlen(auto_type) == 1)
	{
		arr = (char *)malloc(2 * sizeof(char));
		strcpy(arr, auto_type);
		free(auto_type);
		return arr;
	}

	int iter = 0, tmp = 0;

	bool cmp_flag = false, string_work = false, real_work = false,
		int_work = false, arit_oper = false, div_oper = false, add_work = false;
	while (auto_type[iter] != '\0') {
		if (auto_type[iter] == '(') {
			tmp = iter;
		}
		else if (auto_type[iter] == ')') {
			iter++;
			break;
		}
		iter++;
	}

	arr = (char *)malloc((iter - tmp + 1) * sizeof(char));
	if (arr == NULL) return NULL;
	memset(arr, '\0', (iter - tmp + 1));
	strncpy(arr, auto_type + tmp, (iter - tmp));

	for (int i = 0; arr[i] != '\0'; i++) {

		if ((arr[i] == '=') || (arr[i] == '<') || (arr[i] == '>') || (arr[i] == '!'))
			cmp_flag = true;
		else if (arr[i] == 's')
			string_work = true;
		else if (arr[i] == 'd')
			real_work = true;
		else if (arr[i] == 'i')
			int_work = true;
		else if ((arr[i] == '+'))
			add_work = true;
		else if (((arr[i] == '*')) || ((arr[i] == '-')))
			arit_oper = true;
		else if (arr[i] == '/')
			div_oper = true;
		else if ((arr[i] == '(') && (arr[i + 1] == ')'))
			return NULL;
	}

	if (string_work && (real_work || int_work)) return NULL;
	else if (string_work && (arit_oper || div_oper)) return NULL;
	else if (string_work && add_work) repl = "s";
	else if (cmp_flag && (!arit_oper || !div_oper || !add_work)) repl = "i";
	else if (arit_oper || add_work) {
		if (real_work) repl = "d";
		else repl = "i";
	}
	else if (div_oper && !real_work) repl = "i";
	else if (div_oper && real_work) repl = "d";
	else if (int_work) repl = "i";
	else if (real_work) repl = "d";
	else if (string_work) repl = "s";
	else return NULL;

	arr = (char *)realloc(arr, (strlen(auto_type) + 2) - (iter - tmp));
	if (arr == NULL) return NULL;
	memset(arr, '\0', (((strlen(auto_type)) + 2) - (iter - tmp)));
	strncpy(arr, auto_type, tmp);

	strcat(arr, repl);
	strcat(arr, auto_type + iter);
	free(auto_type);
	if (strlen(arr) == 1) return arr;
	else                arr = GetType(arr);
	return arr;
}

bool __alloc( char **c, char* arr) {

	
	if (*c == NULL)
	{
		*c = (char*)malloc(strlen(arr) + 1);
		strcpy(*c, arr);
		return true;
	}
	else
	{
		char * temp = (char *)malloc(strlen(*c) + strlen(arr) + 1);
		strcpy(temp, *c);
		strcat(temp, arr);
		free(*c);
		*c = temp;
		return true;
	}
}

