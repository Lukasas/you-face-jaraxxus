#include "instruction.h"

#include <malloc.h>
#include <string.h>
struct InsList * InstructionInitQueue(void)
{
	struct InsList * Queue = (struct InsList*)malloc(sizeof(struct InsList) * 1);
	Queue->Instr = NULL;
	Queue->Next = NULL;
	Queue->Prev = NULL;
	Queue->Function = 0;
	return Queue;
}

struct InsList * InstructionInitQueueFunction(void)
{
	struct InsList * Queue = (struct InsList*)malloc(sizeof(struct InsList) * 1);
	Queue->Instr = NULL;
	Queue->Next = NULL;
	Queue->Prev = NULL;
	Queue->Function = 1;
	return Queue;
}

void InstructionFreeParams(struct Instruction * Instr)
{
	if (Instr->instr == PRINTADDSTR)
	{
		if (Instr->param1 != 0)
			free(Instr->param1);
		if (Instr->param2 != 0)
			free(Instr->param1);
		if (Instr->param3 != 0)
			free(Instr->param1);
	}
	if (Instr->instr == FNCALL)
	{
		free(Instr->param2); // CLASSNAME
	}
	if (Instr->instr == STRLOADV)
	{
		free(Instr->param1);
	}
	if (Instr->instr == STRAPPV)
	{
		free(Instr->param1);
	}
	if (Instr->instr == ADDD ||
		Instr->instr == SUBD ||
		Instr->instr == MULD ||
		Instr->instr == DIVD)
	{
		free(Instr->param1);
		free(Instr->param2);
	}
	if (Instr->instr == ADDXVD ||
		Instr->instr == SUBXVD ||
		Instr->instr == MULXVD ||
		Instr->instr == DIVXVD)
	{
		free(Instr->param2);
	}
	if (Instr->instr == ADDVXD ||
		Instr->instr == SUBVXD ||
		Instr->instr == MULVXD ||
		Instr->instr == DIVVXD)
	{
		free(Instr->param1);
	}
	if (Instr->instr == STRAPPD)
		free(Instr->param1);

	if (Instr->instr == TESTDD)
	{
		free(Instr->param1);
		free(Instr->param2);
	}

	if (Instr->instr == TESTVD)
		free(Instr->param2);

	if (Instr->instr == TESTDV)
		free(Instr->param1);

	if (Instr->instr == FNSTS)
		free(Instr->param1);

	if (Instr->instr == FNSTD)
		free(Instr->param1);

	if (Instr->instr == STRLENV)
		free(Instr->param1);
}

void InstructionFreeQueue(struct InsList ** Queue)
{
	if ((*Queue) == NULL)
		return;

	if ((*Queue)->Instr == NULL)
		return;

	while((*Queue)->Prev != NULL)
		(*Queue) = (*Queue)->Prev;

	if ((*Queue)->Next != NULL)
	{
		while ((*Queue)->Next != NULL)
		{
			struct InsList * DelIns = (*Queue)->Next;
			(*Queue)->Next = DelIns->Next;
			InstructionFreeParams(DelIns->Instr);
			free(DelIns->Instr);
			DelIns->Instr = NULL;
			free(DelIns);
		}
	}
	InstructionFreeParams((*Queue)->Instr);
	free((*Queue)->Instr);
	(*Queue)->Instr = NULL;
	free((*Queue));
	(*Queue) = NULL;
}

// Return address can be used for jumps
struct InsList * InstructionAppendToList(struct InsList * Queue, struct Instruction * Instr)
{
	struct InsList * point = Queue;
	if (point->Instr == NULL) /* First item in Queue */
	{
		Queue->Instr = Instr;
	}
	else
	{
		while (point->Next != NULL)
		{
			point = point->Next;
		}

		point->Next = (struct InsList*)malloc(sizeof(struct InsList) * 1);
		point->Next->Next = NULL;
		point->Next->Prev = point;
		point->Next->Instr = Instr;
		point = point->Next;
	}
	return point;
}

struct InsList * InstructionAddToBegin(struct InsList * Queue, struct Instruction * Instr)
{
	struct InsList * point = Queue;
	if (point->Instr == NULL) /* First item in Queue */
	{
		Queue->Instr = Instr;
	}
	else
	{
		while (point->Prev != NULL)
		{
			point = point->Prev;
		}

		point->Prev = (struct InsList*)malloc(sizeof(struct InsList) * 1);
		point->Prev->Next = point;
		point->Prev->Prev = NULL;
		point->Prev->Instr = Instr;
		point = point->Prev;
	}
	return point;
}

struct InsList * InstructionRemoveFromEnd(struct InsList * Queue)
{
	if(Queue->Instr == NULL) // Queue is empty
		return Queue;

	struct InsList * point = Queue;

	while(point->Next != NULL)
	{
		point = point->Next;
	}

	point->Prev->Next = NULL;
	free(point->Instr);
	free(point);
	return Queue;
}

struct InsList * InstructionGotoPos(struct InsList * Queue, int pos)
{
	struct InsList * point = Queue;
	while(point->Prev != NULL)
	{
		point = point->Prev;
	}
	for(int i = 0; point->Next != NULL && i < pos; i++)
		point = point->Next;

	return point;
}

struct InsList * InstructionRemoveFromStart(struct InsList * Queue)
{
	if(Queue->Instr == NULL) // Queue is empty
		return Queue;

	struct InsList * point = Queue;

	while(point->Prev != NULL)
	{
		point = point->Prev;
	}
		
	free(point->Instr);
	point->Instr = NULL;

	if(point->Next != NULL)
	{
		point = point->Next;
		point->Next->Prev = NULL;
		free(point->Prev);
	}

	return point;
}

struct Instruction * InstructionGetFirst(struct InsList * Queue)
{
	struct InsList * point = Queue;
	while(point->Prev != NULL) point = point->Prev;
	return point->Instr;
}

struct Instruction * InstructionCreate(enum Ins Instr, long long int param1, long long int param2, long long int param3)
{
	struct Instruction * InsCrt = (struct Instruction*)malloc(sizeof(struct Instruction) * 1);
	InsCrt->instr = Instr;
	InsCrt->param1 = param1;
	InsCrt->param2 = param2;
	InsCrt->param3 = param3;
	return InsCrt;
}

/*

	ACC <= VAL OP VAL
	ACC <= ACC OP VAL
	ACC <= ACC OP ACC


*/


// ADD

struct InsList * InsSumIntInt(struct InsList * Queue, int p1, int p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(ADD, p1, p2, op));
}

// Add value p1 to accumulator (ACC + p1)
struct InsList * InsSumAccInt(struct InsList * Queue, int p1)
{
	return InsSumIntInt(Queue, p1, 0, opSUM);
}

struct InsList * InsSumVarInt(struct InsList * Queue, long long p1, int p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(ADDXV, p1, p2, op));
}

struct InsList * InsSumIntVar(struct InsList * Queue, long long p1, int p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(ADDVX, p1, p2, op));
}

struct InsList * InsSumVarVar(struct InsList * Queue, long long p1, long long p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(ADDXX, p1, p2, op));
}

struct InsList * InsSumDblDbl(struct InsList * Queue, double p1, double p2, OPCODE op)
{
	double * var1 = (double*)malloc(sizeof(double));
	*var1 = p1;
	double * var2 = (double*)malloc(sizeof(double));
	*var2 = p2;
	return InstructionAppendToList(Queue, InstructionCreate(ADDD, var1, var2, op));
}

// Sums Acc with p1
struct InsList * InsSumAccDbl(struct InsList * Queue, double p1)
{
	return InsSumDblDbl(Queue, p1, 0, opSUM);
}

struct InsList * InsSumVarDbl(struct InsList * Queue, long long p1, double p2, OPCODE op)
{
	double * var2 = (double*)malloc(sizeof(double));
	*var2 = p2;
	return InstructionAppendToList(Queue, InstructionCreate(ADDXVD, p1, var2, op));
}

struct InsList * InsSumAccVarDbl(struct InsList * Queue, long long p1)
{
	return InsSumVarDbl(Queue, p1, 0, opSUM);
}

struct InsList * InsSumDblVar(struct InsList * Queue, double p1, long long p2, OPCODE op)
{
	double * var1 = (double*)malloc(sizeof(double));
	*var1 = p1;
	return InstructionAppendToList(Queue, InstructionCreate(ADDVXD, var1, p2, op));
}

struct InsList * InsSumVarVarD(struct InsList * Queue, long long p1, long long p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(ADDXXD, p1, p2, op));
}

// --------- ADD END

// SUB

struct InsList * InsSubIntInt(struct InsList * Queue, int p1, int p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(SUB, p1, p2, op));
}

struct InsList * InsSubVarInt(struct InsList * Queue, long long p1, int p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(SUBXV, p1, p2, op));
}

struct InsList * InsSubIntVar(struct InsList * Queue, int p1, long long p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(SUBVX, p1, p2, op));
}

struct InsList * InsSubVarVar(struct InsList * Queue, long long p1, long long p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(SUBXX, p1, p2, op));
}

struct InsList * InsSubDblDbl(struct InsList * Queue, double p1, double p2, OPCODE op)
{
	double * var1 = (double*)malloc(sizeof(double));
	*var1 = p1;
	double * var2 = (double*)malloc(sizeof(double));
	*var2 = p2;
	return InstructionAppendToList(Queue, InstructionCreate(SUBD, var1, var2, op));
}

struct InsList * InsSubAccDbl(struct InsList * Queue, double p1)
{
	return InsSubDblDbl(Queue, p1, 0, opSUB);
}

struct InsList * InsSubVarDbl(struct InsList * Queue, long long p1, double p2, OPCODE op)
{
	double * var2 = (double*)malloc(sizeof(double));
	*var2 = p2;
	return InstructionAppendToList(Queue, InstructionCreate(SUBXVD, p1, var2, op));
}

struct InsList * InsSubAccVarDbl(struct InsList * Queue, long long p1)
{
	return InsSubVarDbl(Queue, p1, 0, opSUB);
}

struct InsList * InsSubDblVar(struct InsList * Queue, double p1, long long p2, OPCODE op)
{
	double * var1 = (double*)malloc(sizeof(double));
	*var1 = p1;
	return InstructionAppendToList(Queue, InstructionCreate(SUBVXD, var1, p2, op));
}

struct InsList * InsSubVarVarD(struct InsList * Queue, long long p1, long long p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(SUBXXD, p1, p2, op));
}

// --------- SUB END

// MUL

struct InsList * InsMulIntInt(struct InsList * Queue, int p1, int p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(MUL, p1, p2, op));
}

struct InsList * InsMulVarInt(struct InsList * Queue, long long p1, int p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(MULXV, p1, p2, op));
}

struct InsList * InsMulIntVar(struct InsList * Queue, int p1, long long p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(MULVX, p1, p2, op));
}

struct InsList * InsMulVarVar(struct InsList * Queue, long long p1, long long p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(MULXX, p1, p2, op));
}

struct InsList * InsMulDblDbl(struct InsList * Queue, double p1, double p2, OPCODE op)
{
	double * var1 = (double*)malloc(sizeof(double));
	*var1 = p1;
	double * var2 = (double*)malloc(sizeof(double));
	*var2 = p2;
	return InstructionAppendToList(Queue, InstructionCreate(MULD, var1, var2, op));
}

struct InsList * InsMulAccDbl(struct InsList * Queue, double p1)
{
	return InsMulDblDbl(Queue, p1, 1, opMUL);
}

struct InsList * InsMulVarDbl(struct InsList * Queue, long long p1, double p2, OPCODE op)
{
	double * var2 = (double*)malloc(sizeof(double));
	*var2 = p2;
	return InstructionAppendToList(Queue, InstructionCreate(MULXVD, p1, var2, op));
}

struct InsList * InsMulAccVarDbl(struct InsList * Queue, long long p1)
{
	return InsMulVarDbl(Queue, p1, 1, opMUL);
}

struct InsList * InsMulDblVar(struct InsList * Queue, double p1, long long p2, OPCODE op)
{
	double * var1 = (double*)malloc(sizeof(double));
	*var1 = p1;
	return InstructionAppendToList(Queue, InstructionCreate(MULVXD, var1, p2, op));
}

struct InsList * InsMulVarVarD(struct InsList * Queue, long long p1, long long p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(MULXXD, p1, p2, op));
}

// --------- MUL END

// DIV

struct InsList * InsDivIntInt(struct InsList * Queue, int p1, int p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(DIV, p1, p2, op));
}

struct InsList * InsDivVarInt(struct InsList * Queue, long long p1, int p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(DIVXV, p1, p2, op));
}

struct InsList * InsDivIntVar(struct InsList * Queue, int p1, long long p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(DIVVX, p1, p2, op));
}

struct InsList * InsDivVarVar(struct InsList * Queue, long long p1, long long p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(DIVXX, p1, p2, op));
}

struct InsList * InsDivDblDbl(struct InsList * Queue, double p1, double p2, OPCODE op)
{
	double * var1 = (double*)malloc(sizeof(double));
	*var1 = p1;
	double * var2 = (double*)malloc(sizeof(double));
	*var2 = p2;
	return InstructionAppendToList(Queue, InstructionCreate(DIVD, var1, var2, op));
}

struct InsList * InsDivAccDbl(struct InsList * Queue, double p1)
{
	return InsDivDblDbl(Queue, p1, 1, opDIV);
}

struct InsList * InsDivVarDbl(struct InsList * Queue, long long p1, double p2, OPCODE op)
{
	double * var2 = (double*)malloc(sizeof(double));
	*var2 = p2;
	return InstructionAppendToList(Queue, InstructionCreate(DIVXVD, p1, var2, op));
}

struct InsList * InsDivAccVarDbl(struct InsList * Queue, long long p1)
{
	return InsDivVarDbl(Queue, p1, 1, opDIV);
}

struct InsList * InsDivDblVar(struct InsList * Queue, double p1, long long p2, OPCODE op)
{
	double * var1 = (double*)malloc(sizeof(double));
	*var1 = p1;
	return InstructionAppendToList(Queue, InstructionCreate(DIVVXD, var1, p2, op));
}

struct InsList * InsDivVarVarD(struct InsList * Queue, long long p1, long long p2, OPCODE op)
{
	return InstructionAppendToList(Queue, InstructionCreate(DIVXXD, p1, p2, op));
}

// --------- DIV END

// CLEARING ACCUMULATOR
struct InsList * InsClearIntAcc(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(CLRA, 0, 0, 0));
}

struct InsList * InsClearDblAcc(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(CLRAD, 0, 0, 0));
}

struct InsList * InsSumAccAccDbl(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(SUMAAD, 0, 0, 0));
}

struct InsList * InsSubAccAccDbl(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(SUBAAD, 0, 0, 0));
}

struct InsList * InsMulAccAccDbl(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(MULAAD, 0, 0, 0));
}

struct InsList * InsDivAccAccDbl(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(DIVAAD, 0, 0, 0));
}

struct InsList * InsSwapAccAccDbl(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(SWAPAAD, 0, 0, 0));
}

// Setting accumulator values
struct InsList * InsSetIntAcc(struct InsList * Queue, int p1)
{
	return InstructionAppendToList(Queue, InstructionCreate(SETA, p1, 0, 0));
}

struct InsList * InsSetDblAcc(struct InsList * Queue, double p1)
{
	double * var1 = (double*)malloc(sizeof(double));
	*var1 = p1;
	return InstructionAppendToList(Queue, InstructionCreate(SETAD, var1, 0, 0));
}

// STORING VALUE FROM ACCUMULATOR TO VARIABLE
struct InsList * InsStoreIntVal(struct InsList * Queue, long long int var)
{
	return InstructionAppendToList(Queue, InstructionCreate(STOREA, var, 0, 0));
}

struct InsList * InsStoreDblVal(struct InsList * Queue, long long int var)
{
	return InstructionAppendToList(Queue, InstructionCreate(STORED, var, 0, 0));
}

struct InsList * InsStoreStaticDbl(struct InsList * Queue, long long int var)
{
	return InstructionAppendToList(Queue, InstructionCreate(STORESTATICD, var, 0, 0));
}

// LOADING VALUE TO ACCUMULATOR FROM VARIABLE
struct InsList * InsLoadIntVal(struct InsList * Queue, long long int var)
{
	return InstructionAppendToList(Queue, InstructionCreate(LOADA, var, 0, 0));
}

struct InsList * InsLoadDblVal(struct InsList * Queue, long long int var)
{
	return InstructionAppendToList(Queue, InstructionCreate(LOADD, var, 0, 0));
}

// Initialize Print function
struct InsList * InsPrintInit(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(PRINTINIT, 0, 0, 0));
}

// Adding String into Buffer for Print
struct InsList * InsPrintAddStrVar(struct InsList * Queue, long long int var1, long long int var2, long long int var3)
{
	return InstructionAppendToList(Queue, InstructionCreate(PRINTADD, var1, var2, var3));
}

// Adding plain text into Buffer for Print
struct InsList * InsPrintAddStr(struct InsList * Queue, char * string)
{
	char * holder = malloc(strlen(string) + 1);
	strcpy(holder, string);
	return InstructionAppendToList(Queue, InstructionCreate(PRINTADDSTR, holder, 0, 0));
}

// Executing Print operation
struct InsList * InsPrintExecute(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(PRINT, 0, 0, 0));
}

// Initializing While
struct InsList * InsWhileStart(struct InsList * Queue)
{
	//InstructionAppendToList(Queue, InstructionCreate(TESTCLEAR, 0, 0, 0));	
	return InstructionAppendToList(Queue, InstructionCreate(WHILESTART, 0, 0, 0));
}

// Testing While condition
struct InsList * InsTestIntInt(struct InsList * Queue, int p1, int p2, CMPCODE code)
{
	return InstructionAppendToList(Queue, InstructionCreate(TESTII, p1, p2, code));
}

// Testing While condition
struct InsList * InsTestIntVar(struct InsList * Queue, int p1, long long p2, CMPCODE code)
{
	return InstructionAppendToList(Queue, InstructionCreate(TESTIV, p1, p2, code));
}

struct InsList * InsTestVarInt(struct InsList * Queue, long long p1, int p2, CMPCODE code)
{
	return InstructionAppendToList(Queue, InstructionCreate(TESTVI, p1, p2, code));
}

struct InsList * InsTestIntDbl(struct InsList * Queue, int p1, double p2, CMPCODE code)
{
	double * var2 = (double*)malloc(sizeof(double));
	*var2 = p2;
	return InstructionAppendToList(Queue, InstructionCreate(TESTID, p1, var2, code));
}

struct InsList * InsTestDblInt(struct InsList * Queue, double p1, int p2, CMPCODE code)
{
	double * var1 = (double*)malloc(sizeof(double));
	*var1 = p1;
	return InstructionAppendToList(Queue, InstructionCreate(TESTDI, var1, p2, code));
}

struct InsList * InsTestDblDbl(struct InsList * Queue, double p1, double p2, CMPCODE code)
{
	double * var1 = (double*)malloc(sizeof(double));
	*var1 = p1;
	double * var2 = (double*)malloc(sizeof(double));
	*var2 = p2;
	return InstructionAppendToList(Queue, InstructionCreate(TESTDD, var1, var2, code));
}

struct InsList * InsTestVarDbl(struct InsList * Queue, long long p1, double p2, CMPCODE code)
{
	double * var2 = (double*)malloc(sizeof(double));
	*var2 = p2;
	return InstructionAppendToList(Queue, InstructionCreate(TESTVD, p1, var2, code));
}

struct InsList * InsTestDblVar(struct InsList * Queue, double p1, long long p2, CMPCODE code)
{
	double * var2 = (double*)malloc(sizeof(double));
	*var2 = p2;
	return InstructionAppendToList(Queue, InstructionCreate(TESTDV, p1, var2, code));
}

struct InsList * InsTestVarVar(struct InsList * Queue, long long p1, long long p2, CMPCODE code)
{
	return InstructionAppendToList(Queue, InstructionCreate(TESTVV, p1, p2, code));
}

// While end mark
struct InsList * InsWhileEnd(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(WHILEEND, 0, 0, 0));
}

// Loading string from variable into buffer
struct InsList * InsStrLoadX(struct InsList * Queue, long long p1)
{
	return InstructionAppendToList(Queue, InstructionCreate(STRLOADX, p1, 0, 0));
}

// Loading string from plain text into buffer
struct InsList * InsStrLoadV(struct InsList * Queue, char * string)
{
	char * holder = malloc(strlen(string) + 1);
	strcpy(holder, string);
	return InstructionAppendToList(Queue, InstructionCreate(STRLOADV, holder, 0, 0));
}

// Append string from variable into to buffer
struct InsList * InsStrAppX(struct InsList * Queue, long long p1)
{
	return InstructionAppendToList(Queue, InstructionCreate(STRAPPX, p1, 0, 0));
}

// Append string from plain text into buffer
struct InsList * InsStrAppV(struct InsList * Queue, char * string)
{
	char * holder = malloc(strlen(string) + 1);
	strcpy(holder, string);
	return InstructionAppendToList(Queue, InstructionCreate(STRAPPV, holder, 0, 0));
}

// Clears buffer
struct InsList * InsStrBufClr(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(STRBUFCLR, 0, 0, 0));
}

struct InsList * InsStrAppI(struct InsList * Queue, int p1)
{
	return InstructionAppendToList(Queue, InstructionCreate(STRAPPI, p1, 0, 0));
}

struct InsList * InsStrAppD(struct InsList * Queue, double p1)
{
	double * var1 = malloc(sizeof(double));
	*var1 = p1;
	return InstructionAppendToList(Queue, InstructionCreate(STRAPPD, var1, 0, 0));
}

struct InsList * InsStrAppXI(struct InsList * Queue, long long p1)
{
	return InstructionAppendToList(Queue, InstructionCreate(STRAPPXI, p1, 0, 0));
}

struct InsList * InsStrAppXD(struct InsList * Queue, long long p1)
{
	return InstructionAppendToList(Queue, InstructionCreate(STRAPPXD, p1, 0, 0));
}

struct InsList * InsCallFunction(struct InsList * Queue, long long p1, char * CLASSNAME)
{
	char * holder = malloc(strlen(CLASSNAME) + 1);
	strcpy(holder, CLASSNAME);
	return InstructionAppendToList(Queue, InstructionCreate(FNCALL, p1, holder, 0));
}

struct InsList * InsCallFirstFunction(struct InsList * Queue, long long p1, char * CLASSNAME)
{
	char * holder = malloc(strlen(CLASSNAME) + 1);
	strcpy(holder, CLASSNAME);
	return InstructionAddToBegin(Queue, InstructionCreate(FNCALL, p1, holder, 0));
}

struct InsList * InsSetCurrAcc(struct InsList * Queue, int accID)
{
	return InstructionAppendToList(Queue, InstructionCreate(SETCURACC, accID, 0, 0));
}

struct InsList * InsStrStore(struct InsList * Queue, long long p1)
{
	return InstructionAppendToList(Queue, InstructionCreate(STRSTORE, p1, 0, 0));
}

struct InsList * InsLoadFncParam(struct InsList * Queue, long long p1)
{
	return InstructionAppendToList(Queue, InstructionCreate(FNLDPM, p1, 0, 0));
}

struct InsList * InsDeclareVar(struct InsList * Queue, long long p1)
{
	return InstructionAppendToList(Queue, InstructionCreate(DECLRVAR, p1, 0, 0));
}

struct InsList * InsStoreFncParamS(struct InsList * Queue, char * string)
{
	char * holder = malloc(strlen(string) + 1);
	strcpy(holder, string);
	return InstructionAppendToList(Queue, InstructionCreate(FNSTS, holder, 0, 0));
}

struct InsList * InsStoreFncParamI(struct InsList * Queue, int p1)
{
	return InstructionAppendToList(Queue, InstructionCreate(FNSTI, p1, 0, 0));
}

struct InsList * InsStoreFncParamD(struct InsList * Queue, double p1)
{
	double * var = malloc(sizeof(double));
	*var = p1;
	return InstructionAppendToList(Queue, InstructionCreate(FNSTI, var, 0, 0));
}

struct InsList * InsStoreFncParamX(struct InsList * Queue, long long p1)
{
	return InstructionAppendToList(Queue, InstructionCreate(FNSTX, p1, 0, 0));
}

struct InsList * InsEndFunction(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(FNCEND, 0, 0, 0));
}

struct InsList * InsCallInputRead(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(INPUTSTRING, 0, 0, 0));
}
struct InsList * InsCallInputInt(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(INPUTINT, 0, 0, 0));
}
struct InsList * InsCallInputDouble(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(INPUTDOUBLE, 0, 0, 0));
}

struct InsList * InsCallLenX(struct InsList * Queue, long long p1)
{
	return InstructionAppendToList(Queue, InstructionCreate(STRLENX, p1, 0, 0));
}

struct InsList * InsPrintAddV(struct InsList * Queue, long long p1)
{
	return InstructionAppendToList(Queue, InstructionCreate(PRINTADDX, p1, 0, 0));
}

struct InsList * InsCallLenV(struct InsList * Queue, char * string)
{
	char * holder = malloc(strlen(string) + 1);
	strcpy(holder, string);
	return InstructionAppendToList(Queue, InstructionCreate(STRLENV, holder, 0, 0));
}

//IF
struct InsList * InsIfStart(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(IFSTART, 0, 0, 0));
}

struct InsList * InsIfElse(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(IFELSE, 0, 0, 0));
}

struct InsList * InsIfEnd(struct InsList * Queue)
{
	return InstructionAppendToList(Queue, InstructionCreate(IFEND, 0, 0, 0));
}

/*  STACKS  */

struct stackI* stackPushI(struct stackI* stackPointer, int I)
{   // funkce pushuje tToken na zasobnik, vraci ukazatel na vrchol zasobniku, pokud byl push neuspesny, vraci NULL
	struct stackI* oldPointer = stackPointer;
	struct stackI* newPointer = malloc(sizeof(struct stackI));
	if (newPointer == NULL) return NULL;
	newPointer->a = I;
	newPointer->next = oldPointer;
	return newPointer;
}

struct stackI* stackPopI(struct stackI* stackPointer, int * I)
{   // funkce popuje vrchol zasobniku, data ulozi do ttPointer a vraci novy vrchol zasobniku
	if (stackPointer == NULL)
	{
		*I = -1;
		return NULL;
	}
	struct stackI* oldPointer = stackPointer;
	struct stackI* newPointer = stackPointer->next;

	*I = oldPointer->a;

	free(oldPointer);

	return newPointer;
}

struct stackD* stackPushD(struct stackD* stackPointer, double I)
{   // funkce pushuje tToken na zasobnik, vraci ukazatel na vrchol zasobniku, pokud byl push neuspesny, vraci NULL
	struct stackD* oldPointer = stackPointer;
	struct stackD* newPointer = malloc(sizeof(struct stackD));
	if (newPointer == NULL) return NULL;
	newPointer->a = I;
	newPointer->next = oldPointer;
	return newPointer;
}

struct stackD* stackPopD(struct stackD* stackPointer, double * I)
{   // funkce popuje vrchol zasobniku, data ulozi do ttPointer a vraci novy vrchol zasobniku
	if (stackPointer == NULL)
	{
		*I = -1;
		return NULL;
	}
	struct stackD* oldPointer = stackPointer;
	struct stackD* newPointer = stackPointer->next;

	*I = oldPointer->a;

	free(oldPointer);

	return newPointer;
}

struct stackS* stackPushS(struct stackS* stackPointer, char *I)
{   // funkce pushuje tToken na zasobnik, vraci ukazatel na vrchol zasobniku, pokud byl push neuspesny, vraci NULL
	struct stackS* oldPointer = stackPointer;
	struct stackS* newPointer = malloc(sizeof(struct stackS));
	if (newPointer == NULL) return NULL;
	newPointer->a = malloc(strlen(I) + I);
	strcpy(newPointer->a, I);
	newPointer->next = oldPointer;
	return newPointer;
}

struct stackS* stackPopS(struct stackS* stackPointer, char ** I)
{   // funkce popuje vrchol zasobniku, data ulozi do ttPointer a vraci novy vrchol zasobniku
	if (stackPointer == NULL)
	{
		*I = -1;
		return NULL;
	}
	struct stackS* oldPointer = stackPointer;
	struct stackS* newPointer = stackPointer->next;

	if (*I != NULL)
	{
		free(*I);
	}
	*I = malloc(strlen(oldPointer->a) + 1);
	strcpy(*I, oldPointer->a);
	free(oldPointer->a);
	free(oldPointer);

	return newPointer;
}