#pragma once
#include <malloc.h>
#include <string.h>
#define ST_DATA_TYPE char *
// If ST_DATA_TYPE is a pointer, set FREE_DATA to 1
#define FREE_DATA 1
typedef struct STree
{
	struct STree *parent;
	struct STree *LNode;
	struct STree *RNode;
	ST_DATA_TYPE data;
}STree;

STree * STCreateNode(); // GB Collector needed
// Returns created node
STree * STAddLNode(STree *parent, ST_DATA_TYPE data);
// Returns created node
STree * STAddRNode(STree *parent, ST_DATA_TYPE data);
// Removes all nodes from selected Node
void STRemoveNode(STree * Node);

inline ST_DATA_TYPE STGetValue(STree * Node)
{
	return Node->data;
}

ST_DATA_TYPE STCreateValue(void * data);
#if FREE_DATA == 1
ST_DATA_TYPE STCreateValueChar(char * data, int size);
#endif

inline ST_DATA_TYPE STSetValue(STree * Node, ST_DATA_TYPE data)
{
	if (FREE_DATA)
	{
		if (Node->data != NULL)
			free(Node->data);
	}
	Node->data = data;
}