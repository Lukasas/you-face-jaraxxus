#pragma once

#include "semantics_analyzator.h"
//#include "sem_stack.h"



void SemanticInit()
{
	hashTable = HashTable_Init();
	classNamespace = NULL;
	methodRecord = NULL;
}

void ResetCurrentMethod()
{
	methodRecord = NULL;
}

void ResetCurrentClass()
{
	classNamespace = NULL;
}

// class Main{..}
//done
int DefineClass(SStack *stack)
{
	//methodRecord = NULL;
	SStack *tmp = stack;
	int retcode;
	while (tmp != NULL)
	{
		unsigned id = tmp->tt.token_id;
		if (id == T_KEYWORD)			// KEYWORD CLASS
			;
		else if (id == 1)		// RESERVED , ONLY MAIN ALLOWED
		{
			if (strcmp(tmp->tt.token_value, "Main") != 0)
			{
				HashTable_Free(hashTable);
				return SEM_UNDEFINED_REDEF;
			}
			if (HashTable_FindClass(hashTable, "Main") != NULL)
			{
				HashTable_Free(hashTable);
				return SEM_UNDEFINED_REDEF;
			}
			classNamespace = HashTable_AddClass(hashTable, tmp->tt.token_value);
			return SEM_OK;
		}
		else if (id == T_IDENTIFICATOR)		// ID
		{
			if (HashTable_FindClass(hashTable, tmp->tt.token_value) != NULL)
			{
				HashTable_Free(hashTable);
				return SEM_UNDEFINED_REDEF;
			}
			classNamespace = HashTable_AddClass(hashTable, tmp->tt.token_value);
		}
		else
		{
			HashTable_Free(hashTable);
			return SEM_ELSE;
		}
		tmp = tmp->next;
	}
	return SEM_OK;
}

// static void run()
// static int foo(int i)
//done
int DefineMethod(SStack *stack)
{
	SStack *tmp = stack;
	int retcode, offset;
	char *sig = CreateFunParamSignature(tmp, &retcode, &offset);
	if (sig == NULL)
		return retcode;
	for (int i = 0; i <= offset; i++)
		tmp = tmp->next;
	// tmp --> (
//	tmp = tmp->next;
	// tmp --> id

	if (strcmp(tmp->tt.token_value, "run") == 0)
	{
		if (strlen(sig) != 0)
		{
			free(sig);
			return SEM_ELSE;
		}
	}
	if (strcmp(tmp->tt.token_value, classNamespace->className) == 0)
	{
		free(sig);
		return SEM_UNDEFINED_REDEF;
	}
	SMethodRecord *met = HashTable_FindMethod(hashTable, classNamespace->className, tmp->tt.token_value);
	if (met != NULL)
	{
		free(sig);
		return SEM_UNDEFINED_REDEF;
	}
	char *metName = tmp->tt.token_value;
	tmp = tmp->next;
	char c = VarTypeByStr(tmp->tt.token_value);	// type
	int l = strlen(sig);
	sig = (char*)realloc(sig, l + 2);
	sig[l] = c;
	sig[l + 1] = 0;
	reverseWords(sig);

	methodRecord = HashTable_AddMethodToClass(classNamespace, metName, sig);
	HashTable_AddBlockToMethod(methodRecord, 0);
	//free(sig);
	tmp = stack; // reset
	return DefineFunParams(tmp);
}

// int i;
// static int i;
//done
int DeclareVar(SStack *stack)
{
	if (stack == NULL)
		return -1;
	SStack *tmp = stack;
	EDataTypes d;
	int i = 0;
	//const char *insert;
	char *varName = NULL;
	while (tmp != NULL)
	{
		unsigned id = tmp->tt.token_id;
		if (id == T_IDENTIFICATOR)
		{
			varName = (char*)malloc(strlen(tmp->tt.token_value) * sizeof(char) + 1);
			strcpy(varName, tmp->tt.token_value);
		}
		else if (id == T_KEYWORD)
		{
			i++;
			if (i == 1)
				d = VarEnum(tmp->tt.token_value);
		}
		tmp = tmp->next;
	}
	if (i > 1)	// static var
	{
		if (HashTable_FindStaticVariable(hashTable, classNamespace->className, varName) == NULL)
		{
			HashTable_AddVariableToClass(classNamespace, varName, d, "", false);
		}
		else
		{
			free(varName);
			HashTable_Free(hashTable);
			return SEM_UNDEFINED_REDEF;
		}
	}
	else
	{
		if (HashTable_FindLocalVariable(methodRecord->first, varName) == NULL)
		{
			HashTable_AddVariableToBlock(methodRecord->first, varName, d, "", false);
			free(varName);
		}
		else
		{
			free(varName);
			HashTable_Free(hashTable);
			return SEM_UNDEFINED_REDEF;
		}
	}
	return SEM_OK;
}

// any of
// static type i = 5 + a;
// static type i = a;
// static type i = differentclass.a + 5
// static type i = 5;
//done
int DefineStaticVarWithExpr(SStack *stack)
{
	SStack *tmp = stack;
	int retcode, offset;
	char *sig = CreateSignature(tmp, &retcode, &offset, false);
	if (sig == NULL)
	{
		HashTable_Free(hashTable);
		if (retcode == 15)
			retcode = SEM_UNDEFINED_REDEF;
		return retcode;
	}
	char c = GetType(sig);
	free(sig);
	if (c == 0)
	{
		HashTable_Free(hashTable);
		return SEM_INCOMPATIBLE;
	}
	for (int i = 0; i <= offset + 1; i++)
		tmp = tmp->next;
	char *varName = tmp->tt.token_value;
	int i = DeclareVar(tmp);
	if (i != 0)
		return i;
	SVariableRecord* var = HashTable_FindStaticVariable(hashTable, classNamespace->className, varName);
	if (var == NULL)			// very very bad
	{
		HashTable_Free(hashTable);
		return 99;
	}
	char t = VarType(var->type);
	if (Conversions(c, t))
	{
		var->defined = true;
		return SEM_OK;
	}
	else
	{
		HashTable_Free(hashTable);
		return SEM_INCOMPATIBLE;
	}
}


// int i = foo()
// or 
// int i = 5 + 5
int DefineLocalVar(SStack *stack)
{
	SStack *tmp = stack;
	if (isFunc(tmp))
		return DefineLocalVarWithFunction(stack);
	else
		return DefineLocalVarWithExpr(stack);
}

// any of
// type j = 5 + a;
// type i = 5;
// type i = a;
//done
int DefineLocalVarWithExpr(SStack *stack)
{
	SStack *tmp = stack;
	int retcode, offset;
	char *sig = CreateSignature(tmp, &retcode, &offset, false);
	if (sig == NULL)
	{
		if (retcode != 15)
			HashTable_Free(hashTable);
		return retcode;
	}
	char c = GetType(sig);
	free(sig);
	if (c == 0)
	{
		HashTable_Free(hashTable);
		return SEM_INCOMPATIBLE;
	}
	for (int i = 0; i <= offset + 1; i++)
		tmp = tmp->next;
	char *varName = tmp->tt.token_value;
	int i = DeclareVar(tmp);
	if (i != 0)
		return i;
	SVariableRecord* var = HashTable_FindLocalVariable(methodRecord->first, varName);
	if (var == NULL)			// very very bad
	{
		HashTable_Free(hashTable);
		return 99;
	}
	char t = VarType(var->type);
	if (Conversions(c, t))
	{
		var->defined = true;
		return SEM_OK;
	}
	else
	{
		HashTable_Free(hashTable);
		return SEM_INCOMPATIBLE;
	}
}

// any of
// static type i = foo()	.... apparently not supported... who knew .......
// NOPE
int DefineStaticVarWithFunction(SStack *stack)
{
	SStack *tmp = stack;
	int retcode, offset;
	
	char	*sig = CreateFunCallSignature(tmp, &retcode, &offset);
	//char *sig = Creat(tmp, &retcode, &offset, false);
	if (sig == NULL)
	{
		HashTable_Free(hashTable);
		if (retcode == 15)
			retcode = SEM_UNDEFINED_REDEF;
		return retcode;
	}
	for (int i = 0; i <= offset + 1; i++)
		tmp = tmp->next;
	char *ident;
	char *className;
	char *id;
	bool al = false;
	if (tmp->tt.token_id == S_IDENTIFICATOR_POINT_IDENTIFICATOR)  // tmp --> class.foo
	{
		ident = (char*)malloc((strlen(tmp->tt.token_value) + 1) * sizeof(char));
		if (ident == NULL)		// all hell broke loose
		{
			free(sig);
			HashTable_Free(hashTable);
			return 99;
		}

		strcpy(ident, tmp->tt.token_value);
		className = strtok(ident, ".");
		id = strtok(NULL, ".");
		al = true;
	}
	else // tmp --> foo
	{
		className = (char*)classNamespace->className;
		id = tmp->tt.token_value;
	}
	SMethodRecord *met = HashTable_FindMethod(hashTable, className, id);	// not defined method
	if (al)
		free(ident);
	if (met == NULL)
	{
		free(sig);
		return 15;
	}
	tmp = tmp->next; // tmp --> =
	tmp = tmp->next; // tmp --> id

	char *varName = tmp->tt.token_value;
	int i = DeclareVar(tmp);
	if (i != 0)
	{
		free(sig);
		return i;
	}
	SVariableRecord* var = HashTable_FindStaticVariable(hashTable, classNamespace->className, varName);
	if (var == NULL)			// very very bad
	{
		free(sig);
		HashTable_Free(hashTable);
		return 99;
	}
	char t = VarType(var->type);
	int l = strlen(sig);
	sig = (char*)realloc(sig, (l + 2) * sizeof(char));
	sig[l + 1] = met->retType;
	sig[l + 2] = 0;
	reverseWords(sig);
	if (strcmp(sig, met->typeSignature) == 0)
	{
		if (Conversions(t, met->retType))
			retcode = SEM_OK;
		else
			retcode = SEM_INCOMPATIBLE;
	}
	else
	{
		retcode = SEM_INCOMPATIBLE;
	}
	free(sig);
	return retcode;

}

// any of
// int i = foo(i..)
// int i = c.foo(int i ...)
// int i = foo();
//done
int DefineLocalVarWithFunction(SStack *stack)
{
	SStack *tmp = stack;
	int retcode, offset;

	char	*sig = CreateFunCallSignature(tmp, &retcode, &offset);
	//char *sig = Creat(tmp, &retcode, &offset, false);
	if (sig == NULL)
	{
		if (retcode != 15)
			HashTable_Free(hashTable);
		return retcode;
	}
	for (int i = 0; i <= offset ; i++)
		tmp = tmp->next;
	char *ident;
	char *className;
	char *id;
	bool al = false;
	if (tmp->tt.token_id == S_IDENTIFICATOR_POINT_IDENTIFICATOR)  // tmp --> class.foo
	{
		ident = (char*)malloc((strlen(tmp->tt.token_value) + 1) * sizeof(char));
		if (ident == NULL)		// all hell broke loose
		{
			free(sig);
			HashTable_Free(hashTable);
			return 99;
		}

		strcpy(ident, tmp->tt.token_value);
		className = strtok(ident, ".");
		id = strtok(NULL, ".");
		al = true;
	}
	else // tmp --> foo
	{
		className = (char*)classNamespace->className;
		id = tmp->tt.token_value;
	}
	SMethodRecord *met = HashTable_FindMethod(hashTable, className, id);	// not defined method
	if (al)
		free(ident);
	if (met == NULL)
	{
		free(sig);
		return 15;
	}
	tmp = tmp->next; // tmp --> =
	tmp = tmp->next; // tmp --> id

	char *varName = tmp->tt.token_value;
	int i = DeclareVar(tmp);
	if (i != 0)
	{
		free(sig);
		return i;
	}
	SVariableRecord* var = HashTable_FindLocalVariable(methodRecord->first, varName);
	if (var == NULL)			// very very bad
	{
		free(sig);
		HashTable_Free(hashTable);
		return 99;
	}

	if (strcmp(sig, "v") == 0)
	{
		strcpy(sig, "");
	}

	char t = VarType(var->type);
	int l = strlen(sig);
	sig = (char*)realloc(sig, (l + 2) * sizeof(char));
	sig[l] = met->retType;
	sig[l + 1] = 0;
	reverseWords(sig);
	if (strcmp(sig, met->typeSignature) == 0)
	{
		if (Conversions(t, met->retType))
			retcode = SEM_OK;
		else
			retcode = SEM_INCOMPATIBLE;
	}
	else
	{
		retcode = SEM_INCOMPATIBLE;
	}
	free(sig);
	return retcode;
}

// foo()
// class.foo()
// done
int CheckCall(SStack *stack)
{
	SStack *tmp = stack;
	int retcode, offset;
	char *signature = CreateFunCallSignature(tmp, &retcode, &offset);
	if (signature == NULL)
	{
		if (retcode != 15)
			HashTable_Free(hashTable);
		return retcode;
	}

	char *className;
	char *fName;
	char *ident = malloc(1);
	for (int i = 0; i <= offset; i++)
		tmp = tmp->next;
//	tmp = tmp->next;
	// tmp --> id
	if (tmp->tt.token_id == S_IDENTIFICATOR_POINT_IDENTIFICATOR)
	{
		free(ident);
		ident = (char*)malloc((strlen(tmp->tt.token_value) + 1) * sizeof(char));
		if (ident == NULL)		// all hell broke loose
		{
			free(signature);
			HashTable_Free(hashTable);
			return 99;
		}

		strcpy(ident, tmp->tt.token_value);
		className = strtok(ident, ".");
		fName = strtok(NULL, ".");
	}
	else
	{
		className = classNamespace->className;
		fName = tmp->tt.token_value;
	}
	SMethodRecord *met = HashTable_FindMethod(hashTable, className, fName);
	if (met == NULL) // call not defined
	{
		free(ident);
		return 15;
	}
	free(ident);

	//char *mSig = met->typeSignature;


	if (strcmp(signature, "v") == 0)
	{
		strcpy(signature, "");
	}

	int l = strlen(signature);
	signature = (char*)realloc(signature, (l + 2) * sizeof(char));
	signature[l] = met->retType;
	signature[l + 1] = 0;
	reverseWords(signature);

	if (strcmp(met->typeSignature, signature) == 0)
	{
		retcode =  SEM_OK;
	}
	else
	{
		retcode = SEM_INCOMPATIBLE;
	}
	free(signature);
	return retcode;
}

// int foo(){ ... return 5;}
// Done
int CheckTypeForReturnByExp(SStack *stack)
{
	if (stack == NULL || methodRecord == NULL)
	{
		fprintf(stderr, "STACK %p, METHOD %p", stack, methodRecord);
		return -1;
	}
	//char* typeSignature		= methodRecord->typeSignature;
	char retType = methodRecord->retType;
	int retcode, offset;
	char *dTypes = CreateSignature(stack, &retcode, &offset, false);
	if (dTypes == NULL)
	{
		if (retcode != 15)
			HashTable_Free(hashTable);
		return retcode;
	}

	char c = GetType(dTypes);
	if (Conversions(c, retType))
		retcode = SEM_OK;
	else
		retcode = SEM_INCOMPATIBLE;
	free(dTypes);
	return retcode;

}
// ( i < i ) 
int CheckCondition(SStack *stack)
{
	SStack *tmp = stack;
	int retcode, offset;
	char *sig = CreateSignature(tmp, &retcode, &offset, true);
	if (sig == NULL)
	{
		HashTable_Free(hashTable);
		return SEM_INCOMPATIBLE;
	}
	char c = GetType(sig);
	free(sig);
	if (c == 'b')
	{
		return SEM_OK;
	}
	else
	{
		HashTable_Free(hashTable);
		return SEM_INCOMPATIBLE;
	}
}

// return foo(int i...)
// done
int CheckTypeForReturnByFunc(SStack *stack)
{
	SStack *tmp = stack;
	int retcode, offset;
	char	*sig = CreateFunCallSignature(tmp, &retcode, &offset);
	if (sig == NULL)
	{
		HashTable_Free(hashTable);
		return retcode;
	}

	for (int i = 0; i <= offset; i++)
		tmp = tmp->next;

	//tmp = tmp->next;
	// tmp -- > foo;
	char *ident;
	char *className;
	char *id;
	bool al = false;
	if (tmp->tt.token_id == S_IDENTIFICATOR_POINT_IDENTIFICATOR)  // tmp --> class.foo
	{
		ident = (char*)malloc((strlen(tmp->tt.token_value) + 1) * sizeof(char));
		if (ident == NULL)		// all hell broke loose
		{
			free(sig);
			HashTable_Free(hashTable);
			return 99;
		}
		al = true;
		strcpy(ident, tmp->tt.token_value);
		className = strtok(ident, ".");
		id = strtok(NULL, ".");

	}
	else // tmp --> foo
	{
		className = (char*)classNamespace->className;
		id = tmp->tt.token_value;
	}
	SMethodRecord *met = HashTable_FindMethod(hashTable, className, id);	// not defined method
	if (al)
		free(ident);
	if (met == NULL)
	{
		free(sig);
		return 15;
	}

	int l = strlen(sig);
	sig = (char*)realloc(sig, (l + 2) * sizeof(char));
	sig[l] = met->retType;
	sig[l + 1] = 0;
	reverseWords(sig);

	if ((strcmp(met->typeSignature, sig) == 0) && (met->retType== methodRecord->retType))
	{
		retcode = SEM_OK;
	}
	else
	{
		retcode = SEM_INCOMPATIBLE;
	}
	free(sig);
	return retcode;

}

int CheckTypeForReturn(SStack *stack)
{
	SStack *tmp = stack;
	if (isFunc(stack))
		return CheckTypeForReturnByFunc(stack);
	else
		return CheckTypeForReturnByExp(stack);
}


// i = foo()
// or 
// i = 5 + 5
//donezo
int CheckTypeForExpression(SStack *stack)
{
	SStack *tmp = stack;
	if (isFunc(tmp))
		return CheckTypeForFunCall(stack);
	else
		return CheckTypeForExpressionTruely(stack);
}

/* a = foo() */
//done
int CheckTypeForFunCall(SStack *stack)
{
	SStack *tmp = stack;
	int retcode, offset;

	char	*sig = CreateFunCallSignature(tmp, &retcode, &offset);
	//char *sig = Creat(tmp, &retcode, &offset, false);
	if (sig == NULL)
	{
		if (retcode != 15)
			HashTable_Free(hashTable);
		return retcode;
	}
	for (int i = 0; i <= offset; i++)
		tmp = tmp->next;

	//tmp = tmp->next;
	// tmp -- > foo;
	char *ident;
	char *className;
	char *id;
	bool al = false;
	if (tmp->tt.token_id == S_IDENTIFICATOR_POINT_IDENTIFICATOR)  // tmp --> class.foo
	{
		ident = (char*)malloc((strlen(tmp->tt.token_value) + 1) * sizeof(char));
		if (ident == NULL)		// all hell broke loose
		{
			free(sig);
			HashTable_Free(hashTable);
			return 99;
		}
		al = true;
		strcpy(ident, tmp->tt.token_value);
		className = strtok(ident, ".");
		id = strtok(NULL, ".");

	}
	else // tmp --> foo
	{
		className = (char*)classNamespace->className;
		id = tmp->tt.token_value;
	}
	SMethodRecord *met = HashTable_FindMethod(hashTable, className, id);	// not defined method
	if (al)
		free(ident);
	if (met == NULL)
	{
		free(sig);
		return 15;
	}
	tmp = tmp->next; // tmp --> =
	tmp = tmp->next; // tmp --> id
	SVariableRecord *var;
	if (tmp->tt.token_id == S_IDENTIFICATOR_POINT_IDENTIFICATOR)  // tmp --> class.a
	{
		ident = (char*)malloc((strlen(tmp->tt.token_value) + 1) * sizeof(char));
		if (ident == NULL)		// all hell broke loose
		{
			free(sig);
			HashTable_Free(hashTable);
			return 99;
		}

		strcpy(ident, tmp->tt.token_value);
		className = strtok(ident, ".");
		id = strtok(NULL, ".");
		var = HashTable_FindStaticVariable(hashTable, className, id);

	}
	else // tmp --> a
	{
		className = (char*)classNamespace->className;
		id = tmp->tt.token_value;
		var = HashTable_FindLocalVariable(methodRecord->first, id);
		if (var == NULL)
		{
			var = HashTable_FindStaticVariable(hashTable, className, id);
		}

	}
	if (var == NULL)			// very very bad
	{
		free(sig);
		return 15;
	}

	char t = VarType(var->type);
	int l = strlen(sig);
	sig = (char*)realloc(sig, (l + 2) * sizeof(char));
	sig[l] = met->retType;
	sig[l + 1] = 0;
	reverseWords(sig);
	if (strcmp(sig, met->typeSignature) == 0)
	{
		if (Conversions(t, met->retType))
			retcode = SEM_OK;
		else
			retcode = SEM_INCOMPATIBLE;
	}
	else
	{
		retcode = SEM_INCOMPATIBLE;
	}
	free(sig);
	return retcode;
}

// a = 5 + x; 
// done
int CheckTypeForExpressionTruely(SStack *stack)
{
	SStack			*tmp = stack;
	char			*expRes = NULL;
	int				retCode;
	int				offset;
	unsigned		counter = 0;
	SVariableRecord	*var = NULL;


	char *dTypes = CreateSignature(stack, &retCode, &offset, false);
	if (dTypes == NULL)
		return retCode;
	char res = GetType(dTypes);
	free(dTypes);
	for (int i = 0; i <= offset + 1; i++)
		tmp = tmp->next;

	char *className;
	char *iden;
	bool global = false;
	char *placeholder = (char*)malloc(sizeof(char));
	if (placeholder == NULL)
		return 99;

	if (tmp->tt.token_id == S_IDENTIFICATOR_POINT_IDENTIFICATOR)	//
	{
		free(placeholder);
		placeholder = (char*)malloc(strlen(tmp->tt.token_value) + 1);
		if (placeholder == NULL)
			return 99;
		strcpy(placeholder, tmp->tt.token_value);
		className = strtok(placeholder, ".");
		iden = strtok(NULL, ".");
		global = true;
		var = HashTable_FindStaticVariable(hashTable, className, iden);

	}
	else
	{
		className = classNamespace->className;
		iden = tmp->tt.token_value;
		var = HashTable_FindLocalVariable(methodRecord->first, iden);
		if (var == NULL)
			var = HashTable_FindStaticVariable(hashTable, className, iden);
	}
	free(placeholder);
	if (var == NULL)
	{
			if (global)
				return 15;
			HashTable_Free(hashTable);
			return SEM_UNDEFINED_REDEF;
	}
	
	switch (var->type)
	{
	case INT:
		expRes = "i";
		break;
	case DOUBLE:
		expRes = "d";
		break;
	case STRING:
		expRes = "s";
		break;
	}
	
	//	unsigned retCode = 0;
	if (res == 0)
		retCode = SEM_INCOMPATIBLE;
	if (Conversions(res, expRes[0]))
		retCode = SEM_OK;
	else
		retCode = SEM_INCOMPATIBLE;

	//free(res);
	return retCode;
}

char VarType(EDataTypes e)
{
	switch (e)
	{
	case INT:
		return 'i';
	case DOUBLE:
		return 'd';
	case STRING:
		return 's';
	}
	return 0;
}

void reverseWords(char *s)
{
	char *origin = s;
	int z = 0;
	char *temp = malloc(strlen(origin)+1); 
	for (int i = strlen(origin)-1;i>=0;i--)
	{
		temp[z] = origin[i];
		z++;
	}
	temp[strlen(origin)] = '\0';
	strcpy(s, temp);
	free(temp);
}

char VarTypeByStr(char *str)
{
	if (strcmp(str, "int") == 0)
		return 'i';
	else if (strcmp(str, "double") == 0)
		return 'd';
	else if (strcmp(str, "String") == 0)
		return 's';
	else if (strcmp(str, "void") == 0)
		return 'v';
}

EDataTypes VarEnum(char* arr)
{
	if (strcmp(arr, "int") == 0)
		return INT;
	else if (strcmp(arr, "double") == 0)
		return DOUBLE;
	else
		return STRING;
	
}

char *CreateFunParamSignature(SStack *stack, int *retcode, int *offset)
{
	SStack *tmp = stack;
	int counter = 0;
	int allocator = 0;
	char c;
	char *paramSig = (char*)malloc(sizeof(char));
	paramSig[0] = 0;
	while (tmp->tt.token_id != T_L_BRACKET)
	{
		counter++;
		//f = stackPush(f, &(tmp->tt));
		if (tmp->tt.token_id == T_R_BRACKET || tmp->tt.token_id == T_COMMA || tmp->tt.token_id == T_SEMICOLON || tmp->tt.token_id == T_IDENTIFICATOR)
		{
			tmp = tmp->next;
			continue;
		}
		if (tmp->tt.token_id == T_IDENTIFICATOR_POINT_IDENTIFICATOR)
		{
				free(paramSig);
				*retcode = SEM_ELSE;
				return NULL;
		}
		else if (tmp->tt.token_id == T_KEYWORD)			// type
		{
			c = VarTypeByStr(tmp->tt.token_value);
		}
		paramSig = (char*)realloc(paramSig, allocator + 2);
		if (paramSig == NULL)			// error
		{
			*retcode = 99;
			return NULL;
		}
		paramSig[allocator++] = c;
		paramSig[allocator] = 0;
		tmp = tmp->next;
	}
	*offset = counter;
	*retcode = SEM_OK;
	return paramSig;
}

int DefineFunParams(SStack *stack)
{
	SStack *tmp = stack;
	//int counter = 0;
	EDataTypes e;
	char *paramName;;
	while (tmp->tt.token_id != T_L_BRACKET)
	{
		//counter++;
		if (tmp->tt.token_id == T_R_BRACKET || tmp->tt.token_id == T_COMMA || tmp->tt.token_id == T_SEMICOLON)
		{
			tmp = tmp->next;
			continue;
		}
		else if (tmp->tt.token_id == T_IDENTIFICATOR)
		{
			paramName = tmp->tt.token_value;
		}
		else if (tmp->tt.token_id == T_KEYWORD)
		{
			e = VarEnum(tmp->tt.token_value);
			//e = VarTypeByStr(tmp->tt.token_value);
			if (HashTable_FindLocalVariable(methodRecord->first, paramName) != NULL)
			{
				return SEM_UNDEFINED_REDEF;
			}
			InsLoadFncParam(methodRecord->Function, HashTable_AddVariableToBlock(methodRecord->first, paramName, e, "", true));
		}
		tmp = tmp->next;
	}
	return SEM_OK;
}

char *CreateFunCallSignature(SStack *stack, int *retcode, int *offset)
{
	SStack *tmp = stack;
	int counter = 0;
	int allocator = 0;
	char c;
	char *paramSig = (char*)malloc(sizeof(char));
	paramSig[0] = 0;
	while (tmp->tt.token_id != T_L_BRACKET)
	{
		counter++;
		//f = stackPush(f, &(tmp->tt));
		if (tmp->tt.token_id == T_R_BRACKET || tmp->tt.token_id == T_COMMA || tmp->tt.token_id == T_SEMICOLON)
		{
			tmp = tmp->next;
			continue;
		}
		if (tmp->tt.token_id == T_IDENTIFICATOR)
		{
			SVariableRecord * var = HashTable_FindLocalVariable(methodRecord->first, tmp->tt.token_value);
			if (var == NULL)
			{
				var = HashTable_FindStaticVariable(hashTable, classNamespace->className, tmp->tt.token_value);
				if (var == NULL)			// store the var for later findings
				{
					free(paramSig);
					*retcode = 15;
					return NULL;
				}
			}
			c = VarType(var->type);
		}
		else if (tmp->tt.token_id == T_IDENTIFICATOR_POINT_IDENTIFICATOR)
		{
			char *ident = (char*)malloc((strlen(tmp->tt.token_value) + 1) * sizeof(char));
			if (ident == NULL)		// all hell broke loose
			{
				free(paramSig);
				*retcode =  99;
				return NULL;
			}

			strcpy(ident, tmp->tt.token_value);
			char *className;
			char *id;
			className = strtok(ident, ".");
			id = strtok(NULL, ".");

			SVariableRecord *var = HashTable_FindStaticVariable(hashTable, className, id);
			if (var == NULL)
			{
				// store the var for later finding;
				*retcode = 15;
				free(paramSig);
				return NULL;
			}
			free(ident);
			c = VarType(var->type);
		}
		else				// constant
		{
			switch (tmp->tt.token_id)
			{
			case T_INT:
				c = 'i';
				break;
			case T_DOUBLE:
				c = 'd';
				break;
			case T_STRING:
				c = 's';
				break;
			default:
				*retcode = SEM_ELSE;
				return NULL;
			}
		}
		paramSig = (char*)realloc(paramSig, allocator + 2);
		if (paramSig == NULL)			// error
		{
			*retcode = 99;
			return NULL;
		}
		paramSig[allocator++] = c;
		paramSig[allocator] = 0;
		tmp = tmp->next;
	}
	if (strlen(paramSig) == 0)
	{
		paramSig = (char*)realloc(paramSig, allocator + 2);
		if (paramSig == NULL)			// error
		{
			*retcode = 99;
			return NULL;
		}
		paramSig[allocator++] = 'v';
		paramSig[allocator] = 0;
	}
	*offset = counter;
	*retcode = SEM_OK;
	return paramSig;
}

bool Conversions(char a, char b)
{
	if (a == b || (a == 'd' && b == 'i') /*|| (a == 'i' && b == 'd')*/)
		return true;
	else
		return false;
}

char *CreateSignature(SStack *stack, int *retcode, int *offset, bool condition)
{
	SStack *tmp = stack;
	int counter = 0;
	int relCounter = 0;
	char	*dTypes = (char*)malloc(sizeof(char) * (REALOC_NUM + 1));

	if (dTypes == NULL)
	{
		*retcode = 99;
		return NULL;
	}
	while (tmp != NULL)
	{
		unsigned id = tmp->tt.token_id;
		if (counter > REALOC_NUM)
		{
			dTypes = (char*)realloc(dTypes, (counter + 2)* sizeof(char));
			dTypes[counter] = '\0';
		}
		//unsigned id = tmp->tt.token_id;
		
		if (id == T_ASSIGN)
			break;
		if (strcmp(tmp->tt.token_value, "return") == 0)
		{
			if (counter == 0)
				*retcode = 42;
			break;
		}
		if (id == T_SEMICOLON)
		{
			tmp = tmp->next;
			continue;
		}
		else if (id == T_IDENTIFICATOR_POINT_IDENTIFICATOR)
		{
			char *all = (char*)malloc(strlen(tmp->tt.token_value) + 1);
			if (all == NULL)
			{
				free(dTypes);
				*retcode = 99;
				return NULL;
			}
			strcpy(all, tmp->tt.token_value);
			char *className = NULL;
			char *identifier = NULL;
			className = strtok(all, ".");
			identifier = strtok(NULL, ".");
			SVariableRecord *var = HashTable_FindStaticVariable(hashTable, className, identifier);
			free(all);
			if (var == NULL)
			{
				free(dTypes);
				*retcode = 15;
				return NULL;
			}
			switch (var->type)
			{
			case INT:
				strcpy(dTypes + counter, "i");
				break;
			case DOUBLE:
				strcpy(dTypes + counter, "d");
				break;
			case STRING:
				strcpy(dTypes + counter, "s");
				break;
			}

		}
		else if (id == T_IDENTIFICATOR)				// id
		{
			//id = 2;
			SVariableRecord* var;
			if (methodRecord != NULL)
				var = HashTable_FindLocalVariable(methodRecord->first, tmp->tt.token_value);
			else
				var = NULL;
			if (var == NULL)
			{
				var = HashTable_FindStaticVariable_ByPointer(classNamespace, tmp->tt.token_value);
				if (var == NULL)
				{
					free(dTypes);
					*retcode = 15;
					return NULL;
				}
			}

			switch (var->type)
			{
			case INT:
				strcpy(dTypes + counter, "i");
				break;
			case DOUBLE:
				strcpy(dTypes + counter, "d");
				break;
			case STRING:
				strcpy(dTypes + counter, "s");
				break;
			}
		}
		else if (id == T_INT)			// int 
			strcpy(dTypes + counter, "i");
		else if (id == T_DOUBLE)			// double
			strcpy(dTypes + counter, "d");
		else if (id == T_ADD)
			strcpy(dTypes + counter, "+");
		else if (id == T_MUL)
			strcpy(dTypes + counter, "*");
		else if (id == T_SUB)
			strcpy(dTypes + counter, "-");
		else if (id == T_DIV)
			strcpy(dTypes + counter, "/");
		else if ((id >= T_SMALLER && id <= T_EQ) || id == T_NOT_EQ)
		{
			if (condition)
			{
				relCounter++;
				strcpy(dTypes + counter, "<");				// na relacnim operatoru nezalezi, typ to nezmeni
			}
			else
			{
				free(dTypes);
				*retcode = SEM_INCOMPATIBLE;
				return NULL;
			}

		}
		else if (id == T_L_BRACKET)			//  (
			strcpy(dTypes + counter, ")");
		else if (id == T_R_BRACKET)
			strcpy(dTypes + counter, "(");
		else if (id == T_STRING)			// string
			strcpy(dTypes + counter, "s");
		else
		{
			break;
		}
		counter++;
		//dTypes[counter] = 0;
		tmp = tmp->next;
	}
	if (relCounter > 1 || (condition && relCounter == 0))
	{
		free(dTypes);
		*retcode = SEM_INCOMPATIBLE;
		return NULL;

	} 
	if (*retcode == 42)
	{
		strcpy(dTypes + counter, "v");
		*retcode == SEM_OK;
	}
	*offset = counter;
	return dTypes;
}


bool isFunc(SStack *stack)
{
	SStack *tmp = stack;
	bool bracket = false;
	while (tmp->tt.token_id != T_ASSIGN)
	{
		if (tmp->tt.token_id == T_KEYWORD)
		{
			if (strcmp(tmp->tt.token_value, "return") == 0)
				break;
		}
		if (tmp->tt.token_id == T_L_BRACKET)
		{
			bracket = true;
		}
		else if (bracket && (tmp->tt.token_id == T_IDENTIFICATOR_POINT_IDENTIFICATOR || tmp->tt.token_id == T_IDENTIFICATOR))
		{
			return true;
		}
		else
		{
			bracket = false;
		}
		tmp = tmp->next;
	}
	return false;
}


typedef struct SItem
{
	struct SItem *prev;
	char c;
}SItem;

SItem *push_p(SItem *a, char c)
{
	SItem * si = (SItem*)malloc(sizeof(SItem));
	si->prev = a;
	si->c = c;
	return si;
}

SItem *pop(SItem *a)
{
	if (a->prev == NULL)
		return a;
	SItem * si = a->prev;
	free(a);
	return si;
}

char top(SItem *a)
{
	return a->c;
}

int stack_len(SItem *s)
{
	SItem *tmp = s;
	int i = 0;
	while (tmp != NULL)
	{
		i++;
		tmp = tmp->prev;
	}
	return i;
}

int Prior(char c)
{
	switch (c)
	{
	case '-':
		return 1;
		break;
	case '+':
		return 2;
		break;
	case '*':
		return 3;
		break;
	case '/':
		return 4;
		break;
	case '<':
		return 0;
		break;
	}
	return -1;
}

char* PostFixConverter(const char * input)
{
	SItem * s = (SItem*)malloc(sizeof(SItem));
	s->prev = NULL;
	s->c = 0;
	char *postFixInput = (char*)malloc(sizeof(char)*1);
	char c;
	char elem;
	bool jump = false;
	unsigned allocated = 1;
	unsigned u = 0;

	for (unsigned int i = 0; i < strlen(input); i++)
	{
		c = input[i];
		if (c == '+' || c == '-' || c == '*' || c == '/' || c == '<')
		{
			if (!jump)
				while (Prior(c) <= Prior(top(s)))
				{
					elem = top(s);
					postFixInput = (char*)realloc(postFixInput, ++allocated * sizeof(char));
					postFixInput[u++] = elem;
					postFixInput[u] = 0;
					//printf("%c", elem);
					s = pop(s);
				}
			s = push_p(s, c);
		}
		else if (c == '(')
		{
			s = push_p(s, c);
			jump = true;
		}
		else if (c == ')')
		{
			jump = false;
			while (top(s) != '(')
			{
				postFixInput = (char*)realloc(postFixInput, ++allocated * sizeof(char));
				postFixInput[u++] = top(s);
				postFixInput[u] = 0;
				//printf("%c", top(s));
				s = pop(s);
			}
			s = pop(s);
		}
		else
		{
			postFixInput = (char*)realloc(postFixInput, ++allocated * sizeof(char));
			postFixInput[u++] = c;
			postFixInput[u] = 0;
			//printf("%c", c);
		}
	}

	while (top(s) != NULL)
	{
		postFixInput = (char*)realloc(postFixInput, ++allocated * sizeof(char));
		postFixInput[u++] = top(s);
		postFixInput[u] = 0;
		//printf("%c", top(s));
		s = pop(s);
	}
	return postFixInput;
}

void stack_free(SItem *s)
{
	SItem *tmp = s;
	while (s != NULL)
	{
		tmp = s->prev;
		free(s);
		s = tmp;
	}
}

bool isOperator(char c)
{
	if (c == '*' || c == '/' || c == '+' || c == '-' || c == '<')
		return true;
	return false;
}

char ExpResult(char l, char r, char operand)
{
	switch (operand)
	{
	case '*':
		if (l == 's' || r == 's')
			return 'x';
		else if (l == 'd' || r == 'd')
			return 'd';
		else
			return 'i';
		break;
	case '/':
		if (l == 's' || r == 's')
			return 'x';
		else if (l == 'i' && r == 'i')
			return 'i';
		else
			return 'd';
		break;
	case '-':
		if (l == 's' || r == 's')
			return 'x';
		else if (l == 'd' || r == 'd')
			return 'd';
		else
			return 'i';
		break;
	case '+':
		if (l == 's' || r == 's')
			return 's';
		else if (l == 'd' || r == 'd')
			return 'd';
		else
			return 'i';
		break;
	case '<':
		if (l == 's' || r == 's')
			return 'x';
		else if (l == r)
			return 'b';
		else
			return 'b';
		break;
	}
	return 'x';
}

char GetType(char* signature)
{
	SItem * s = (SItem*)malloc(sizeof(SItem));
	s->prev = NULL;
	s->c = 0;
	char *postfix = PostFixConverter(signature);
	//char operands[2];
	unsigned i = 0;
	while (true)
	{

		while (!isOperator(postfix[i]))
		{
			if (postfix[i] == 0)
				break;
			s = push_p(s, postfix[i++]);
			
		}
			

		if (stack_len(s) == 2)
			break;
		else
		{
			switch (postfix[i])
			{
			case '/':
			case '-':
			case '*':
			case '<':
			case '+':
			{
				char l = top(s); s = pop(s);
				char r = top(s); s = pop(s);
				char res = ExpResult(l, r, postfix[i++]);
				if (res != 'x')
				{
					if (res == 'b')
					{
						if (strlen(postfix) != 3)
						{
							free(postfix);
							stack_free(s);
							return 0;
						}
					}
					s = push_p(s, res);
				}
				else
				{
					free(postfix);
					stack_free(s);
					return 0;
				}
				break;
			}
			i++;
			}
		}
	}
	char r = top(s);
	s = pop(s);
	free(s);
	free(postfix);
	return r;
}