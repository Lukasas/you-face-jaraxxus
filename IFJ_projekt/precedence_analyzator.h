#pragma once

#include "lexical_analyzator.h"
#include "token_stack.h"
#include "ial.h"
#include "instruction.h"
#include "semantics_analyzator.h"

#define PTABLESIZE 14

enum types {
	E_ADD = 0,						//0  -------Terminaly-------
	E_SUB,							//1
	E_MUL,							//2
	E_DIV,							//3
	E_SMALLER,						//4
	E_GREATER,						//5
	E_SMALLER_EQUAL,				//6
	E_GREATER_EQUAL,				//7
	E_EQUAL,						//8
	E_NOT_EQUAL,					//9
	E_L_BRACKET,					//10
	E_R_BRACKET,					//11
	E_IDENTIFICATOR,				//12
	E_SEMICOLON,					//13
	E_DATATYPE,						//14  -------Terminaly-------
	E_EXPRESSION,					//15
	E_LESS,							//16
	E_ACCUMULATOR_1,				//17
	E_ACCUMULATOR_2					//18
};

static exprRules[13][3] = {
	{ E_IDENTIFICATOR, -1, -1 },										//0
	{ E_DATATYPE, -1, -1 },												//1
	{ E_EXPRESSION, E_ADD, E_EXPRESSION },								//2
	{ E_EXPRESSION, E_SUB, E_EXPRESSION },								//3
	{ E_EXPRESSION, E_MUL, E_EXPRESSION },								//4
	{ E_EXPRESSION, E_DIV, E_EXPRESSION },								//5
	{ E_EXPRESSION, E_SMALLER, E_EXPRESSION },							//6
	{ E_EXPRESSION, E_GREATER, E_EXPRESSION },							//7
	{ E_EXPRESSION, E_SMALLER_EQUAL, E_EXPRESSION },					//8
	{ E_EXPRESSION, E_GREATER_EQUAL, E_EXPRESSION },					//9
	{ E_EXPRESSION, E_EQUAL, E_EXPRESSION },							//10
	{ E_EXPRESSION, E_NOT_EQUAL, E_EXPRESSION },						//11
	{ E_L_BRACKET, E_EXPRESSION, E_R_BRACKET }							//12
};

static char precedence_table[PTABLESIZE][PTABLESIZE] = {
	/*         +   -   *   /   <   >   <=  >=  ==  !=  (   )   id  ; */
	/* +  */{ '>','>','<','<','>','>','>','>','>','>','<','>','<','>' },
	/* -  */{ '>','>','<','<','>','>','>','>','>','>','<','>','<','>' },
	/* *  */{ '>','>','>','>','>','>','>','>','>','>','<','>','<','>' },
	/* /  */{ '>','>','>','>','>','>','>','>','>','>','<','>','<','>' },
	/* <  */{ '<','<','<','<','>','>','>','>','>','>','<','>','<','>' },
	/* >  */{ '<','<','<','<','>','>','>','>','>','>','<','>','<','>' },
	/* <= */{ '<','<','<','<','>','>','>','>','>','>','<','>','<','>' },
	/* >= */{ '<','<','<','<','>','>','>','>','>','>','<','>','<','>' },
	/* == */{ '<','<','<','<','>','>','>','>','>','>','<','>','<','>' },
	/* != */{ '<','<','<','<','>','>','>','>','>','>','<','>','<','>' },
	/* (  */{ '<','<','<','<','<','<','<','<','<','<','<','=','<','X' },
	/* )  */{ '>','>','>','>','>','>','>','>','>','>','X','>','X','>' },
	/* id */{ '>','>','>','>','>','>','>','>','>','>','X','>','X','>' },
	/* ;  */{ '<','<','<','<','<','<','<','<','<','<','<','X','<','X' }
};

int Rule_expression(FILE *f, struct tToken *tt);