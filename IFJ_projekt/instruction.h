#pragma once

enum Ins
{
	END = 0,
	CLRA, // param1 = ignored, param2 = ignored
	CLRAD,
	SETA, // param1 = int, param2 ignored
	SETAD,
	SETCURACC,
	ADD, // param1 = int, param2 = int
	ADDXV, // param1 = VarAddr, param2 = int
	ADDVX,
	ADDXX, // param 1 = VarAddr, param2 = VarAddr
	SUB, // param1 = int, param2 = int | param1 - param2
	SUBXV, // param1 = VarAddr, param2 = int | param1 - param2
	SUBVX,
	SUBXX, // param1 = VarAddr, param2 = Var | param1 - param2
	MUL, // param1 = int, param2 = int
	MULXV, // param1 = VarAddr, param2 = int
	MULVX,
	MULXX, // param 1 = VarAddr, param2 = VarAddr
	DIV, // param1 = int, param2 = int
	DIVXV, // param1 = VarAddr, param2 = int
	DIVVX,
	DIVXX, // param 1 = VarAddr, param2 = VarAddr
	ADDD, // param1 = int, param2 = int
	ADDXVD, // param1 = VarAddr, param2 = int
	ADDVXD,
	ADDXXD, // param 1 = VarAddr, param2 = VarAddr
	SUBD, // param1 = int, param2 = int | param1 - param2
	SUBXVD, // param1 = VarAddr, param2 = int | param1 - param2
	SUBVXD,
	SUBXXD, // param1 = VarAddr, param2 = Var | param1 - param2
	MULD, // param1 = int, param2 = int
	MULXVD, // param1 = VarAddr, param2 = int
	MULVXD,
	MULXXD, // param 1 = VarAddr, param2 = VarAddr
	DIVD, // param1 = int, param2 = int
	DIVXVD, // param1 = VarAddr, param2 = int
	DIVVXD,
	DIVXXD, // param 1 = VarAddr, param2 = VarAddr
	SUMAAD,
	SUBAAD,
	MULAAD,
	DIVAAD,
	SWAPAAD,
	LOADA, // param1 = VarAddr, param2 = ignored
	STOREA, // param1 = VarAddr, param2 = ignored
	LOADD,
	STORED,
	STORESTATICD,
	JUMP, // param1 = PosJump, param2 = ignored
	// STRING INSTRUCTIONS -----------------------
	STRLOADX, // param1 = VarAddr		InsStrLoadX
	STRLOADV, // param1 = "String"		InsStrLoadV
	STRAPPX, // param1 = VarAddr		InsStrAppX
	STRAPPV, // param1 = "String"		InsStrAppV
	STRAPPI, // param1 = Int			InsStrAppI
	STRAPPD, // param1 = Dbl			InsStrAppD
	STRAPPXI, // param1 = IntVar			
	STRAPPXD, // param1 = DblVar			
	STRBUFCLR, // param1-3 = ignored	| Clear buffer
	STRBUFADD, // TODO: Create function, case in ExecuteInstruction
	STRALC, // param1 = VarAddrAddr, param2 = len
	STRCAT, // param1 = VarStrAddr, param2 = VarStrAddr
	STRSTORE, // param1 = VarStrAddr
	// STRING INSTRUCTIONS END -----------------------
	PRINTINIT, // param1-3 Ignored
	PRINTADD, // param1-3 VarStrAddrs
	PRINTADDSTR,
	PRINTADDI,
	PRINTADDD,
	PRINTADDX,
	PRINT, // param1-3 Ignored
	FNCALL, // param1 = , param2 = 
	FNLDPM,
	FNCEND,
	SETMETHOD,
	TESTCLEAR,
	WHILESTART,
	WHILEEND,
	IFSTART,
	IFEND,
	IFELSE,
	TESTII,
	TESTIV,
	TESTVI,
	TESTDD,
	TESTVD,
	TESTDV,
	TESTVV,
	TESTID,
	TESTDI,
	STATICLFAI, // Load from accumulator into static int variable
	STATICLFAD, // Load from accumulator into static double variable
	STATICLFAS, // Load from accumulator into static string variable
	DECLRVAR, // Set that the point of declaration has occured
	FNSTS, // STORE STRING FOR FUNCTION
	FNSTI, // STORE INT FOR FUNCTION
	FNSTD, // STORE DOUBLE FOR FUNCTION
	FNSTX, // STORE VAR FOR FUNCTION
	INPUTINT, // InsCallInputInt
	INPUTDOUBLE, // InsCallInputDouble
	INPUTSTRING, // InsCallInputRead
	STRLENX, // InsCallLenX
	STRLENV,
	COMPARE,
	FIND,
	SORT,
};

typedef enum OPERATION
{
	opSUM,
	opSUB,
	opMUL,
	opDIV
} OPCODE;

typedef enum COMPARE
{
	EQ, // ==
	LT, // <
	GT, // >
	LEQ, // <=
	GEQ, // >=
	NEQ,
} CMPCODE;

typedef struct InstrJump
{
	int retpos;
	struct InstrJump * next;
} InstrJump;

typedef struct Instruction
{
	enum Ins instr;
	long long int param1;
	long long int param2;
	long long int param3;
} Instruction;

typedef struct InsList
{
	struct Instruction * Instr;
	struct InsList * Prev;
	struct InsList * Next;
	int Function;
} insList;

struct InsList * Program;

struct InsList * InstructionInitQueue(void);
struct InsList * InstructionInitQueueFunction(void);
void InstructionFreeQueue(struct InsList ** Queue);
struct InsList * InstructionAppendToList(struct InsList * Queue, struct Instruction * Instr); 
struct InsList * InstructionAddToBegin(struct InsList * Queue, struct Instruction * Instr);
struct InsList * InstructionRemoveFromEnd(struct InsList * Queue);
struct InsList * InstructionRemoveFromStart(struct InsList * Queue);
struct Instruction * InstructionGetFirst(struct InsList * Queue);
struct InsList * InstructionGotoPos(struct InsList * Queue, int pos);

struct InstrJump *popJump(struct InstrJump * jmpStack, int * returnpos);
struct InstrJump *pushJump(struct InstrJump * jmpStack, int returnpos);
struct Instruction * InstructionCreate(enum Ins Instr, long long int param1, long long int param2, long long int param3);

struct InsList * InsSetCurrAcc(struct InsList * Queue, int accID);

//ADD
struct InsList * InsSumIntInt(struct InsList * Queue, int val1, int val2, OPCODE op);
struct InsList * InsSumAccInt(struct InsList * Queue, int p1);
struct InsList * InsSumVarInt(struct InsList * Queue, long long val1, int val2, OPCODE op);
struct InsList * InsSumIntVar(struct InsList * Queue, long long p1, int p2, OPCODE op);
struct InsList * InsSumVarVar(struct InsList * Queue, long long val1, long long val2, OPCODE op);

//SUB
struct InsList * InsSubIntInt(struct InsList * Queue, int val1, int val2, OPCODE op);
struct InsList * InsSubVarInt(struct InsList * Queue, long long p1, int p2, OPCODE op);
struct InsList * InsSubIntVar(struct InsList * Queue, int p1, long long p2, OPCODE op);
struct InsList * InsSubVarVar(struct InsList * Queue, long long p1, long long p2, OPCODE op);

//MUL
struct InsList * InsMulIntInt(struct InsList * Queue, int p1, int p2, OPCODE op);
struct InsList * InsMulVarInt(struct InsList * Queue, long long p1, int p2, OPCODE op);
struct InsList * InsMulIntVar(struct InsList * Queue, int p1, long long p2, OPCODE op);
struct InsList * InsMulVarVar(struct InsList * Queue, long long p1, long long p2, OPCODE op);

//DIV
struct InsList * InsDivIntInt(struct InsList * Queue, int p1, int p2, OPCODE op);
struct InsList * InsDivVarInt(struct InsList * Queue, long long p1, int p2, OPCODE op);
struct InsList * InsDivIntVar(struct InsList * Queue, int p1, long long p2, OPCODE op);
struct InsList * InsDivVarVar(struct InsList * Queue, long long p1, long long p2, OPCODE op);

//ADD --------------------------------- DOUBLE ---------------------------------
struct InsList * InsSumDblDbl(struct InsList * Queue, double p1, double p2, OPCODE op);
struct InsList * InsSumVarDbl(struct InsList * Queue, long long p1, double p2, OPCODE op);
struct InsList * InsSumDblVar(struct InsList * Queue, double p1, long long p2, OPCODE op);
struct InsList * InsSumVarVarD(struct InsList * Queue, long long p1, long long p2, OPCODE op);
struct InsList * InsSumAccDbl(struct InsList * Queue, double p1);
struct InsList * InsSumAccVarDbl(struct InsList * Queue, long long p1);

//SUB
struct InsList * InsSubDblDbl(struct InsList * Queue, double p1, double p2, OPCODE op);
struct InsList * InsSubVarDbl(struct InsList * Queue, long long p1, double p2, OPCODE op);
struct InsList * InsSubDblVar(struct InsList * Queue, double p1, long long p2, OPCODE op);
struct InsList * InsSubVarVarD(struct InsList * Queue, long long p1, long long p2, OPCODE op);
struct InsList * InsSubAccDbl(struct InsList * Queue, double p1);
struct InsList * InsSubAccVarDbl(struct InsList * Queue, long long p1);

//MUL
struct InsList * InsMulDblDbl(struct InsList * Queue, double p1, double p2, OPCODE op);
struct InsList * InsMulVarDbl(struct InsList * Queue, long long p1, double p2, OPCODE op);
struct InsList * InsMulDblVar(struct InsList * Queue, double p1, long long p2, OPCODE op);
struct InsList * InsMulVarVarD(struct InsList * Queue, long long p1, long long p2, OPCODE op);
struct InsList * InsMulAccDbl(struct InsList * Queue, double p1);
struct InsList * InsMulAccVarDbl(struct InsList * Queue, long long p1);

//DIV
struct InsList * InsDivDblDbl(struct InsList * Queue, double p1, double p2, OPCODE op);
struct InsList * InsDivVarDbl(struct InsList * Queue, long long p1, double p2, OPCODE op);
struct InsList * InsDivDblVar(struct InsList * Queue, double p1, long long p2, OPCODE op);
struct InsList * InsDivVarVarD(struct InsList * Queue, long long p1, long long p2, OPCODE op);
struct InsList * InsDivAccDbl(struct InsList * Queue, double p1);
struct InsList * InsDivAccVarDbl(struct InsList * Queue, long long p1);

//Clears the Accumulator for Int calculations
struct InsList * InsClearIntAcc(struct InsList * Queue);

struct InsList * InsClearDblAcc(struct InsList * Queue);

struct InsList * InsSetIntAcc(struct InsList * Queue, int p1);

struct InsList * InsSetDblAcc(struct InsList * Queue, double p1);

//Stores the value from Accumulator to Variable
// WORKING WITH STRING
struct InsList * InsStoreIntVal(struct InsList * Queue, long long int var);
struct InsList * InsStoreDblVal(struct InsList * Queue, long long int var);
struct InsList * InsLoadIntVal(struct InsList * Queue, long long int var);
struct InsList * InsLoadDblVal(struct InsList * Queue, long long int var);

struct InsList * InsSumAccAccDbl(struct InsList * Queue);
struct InsList * InsSubAccAccDbl(struct InsList * Queue);
struct InsList * InsMulAccAccDbl(struct InsList * Queue);
struct InsList * InsDivAccAccDbl(struct InsList * Queue);


// PRINT

// Initialize print for print operations
struct InsList * InsPrintInit(struct InsList * Queue);

// Adds string variables to print buffer
// Var1 - Address to string Variable
// Var2 - (Optional) Address to string Variable
// Var3 - (Optional) Address to string Variable
struct InsList * InsPrintAddStrVar(struct InsList * Queue, long long int var1, long long int var2, long long int var3);

// Experimantal
struct InsList * InsPrintAddStr(struct InsList * Queue, char * string);

// Executes the Print
struct InsList * InsPrintExecute(struct InsList * Queue);
struct InsList * InsPrintAddI(struct InsList * Queue, int p1);
struct InsList * InsPrintAddD(struct InsList * Queue, double p1);
struct InsList * InsPrintAddV(struct InsList * Queue, long long p1);

struct InsList * InsStrBufClr(struct InsList * Queue);
struct InsList * InsStrLoadX(struct InsList * Queue, long long p1);
struct InsList * InsStrLoadV(struct InsList * Queue, char * string);
struct InsList * InsStrAppX(struct InsList * Queue, long long p1);
struct InsList * InsStrAppV(struct InsList * Queue, char * string);
struct InsList * InsStrAppI(struct InsList * Queue, int p1);
struct InsList * InsStrAppD(struct InsList * Queue, double p1);
struct InsList * InsStrAppXI(struct InsList * Queue, long long p1);
struct InsList * InsStrAppXD(struct InsList * Queue, long long p1);
struct InsList * InsStrStore(struct InsList * Queue, long long p1);

	//
	// p1 = SMethodRecord *
struct InsList * InsCallFunction(struct InsList * Queue, long long p1, char * CLASSNAME);
struct InsList * InsEndFunction(struct InsList * Queue);
struct InsList * InsCallFirstFunction(struct InsList * Queue, long long p1, char * CLASSNAME);

// COMPARE
struct InsList * InsTestIntInt(struct InsList * Queue, int p1, int p2, CMPCODE code);
struct InsList * InsTestIntVar(struct InsList * Queue, int p1, long long p2, CMPCODE code);
struct InsList * InsTestVarInt(struct InsList * Queue, long long p1, int p2, CMPCODE code);
struct InsList * InsTestIntDbl(struct InsList * Queue, int p1, double p2, CMPCODE code);
struct InsList * InsTestDblInt(struct InsList * Queue, double p1, int p2, CMPCODE code);
struct InsList * InsTestDblDbl(struct InsList * Queue, double p1, double p2, CMPCODE code);
struct InsList * InsTestVarDbl(struct InsList * Queue, long long p1, double p2, CMPCODE code);
struct InsList * InsTestDblVar(struct InsList * Queue, double p1, long long p2, CMPCODE code);
struct InsList * InsTestVarVar(struct InsList * Queue, long long p1, long long p2, CMPCODE code);

struct InsList * InsLoadFncParam(struct InsList * Queue, long long p1);
struct InsList * InsStoreFncParamS(struct InsList * Queue, char * string);
struct InsList * InsStoreFncParamI(struct InsList * Queue, int p1);
struct InsList * InsStoreFncParamD(struct InsList * Queue, double p1);
struct InsList * InsStoreFncParamX(struct InsList * Queue, long long p1);

struct InsList * InsDeclareVar(struct InsList * Queue, long long p1); // USE WHEN int a; | String b;...


typedef struct stackI
{
	int a;
	struct stackI * next;
} SStackI;
typedef struct stackD
{
	double a;
	struct stackD * next;
} SStackD;
typedef struct stackS
{
	char * a;
	struct stackS * next;
} SStackS;


struct stackI* stackPushI(struct stackI* stackPointer, int I);
struct stackD* stackPushD(struct stackD* stackPointer, double I);
struct stackS* stackPushS(struct stackS* stackPointer, char * I);

struct stackI* stackPopI(struct stackI* stackPointer, int * I);
struct stackD* stackPopD(struct stackD* stackPointer, double * I);
struct stackS* stackPopS(struct stackS* stackPointer, char ** I);

//WHILE
struct InsList * InsWhileStart(struct InsList * Queue);
struct InsList * InsWhileEnd(struct InsList * Queue);
//IF
struct InsList * InsIfStart(struct InsList * Queue);
struct InsList * InsIfElse(struct InsList * Queue);
struct InsList * InsIfEnd(struct InsList * Queue);